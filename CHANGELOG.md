## [1.17.2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.17.1...v1.17.2) (2024-05-28)


### Bug Fixes

* add mapper_args to all models ([8058e43](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/8058e43bc8d942d997db6d38e4a95d7cbade53b1))
* change display for subform ([b9acfda](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b9acfda8dd71fd4c3a0b91c2926fec40dc0fc98f))

## [1.17.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.17.0...v1.17.1) (2024-03-26)


### Bug Fixes

* error when month 01 ([4a202b6](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4a202b6684cf3cec2eba408b1dd99e78f602ece1))

# [1.17.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.16.0...v1.17.0) (2024-03-26)


### Bug Fixes

* add fakeValue method for display value only and edit mode work as create ([0439049](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/04390492483b552b7a7c28f0a938f10ebbf256c6))
* display only hours for time fields ([07ea8a1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/07ea8a1874ed0d8348d7a6287965c83d945d5bbd))
* modify experimentation node for nests and broods ([bbfe508](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/bbfe508fd469e782ca05bc9e5cff059a12a8fa59))
* nest description protocol - Item number is not saving ([5906b87](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/5906b87d8bff12729cbf89cfbd92d66e976cf58b))
* time format when retrieve datas from back ([ae00a7b](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ae00a7bd382f6d28f46e3dfc2b88e65298059546))


### Features

* procedures for differential admin areas update ([1c3d00c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/1c3d00cc86805f6afd36fa1248696fbacf486ba9))
* remove code for ini ([112a819](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/112a819b9a094f4e74b3057eb09b3e24c0676bb1))

# [1.16.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.15.0...v1.16.0) (2024-03-04)


### Bug Fixes

* add isPopup function for resolve scrolling in grid ([4d8c3b1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4d8c3b1b4f50e7ea83b875780745d1e0aa367e53))
* add text-overflow ellipsis for all inputs in a form-control class ([e4c92c0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/e4c92c04b143a41ff462ccac26ed9db9217afd3e))
* avoid save when incomplet sub protocol ([fd12fd2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/fd12fd21f272b3669a65325f749fb33191508557))
* change font weight for picker and display in filter ([a5a4bf0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a5a4bf08f01315e2cdaef03f42035e054c8b8670))
* change from - to / date separators ([9a60dfe](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9a60dfe8702da54499adbfe2c069da636c47494b))
* change user used to backup the database and update fieldworkareas process ([dd37e23](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/dd37e2307cf198ffc14037ef98be0f910968f5ac))
* create forcevalue in new picker ([26f871c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/26f871cfe55547c675cf53843be28073a7d127b4))
* css for thesaurus picker in protocol forms ([82a9a02](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/82a9a023ff2e61aeeb5deff9c7df78f0469617a7))
* delete media only if stored on device ([005f99a](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/005f99a697430a2294b48e33b20f0d6745f04c29))
* delete unproperly saved media ([a3707ea](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a3707ea6262044298f6eeb592f2c6cae2eaa3358))
* display admin area for monitored site when created from station ([c4580da](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/c4580da75dde9c8b36ffbccbd28d76cb5859649a))
* display of fieldwork areas equal to zero and update intersection calculation functions ([0d5f41b](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0d5f41b5ffd79f1a8e5720f00390634cbcd4c6b4))
* improve error management for media file deletion ([d52c450](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/d52c450ca40fa80ed5d1a345c8f972619626500b))
* input size in grid ([a920b30](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a920b30cc2509082282085eecead74a49a8ab8c8))
* input size in grid (2) ([45f3b1f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/45f3b1feef63bf9608bf36dfb3ce6f1d9bff63c2))
* only call sp on non referential instance ([d42692c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/d42692c40a43867f45eca1d12eb856764699b3fd))
* picker display ([3e5c727](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/3e5c7273eae27b6e4d939df27b68f96e039326dd))
* remove useless comments ([0b7bc92](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0b7bc92df12868ec02078bbeb220bcc00c8fc7cf))
* rename EXEC_DeleteMediaFile procedure ([f2de001](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/f2de0012ece150114ac2fc91f14d8a552f57c4b2))
* replace tabs by spaces in last alembic script ([5d3a596](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/5d3a596c8145d90db19128a43035c501bce65807))
* requirejs-text! deprecated replace by text! ([27d94f4](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/27d94f42547fae64537f46fbe39209d6d39d5e44))
* save station with admin area after edit ([221a737](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/221a73737e1ffe97aa151dfb11f2249b8ca93256))
* thesaurus css ([4076c1a](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4076c1a9b1add83cffc59add1ce8066e92475925))
* upgrade picker version ([09ba339](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/09ba33965bd52e19c5e1f5142c19f243554b2c48))
* users cannot delete a Media_Files observation if there is an upload error ([2a1d0e3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/2a1d0e3f6c5c93d1397510c17c5d2c1ee545b658))


### Features

* add datetime picker with new thesaurus, fix filters ([7e23b6e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/7e23b6e78088a7871ee35f69e31bdc731c0375a4))
* add experimentation configuration in database ([b2b343e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b2b343e6aedab84f95d6b56202a71a3f0e3a686f))
* add procedure to delete media file ([b1ddb36](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b1ddb36b6236cbe7fb2ff761b967adfcfe177acf))
* add reneco-hierarchized-picker ([2ce6746](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/2ce6746c3c20088247635c8e2b60aa3586c14692))
* case sensitive autocomplete input and filters for fieldworkarea, administrativearea ([477251e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/477251edf5516e0271f9fa7c5ee5ebe451e861f3))
* display redirection link in station popup ([a51d586](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a51d58621dfbe85675c359295c03a8ffd504c416))
* log error message to help to understand the issue ([581c09e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/581c09ea1a5589a004038a07b0cb1bcd71ab242f))

# [1.15.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.14.0...v1.15.0) (2023-11-15)


### Bug Fixes

* change sp name Remove_FK_Station_References_Before_Delete ([0cdd90d](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0cdd90d79d06df062d772643f535ae58c46579cf))
* retrieve proto liste with fa and proto instanciate ([bc03255](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/bc0325586ce50c62a09e74787119ce91f859547e))


### Features

* add business rules before delete stations ([818e34e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/818e34ed69920931edf4fc61cd3b0984d9a24d85))
* calling portal api to retrieve tab_name ([ee863bf](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ee863bfdef476f857e43c70b7124b3b14ee50b89))
* replace config file by data.json and set fieldworkarea as not required ([0d370ff](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0d370ffdfb639079910871c54acb62768e8ad87b))

# [1.14.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.13.3...v1.14.0) (2023-10-04)


### Bug Fixes

* missing request object ([96d0c1c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/96d0c1c5a68eb071c9f0c0f2279441b642f31aa4))


### Features

* added a first basic version of backend health check ([2add5f5](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/2add5f5839ee571979a42663d2f24305ecd014fa))
* remove mydata tile ([f2a4d91](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/f2a4d919d592234707114a1585871113fc4544f2))

## [1.13.3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.13.2...v1.13.3) (2023-08-01)


### Bug Fixes

* administrative area picker and query builder ([8c60006](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/8c60006e69f67ebc07e860713280dc7349deb2cc))
* display splitted fullpath as administrative area for standard station ([6d5f56b](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6d5f56b48aab6d16acef116f13da84578ef5bc64))
* save station with admin area after edit ([1dce72e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/1dce72e7f6dd68240161371b6cb485fbc415fd7d))

## [1.13.2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.13.1...v1.13.2) (2023-06-22)


### Bug Fixes

* not the good version ([cfd0f52](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/cfd0f52c060247e2c955d4637ce71325c679bada))

## [1.13.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.13.0...v1.13.1) (2023-06-22)


### Bug Fixes

* missing sp for pulling business rules conf ([4bbfc6f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4bbfc6f126684f80419a38756656b7a1e10c021e))

# [1.13.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.12.1...v1.13.0) (2023-06-20)


### Features

* fieldworkarea no more required for station without coordinates ([428344e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/428344e98b540aacd55c9009291fcb361f733b1a))

## [1.12.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.12.0...v1.12.1) (2023-06-19)


### Bug Fixes

* url for administrativearea picker ([67e24ec](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/67e24ec7e0887c29ea13110874b397c42f87bbdd))

# [1.12.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.11.0...v1.12.0) (2023-06-19)


### Bug Fixes

* change order migration ([daa289e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/daa289e1f6e43569d56cf26adbf38fac24c674cf))
* issues in migrations ([62d999c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/62d999c23ed4d42b271be0a62997558c6f24749b))
* move modifications must be apply on every instances ([dd3e2bd](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/dd3e2bd41633b723dd33566eacc0b5c6b4c35174))


### Features

* handle administrative area ([72095c2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/72095c293ac8760a4ea8f1a748ce6110748da627))
* update fieldwork area process ([4216e35](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4216e3516a7224bedfcfabeae344b9cdd15cc560))

# [1.11.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.10.2...v1.11.0) (2023-04-17)


### Bug Fixes

* add check if station exist before delete monitored site ([746b38f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/746b38fa5c6bad2eb120c28566008c5f2d126f43))
* add missing model for get_bind ([925f1bb](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/925f1bbc4b09deff2bf80be9b1b10d5fd1caeef1))
* change from "new" to "browse" media file ([ac61a77](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ac61a7763c18f05b6fd12ff784488dfcf475426d))
* refact manual import gsm file ([a8caf4f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a8caf4f520cf1c970533050d35bc6935d7fd58ce))
* remove NegAlt and 2D Fix from na_values when parsing csv, set it to nan after ([e39f3b0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/e39f3b053ff514816dd94d1f51b339f773fd12a2))
* remove the double jetlag implementation ([008a255](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/008a255ea107547080e23a765f37d9b392cffc27))
* typo ([5714c50](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/5714c50e605d2c191f1423e34d3fafd3e04e76cb))
* uninstall packages before install it ([9e9c969](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9e9c9695d5018430d26bba361139d01f380612f4))
* when property is obj and date is null use useDate ([ce0fd24](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ce0fd243e38d131a2c28b8e56f5fa0dcc52d0c25))


### Features

* hide empty observations when readonly or user rights is guest ([53e85df](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/53e85df64b7a8f27448025da52d63212e352fccb))

## [1.10.2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.10.1...v1.10.2) (2023-02-22)


### Bug Fixes

* fieldactivity withtout protocoles associated are correctly return ([870c9fa](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/870c9faf9d7e80e8ac63e5cdc93e0a9b2f9b0771))
* release is processed by batch now ([a3b1b99](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a3b1b9952f530b3e5875c2471d6f0e90c25c5092))
* release use temporary table ([55bc4b8](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/55bc4b855cb3da4b3f11c53f968715e34e5e8b15))
* update conf in DBB ([78b2d48](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/78b2d48892e5d648de4e440277c3dd7c18ad2115))

## [1.10.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.10.0...v1.10.1) (2023-01-31)


### Bug Fixes

* fix version for pip and setuptools ([77606b6](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/77606b6ed2c38194b65ed70ffa819a31ab43be09))

# [1.10.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.9.0...v1.10.0) (2023-01-24)


### Bug Fixes

* allow reader right for guests in autocomplete ([1b3d6af](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/1b3d6af62adb3066637c97f935ab756b98661f4f))
* limit number of rows returned by autocomplete to 50 ([1f2ab9a](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/1f2ab9a650196d012447504db8f26280b1f0d712))
* name of the spatial index in sp_FA_Compute ([2e174e6](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/2e174e6648e347209a24625433a0ff42473f49a3))
* raise error when station created but attach id to camtrap failed ([3ab0db4](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/3ab0db4152acaac18d2419da7ca8f34f3a687778))
* replace is_centralisation_instance to is_referential_instance ([167b3c3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/167b3c3a3a22a33fd7445d153e8de535ce204cde))


### Features

* add sp procedure for ressed all tables ([3984710](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/3984710d0d04c298e6474018898d1405fc70289c))
* apply migration and pulling datas in same transaction ([13f9891](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/13f98917704c4e33988858482bc8cd514f7028a6))

# [1.9.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.8.1...v1.9.0) (2022-12-22)


### Bug Fixes

* is_a_read_only_instance is in alembic section in ini ([f18d44a](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/f18d44a079f1ead28976fc9058a486b1698b5395))


### Features

* add new env variable is_a_read_only_instance ([a9ebe33](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a9ebe337fb333d1b13c9b10ace3d5b71844d9c85))

## [1.8.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.8.0...v1.8.1) (2022-12-15)


### Bug Fixes

* delete btn in history work with CodeName now ([9f33f0b](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9f33f0b40a0d79350ffd3f35bb0f24ef13a2ce95))
* delete btn in history work with CodeName now for individuals ([58f27f7](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/58f27f7c6042a1d2b720be87d167a5f954c6fa8c))
* issue with merge replication and retrieve id of inserted objects ([d1aa768](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/d1aa768be020e76442e35bc2260c92fcd9f906a5))
* modify name of the spatial index in fieldwork area update process ([88c4fd5](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/88c4fd50e35958cc0eec9cbb147845647a01ccee))
* new revision for EXEC_MERGE_All_Referential_Configurations_Tables ([bca0bd6](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/bca0bd61a35af7cc577b33cefcf96deec7370590))
* remove error only when nestandbrood ([58a90e3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/58a90e3af3deff2f4ff050d95a8c7b0b3a76eae7))

# [1.8.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.7.0...v1.8.0) (2022-12-13)


### Bug Fixes

* add empty directory ([7d4464d](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/7d4464de2073131a6fb9b17b29eaa6c8e1643aba))
* alembic script and pipe to display 'Comment' ([0530f00](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0530f003211ab2db50f55d00be0c184be3f6766a))
* avoid to create duplicate ([23f4d97](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/23f4d977281c469027477832289fbe9bec79dec0))
* avoid to create duplicate ([6b0f119](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6b0f119dab40a9959febcd1fd66bec8b6cfc1c9f))
* change on last alembic version ([79e1948](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/79e1948c1abda57d995ef815563752f19d44298d))
* correct the legends in module forms ([6f8efe6](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6f8efe6d5da483a748cc4fdd19aaf23b05857c4c))
* display sensor links for monitored sites ([4dff108](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4dff108a3020b5824aa97f2be87236bc7ff3e1fa))
* enable filtering on individual location ID ([faf5553](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/faf5553f7eee5b560fdcb235d444bcf0ee9a4aa3))
* export csv ([d86e343](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/d86e3437e3f4e0a8d47dfff8273e978157fb8d54))
* ID filter in individuals location grid ([bb90462](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/bb9046209e1969d4337b454a8d383724195d979a))
* provide lastImported as extraCriteria ([bcaa5d2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/bcaa5d243652b721baa6bd02e7db8ec202a6052c))
* reactivate save button in all cases ([a35c3e0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a35c3e097b0983deffb7f515ce481e308d117baa))
* revert last changes ([0727d0f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0727d0fdf60f2cc00a1388d6262fb2e45ae331b9))
* same order of columns in grids and csv and remove brackets ([6dc250a](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6dc250adb54e2fe1ebfdb3b0d3769aaec315c56c))
* use labels in sensors and individuals history ([6531111](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6531111f4a7078957ba7d7f8e5a2b0846545657e))


### Features

* add nest and brood category in monitored sites ([86891da](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/86891dab7c0937dd58bf8e36860c5727d29c8329))
* changes for prod ([896f34e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/896f34efb0c5770781603b16f073b0e120a32f3e))
* integrate alembic ([b95aa96](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b95aa96acbe468ecaef1768f4529eca203abe8c6))

# [1.7.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.6.2...v1.7.0) (2022-09-07)


### Bug Fixes

* fix logic when handle string value ([58df294](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/58df294ec1132e17edca1205ab861c514c2e834d))
* manage None string values ([029e623](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/029e623be05df8fd125c88d1a8e4b12143fbe7fb))
* missing initialise variable ([6bf48c8](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6bf48c85d126d1289dba9b68222cd16b0d4a8f0e))
* required validators could not be in first position ([2ddd8cc](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/2ddd8cc38831129828855e085aeefd708999062e))
* successive adds of tags for groups of images ([de1c1e2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/de1c1e27a0e25de5058d48915edd304d72bd4260))


### Features

* add new option for disable/activate stats logger ([2e68bd3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/2e68bd3954ee21dec6826e12c20640326dc100aa))

## [1.6.2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.6.1...v1.6.2) (2022-08-09)


### Bug Fixes

* object picker fixed for release ([c8b3748](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/c8b3748731ad3df83d54bd3b0d8d4e8bed9bcbf8))
* retrieve station data for monitored site creation ([c1c9b51](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/c1c9b5146829f39ac1acf73002eb0043cb3e8c77))
* wildcard in select bug in merge replication ([f140d3b](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/f140d3bc9ae06be0e242d499463fec1fad56b873))

## [1.6.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.6.0...v1.6.1) (2022-07-29)


### Bug Fixes

* retrieve station data for monitored site creation ([9cfe567](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9cfe5673e9830883b0faf1823be50da2cbfb987a))

# [1.6.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.5.0...v1.6.0) (2022-07-27)


### Bug Fixes

* add rights to superuser and user for objectpicker ([62a177f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/62a177fd071efb6c93a6ec3fd12482243a19507f))
* create new monitoredsite from modale ([366f151](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/366f151216f772bebbe81c252001cedb27acfc6d))


### Features

* handle null values for objectPicker ([7930dd5](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/7930dd59884cd12d424d92e97b5e7af3979dbba9))

# [1.5.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.4.0...v1.5.0) (2022-07-25)


### Bug Fixes

* rename compagny to company ([9729592](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9729592cb058aa80cb31486019534c96949db244))
* typo stored procedure ([2a1e401](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/2a1e401d04dd1522e30425ad511187a30caf0463))


### Features

* create update process for update FA ([c40588f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/c40588f77024cb75926d7df49da21960511ea02c))

# [1.4.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.3.1...v1.4.0) (2022-07-11)


### Bug Fixes

* add explicit error message for monitored site name already used ([ff67b34](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ff67b34f444625ff88e8a510b655743ca6a37ab9))
* date accessibility in individual form ([02505e1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/02505e144d02a4e208359b65ed686703dc50d1e4))
* datetime conversion in new monitored site ([ef76f92](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ef76f92bf9807faa0e2a8c6d4d43b7057a7f9fec))
* datetime conversion in new monitored site ([230d7fb](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/230d7fb07daab47f34d0bfabd1537f2d058b1893))
* display validation in cameratrap module ([0988cd2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0988cd24ef0fabef085ec40ec8371592d5cc1f2c))
* handle all use case for equipement ([5d69b01](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/5d69b012af4295e067a5897f2bfd2b5bffea34dd))
* handle multiple schema for importing engineering file ([7f16418](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/7f16418ff5244b2eade853677178e9f2a65f82a4))
* hide validate btn when full screen ([4542ffb](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4542ffb36e364413ec9266d253b69aa3da1a26fb))
* **import:** negative offset can be applied and 24h format ([e3f1848](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/e3f184821239aa8342e1718a2ded23f6259f2121))
* issue with path difference between dev and prod installation for MTIwinGPS ([8c580f5](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/8c580f51c6bfb21d25bc63e073c9e50686fc50ce))
* no blank space in display of  monitored sites ([3c59340](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/3c59340e9f902acbc3f3ed2b2f7eb698c8d6889f))
* pipe for remove css class for objectpicker ([fedfc31](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/fedfc31d7e6651ea9060b9df26b845a6785537f9))
* Pop-up and messages in import and validate for camera trap ([19bab64](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/19bab64c62e9f23ed57551ae97a70e7305f226b8))
* pop-up message about stations in cameratrap validation ([ee0ddb4](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ee0ddb492c1c8888b7a7f7a0c30454895fde66a4))
* pre-entry when create new monitored site from station and when create new sensor ([b38d393](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b38d393f38c79de77b60c4ae915e24ca52878d5c))
* remove changes for company field ([64b88c5](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/64b88c531baef944dd14612e6e7a2bba4516f347))
* sql pipe for updte species filter on station ([6810a2b](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6810a2b1b150536c744efb0467601c7cd2cb3d09))
* time interval check in cameratrap ([71a4b38](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/71a4b382939722526190d6a2a1f77f72b8789b02))
* transmitter company name and label ([d33cc6c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/d33cc6c23544edda3c41470e43478850101b95c2))
* update station map display and hierarchical order in FA levels ([7a8f711](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/7a8f711bdfc9382b71108b786399e8f68d300d7d))
* use case when no data in db for a sensor with date session ([018e4c4](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/018e4c4d04e3723e89b01e8d23c7b1836d9cc2ff))
* warning pop-up when deleting station(s) and correct number ([68de448](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/68de4484810216a2a188fbb74509ca48a816dfe3))


### Features

* execute stored procedure when fetch releasable individuals ([a03d236](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a03d236ec58831a25a57342489d9f8d4d771f328))
* generic object picker on static and dynamic properties ([bb7c71a](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/bb7c71af2876a7f07fe995998106413db8fd0e9d))
* Objectpicker configuration for formbuilder ([f24c624](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/f24c6246f31b803063b72acec84fa72ab628a81d))
* refact import camtrap step 1 ([60197f5](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/60197f5ab818884060b27fbc0ade6dce192c989d))
* remove age field on unidentified indiv form and filters ([cf940ef](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/cf940efedd018dd49abe7b631f0679b727a70e92))
* sql changes for handle thesaurus in english ([4d8bf15](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/4d8bf158ab4b9b0ec4e5b9a7b256362ddcdc4dd5))

## [1.3.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.3.0...v1.3.1) (2022-05-18)


### Bug Fixes

* allow superusers to access releasable individuals ([aa48775](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/aa487750525bc9ebbaf2dddf14c4b279d4f42c8d))

# [1.3.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.2.0...v1.3.0) (2022-05-11)


### Features

* new version for master ([bb6bb96](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/bb6bb965eb7697677c6eca2edce356610d1af706))

# [1.2.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.1.1...v1.2.0) (2022-04-04)


### Bug Fixes

* add baseurls in config for renecolib ([9e8437d](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9e8437d7e7cd099399017d9d5ec4fbf876a5dcca))
* add new variable in config for renecolib mode ([a385c0a](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a385c0a1d46c480f487da2a011749393d53fa720))
* conflict from merge with relative url  and redirect_uri ([c6d3345](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/c6d33458a5b6332fe8dc9b7c35b1c3e6afdb2380))
* no more use config.erdApiUrl ([fdedbcd](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/fdedbcd426d64c57835d30103e03b6ee72aff453))
* remove file no more used ([0f9dd86](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0f9dd865ee86dcddd12386c088576a587cd595ac))
* remove no more used dependencies ([9cc4ec7](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9cc4ec7472f3ef74e83c1d4bcb3ad78d09c66111))
* revert changes for build renecolib with babel in ecoreleve ([078ee00](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/078ee00b802bddab2ad4970ba4b03a77c73a1bce))
* typo for role superUser ([c3f89c3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/c3f89c3b39d6cd27dcf377b8b306c4bc994b1fa6))
* typo referential not referentiel ([66193fa](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/66193fab4a741ec500f7361f46a4ce2326859065))
* undo merge pre_prod into new_portal_and_api_gateway ([7643965](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/7643965c019d10321f29be48b8ebb1acacc15779))
* upgrade reneco-font version and replace icon in validate camtrap ([cd19243](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/cd192432ca9f1eb8dced0a5d098c6e06ec587702))
* upgrade version used for reneco-lib ([dcf0a1d](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/dcf0a1db15787e16f6e4e0cbd9555078de6e4117))
* use new bundle for reneco-lib and change instantiation for use it ([8ac1f9d](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/8ac1f9dc0eaf20f5e81edb9a5dd33e1b8b1e0f09))


### Features

* add new property in ini for referential instance ([d64e041](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/d64e041dd5e2f872d7aa3acbbfe82019470fd322))
* install and import reneco-lib in ecoreleve ([6086110](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6086110bac346ad2a0e8b35f552b9aee0e76cb32))
* use reneco lib in app ([ce3d059](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ce3d059cb4fa8197f376e1016beb156a56cc835d))
* wip build release for reneco-lib ([e497de3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/e497de3210375ce003b5ff91111c1afbc056a52e))

## [1.1.1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.1.0...v1.1.1) (2022-03-01)


### Bug Fixes

* permission for admin on gsm ([8043b5c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/8043b5cc94b661305ef16b7c9b186c1194cfc395))

# [1.1.0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.0.4...v1.1.0) (2022-03-01)


### Bug Fixes

* add ALL_PERMISSIONS for user and superuser on mediasfiles resources (same permissions as observations) and  icon name with updated reneco-font ([ca64a69](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/ca64a69830fbb98ccb354af8f54b2a45a91bf64c))
* change icon name ([8ead58d](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/8ead58d8a5e041e32e4b902314e3d74863ee1579))
* change name sp call in backend and pipe all modifications for db ([87ce347](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/87ce3479f91e5d5911d316474cebf89e5039d5c4))
* change variable name in ini and back for decode token ([1548f55](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/1548f55e4e39f1ca17bcaae053054a414c6b2f4c))
* **equipment:** add case when update observation without editing indiv or sensor ([96b200c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/96b200c48a436972357667239a7cde8b9568dafe))
* fix version of crypto-js to 4.0.0 (4.1.0 not working) ([5178596](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/517859689b167ec65161c0e95356c076bc0a70f3))
* google map, icons and release ([24f4107](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/24f4107f86c099a5b19a79a01b6aaac57fbedc3b))
* handle site equipment/unequipment as individual equipment/unequipment ([64a4310](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/64a4310b4a8878619f4bcbb849167105a2836cad))
* **import:** Fix import rfid ([af48777](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/af48777ea25338e202d7d93b6291b304560b0594))
* Merge branch 'master' into pre_prod ([3fff6ef](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/3fff6ef6e094479997a50b0928348fdcea392992))
* typo on superuser and user ([6431eb7](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/6431eb7399cf159a15c355c3419ba24c2475e12e))


### Features

*  add sql pipe for alter sensor schema on referential and update conf on site instance ([466e967](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/466e967bdcc85b8204e6289111299e345d505614))
* add refresh on sensor form when delete history ([0593ad1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0593ad1f39abda0df1dc26c3ab1efab8e79d2780))
* add two sensordynprop available for being remove from history ([337852e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/337852e1b2938281f4f7978629686451dd98a98d))
* increase security ([1105897](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/1105897536c0a153636b457e8c2579bc10ba82f9))
* new conf for individuals forms ([07e6a42](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/07e6a42af0bc0db8397b8271d4bd835c64947bb8))
* **release:** refact release in sql ([726fd50](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/726fd50aebda722c510ca519b17d523d97ae4d6e))
* remove automatic creation for views of dynamic objects and sql pipe ([b4b6355](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b4b6355e16ce69d10ee245ba1f5b4333541dc9f3))
* sync between form and history ([f46dbfe](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/f46dbfe4d8072b316ae0c8686f01807a59662b07))


### Reverts

* merge conflict locally and embbed new_portal_and_api_gateway commits ([b02ded3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b02ded3404cfec23ed8bb3b9eb1d9df61fd649fd))

## [1.0.4](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.0.3...v1.0.4) (2022-02-16)


### Bug Fixes

* change http to https ([39118ca](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/39118caea9008be640190404aec00e47a2590286))

## [1.0.3](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.0.2...v1.0.3) (2021-12-16)


### Bug Fixes

* set permission='update' for PATCH camtrap item ([a0f7a8f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/a0f7a8fec52624a6711b17495a837785620a8526))

## [1.0.2](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/compare/v1.0.1...v1.0.2) (2021-11-30)


### Bug Fixes

* add certifi dependencies for fix [SSL: CERTIFICATE_VERIFY_FAILED] error ([193eedb](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/193eedbfe308c0869ae40c202ece44016a4b9383))
* add create rights on sensors for admin and superuser ([fe84cb0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/fe84cb0f135aaf54cb40d44b6728cd780893840a))
* add step for install bower ([9e4dd1e](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9e4dd1e43e8266e54ecb1109457172ac844eec22))
* array start by 0 ([9f6eea0](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/9f6eea0e339576ecbaa6e275aeb6948eaed68b0d))
* change selector for total records in grid ([618d490](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/618d4903b53c827d11044359e75eee6d48810874))
* increase chunksize to 10mb ([008163f](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/008163fb8e6d0bf0c16bea7dcfe69e6bf1ef9c2f))
* install grunt ([65020e1](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/65020e1b9ece385c173fe19706e7bc498078d073))
* nbfixed work as expected ([b91522c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/b91522cd7109222d09755fd2802d8ceabbe989b1))
* typo dynprop name Survey_type to Survey_Type ([0cdfa7d](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0cdfa7d895ab8bf73ceb7cb3fd0293742669c395))
* UPDT_ModuleForms force convert varchar ([04ed9cb](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/04ed9cb9355891b1f61aaedb4078aa9c0f38cdab))


### Reverts

* Revert "NSFilter : up version" ([d279b88](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/d279b88153e66f225f8dbae28025f4852ececcf4))
* Revert "add console log" ([0e6230c](https://gitlab.com/natural-solutions/reneco/ecoreleve-data/commit/0e6230c6b198cd2a8053e6ff7b7f48c12fa90230))
