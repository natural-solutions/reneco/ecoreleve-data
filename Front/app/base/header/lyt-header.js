
define([
  'jquery',
  'marionette',
  './lyt-breadCrumb',
  'backbone',
  'instance_reneco_lib',
  'bootstrap',
],
function($,Marionette, Breadcrumb, Backbone, RenecoLib) {
  'use strict';
  return Marionette.LayoutView.extend({
    template: 'app/base/header/tpl-header.html',
    className: 'header',
    events: {
      'click #logout': 'logout'
    },
    regions: {
      'breadcrumb': '#breadcrumb'
    },

    ui: {
      'userName': '#userName',
    },

    logout: function() {
      RenecoLib.auth.logOut();
    },

    onShow: function() {
      var _this = this;

      this.breadcrumb.show(new Breadcrumb());
      window.app.user.fetch().done( function(datas) {
        /*
        TODO:REFACT
          set role in the body for front app
        */
        $('body').addClass(datas['role']);
        $.xhrPool.allowAbort = true;
        _this.ui.userName.html(datas['fullname']);
      }).fail(function(error){
        console.log(error)
      })
      this.listenTo(window.app.user, "change", this.starthistory);
    },

    starthistory: function() {
      Backbone.history.start()
      this.stopListening(window.app.user)
    },
  });
});