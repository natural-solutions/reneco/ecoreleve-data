define(['marionette', 'tilesCollection'],
function(Marionette, TilesCollection) {
  'use strict';
  return Marionette.LayoutView.extend({
    template: 'app/base/header/tpl-breadcrumb.html',
    collection: TilesCollection,

    initialize: function() {
      this.listenTo(window.app.user, 'change', this.render)
    },

    onDestroy: function () {
      this.stopListening(window.app.user)
    }
  });
});
