define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {
/*
                'Super Utilisateur': 'group:superUser',
                'Utilisateur': 'group:user',
                'Administrateur': 'group:admin',
                'Lecteur': 'group:guest'
*/
  'use strict';
  var TilesModel = Backbone.Model.extend({
    default: {
      order: 0,
      href: '',
      className: [],
      icon: '',
      i18n: '',
      legend: '',
      availableForRoles: ['guest','user','superUser','admin'],
      position: ''
    }
  });
  var TilesCollection = Backbone.Collection.extend({
    model: TilesModel
  });
  var mockDatasFetch = [{
    order: 0,
    href: 'importFile',
    actionFunction: ['importFile'],
    className: ['small_tile', 'tile-pomegranate'],
    icon: 'reneco-import',
    i18n: 'tiles.manual-import',
    legend: 'Manual import',
    availableForRoles: ['user','superUser','admin'],
    position: 'left'
  }, {
    order: 1,
    href: 'stations/new',
    actionFunction: ['newStation'],
    className: ['small_tile', 'last', 'tile-pumpkin'],
    icon: 'reneco-entrykey',
    i18n: 'tiles.new-station',
    legend: 'New station',
    availableForRoles: ['user','superUser','admin'],
    position: 'left'
  }, {
    order: 2,
    href: 'release',
    actionFunction: ['release','releaseIndividuals'],
    className: ['small_tile', 'tile-pumpkin', 'release'],
    icon: 'reneco-TRACK-to_release',
    i18n: 'tiles.release',
    legend: 'Release',
    availableForRoles: ['superUser','admin'],
    position: 'left'
  }, {
    order: 3,
    href: 'importHistory',
    actionFunction: ['importHistory'],
    className: ['small_tile', 'tile-alizarin'],
    icon: 'reneco-story',
    i18n: 'tiles.history-import',
    legend: 'History import',
    availableForRoles: ['admin'],
    position: 'left'
  }, {
    order: 4,
    href: 'validate',
    actionFunction: ['validate','validateType','validateDetail'],
    className: ['big_tile', 'last', 'tile-carrot'],
    icon: 'reneco-checkbox_on',
    i18n: 'tiles.validation',
    legend: 'Validate',
    availableForRoles: ['user','superUser','admin'],
    position: 'left'
  }, {
    order: 0,
    href: 'stations',
    actionFunction:  ['station','stations'],
    className: ['small_tile', 'first', 'tile-green'],
    icon: 'reneco-stations_list',
    i18n: 'tiles.stations',
    legend: 'Stations',
    availableForRoles: ['guest','user','superUser','admin'],
    position: 'right'
  }, {
    order: 1,
    href: 'individuals',
    className: ['small_tile', 'tile-green'],
    icon: 'reneco-individuals_list',
    i18n: 'tiles.individuals',
    legend: 'Individuals',
    availableForRoles: ['guest','user','superUser','admin'],
    position: 'right'
  }, {
    order: 2,
    href: 'sensors',
    actionFunction:  ['sensor','sensors'],
    className: ['small_tile', 'first', 'tile-green'],
    icon: 'reneco-sensors_list',
    i18n: 'tiles.sensors',
    legend: 'Sensors',
    availableForRoles: ['guest','user','superUser','admin'],
    position: 'right'
  }, {
    order: 3,
    href: 'monitoredSites',
    className: ['small_tile', 'tile-green', 'monitoredSites'],
    icon: 'reneco-sites_list',
    i18n: 'tiles.sites',
    legend: 'Monitored sites',
    availableForRoles: ['guest','user','superUser','admin'],
    position: 'right'
  }]

  return new TilesCollection(mockDatasFetch)
});
