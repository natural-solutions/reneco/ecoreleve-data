define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {
  /*
    'Super Utilisateur': 'group:superUser',
    'Utilisateur': 'group:user',
    'Administrateur': 'group:admin',
    'Lecteur': 'group:guest'
  */
  'use strict';
  const ALL_PERMISSIONS = true;
  var ACLModel = Backbone.Model.extend({
    default: {
      name: '',
      guest: [],
      user: [],
      superUser: [],
      admin:[]
    }
  });
  var ACLCollection = Backbone.Collection.extend({
    model: ACLModel
  });
  var mockDatasFetch = [{
    name: 'stations',
    guest: ['read'],
    user: ['create','save','read','update','delete_station_cam'],
    superUser: ['create','save','read','update','delete_station_cam'],
    admin: [ALL_PERMISSIONS]
  }, {
    name: 'observations',
    guest: ['read'],
    user: [ALL_PERMISSIONS],
    superUser: [ALL_PERMISSIONS],
    admin: [ALL_PERMISSIONS]
  }, {
    name: 'sensors',
    guest: ['read'],
    user: ['read'],
    superUser: ['create','save','read','update','delete'],
    admin: ['create','save','read','update','delete']
  },{
    name: 'sensors_history',
    guest: ['read'],
    user: ['read'],
    superUser: ['read','update','delete'],
    admin: ['read','update','delete']
  }, {
    name: 'monitoredSites',
    guest: ['read'],
    user: ['create','save','read','update'],
    superUser: ['create','save','read','update'],
    admin: [ALL_PERMISSIONS]
  },{
    name: 'individuals',
    guest: ['read'],
    user: ['read'],
    superUser: ['save','read','update'],
    admin: ['create','save','read','update']
  }, {
    name: 'individuals_locations',
    guest: ['read'],
    user: ['read'],
    superUser: ['read'],
    admin: ['read','update','delete']
  }, {
    name: 'individuals_history',
    guest: ['read'],
    user: ['create','read','update'],
    superUser: ['create','read','update'],
    admin: [ALL_PERMISSIONS]
  }]

  return new ACLCollection(mockDatasFetch)
});
