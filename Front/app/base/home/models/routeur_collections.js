define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {
  'use strict';
  var RouterModel = Backbone.Model.extend({
    default: {
      actionFunction:'',
      availableForRole:[]
    }
  });
  var RouteurCollection = Backbone.Collection.extend({
    model: RouterModel
  });
  var mockDatasFetch = [{
    actionFunction: 'importFile',
    availableForRole: ['user','superUser','admin']
  }, {
    actionFunction:'importHistory',
    availableForRole: ['user','superUser','admin']
  }, {
    actionFunction:'newIndividual',
    availableForRole: ['admin']
  }, {
    actionFunction:'individual',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'individuals',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'newMonitoredSite',
    availableForRole: ['user','superUser','admin']
  }, {
    actionFunction:'monitoredSite',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'monitoredSites',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'redirectToStations',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'newSensor',
    availableForRole: ['superUser','admin']
  }, {
    actionFunction:'sensor',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'sensors',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'newStation',
    availableForRole: ['user','superUser','admin']
  }, {
    actionFunction:'stations',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'station',
    availableForRole: ['guest','user','superUser','admin']
  }, {
    actionFunction:'releaseIndividuals',
    availableForRole: ['superUser','admin']
  }, {
    actionFunction:'release',
    availableForRole: ['superUser','admin']
  }, {
    actionFunction:'validate',
    availableForRole: ['user','superUser','admin']
  }, {
    actionFunction:'validateType',
    availableForRole: ['user','superUser','admin']
  }, {
    actionFunction:'validateDetail',
    availableForRole: ['user','superUser','admin']
  }, {
    actionFunction:'home',
    availableForRole: ['guest','user','superUser','admin']
  }]

  return new RouteurCollection(mockDatasFetch)
});