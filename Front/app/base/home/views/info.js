define([
    'underscore',
    'backbone',
    'config_datas',
    'marionette',
    'moment'
], function(_, Backbone, config_datas, Marionette, moment) {
  'use strict';
  return Marionette.ItemView.extend({
    template: 'app/base/home/tpl/tpl-info.html',

    model: new Backbone.Model({
      siteName: config_datas.get('siteName'),
      date: moment().format('dddd, MMMM Do YYYY'),
      nbIndiv: 0
    }),

    modelEvents: {
      'change': 'render'
    },

    initialize: function() {
      this.$el.hide();
      $.ajax({
        context: this,
        url: 'individuals/count',
      }).done(function(data) {
        this.model.set('nbIndiv', data);
        this.$el.fadeIn();
      });
    },

    onDestroy: function() {
      delete this.model;
    }
  });
});
