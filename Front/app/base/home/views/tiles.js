define([
  'underscore',
  'backbone',
  'marionette',
  'tilesCollection'
], function (_, Backbone, Marionette, TilesCollection) {
  'use strict';



  // var tilesToDisplay = new TilesCollection(mockDatasFetch)
  return Marionette.ItemView.extend({
    template: 'app/base/home/tpl/tpl-tiles.html',
    className: 'clearfix mid-height',
    collection: TilesCollection,
    initialize: function () {
      this.listenTo(window.app.user, 'change', this.render)
    },

    onDestroy: function () {
      this.stopListening(window.app.user)
    }
  });
});
