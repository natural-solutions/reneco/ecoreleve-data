define(['marionette',
	'./views/curveGraph',
	'./views/donutGraph',
	'./views/info',
  './views/tiles'
	],
function(Marionette, CurveGraphView, DonutGraphView, InfoView, TilesView) {
  'use strict';

  return Marionette.LayoutView.extend({
    template: 'app/base/home/tpl/tpl-home.html',
    className: 'home-page ns-full-height animated',
    events: {
    },
    regions: {
      graph: '#graph',
      info: '#info',
      tiles: '#ns-tiles'
    },

    ui: {
      'donuts': '#donuts',
      'userFirst': '#userFirst',
      'userLast': '#userLast',
    },

    animateIn: function() {
      this.$el.addClass('zoomInDown');

      this.$el.animate(
            {opacity: 1},
            200,
            _.bind(this.trigger, this, 'animateIn')
            );
    },

    // Same as above, except this time we trigger 'animateOut'
    animateOut: function() {
      this.$el.removeClass('zoomInUp');

      this.$el.animate(
      {opacity: 0},
      200,
      _.bind(this.trigger, this, 'animateOut')
      );
    },

    initStats: function() {
      var collGraphObj = [{
        url: 'sensor/uncheckedDatas/graph',
        ele: '#validate',
        title: 'pending',
        stored: false,
        template: 'app/base/home/tpl/tpl-dounutGraph.html'
      },{
        url: 'individuals/location/graph',
        ele: '#locations',
        title: 'location',
        stored: false,
        template: 'app/base/home/tpl/tpl-dounutGraph2.html'
      }/*,{
				url : 'stats/individuals/monitored/graph',
				ele : '#monitored',
				title : 'monitored',
				stored : false,
				template : 'app/base/home/tpl/tpl-dounutGraph3.html'
			}*/];
      var collGraph = new Backbone.Collection(collGraphObj);


      var GraphViews = Backbone.Marionette.CollectionView.extend({
        childView: DonutGraphView,
      });
      this.donutGraphs = new GraphViews({collection: collGraph});
      this.infoStat = new InfoView();
      this.curveGraph = new CurveGraphView();
      this.tilesViewObj = new TilesView();
    },

    onRender: function() {
      this.initStats();
      this.donutGraphs.render();
    },

    onShow: function(options) {

      this.ui.donuts.html(this.donutGraphs.el);
      $('.hello').addClass('masqued');
      this.info.show(this.infoStat);
      this.graph.show(this.curveGraph);
      this.tiles.show(this.tilesViewObj , { replaceElement: true });

      this.$el.i18n();
    },
    getUser : function(){
      var _this = this;
      var userModel = Backbone.Model.extend({
        defaults : {
          Firstname: null,
          Language: null,
          Lastname: null,
          PK_id: null,
          fullname: null,
          role: null
        },
        urlRoot: 'currentUser'
      })
      user = new userModel();
      user.fetch().done(function(datas) {
          _this.ui.userFirst.html(datas['Firstname']);
          _this.ui.userLast.html(datas['Lastname']);
        });
    }
  });
});
