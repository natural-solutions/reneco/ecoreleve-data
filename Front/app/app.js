define([
  'marionette',
  'lyt-rootview',
  'router',
  'controller',
  'sweetAlert',
  'config_datas',
  'jquery',
  'backbone',
  'instance_reneco_lib',
  'stats_logger',
  //circular dependencies, I don't konw where to put it 4 the moment

  'ns_modules/ns_bbfe/bbfe-timePicker',
  'ns_modules/ns_bbfe/bbfe-dateTimePicker',
  'ns_modules/ns_bbfe/bbfe-autocomplete',
  'ns_modules/ns_bbfe/bbfe-listOfNestedModel/bbfe-listOfNestedModel',
  'ns_modules/ns_bbfe/bbfe-gridForm',
  'ns_modules/ns_bbfe/bbfe-autocompTree',
  'ns_modules/ns_bbfe/bbfe-fileUpload',
  'ns_modules/ns_bbfe/bbfe-select',
  'ns_modules/ns_bbfe/bbfe-gridForm',
  'ns_modules/ns_bbfe/bbfe-ajaxButton',
  'ns_modules/ns_bbfe/bbfe-lon',
  'ns_modules/ns_bbfe/bbfe-lat',
  'ns_modules/ns_bbfe/bbfe-fieldworkarea-autocomplete',
  'ns_modules/ns_bbfe/bbfe-objectPicker/bbfe-objectPicker',
  'ns_modules/ns_bbfe/bbfe-mediaFile/bbfe-mediaFile',
  'ns_modules/ns_bbfe/bbfe-administrativearea/bbfe-administrativearea',

  ],


function( Marionette, LytRootView, Router, Controller,Swal,config_datas, $, Backbone, renecolib, stats_logger) {

    var app = {};
    var JST = window.JST = window.JST || {};

    window.xhrPool = [];

    window.onkeydown = function (e) {
      if (e.keyCode == 8) { //backspace key
        if (!(e.target.tagName == 'INPUT' || e.target.tagName == 'TEXTAREA')) { //handle event if not in input or textarea
          e.preventDefault();
          e.stopPropagation();
        }
      }
    };

    window.addEventListener('mousewheel', function (event) {
      if (document.activeElement.type == "number") {
        event.preventDefault();
        event.stopPropagation();
        document.activeElement.blur();
      }
    });

    Backbone.Marionette.Renderer.render = function (template, data) {
      if (!JST[template]) throw 'Template \'' + template + '\' not found!';
      return JST[template](data);
    };


    var xhrAccessToken;



    $(document).bind("ajaxSend", function (a, b, c) {
      // console.log('ajaxStart', c.url);
    });

    // $( document ).ajaxStart(function() {
    //   console.log('ajaxStart');
    // })
    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
      //TODO
    });
    $.ajaxSetup({
      // before jQuery send the request we will push it to our array
      beforeSend: function (jqxhr, options) {
        // if(options.type === 'GET' || options.url.indexOf('http://') !==-1 ){ //should be a GET!! (thesaurus calls)
        //   $.xhrPool.calls.push(jqxhr);
        // }
        
        if (!stats_logger.disabled){
          stats_logger.sendServerCall({"url":options.url, "data":options.data});
        }
        
      },
      // when some of the requests completed it will splice from the array
      complete: function (jqxhr, options) {
        var index = $.xhrPool.calls.indexOf(jqxhr);
        if (index > -1) {
          $.xhrPool.calls.splice(index, 1);
        }
      },
      error: function (jqxhr, options) {
        if (jqxhr.status == 401) {

          console.log(arguments, "you are not logged or the api could not identify you, you will be redirected to the portal")
        }
        if (jqxhr.status == 403) {
          Swal({
            heightAuto: false,
            title: 'Unauthorized',
            text: "You don't have permission",
            type: 'warning',
            showCancelButton: false,
            confirmButtonColor: 'rgb(240, 173, 78)',
            confirmButtonText: 'OK'
          });
        }
        if (jqxhr.status == 409) {
          Swal({
            heightAuto: false,
            title: 'Data conflicts',
            text: jqxhr.responseText,
            type: 'warning',
            showCancelButton: false,
            confirmButtonColor: 'rgb(240, 173, 78)',
            confirmButtonText: 'OK'
          });
        }
      }
    });
    var maxRetry = 10;

    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {

      //if _retry is a number decrease it else set it to 10
      originalOptions._retry = isNaN(originalOptions._retry) ? maxRetry : originalOptions._retry - 1;

      //set relative url to absolute url
      if (
          options.url.indexOf(renecolib.apis.portal.axios.defaults.baseURL) > -1
          ||
          options.url.indexOf(renecolib.apis.thesaurus.axios.defaults.baseURL) > -1
          ||
          options.url.indexOf(renecolib.apis.ecoreleve.axios.defaults.baseURL) > -1
        ) {
        options.url = options.url;
      } else {
        options.url = renecolib.apis.ecoreleve.axios.defaults.baseURL + options.url;
      }

      // we only add our authorization token if we are calling our apis
      // if he exist and still valid
      if (options.url.indexOf(renecolib.apis.ecoreleve.axios.defaults.baseURL) > -1 || options.url.indexOf(renecolib.apis.thesaurus.axios.defaults.baseURL) > -1) {
        var accessToken = renecolib.auth.accessToken
        if (accessToken) {
          jqXHR.setRequestHeader('Authorization', 'Bearer ' + accessToken)
        }
      }

      // save the original error callback for later
      if (originalOptions.error) {
        originalOptions._error = originalOptions.error;
      }

      // overwrite *current request* error callback
      options.error = function () {};

      // setup our own deferred object to also support promises that are only invoked
      // once all of the retry attempts have been exhausted
      var dfd = $.Deferred();

      jqXHR.done(dfd.resolve);

      jqXHR.fail(function () {
        var args = Array.prototype.slice.call(arguments);
        if (jqXHR.status === 401 && originalOptions._retry > 0) {
          // a 401 from the back means no valid authorization in the request
          // try to refresh the oauth credentials for the next attempt(s)
            renecolib.apis.ecoreleve.awaitToken().then(()=> {
              // retry with originalOptions copy.
              // refreshed access token will be added in header when
              // this ajax will be send and will be "intercept" like all ajax
              // by ajaxPrefilter
              $.ajax(originalOptions).then(dfd.resolve, dfd.reject);
            })
        } else {
          // if the status code is not a 401 or the call return 10 times a 401
          // add our _error callback to our promise object
          if (originalOptions._error) {
            dfd.fail(originalOptions._error);
          }
          dfd.rejectWith(jqXHR, args);
        }
      });

      // NOW override the jqXHR's promise functions with our deferred
      return dfd.promise(jqXHR);
    });



    app = new Marionette.Application();
    app.on('start', function () {
      renecolib.auth.init().then(() => {
        app.rootView = new LytRootView();
        app.controller = new Controller();
        app.router = new Router({
          controller: app.controller
        });
        app.rootView.render();
        $.ajax({
          context: this,
          url: renecolib.apis.ecoreleve.axios.defaults.baseURL + 'security/has_access'
        }).done(function (data) {
          if (config_datas.get('loggerOptions').get('disabled') === false) {
            let options = {
              address: config_datas.get('loggerOptions').get('address'),
              port: config_datas.get('loggerOptions').get('port'),
              disabled: config_datas.get('loggerOptions').get('disabled'),
            }

            stats_logger.initialize(options).then(() => {
              stats_logger.sendSession(window.app.user.attributes.PK_id, config_datas.get('siteName'));
              let isHome = window.location.origin + window.location.pathname == window.location.href.replaceAll('#', '');
              stats_logger.sendVisit(window.location.hash, window.location.pathname.replaceAll('/', ''), (isHome ? 1 : 0));

              $("body").on("click", function (evt) {
                stats_logger.sendClickedLocationWithEvent(evt);
              });

            });
          }
          return;
        });

      })
    });

    window.swal = function (opt, type, callback, showCancelBtn) {
      var btnColor;
      switch (type) {
        case 'success':
          btnColor = 'green';
          opt.title = 'Success';
          break;
        case 'error':
          btnColor = 'rgb(147, 14, 14)';
          opt.title = 'Error';
          break;
        case 'warning':
          if (!opt.title) {
            opt.title = 'warning';
          }
          btnColor = 'orange';
          break;
        default:
          return;
          break;
      }
      Swal({
        heightAuto: false,
        title: opt.title,
        text: opt.text || '',
        type: type,
        showCancelButton: showCancelBtn,
        confirmButtonColor: btnColor,
        confirmButtonText: 'OK'
      }).then(result => {
        if (result.value && callback) {
          callback();
        }
      });
    };

    window.thesaurus = {};
    window.RegionLayers = {};

    $(window).ajaxStart(function (e) {
      $('#header-loader').removeClass('hidden');
    });
    $(window).ajaxStop(function () {
      $('#header-loader').addClass('hidden');
    });
    $(window).ajaxError(function () {
      $('#header-loader').addClass('hidden');
    });
    window.onerror = function () {
      $('#header-loader').addClass('hidden');
    };

    $.xhrPool = {};

    $.xhrPool.calls = []; // array of uncompleted requests

    $.xhrPool.allowAbort = false;

    $.xhrPool.abortAll = function () { // our abort function
      if ($.xhrPool.allowAbort) {
        this.calls.map(function (jqxhr) {
          jqxhr.abort();
        });
        $('#header-loader').addClass('hidden');
        $.xhrPool.calls = [];
      }
    };

    window.formInEdition = {};

    // get not allowed urls in config.js
    window.notAllowedUrl = [];
    if (config_datas.get('disabledFunc')) {
      var disabled = config_datas.get('disabledFunc');
      for (var i = 0; i < disabled.length; i++) {
        window.notAllowedUrl.push(disabled[i]);
      }
    }

    window.checkExitForm = function (confirmCallback, cancelCallback) {
      var i = 0;
      var urlChangeMax = 0;
      var indexMax = 0;
      if (!$.isEmptyObject(window.formInEdition)) {

        var newUrlSplit = window.location.hash.split('?');
        var oldUrlSplit = window.formInEdition.form.baseUri.replace(window.location.origin, '').replace(window.location.pathname, '').split('?');

        var toto = Object.keys(window.formInEdition.form).map(function (key2, index2) {
          if ((newUrlSplit[index2 - 1] != oldUrlSplit[index2 - 1]) || newUrlSplit[0] != oldUrlSplit[0]) {
            if (window.formInEdition.form[key2].formChange) {
              i++;
            }
            urlChangeMax++;
            return 1;
          } else {
            indexMax++;
            return 0;
          }
        });
      }

      if (i > 0) {
        var title = i18n.translate('swal.savingForm-title');
        var savingFormContent = i18n.translate('swal.savingForm-content');
        window.onExitForm = $.Deferred();
        Swal({
          heightAuto: false,
          title: title,
          text: savingFormContent,
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: 'rgb(221, 107, 85)',
          confirmButtonText: 'Quit',
          customClass: 'swal-cancel-btn-green',
          cancelButtonText: 'Continue edition'
        }).then((result) => {
          if ('dismiss' in result) {
            if (cancelCallback) {
              window.onExitForm.reject();
              cancelCallback();
            }
          } else if ('value' in result) {
            if (confirmCallback) {
              if (indexMax - urlChangeMax <= 0) {
                window.formInEdition = {};
              }
              window.onExitForm.resolve();
              confirmCallback();
            }
          }
        });
      } else {
        if (confirmCallback) {
          if (indexMax - urlChangeMax <= 0) {
            window.formInEdition = {};
          }
          //window.onExitForm.resolve();
          confirmCallback();
        }
      }
    };

    window.app = app;
    var userModel = Backbone.Model.extend({
      defaults : {
        Firstname: null,
        Language: null,
        Lastname: null,
        PK_id: null,
        fullname: null,
        role: null
      },
      urlRoot: renecolib.apis.ecoreleve.axios.defaults.baseURL + 'currentUser'
    })
    window.app.user =  new userModel()

    return app;
  });
