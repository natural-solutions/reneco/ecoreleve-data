define([
    'backbone',
    'text!./data.json'
], function(Backbone, configData) {

    var publicKeysModel = Backbone.Model.extend({
        defaults:{
            access: null,
            refresh: null
        }
    });

    var baseurlsModel = Backbone.Model.extend({
        defaults:{
            thesaurus: "/ThesaurusCore/api/thesaurus/"
        }
    });

    var loggerOptionsModel = Backbone.Model.extend({
        defaults:{
            address: "localhost",
            port: 5655,
            disabled: true
        }
    });

    var ConfigModel = Backbone.Model.extend({
        defaults:{
            devorprod: "dev",
            publicKeys : {},
            baseurls: {},
            loggerOptions: {},
            isReadOnly: false,
            instanceID: 0,
            siteName: null,
            googleAPIkey: null,
            mapZoom: null,
            client_id: null
        },

        initialize: function(){
            if (this.get('publicKeys') instanceof Object && !(this.get('publicKeys') instanceof Backbone.Model)) {
                this.set('publicKeys', new publicKeysModel(this.get('publicKeys')));
            }
            if (this.get('baseurls') instanceof Object && !(this.get('baseurls') instanceof Backbone.Model)) {
                this.set('baseurls', new baseurlsModel(this.get('baseurls')));
            }
            if (this.get('loggerOptions') instanceof Object && !(this.get('loggerOptions') instanceof Backbone.Model)) {
                this.set('loggerOptions', new loggerOptionsModel(this.get('loggerOptions')));
            }
        }
    });

    var configInstance = new ConfigModel(JSON.parse(configData));

    return configInstance;
});