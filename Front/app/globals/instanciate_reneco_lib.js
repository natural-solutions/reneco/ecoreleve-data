define(['renecolib', 'config_datas'], function (RenecoLib, config_datas) {
    let app_config = {
        client_id: config_datas.get('client_id'),
        publicKeys: {
            access: config_datas.get('publicKeys').get('access'),
            refresh: config_datas.get('publicKeys').get('refresh'),
        },
        mode: config_datas.get('devorprod'),
        baseUrls: {
            thesaurus: config_datas.get('baseurls').get('thesaurus')
        }
    }
    this.app = new RenecoLib.renecoLib.App(app_config);
    return this.app;
});