define([
  'underscore',
  'jquery',
  'backbone',
  'backbone-forms',
  'jqueryui',
], function(_, $, Backbone, Form
) {
  'use strict';
  return Form.editors.FieldworkingAreaEditor = Form.editors.Base.extend({

    previousValue: '',

    events: {
        'hide': 'hasChanged',

    },
    template: '<div>\
    <div class="input-group">\
        <span class="input-group-addon reneco reneco-FOR-autocomplete"></span>\
        <input class="form-control" type="text" data_value="<%= data_value %>" name="<%= key %>" id="<%=id%>" value="<%=value%>" data_value="<%=data_value%>" initValue="<%=initValue%>"/></div>\
        </div>\
    </div>',

    initialize: function (options) {
        Form.editors.Base.prototype.initialize.call(this, options);
        var _this = this;
        this.editable = options.schema.editable;
        this.url = 'regions';
        this.required = false;
        this.template = options.template || this.template;

        if(options.schema.validators && Array.isArray(options.schema.validators) && options.schema.validators.includes('required')){
          this.required = true;
        }

        // clone options.schema to avoid modifying source object (pointer)
        this.autocompleteSource = JSON.parse(JSON.stringify(options.schema.options));
        var url = options.schema.options.source;

        this.iconFont = options.schema.options.iconFont || 'hidden';
        if (options.schema.editorAttrs && options.schema.editorAttrs.disabled)  {
            this.iconFont = 'hidden';
        }

        this.validators.push({ type: 'Thesaurus', parent: this}); //?

        if (options.schema.options) {
          this.autocompleteSource.source = 'regions/autocomplete';
            if (typeof options.schema.options.source === 'string'){
                this.autocompleteSource.source = 'regions/autocomplete';
            }
            this.autocompleteSource.select = function(event,ui){
              event.preventDefault();
              _this.setValue(ui.item.value,ui.item.displayLabel,true);
              _this.matchedValue = ui.item;
              _this.isTermError = false;
              _this.displayErrorMsg(false);
            };
            this.autocompleteSource.focus = function(event,ui){
                event.preventDefault();
            };

            this.autocompleteSource.change = function(event,ui){
              var displayValue = _this.$input.val();
              var valueSelected = ui.item;
              if (!valueSelected){
                _this.$input.attr('data_value', null);
                _this.$input.val('');
                if(_this.required){
                  _this.isTermError = true;
                  _this.displayErrorMsg(true);
                }
                if (!_this.required){
                  _this.isTermError = false;
                  _this.displayErrorMsg(false);
                }
              }
              _this.$input.change();
          };
        }
        this.options = options;
    },

    setValue: function(value, displayValue, confirmChange) {
      if (displayValue || displayValue === ''){
        this.$input.val(displayValue);
      } else {
        this.fetchDisplayValue(value);
      }
      if (this.target){
        this.model.set(this.target,value);
      }
      this.$input.attr('data_value',value);
      this.matchedValue = value;

      if(confirmChange){
        this.$input.change();
      }
    },

    getValue: function() {
      if (this.isTermError) {
        return null ;
      }
      if (this.noAutocomp){
        return this.$input.val() || null;
      }
      return this.$input.attr('data_value')|| null;
    },

    getDisplayValue: function() {
      if (this.isTermError) {
        return null ;
      }
      return this.$input.val();
    },

    fetchDisplayValue: function(val){
      var _this = this;
      if (val instanceof Object && val.displayValue){
        val = val.displayValue;
      }
      $.ajax({
        url : _this.url+val,
        success : function(data){
          _this.setValue(val,data[_this.usedLabel],false);
          _this.displayErrorMsg(false);
          _this.isTermError = false;
        }
      });
    },

    displayErrorMsg: function (bool) {
      if (this.isTermError) {
        this.$input.addClass('error');
      } else {
        this.$input.removeClass('error');
      }
    },


    render: function () {
      var _this = this;

      var current_value = this.model.get(this.key);
      var value;

      if (current_value) {
        value = current_value
      }

      if (value) {
          var initValue = this.model.get(this.key);
          $.ajax({
              url : this.url+'/'+this.model.get(this.key),
              context: this,
              success : function(data){
                this.$input.val(data.Name);
              }
          });
      }
      var $el = _.template( this.template, {
          id: this.cid,
          value: value,
          data_value :_this.model.get(_this.key),
          initValue:initValue,
          iconFont:_this.iconFont,
          key : this.options.schema.title
      });

      this.setElement($el);
      let span = this.$el.find('span')
      if (!this.editable) {
        span[0].classList.add('hidden')
      }
      if(_this.required){
        this.$el.find('input').addClass('required');
      }

      this.$input = _this.$el.find('#' + _this.cid);

      _(function () {
          
          _this.$input.autocomplete(_this.autocompleteSource); // HERE

          if (_this.options.schema.editorAttrs && _this.options.schema.editorAttrs.disabled) {
              _this.$input.prop('disabled', true);
          }
      }).defer();
      return this;
  },


  });
});
