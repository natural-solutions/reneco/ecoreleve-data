define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'backbone-forms',
  'text!./tpl-bbfe-medialFile.html',

], function ($, _, Backbone, Marionette, Form, Template) {

  'use strict';

  Backbone.Form.validators.MediaFile = function (options) {
    return function MediaFile(value) {
      if (value === undefined) {
        return {
          type: 'MediaFile',
          message: 'undefined value'
        }
      }

      return null;
    };
  };

  return Form.editors.MediaFile = Form.editors.Base.extend({

    className: 'mediafile-form',
    template: _.template(Template),
    events: {
      'click button.js-btn-add': 'addFile',
      'click button.js-btn-delete': 'removeFile',
      'change input': 'handleNewFile'
    },
    templateData: function () {
      return {
        schema: this.schema,
        value: this.value,
        imgMedia: this.checkIfValueIsImg(this.value)
      }
    },

    initialize: function (options) {
      this.ui = {}
      this.value = null;
      this.fileInput = null;
      Form.editors.Base.prototype.initialize.call(this, options);
    },

    checkIfValueIsImg: function (value) {
      //edit mode new file selected in the input could be and image
      if (this.fileInput) {
        if (this.fileInput.type.startsWith('image/')) {
          return true;
        } else {
          return false;
        }
      }

      //here if no input file
      //but we have a value must be an URL
      //and the url have the extension
      if (value) {
        let segments = value.split('.')
        let extension = segments[segments.length - 1]
        if (['JPG', 'JPEG', 'PNG'].indexOf(extension.toUpperCase()) > -1) {
          return true
        }
      }
      return false
    },

    mapHTMLElem: function () {
      this.ui.inputFile = this.el.getElementsByTagName('input')[0];
      this.ui.btnAdd = this.el.getElementsByClassName('js-btn-add')[0];
      this.ui.btnDelete = this.el.getElementsByClassName('js-btn-delete')[0];
      this.ui.content = this.el.getElementsByClassName('content')[0];
      this.ui.imgElem = this.el.getElementsByTagName('img')[0];
    },

    addFile: function (event) {
      this.ui.inputFile.click()
    },

    removeFile: function (event) {
      this.fileInput = null;
      this.setValue(null);
      this.render();
    },

    handleNewFile: function (event) {
      this.fileInput = this.ui.inputFile.files[0];
      var newValue = URL.createObjectURL(this.fileInput);
      this.setValue(newValue);
      this.render()
    },

    render: function () {
      this.$el.html(this.template(this.templateData()));
      this.mapHTMLElem();

      return this;
    },

    asyncGetValue: async function () {
      var _this = this;
      if (this.fileInput && this.value.startsWith('blob:')) {
        var formData = new FormData();
        formData.append("fileBin", this.fileInput);
        formData.append("FK_Station", this.model.get("FK_Station"));
        await $.ajax({
          type: 'POST',
          url: 'mediasfiles/upload',
          processData: false,
          contentType: false,
          data: formData,
          context: this,
          xhr: function () {
            var xhr = $.ajaxSettings.xhr();

            xhr.upload.onprogress = function (event) {
              console.log("progress ", event);
              // _this.elems.progressBar.style.width = _this.progress(event.loaded, event.total);
            };
            xhr.upload.onload = function (event) {
              // _this.elems.progressBar.style.width = _this.progress(100, 100);
              console.log("well done!");
            };
            return xhr;
          }
        }).done(function (data) {
          _this.setValue(data.url)
        }).fail(function (err) {
          console.log("err")
        })
      }
    },

    getValue: function () {
      return this.value;
    },

    setValue: function (value) {
      let valInModel = this.model.get(this.schema.name);
      if (value != valInModel) {
        this.model.set(this.schema.name, value);
        this.value = this.model.get(this.schema.name);
      }
    },

  });
});
