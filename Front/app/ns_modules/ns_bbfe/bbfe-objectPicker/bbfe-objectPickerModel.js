define([
  'backbone',
], function (Backbone) {

  'use strict';
  return Backbone.Model.extend({
    defaults: {
      key: '',
      value: null,
      displayValue: null,
      editable: false,
      legacy_objectType: '',
      usedLabel: '',
      operator:'',
      triggerAt: 3
    },

    constructor: function(options) {

      let model = options.model;
      let key = options.key;
      let modelValue = model.get(key);
      let value = modelValue;
      let displayValue = null;
      let editable = options.schema.editable;
      let usedLabel = options.schema.options.usedLabel;
      let operator = options.schema.options.operator;
      let triggerAt = options.schema.options.triggerAt;

      if (modelValue instanceof Object) {
        value = modelValue.value;
        displayValue = modelValue.displayValue;
      }
      let legacy_objectType = ''
      switch (key) {
        case 'FK_Individual':
        case 'FK_Individual_1':
        case 'FK_Individual_2':
          legacy_objectType = 'individuals';
          break;
        case 'FK_MonitoredSite':
          legacy_objectType = 'monitoredSites';
          break;
        case 'FK_Sensor':
          legacy_objectType = 'sensors';
          break;
      }
      let args = {
        key: key,
        value: value,
        displayValue: displayValue,
        editable: editable,
        legacy_objectType: legacy_objectType,
        usedLabel: usedLabel,
        operator: operator,
        triggerAt: triggerAt
      }
      Backbone.Model.call(this, args);
    },

    initialize: function() {
      let value = this.get('value');
      if (value){
        this.fetchDisplayValue();
      }
    },

    getUrlForAutocomplete: function(){
      let legacy_objectType = this.get('legacy_objectType');
      let usedLabel = this.get('usedLabel');

      return 'api/autocomplete/' + legacy_objectType + '/' + usedLabel
    },

    getUrlObject: function() {
      let legacy_objectType = this.get('legacy_objectType') || '';
      let value = this.get('value') || '';

      return '#'+'/'+legacy_objectType+'/'+value;
    },

    fetchDisplayValue: async function() {
      let usedLabel = this.get('usedLabel');
      let value = this.get('value');
      let displayValue = this.get('displayValue');
      let legacy_objectType = this.get('legacy_objectType');
      let url = `api/autocomplete/${legacy_objectType}/${value}`;
      let dataToSend =  {'property': usedLabel};

      if (value == null) {
        this.set('displayValue',null);
        return;
      }
      // ALL modifications in the try catch finally
      // should be reported in
      // bbfe-objectpicker.js fetchDatas
      try {
        let response = await $.ajax({
          url: url,
          context: this,
          data: dataToSend
        })
        displayValue = response.label;
        if (displayValue === null) {
          displayValue = `NO_${usedLabel.toUpperCase()}_FOR_ID:${value}`;
        }
      }
      catch (err) {
        displayValue = `CANT_FETCH_${usedLabel.toUpperCase()}_FOR_ID:${value}`;
        if ("status" in err && err.status === 400){
          displayValue = 'NOT_FOUND';
        }
        if ("status" in err && err.status === 404){
          // should never happen
          // Murphy's law
          displayValue = 'NOT_FOUND';
        }
        if ("status" in err && err.status === 409){
          console.log(err)
          displayValue = `MULTIPLE_VALUES_${usedLabel.toUpperCase()}_FOR_ID:${value}`;
        }
      }
      finally{
        this.set('displayValue',displayValue)
      }
    }

  });
});
