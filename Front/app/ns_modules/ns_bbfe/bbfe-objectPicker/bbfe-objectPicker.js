define([
  'jquery',
  'underscore',
  'marionette',
  'backbone-forms',
  'text!./tpl-bbfe-objectPicker.html',
  './bbfe-objectPickerModel'
], function(
  $, _, Marionette, Form, Template, ObjectPickerModel
) {
  'use strict';
  return Form.editors.ObjectPicker = Form.editors.Base.extend({
    tagName: 'objectpicker',
    className: 'input-group',
    template: _.template(Template),
    events: {
      'click button.js-btn-object': 'openObjectInNewTab',
      'click button.js-btn-plus': 'createRegion',
    },

    onBlur: function(event) {
      let valueInput = this.inputElement.val();
      let oldValue = this.model.get('displayValue');
      //fix issue when compare string and int with !==
      let valueChanged = (valueInput != oldValue);
      if( !valueChanged ){
        //if the displayvalue not changed no need to render again
        //fix scenario issue when click on btn
        //click outside input -> blur input -> render -> loose click on btn
        return;
      }
      if (valueInput.trim().length > 0) {
        this.render(true);
      }
      if (valueInput.trim().length == 0){
        let datas = {
          value: null,
          displayValue: null
        }
        this.model.set(datas)
      }
    },

    preInitialize: function(options) {
      this.model = new ObjectPickerModel(options);
    },

    initialize: function(options) {
      this.preInitialize(options);
      options.model = this.model;
      Form.editors.Text.prototype.initialize.call(this, options);
      this.listenTo(this.model, 'change', function(){
        this.render(false)
      })
    },

    autocompleteConfig: function() {
      var _this = this;
      let url = this.model.getUrlForAutocomplete();
      let operator = this.model.get('operator');
      let triggerAt = this.model.get('triggerAt')
      return {
        // source: url,
        source: function(termObj,callback) {
          let term = termObj.term;
          $.getJSON(
            url,
            {
              term: term,
              operator: operator
            },
            callback
          );
        },
        minLength: triggerAt,
        select: function(event, ui) {
          event.preventDefault();
          let datas = {
            value: ui.item.value,
            displayValue: ui.item.label
          }
          _this.setValue(datas)
        },
        focus: function(event, ui){
          // Triggered when focus is moved to an item (not selecting).
          // The default action is to replace the text field's value with the value of the focused item,
          // though only if the event was triggered by a keyboard interaction.
          // Canceling this event prevents the value from being updated,
          // but does not prevent the menu item from being focused.
          event.preventDefault(); //cancel event bubble
        }
      }
    },

    render: function(cancelFocus) {
      if( 'blur input' in this.events ) {
        delete this.events['blur input'];
        this.delegateEvents()
      }
      this.$el.html(this.template(this.templateDatas()));
      if( !('blur input' in this.events) ) {
        this.events['blur input'] = 'onBlur';
        this.delegateEvents()
      }
      this.inputElement = this.$el.find('input');
      this.inputElement.autocomplete(
        this.autocompleteConfig()
      )
      if (!cancelFocus) {
        this.inputElement.focus()
        let inputSize = this.inputElement.val().length
        this.inputElement[0].setSelectionRange(inputSize, inputSize)
      }
      return this;
    },

    getValue: function() {
      let value = this.model.get('value')
      return value;
    },

    setValue: async function(datas) {
      this.model.set(datas);
      await this.afterSetValue(datas);
    },

    displayFakeValueForMonitoredSite: function(datas){
      let _this = this;
      let form_origin = this.form.model.urlRoot;
      let object_type = this.model.get('legacy_objectType');
      if (form_origin == 'monitoredSites' && object_type == 'individuals' && "value" in datas && datas.value) {
        $.ajax({
          url: 'individuals/' + datas.value,
          method: 'GET',
        }).done(function(response) {
          for (const property_name of Object.keys(response)) {
            if (property_name+'_'+_this.key in _this.form.fields)
                // _this.form.fields[property_name+'_'+_this.key].editor.forceValue(response[property_name]);
                _this.form.fields[property_name+'_'+_this.key].editor.fakeValue(response[property_name]);
          }
        }).fail(function() {
          return false;
        });
      }
    },


    afterSetValue: async function(datas){
      let _this = this;
      let form_origin = this.form.model.urlRoot;
      let object_type = this.model.get('legacy_objectType');
      if (form_origin == 'monitoredSites' && object_type == 'individuals') {
        let fullPathSplitted = null;
        let value = null;

        await $.ajax({
          url: 'individuals/' + datas.value,
          method: 'GET',
        }).done(function(response) {
          if(response['Species']) {
            if ('Taxon' in _this.form.fields) {
              _this.form.fields.Taxon.editor.forceValue(response['Species']);
            }
            for (const property_name of Object.keys(response)) {
              if (property_name+'_'+_this.key in _this.form.fields)
                  // _this.form.fields[property_name+'_'+_this.key].editor.forceValue(response[property_name]);
                  _this.form.fields[property_name+'_'+_this.key].editor.fakeValue(response[property_name]);
            }
          }
        }).fail(function(response) {
          Swal({
            heightAuto: false,
            title: 'Impossible to create this monitored site',
            text: 'Please try again.\nIf the problem persists, contact an administrator.',
            type: 'error',
            confirmButtonColor:'rgb(221, 107, 85)',
            confirmButtonText: 'OK'
          });
          return false;
        });
      }
    },

    templateDatas: function() {
      return this.model.attributes
    },

    openObjectInNewTab: function() {
      let url = this.model.getUrlObject();
      var win = window.open(url, '_blank');
      win.focus();
    },

    legacy_getPickerView: function() {
      let typeObj = this.model.get('legacy_objectType');
      var pickerView = window.app.entityConfs[typeObj].entities
      return pickerView;
    },
    legacy_getNewView: function() {
      let typeObj = this.model.get('legacy_objectType');
      var newView = window.app.entityConfs[typeObj].newEntity;
      return newView;
    },

    createRegion: function() {
      this.regionManager = new Marionette.RegionManager();
      this.regionManager.addRegions({
        modal: "#modal_" + this.key,
      });
      this.displayPicker();
    },

    displayNewView: function(objectType, datas) {
      var _this = this;
      let NewView = this.legacy_getNewView();
      this.NewView = NewView.extend({
        back: function(e){
          e.preventDefault();
          _this.displayPicker();
        },
        afterSaveSuccess: async function(response) {
          let id = response.ID;
          await _this.setValue(
            {
              value: id,
              displayValue:''
            }
          )
          await _this.model.fetchDisplayValue()
          _this.hidePicker();

        },
      });

      this.regionManager.get('modal').show(
        new _this.NewView({
          objectType: objectType,
          data: datas
        })
      );
    },

    getDefaultFilter: function() {
      let usedLabel = this.model.get('usedLabel');
      if (usedLabel != 'ID') {
        return [{
          Column: usedLabel,
          Operator: "is not null",
          Value: null
        }]
      }
      return []
    },

    displayPicker: function() {
      var _this = this;
      var PickerView =  this.legacy_getPickerView();

      this.PickerView = PickerView.extend({
        defaultFilters: _this.getDefaultFilter(),
        onRowClicked: async function(row){
          let usedLabel = _this.model.get('usedLabel')
          let datas = {
            value : row.data['ID'],
            displayValue: row.data[usedLabel]
          }
          await _this.setValue(datas);
          _this.hidePicker();
        },

        setDefaultOperatorFilter: function() {
          //will be called when clicked on .js-nav-tabs
          if(this.model.get('objectType') == 2){
            this.firstOperator = 'is null';
          } else {
            this.firstOperator = null;
          }
        },

        back: function(e){
          //when click on back button
          e.preventDefault();
          _this.hidePicker();
        },

        afterShow: function() {
          PickerView.prototype.afterShow.call(this);
          this.$el.find('.js-nav-tabs').removeClass('hide');
        },

        new: function(e){
          e.preventDefault();
          let _self = this;
          let currentTypeObject = this.model.get('type');
          let currentTypeID = this.model.get('objectType');
          let datas = {};
          _self.filters.update();
          for (var i = 0; i < _self.filters.criterias.length; i++) {
            if( _self.filters.criterias[i]['Operator'] == 'Is') {
              datas[_self.filters.criterias[i]['Column']] = _self.filters.criterias[i]['Value'] === 'null' ? '': _self.filters.criterias[i]['Value'];
            }
            else {
              datas[_self.filters.criterias[i]['Column']] = ''
            }
          }

          if (currentTypeObject == 'monitoredSites') {
            datas = {};

            var station_data = _this.form.getValue();
            datas['LAT'] = station_data.LAT;
            datas['LON'] = station_data.LON;
            datas['Name'] = station_data.Name;
            datas['ELE'] = station_data.ELE;
            datas['Precision'] = station_data.precision;
            datas['StartDate'] = station_data.StationDate;
            datas['Place'] = station_data.Place
            datas['FK_FieldworkArea'] = station_data.FK_FieldworkArea
            datas['FK_AdministrativeArea'] = station_data.FK_AdministrativeArea
          }

          if (currentTypeObject == 'individuals') {
            /*
              business rule
              if an individual is created with a protocol
              need to add the properties stationID

              in backend when the properties stationID is set
              will make a request to set the startdate of individual
              with the startdate of the station
            */
            let observationForm = _this.form;
            let stationID = observationForm.model.get('FK_Station')
            if (stationID) {
              datas['stationID'] = stationID;
            }
            _this.displayNewView(currentTypeID, datas);
            return;
          }

          if(this.availableTypeObj && this.availableTypeObj.length>1) {
            let ulElem = document.createElement("ul");
            let tabLength = this.availableTypeObj.length;
            for( let i = 0 ; i < tabLength ; i++ ) {
              let elem = this.availableTypeObj[i];
              let liElem = document.createElement('li');
              liElem.onclick = function(e) {
                let objectType = this.getAttribute('data-value');
                _self.ui.btnNew.tooltipster('close');
                _this.displayNewView(objectType, datas);
              };
              liElem.setAttribute('data-value' , elem.val);
              liElem.innerHTML = elem.label;
              ulElem.appendChild(liElem);
            }

            this.ui.btnNew.tooltipster({
              theme : 'tooltipList',
              position: 'top',
              interactive : true,
              content: '',
              contentAsHTML : true,
              functionReady: function(instance,helper) {
                let elemRoot = instance.elementTooltip();
                let elemContent = elemRoot.getElementsByClassName('tooltipster-content');
                $(elemContent).append(ulElem);
                instance.reposition();
              },
              functionAfter : function( instance, helper) {
                instance.destroy();
              }
            });
            this.ui.btnNew.tooltipster('open');
          }
        },
      });

      this.regionManager.get('modal').show(
        this.pickerView = new this.PickerView({enableNew:true})
      )
      $('#modal_' + this.key).fadeIn('fast');
    },

    hidePicker: function() {
      $('#modal_' + this.key).fadeOut('fast');
      this.inputElement.focus();
    }
  });

});
