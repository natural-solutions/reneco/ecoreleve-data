define([
    'jquery',
    'underscore',
    'marionette',
    'backbone-forms',
    'text!./tpl-bbfe-administrativearea.html',
    './bbfe-administrativeareamodel'
  ], function(
    $, _, Marionette, Form, Template, AdministrativeAreaModel
  ) {
    'use strict';
    return Form.editors.AdministrativeAreaEditor = Form.editors.Base.extend({
      tagName: 'administrativearea',
      className: 'input-group',
      template: _.template(Template),
      events: {
        'focus': 'clickOn',
        'mousedown input': 'clickOn',
        'blur input': 'blurOn'
      },

      blurOn: function(event){
        let datas = {
          value: this.model.get('value'),
          Name: this.model.get('Name'),
          displayValue: this.model.get('displayValue')
        }

        if (this.model.get('editable') ) {
          let currentValue = event.target.value;
          if( currentValue == '') {
            datas['value'] = null;
            datas['Name'] = null;
            datas['displayValue'] = null;
          }
          if( !this.model.get('displayFullpath') ) {
            datas['value'] = null;
            datas['Name'] = null;
            datas['displayValue'] = currentValue;
          }

          this.setValue(datas)
        }
      },

      clickOn: function(event){
        if (document.activeElement == event.target) {
          return;
        }
        if (this.model.get('editable') ) {
          let input = this.$el.find('input')[0];
          let endValue = input.value.length;
          input.setSelectionRange(endValue, endValue);
          input.focus();
        }
      },

      autocompleteConfig: function() {
        var _this = this;
        let url = '/api/autocomplete/carto'
        let operator = this.model.get('operator');
        let triggerAt = 3
        return {
          source: function(termObj,callback) {
            let term = termObj.term;
            $.getJSON(
              url,
              {
                term: term,
                operator: operator
              },
              callback
            );
          },
          minLength: triggerAt,
          select: function(event, ui) {
            event.preventDefault();
            let splitedfullpath = ui.item.fullpath.split('>')
            let datas = {
              value: ui.item.ID,
              // Name: ui.item.Name,
              displayValue: splitedfullpath[splitedfullpath.length - 1]
            }
            _this.setValue(datas)
          },
          focus: function(event, ui){
            // Triggered when focus is moved to an item (not selecting).
            // The default action is to replace the text field's value with the value of the focused item,
            // though only if the event was triggered by a keyboard interaction.
            // Canceling this event prevents the value from being updated,
            // but does not prevent the menu item from being focused.
            event.preventDefault(); //cancel event bubble
          }
        }
      },

      preInitialize: function(options) {
        this.model = new AdministrativeAreaModel(options);
      },

      initialize: function (options) {
        this.preInitialize(options);
        options.model = this.model;
        Form.editors.Text.prototype.initialize.call(this, options);
        this.listenTo(this.model, 'change', function(){
          this.render()
        })
      },

      getValue: function() {
        let displayFullpath = this.model.get('displayFullpath');
        let value = null;

        value = this.model.get('value');
        if (typeof(value) == 'number') {          
          return value;
        }
        if (displayFullpath) {
          return value;
        }
        if (!displayFullpath) {
          let valueInput = this.$el.find('input')[0].value;
          let datas = {
            value: null,
            Name: null,
            displayValue: valueInput
          }
          if( valueInput == '') {
            datas['value'] = null;
            datas['Name'] = null;
            datas['displayValue'] = null;
          }
          this.setValue(datas)
          return this.model.get('displayValue')
        }
      },

      setValue: function(datas){
        this.model.set(datas, {silent: true});
        this.model.trigger('change');
      },

      render: function(){
        let displayFullpath = this.model.get('displayFullpath');
        this.el.classList.remove('form-control');
        this.$el.html(this.template(this.templateDatas()));
        if (this.model.get('editable') ) {
          this.$el[0].tabIndex=0;
          this.inputElement = this.$el.find('input');
          this.inputElement.autocomplete(
            this.autocompleteConfig()
          ).autocomplete( "instance" )._renderItem = function( ul, item ) {
            let fullPath = item.fullpath;
            let valueToDisplay = fullPath;

            if (!displayFullpath && fullPath) {
              let fullPathSplitted = fullPath.split('>');
              valueToDisplay = fullPathSplitted[ fullPathSplitted.length - 1 ]
            };
            return $( "<li>" )
              .append( "<div>" + valueToDisplay + "</div>" )
              .appendTo( ul );
          };
        }
        if (!this.model.get('editable')) {
          let spanElement = this.$el.find('span')[0];
          let inputElement = this.$el.find('input')[0];
          spanElement.classList.add('hidden')
          inputElement.setAttribute('disabled', '');
        }
        return this;
      },

      templateDatas: function() {
        return this.model.attributes;
      }
    });

  });
