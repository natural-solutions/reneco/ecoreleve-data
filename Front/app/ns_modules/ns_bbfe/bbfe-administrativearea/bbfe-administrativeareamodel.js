define([
    'backbone',
  ], function (Backbone) {

    'use strict';
    return Backbone.Model.extend({
      defaults: {
        key: '',
        value: null,
        Name: null,
        displayValue: null,
        displayFullpath: false,
        editable: false,
        usedLabel: '',
        operator:'',
        triggerAt: 3
      },

      constructor: function(options) {

        let model = options.model;
        let key = options.key;
        let modelValue = model.get(key);
        let value = modelValue;
        let Name = null;
        let displayValue = null;
        let editable = options.schema.editable || options.model.attributes.editable;
        let usedLabel = options.schema.options.usedLabel;
        let operator = options.schema.options.operator;
        let triggerAt = options.schema.options.triggerAt;
        let displayFullpath = false;

        if ('schema' in options && 'options' in options.schema && 'displayFullpath' in options.schema.options) {
          displayFullpath = options.schema.options.displayFullpath
        }

        if (modelValue instanceof Object) {
          value = modelValue.value;
          displayValue = modelValue.displayValue;
        }
        let args = {
          key: key,
          value: value,
          Name: Name,
          displayValue: displayValue,
          editable: editable,
          usedLabel: usedLabel,
          operator: operator,
          triggerAt: triggerAt,
          displayFullpath: displayFullpath
        }
        Backbone.Model.call(this, args);
      },

      initialize: function() {
        let value = this.get('value');
        if (value){
          this.fetchDisplayValue();
        }
      },

      getUrlForAutocomplete: function(){
        let usedLabel = this.get('usedLabel');
        return 'api/autocomplete/carto/' + usedLabel
      },

      fetchDisplayValue: async function() {
        let value = this.get('value');
        let url = `api/autocomplete/carto/${value}`;
        let displayValue = null;
        let Name = null;
        let splittedValue;
        let displayFullpath = this.get('displayFullpath');

        if (value == null) {
          this.set('Name', Name);
          this.set('displayValue', displayValue);
          return;
        }
        if (!displayFullpath) {
          this.set('displayValue', value)
        }
        // ALL modifications in the try catch finally
        // should be reported in
        // bbfe-objectpicker.js fetchDatas
        try {
          let response = await $.ajax({
            url: url,
            context: this
          })
          Name = response.Name;
          displayValue = response.fullpath;
          if (displayValue) {
            splittedValue = displayValue.split('>');
            displayValue = splittedValue[ splittedValue.length - 1 ]
          }
        }
        catch (err) {
          if ("status" in err && err.status === 404){
            // should never happen
            // Murphy's law
            displayValue = 'NOT_FOUND';
            Name = 'NOT_FOUND';
          }
        }
        finally{
          this.set('displayValue',displayValue);
          this.set('Name',Name);
        }
      }

    });
  });