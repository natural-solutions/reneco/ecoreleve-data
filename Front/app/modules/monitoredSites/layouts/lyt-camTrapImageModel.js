define([
	'jquery',
	'underscore',
	'backbone',
	'marionette',
	'translater'
], function($, _, Backbone, Marionette, Translater) {

  'use strict';

  return  Backbone.Model.extend({
    defaults:{
      path :'',
      FileName: '',
      id: null,
			tags : null,
			note : '',
			date_creation : '',
    }
  });
});
