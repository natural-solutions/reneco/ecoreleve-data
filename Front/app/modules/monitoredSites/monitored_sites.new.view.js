define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
	'sweetAlert',
  
  'modules/objects/object.new.view',
  './monitored_site.model',

], function(
  $, _, Backbone, Marionette, Swal,
  NewView, MonitoredSiteModel
){

  'use strict';
  return NewView.extend({
    ModelPrototype: MonitoredSiteModel,

    afterSaveSuccess: function(){
      var _this = this;
      Swal({
        heightAuto: false,
        title: 'Success',
        text: 'New ' + _this.model.get('single') + ' created',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: 'green',
        confirmButtonText: 'Create a new ' + _this.model.get('single'),
        cancelButtonText: 'Back to the ' + _this.model.get('single') + ' list'
      }).then( (result) => { 
        if ('dismiss' in result) {
          _this.cancel();
        } else {
          _this.displayForm();
        }
      });
    },
  });
});
