define([
  'jquery',
  'underscore',
  'backbone',
  'sweetAlert',
  'ns_grid/customCellRenderer/decimal5Renderer',
  'ns_grid/customCellRenderer/dateTimeRenderer'
], function(
  $, _, Backbone, Swal, Decimal5Renderer, DateTimeRenderer
){
  'use strict';

  return Backbone.Model.extend({
    defaults: {
      label: 'monitored sites',
      single: 'monitored site',
      type: 'monitoredSites',

      icon: 'reneco-site',

      fk: 'FK_MonitoredSite',
      authorisationLvl: {
        create_new: 'js-user',
      },

      availableOptions: [{
        label: 'Standard',
        val: 'standard'
      }, {
        label: 'Nest and brood',
        val: 'nest_and_brood'
      }],

      formConfig: {
        modelurl: 'monitoredSites',
        displayMode: 'display',
        reloadAfterSave: true,
        afterSaveSuccess : function() {
          this.parent.reloadLocationHistoryGrid();
          this.parent.reloadDynPropertiesHistoryGrid();
        },
        afterShow: function() {
          $('#dateTimePicker').on('dp.change', function(e) {
            $('#dateTimePicker').data('DateTimePicker').maxDate(e.date);
          });
        },
        onSavingModel: async function() {
          let currentMonitoredSiteType = this.BBForm.model.get('FK_MonitoredSiteType'); // new
          if (!currentMonitoredSiteType) {
            currentMonitoredSiteType = this.BBForm.model.get('type_id'); // edition
          };
          currentMonitoredSiteType = Number(currentMonitoredSiteType);
          var nestAndBroodTypeLabel = 'Nest and Brood';

          let monitoredSitesTypes = [];
          let isNestAndBrood = false;

          await $.ajax({
            url: 'monitoredSites/getType'
          }).done(function(response) {
            monitoredSitesTypes = response;
          }).fail(function(response) {
            Swal({
              heightAuto: false,
              title: 'Impossible to create this monitored site',
              text: 'Please try again.\nIf the problem persists, contact an administrator.',
              type: 'error',
              confirmButtonColor:'rgb(221, 107, 85)',
              confirmButtonText: 'OK'
            });
            return false;
          });
          
          for (let monitoredSitesType in monitoredSitesTypes) {
            if (monitoredSitesTypes[monitoredSitesType].val == currentMonitoredSiteType && monitoredSitesTypes[monitoredSitesType].label == nestAndBroodTypeLabel) {
              isNestAndBrood = true;
            }
          }

          if (isNestAndBrood) {
            this.BBForm.fields.FK_Individual_1.el.parentElement.classList.remove('error');
            this.BBForm.commit();
            let idParents = [];
            let parents = [];
            let idParent1 = this.BBForm.model.get('FK_Individual_1');
            let idParent2 = this.BBForm.model.get('FK_Individual_2');
            let taxonInForm = this.BBForm.model.get('Taxon');

            if (idParent1) {
              idParents.push(idParent1);
            };
            if (idParent2) {
              idParents.push(idParent2);
            };

            for (let idParent in idParents) {
              await $.ajax({
                url: 'individuals/' + idParents[idParent],
                method: 'GET',
              }).done(function(response) {
                parents[idParent] = response;
              }).fail(function(response) {
                Swal({
                  heightAuto: false,
                  title: 'Impossible to create this monitored site',
                  text: 'Please try again.\nIf the problem persists, contact an administrator.',
                  type: 'error',
                  confirmButtonColor:'rgb(221, 107, 85)',
                  confirmButtonText: 'OK'
                });
                return false;
              });
            };

            let parentsSpecies = [];
            for (let parent in parents) {
              let currentSpecies = parents[parent].Species;
              if (currentSpecies) {
                parentsSpecies.push(currentSpecies);
                if(!taxonInForm.includes(currentSpecies)) {
                  Swal({
                      heightAuto: false,
                      title: 'Impossible to create this monitored site',
                      text: 'There is a mismatch between at least one parent species and the taxa. Please correct the data.',
                      type: 'error',
                      confirmButtonColor:'rgb(221, 107, 85)',
                      confirmButtonText: 'OK'
                    });
                    this.BBForm.fields.FK_Individual_1.el.parentElement.classList.add('error');
                  return false;
                };
              };
            };
            if ([...new Set(parentsSpecies)].length > 1) {
              Swal({
                heightAuto: false,
                title: 'Impossible to create this monitored site',
                text: 'Parents don\'t belong to the same species. Please correct the data.',
                type: 'error',
                confirmButtonColor:'rgb(221, 107, 85)',
                confirmButtonText: 'OK'
              });
              this.BBForm.fields.FK_Individual_1.el.parentElement.classList.add('error');
              return false;
            }
          }
          return true;
        },
        add_listener_for_autocomptree: function(toListen, pattern, current_key){
          toListen[current_key].editor.picker.on(
            'valueChange',
            function(event){
              const valueEditor = event.detail.newValue; // Object ou undefined
              const splittedKey = current_key.split(pattern);

              if (splittedKey.length > 1) {
                if (splittedKey[0] != '') {
                  const individualKey = pattern+splittedKey[1]
                  let flag = 0;

                  if (valueEditor != undefined) {
                    flag = 1;
                  };

                  if (flag == 0) {
                    for (const fieldName of Object.keys(toListen)){
                      if (fieldName.indexOf(individualKey) > 0) {
                        if (toListen[fieldName].editor.truevalue != undefined) {
                          flag = 1;
                        };
                      };
                    };
                  };

                  if (flag == 0) {
                    toListen[individualKey].editor.model.attributes.editable = true;
                  };
                  if (flag == 1) {
                    // at least one field not null, desactivate objectpicker
                    toListen[individualKey].editor.model.attributes.editable = false;
                  };
                  toListen[individualKey].editor.render(true);
                }
              }
            }
          )

        },

        afterShow: function(){
          let toListen = {};
          const fieldsetName = 'Parentage Information';
          let fieldsets = this.BBForm.fieldsets;
          for( let i = 0; i< fieldsets.length; i++){
            let legend = null;
            let schema = null;
            schema = fieldsets[i].schema;
            if (schema){
              legend = schema.legend;
            }
            if (legend == fieldsetName) {
              toListen = fieldsets[i].fields
            }
          }
          for (const key of Object.keys(toListen)) {
            let current_key = key;
            let editor = toListen[current_key].editor;
            let parent_model = toListen[current_key].model;
            let parent_schema = parent_model.schema;
            let type = parent_schema[current_key].type;

            if (type != 'AutocompTreeEditor') {
              let model = editor.model;
              editor.displayFakeValueForMonitoredSite(model.attributes);
            }
          }
        },

        BeforeShow: function() {
          let toListen = {};
          const fieldsetName = 'Parentage Information';
          const pattern = 'FK_Individual';
          let fieldsets = this.BBForm.fieldsets;
          let fields = this.BBForm.fields;
          for( let i = 0; i< fieldsets.length; i++){
            let legend = null;
            let schema = null;
            schema = fieldsets[i].schema;
            if (schema){
              legend = schema.legend;
            }
            if (legend == fieldsetName) {
              toListen = fieldsets[i].fields
            }
          }
          for (const key of Object.keys(toListen)) {
            let _this = this;
            let current_key = key;
            let editor = toListen[current_key].editor;
            let parent_model = toListen[current_key].model;
            let parent_schema = parent_model.schema;
            let type = parent_schema[current_key].type;

            // The splittedKey used hereafter has
            // length = 1 if key does not contain Individual pattern (e.g., Taxon)
            // in this case, there is nothing to do (i.e., no specific behavior)
            // length > 1 (=2) if key contains Individual pattern
            // in this case, the behavior depends on the key
            //    'FK_Individual_X':  If a value is set, all dynamic fields must be disabled (i.e., XXX_FK_Individual_X fields), otherwise it should be activated.
            //    'XXX_FK_Individual_X': All the dynamic properties concerning the individual must be inspected. If they are all null, then the FK_Individual_X field is activated. If at least one is not null, then the field must be deactivated.
            // 
            // The implementation of the event (change of value in the field) depends on the picker type. For now :
            //    AutoCompTree : it is a dynamic property > look if activate/deactivate Individual fields
            //    not AutoCompTree : it is a Individual field > look if activate/deactivate dyanmic property
            // So, the code above shoud be modified (and could be mutualized) if a parent's dynamic property that it is not a AutoCompTree is added, or if parent's fields are not objectpicker anymore

            if (type == 'AutocompTreeEditor') {
              _this.add_listener_for_autocomptree(toListen, pattern, current_key)

              if (this.model.get(key) && "value" in this.model.get(key) && this.model.get(key).value != null) {
                if (key.indexOf(pattern+'_1') > 0) {
                  toListen[pattern+'_1'].editor.model.attributes.editable = false;
                  toListen[pattern+'_1'].editor.render(true)
                }
                if (key.indexOf(pattern+'_2') > 0) {
                  toListen[pattern+'_2'].editor.model.attributes.editable = false;
                  toListen[pattern+'_2'].editor.render(true)
                }
              }
            }
            if (type != 'AutocompTreeEditor') {

              let model = editor.model;
              setTimeout(() => {
                editor.displayFakeValueForMonitoredSite(model.attributes);
              }, 300);

              model.on('change', function(event) {
                const valueEditor = event.get('value');
                const splittedKey = current_key.split(pattern);

                if (splittedKey.length > 1) {
                  if (splittedKey[0] == '') {
                    for (const fieldName of Object.keys(fields)) {
                      if (fieldName.indexOf(current_key) > 0) {
                        if (valueEditor != null ){
                          fields[fieldName].editor.el.setAttribute('disabled', 'disabled');
                          fields[fieldName].editor.conf.readonly = true;
                        };
                        if (valueEditor == null) {
                          fields[fieldName].editor.el.removeAttribute('disabled');
                          fields[fieldName].editor.conf.readonly = false;
                        };
                        fields[fieldName].editor.forceValue(null);
                        // we loose the listener because the render
                        // instanciate a new object
                        // we need to add the listener on the new object
                        _this.add_listener_for_autocomptree(toListen, pattern, fieldName)
                      }
                    }
                  };
                  // if (splittedKey[0] != '') {
                  //   // it is a dynamic property of a parent (i.e., key ~ XXX_FK_Individual_X)
                  //   // to implement if a new dynamic property that is NOT a AutoCompTree is added
                  //   // same logic/code than in the AutoCompTree part
                  //   ;
                  // };
                };
              });
            }
        }
        }
      },

      uiGridConfs: [
        {
          name: 'locationHistory',
          label: 'Location History'
        },
        {
          name: 'dynPropertiesHistory',
          label: 'History'
        },
        {
          name: 'equipment',
          label: 'Equipment'
        },
        {
          name: 'stations',
          label: 'Stations'
        },
        {
          name: 'cameraTrap',
          label: 'Camera Trap'
        },
      ],

      dynPropertiesHistoryColumnDefs: [{
        field: 'Parent',
        headerName: 'Parent',
      },{
        field: 'Name',
        headerName: 'Name',
      },{
        field: 'StartDate',
        headerName: 'Start Date',
        cellRenderer: DateTimeRenderer
      },{
        field: 'Value',
        headerName: 'Value'
      }],

      equipmentColumnDefs: [{
        field: 'StartDate',
        headerName: 'Start Date',
        cellRenderer: DateTimeRenderer
      },{
        field: 'EndDate',
        headerName: 'End Date',
        cellRenderer: DateTimeRenderer
      },{
        field: 'Type',
        headerName: 'Type',
      },{
        field: 'UnicIdentifier',
        headerName: 'Identifier',
        cellRenderer: function(params){
          if(params.data.UnicIdentifier){
            var url = '#sensors/' + params.data.SensorID;
            return  '<a target="_blank" href="'+ url +'" >' +
            params.value + ' <span class="reneco reneco-info right"></span>' +
            '</a>';
          } else {
            return '';
          }
        }
      }],

      cameraTrapColumnDefs: [{
        field: 'sessionID',
        headerName: 'ID',
        minWidth: 70,
      },{
        field: 'UnicIdentifier',
        headerName: 'IDENTIFIER',
        minWidth: 90,
      },{
        field: 'StartDate',
        headerName: 'START DATE',
        minWidth: 155,
      },{
        field: 'EndDate',
        headerName: 'END DATE',
        minWidth: 155,
      },{
        field: 'nbPhotos',
        headerName: 'NB PHOTOS',
        minWidth: 60,
      },
      {
        field : 'link',
        headerName : '',
        minWidth: 220,
        headerCellTemplate: function (params) {
          var eCell = document.createElement('span');
          var eBtn = document.createElement('button');
          eBtn.id = 'headerDownloadLink'
          eBtn.className = 'js-btndetailssession btn btn-success start hide'
          var eIcone = document.createElement('i');
          eIcone.className = 'glyphicon glyphicon-download-alt'
          var eSpan = document.createElement('span');
          eSpan.innerText = " Download all sessions"
          eIcone.appendChild(eSpan);
          eBtn.appendChild(eIcone);

          eCell.innerHTML =
            // '<div class="ag-header-cell">'+
            '<div id="agResizeBar" class="ag-header-cell-resize"></div>' +
            '<span id="agMenu" class="ag-header-icon ag-header-cell-menu-button"></span>' +
            '<div id="agHeaderCellLabel" class="ag-header-cell-label">' +
            eBtn.outerHTML +
            '<span id="agSortAsc" class="ag-header-icon ag-sort-ascending-icon"></span>' +
            '<span id="agSortDesc" class="ag-header-icon ag-sort-descending-icon"></span>' +
            '<span id="agNoSort" class="ag-header-icon ag-sort-none-icon"></span>' +
            '<span id="agFilter" class="ag-header-icon ag-filter-icon"></span>' +
            '<span id="agText" class="ag-header-cell-text"></span>' +
            // '</div>'+
            '</div>'
            eCell.onclick =  function () {
              var _this = this;
              // var siteID = _this.model.get('id');
              var path = window.location.href.split('/');

              var siteID = path[ path.length - 1 ];
              var url = 'photos/export/?siteid='+siteID

              $.ajax({
                url: url,
                method: 'GET',
                xhrFields: {
                    responseType: 'blob'
                },
                success: function (data, status, xhr) {
                  var a = document.createElement('a');
                  var url = window.URL.createObjectURL(data);
                  var contentDispositionHeader = xhr.getResponseHeader('Content-Disposition')
                  var filename = `${siteID}_all_sessions`
                  var extension ='.zip'
                  a.href = url;
                  if (contentDispositionHeader != null) {
                    var start = contentDispositionHeader.indexOf('filename=')+'filename='.length
                    var end = contentDispositionHeader.indexOf('.zip')
                    if ( end != -1 && start !=-1) {
                      filename = contentDispositionHeader.substring(start,end)
                    }
                  }
                  a.download = filename + extension;
                  document.body.append(a);
                  a.click();
                  a.remove();
                  window.URL.revokeObjectURL(url);
                }
              })

          };
          return eCell;
        },
        cellRenderer: function (params) {

          if (params.api.rowModel.getRowCount() >= 1) {
            var headerCol = document.getElementById('headerDownloadLink')
            headerCol.className = headerCol.className.replace('hide','')
          }
          var eCell = document.createElement('span');
          var eBtn = document.createElement('button');
          eBtn.className = 'js-btndetailssession btn btn-success start'
          var eIcone = document.createElement('i');
          eIcone.className = 'glyphicon glyphicon-download-alt'
          var eSpan = document.createElement('span');
          eSpan.innerText = " Download session"
          eIcone.appendChild(eSpan);
          eBtn.appendChild(eIcone);
          eCell.appendChild(eBtn);
          eCell.onclick =  function () {
            var path = window.location.href.split('/');
            var siteID = path[ path.length - 1 ];
            var sessionID = params.data['sessionID'];
            var url = 'photos/export/?siteid='+siteID+'&sessionid='+sessionID;

            $.ajax({
              url: url,
              method: 'GET',
              xhrFields: {
                  responseType: 'blob'
              },
              success: function (data, status, xhr) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                var contentDispositionHeader = xhr.getResponseHeader('Content-Disposition')
                var filename = `${siteID}_${sessionID}`
                var extension ='.zip'
                a.href = url;
                if (contentDispositionHeader != null) {
                  var start = contentDispositionHeader.indexOf('filename=')+'filename='.length
                  var end = contentDispositionHeader.indexOf('.zip')
                  if ( end != -1 && start !=-1) {
                    filename = contentDispositionHeader.substring(start,end)
                  }
                }
                a.download = filename + extension;
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
              }
            })

          };

          return eCell;
        }
      }

    ],

      stationsColumnDefs: [{
        field: 'ID',
        headerName: 'ID',
        hide: true,
      },{
        field: 'Name',
        headerName: 'Name',
        cellRenderer: function(params){
            var url = '#stations/' + params.data.ID;
            return  '<a target="_blank" href="'+ url +'" >' +
            params.value + ' <span class="reneco reneco-info right"></span>' +
            '</a>';
        }
      },{
        field: 'StationDate',
        headerName: 'date',
        cellRenderer: DateTimeRenderer
      },{
        field: 'LAT',
        headerName: 'latitude',
      }, {
        field: 'LON',
        headerName: 'longitude',
      },{
        field: 'fieldActivity_Name',
        headerName: 'FieldActivity',
      }]
    }
  });
});
