importScripts('../../../../node_modules/exifr/dist/full.umd.js')

onmessage = async function (event) {
  const options = {
    exif: true,
    silentErros: false,
    pick: ['DateTimeOriginal']
  }
  let message = {
    dateFind: undefined,
    error: null
  }

  await exifr.parse(event.data, ['DateTimeOriginal']).then((result) => {
    if( result ===  undefined){
      throw 'NO_EXIF_DATAS'
    }
    message.dateFind = result.DateTimeOriginal;
  }).catch((error) => {
    message.error = error
  }).finally(() => {
    self.postMessage(message)
  })
}
