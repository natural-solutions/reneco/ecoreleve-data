importScripts('../../../../node_modules/hash-wasm/dist/md5.umd.min.js');

init = async function (data) {
  if ('curMd5Wasm' in self) {
    self.curMd5Wasm.init();
  } else {
    self.curMd5Wasm = await hashwasm.createMD5();
  }

  self.calculate(data);
}

calculate = function (data) {
  const view = new Uint8Array(data);
  const hash = self.curMd5Wasm.update(view);
  const checksum = hash.digest();
  self.postMessage({
    checksum: checksum,
    error: null
  });
}

onmessage = function (event) {
  try {
    self.init(event.data);
  } catch (e) {
    self.postMessage({
      checksum: undefined,
      error: e
    });
  }
}