define([
  'jquery',
  'underscore',
  'backbone',
  'marionette',
  'sweetAlert',
  'resumable',
  'ns_grid/grid.view',
  'instance_reneco_lib',
  'moment'
], function ($, _, Backbone, Marionette, Swal,
  Resumable, GridView, RenecoLib, moment
) {

  'use strict';

  return Marionette.LayoutView.extend({
    regions: {
      rgGridPhotos: 'grid'
    },
    ui: {
      'addFile': 'button.js-add_file',
      'cancelUpload': 'button.js-cancel-upload',
      'legendCancelUpload': 'button.js-cancel-upload span',
      'addFileDropZone': 'div.drag-zone-hover',
      'modalUI': '#myPleaseWait'
    },
    events: {
      'click button.js-add_file': 'addFile',
      'click button.js-cancel-upload': 'cancelUpload'
    },
    template: 'app/modules/importFile/camTrap/templates/tpl-step1-camtrap.html',
    name: 'Upload Camera Trap Files',
    tagName:'importcamtrap',

    initialize: function (options) {
      this.maxWorkers = 8;
      this.processingRunning = false;
      this.parent = options.parent;
      this.previousModels = options.parent.models[options.parent.currentStepIndex] || null;

      this.strNbFiles = {
        min: 'Nb File:',
        max: 'Nb Files:'
      }
      this.firstRendered = true;

      this.model = new Backbone.Model();

      if (this.previousModels != null && this.previousModels.get('resumable')) {
        this.r = this.previousModels.get('resumable');
      } else {
        this.r = new Resumable({
          target: RenecoLib.apis.ecoreleve.axios.defaults.baseURL + 'sensorDatas/camtrap/resumable',
          fileType : ['image/*'] ,
          testChunks: true,
          clearInput: false,
          preprocess : this.preprocess.bind(this),
          chunkSize: 10 * 1024 * 1024
        });
      }


      this.model.set('resumable', this.r);

    },

    addHeader: function(resumableChunk) {
      var accessToken = RenecoLib.auth.accessToken;
      var obj1 = resumableChunk.resumableObj.opts.headers
      var obj2 = { Authorization: 'Bearer '+ accessToken };
      resumableChunk.resumableObj.opts.headers = Object.assign({}, obj1, obj2);
    },

    preprocess: function(resumableChunk) {
      var _this = this;
      RenecoLib.apis.ecoreleve.awaitToken().then(() => {
        _this.addHeader(resumableChunk);
        resumableChunk.preprocessFinished();
      })
    },

    createWorker: async function() {
      this.fileReaders = []
      this.MD5Workersasm = []
      this.exifWorkersasm = []
      for(var i=0 ; i < this.maxWorkers; i++) {
        this.fileReaders.push(
          new FileReader()
        )
        this.MD5Workersasm.push(
          new Worker('./app/modules/importFile/camTrap/workermd5wasm.js')
        )
        this.exifWorkersasm.push(
          new Worker('./app/modules/importFile/camTrap/workerexifr.js')
        )
      }

    },

    addDateParsedInResumableFile : function( dateFind, uniqueIdentifier) {
        var resumableFile = this.r.files.find(function (elem) {
        if (elem.uniqueIdentifier === uniqueIdentifier) {
          return elem;
        }
      });
      resumableFile.dateFind = this.parseDateToIso(dateFind);
    },

    parseDateToIso: function (dateToParse) {
      if(dateToParse === undefined) {
        return undefined;
      }
      return {
        dateString: moment(dateToParse).format('YYYY/MM/DD HH:mm:ss'),
        dateObj: dateToParse
      }
    },

    addChecksum: function(checksum, uniqueIdentifier) {
      var resumableFile = this.r.files.find(function (elem) {
        if (elem.uniqueIdentifier === uniqueIdentifier) {
          return elem;
        }
      });
      if (resumableFile) {
        resumableFile.checksum = checksum
      }
    },

    displaySwalFilesError : function(removedFilesExif, removedFilesMD5) {
      let title = 'Photo ';
      let text = 'The photo ';
      let nbFilesExif = removedFilesExif.length;
      let nbFilesMD5 = removedFilesMD5.length;

      var nbFiles =nbFilesExif + nbFilesMD5;
      if (nbFiles > 1) {
        title = 'Photos '
        text = 'All photos '
      }
      title += 'refused'

      text+= 'with a red background will be automatically removed '
      text+= 'when you will click on the next step.<BR>'
      text+= 'If you see Error in exif date column that means the photo doesn\'t have date in metadata<BR>'
      text+= 'If you see Error in checksum that means something goes wrong in the photo when we calculated it<BR><BR>'
      text+= 'Please contact an administrator, if you think all your photos should pass<BR>'

      Swal({
        heightAuto: false,
        title: title,
        html: '<div style="text-align: justify;padding-left: 10px;padding-right: 10px;">'+text+'</div>',
        type: 'warning',
        showCancelButton: false,
        confirmButtonText: 'Ok !',
        //closeOnCancel: true
      })
    },

    changeNbFilesValue: function () {
      let legend = this.strNbFiles.min;
      if (this.r.files.length > 1) {
        legend = this.strNbFiles.max;
      }
      $('span#js-nbFilesKey').html(legend)
      $('span#js-nbFilesValue').html(this.r.files.length);
    },
    changeFilesSizeValue: function () {
      var totalSize = 0;
      this.r.files.forEach(function (element) {
        totalSize += element.size;
      }, this);
      $('span#js-filesSizeValue').html((totalSize / (1024 * 1024)).toFixed(2) + ' Mb');

    },

    cancelUpload: function (e) {
      let selectedRows = this.gridViewPhotos.gridOptions.api.getSelectedRows();
      for (var i=0; i < selectedRows.length; i++){
        let curRow = selectedRows[i];
        this.r.removeFile(curRow.referenceFile)
      }
      this.displayCancelButtonUI(0);
      this.changeNbFilesValue();
      this.changeFilesSizeValue();
      this.displayGridPhotos();
    },

    readFileAsArrayBufferAsync: function (file, reader, worker) {
      return new Promise((resolve, reject) => {

        worker.onmessage = function (event) {
          var workerMessage = {
            checksum: event.data.checksum,
            file: file,
            error: event.data.error
          };
          if(workerMessage.error){
            reject(workerMessage);
            return;
          }
          resolve(workerMessage);
          return;
        };

        reader.onload = function() {
          worker.postMessage(this.result, [this.result] );
        };

        reader.onerror = function(err) {
          reject({
            file: file,
            error: err
          });
          return;
        }
        reader.readAsArrayBuffer(file);
      })
    },

    readFileAsArrayBufferAsyncExif: function (file, reader, worker) {
      const truncatedFile = file.file.slice(0, 131072);
      return new Promise((resolve, reject) => {
        worker.onmessage = function (event) {
          const workerMessage = {
            dateFind: event.data.dateFind,
            file: file,
            error: event.data.error
          };
          if(workerMessage.error){
            reject(workerMessage);
            return;
          }
          resolve(workerMessage);
          return;
        };

        reader.onload = function() {
            worker.postMessage(this.result, [this.result] );
        };
        reader.onerror = function(err) {
          reject({
            file: file,
            error: err
          });
          return;
        };
        reader.readAsArrayBuffer(truncatedFile);
      })
    },

    displayModal: function() {
      this.ui.modalUI.modal('show');
    },

    processExif: async function (files, filesAfterExif, removedFilesExif){
      try {
        for( var i=0; !(i >= files.length); i+=this.maxWorkers) {
          let stackFilesPart = []
          let stackFiles = []
          for (var j=0; j < this.maxWorkers && i+j < files.length; j++) {
            stackFiles.push(files[i+j])
          }
          await Promise.allSettled(stackFiles.map(async(file, index) => {
            let curWorker = this.exifWorkersasm[index];
            let curReader = this.fileReaders[index]
            return await this.readFileAsArrayBufferAsyncExif(file, curReader,curWorker);
          })).then((results)=> {
            let curResult = null;
            let value = null;
            let uniqueIdentifier = null;
            for(var i = 0; i < results.length; i++) {
              curResult = results[i];
              if (curResult.status == 'fulfilled') {
                filesAfterExif.push(curResult.value.file);
                value = curResult.value.dateFind;
                uniqueIdentifier = curResult.value.file.uniqueIdentifier;
              }
              if(curResult.status == 'rejected') {
                // this.r.removeFile(curResult.reason.file);
                removedFilesExif.push(curResult.reason.file);
                value = curResult.reason.dateFind;
                uniqueIdentifier = curResult.reason.file.uniqueIdentifier;
              }
              this.addDateParsedInResumableFile(value, uniqueIdentifier);
            }
            ;
          })
          this.progress(i+this.maxWorkers, files.length,'js-progress-bar-exif');
        }
        return
      }
      catch(error) {
        throw error
      }
    },

    processMD5: async function(files, filesAfterMD5, removedFilesMD5){
      try {
        for( var i=0; !(i >= files.length); i+=this.maxWorkers) {
          let stackFiles = []
          for (var j=0; j < this.maxWorkers && i+j < files.length; j++) {
            stackFiles.push(files[i+j])
          }
          await Promise.allSettled(stackFiles.map(async(file, index) => {
            let curReader = this.fileReaders[index];
            let workermd5wasm = this.MD5Workersasm[index];
            return await this.readFileAsArrayBufferAsync(file.file, curReader, workermd5wasm);
          })).then((results)=> {
            let curResult = null;
            let value = null;
            let uniqueIdentifier = null;
            for(var i = 0; i < results.length; i++) {
              curResult = results[i];
              if (curResult.status == 'fulfilled') {
                filesAfterMD5.push(curResult.value.file);
                value = curResult.value.checksum;
                uniqueIdentifier = curResult.value.file.uniqueIdentifier;
              }
              if(curResult.status == 'rejected') {
                removedFilesMD5.push(curResult.reason.file);
                value = curResult.reason.checksum;
                uniqueIdentifier = curResult.reason.file.uniqueIdentifier;
              }
              this.addChecksum(value, uniqueIdentifier);
            }
          })
          this.progress(i+this.maxWorkers, files.length,'js-progress-bar-md5');
        }
        return
      }
      catch(error) {
        throw error
      }
    },

    processFilesAdded: async function(originalFiles) {
      try {
        this.processingRunning = true;
        await this.createWorker();
        let removedFilesExif = [];
        let filesAfterExif = [];
        let removedFilesMD5 = [];
        let filesAfterMD5 = [];
        this.displayModal();
        //reset exif progress bar
        this.progress(0,0,'js-progress-bar-exif');
        //reset md5 progress bar
        this.progress(0,0,'js-progress-bar-md5');
        //refresh ui display for  nb files
        this.changeNbFilesValue();
        //refresh ui display for total size
        this.changeFilesSizeValue();
        //refresh ui grid
        this.displayGridPhotos();
        await this.processExif(originalFiles, filesAfterExif, removedFilesExif)
        //refresh ui grid
        this.displayGridPhotos();
        await this.processMD5(filesAfterExif, filesAfterMD5, removedFilesMD5);
        //refresh ui grid
        this.displayGridPhotos();
        /*
        add setimeout for hidding the modal after 1 sec for avoid "blink"
        for UX: if you just add 1 file the modal will show and hide too quickly
        */
        setTimeout(() => {
          this.processingRunning = false;
          $('#myPleaseWait').modal('hide');
          this.progress(0,0,'js-progress-bar-exif');
          this.progress(0,0,'js-progress-bar-md5');
          if( (removedFilesExif.length +  removedFilesMD5.length) > 0 ) {
            this.displaySwalFilesError(removedFilesExif, removedFilesMD5);
          }
        }, 1000);
        return;
      }
      catch(error) {
        throw error;
      }
    },

    onAttach: function () {
      this.r.on('filesAdded', async (files,filesSkipped) => {

        try {
          await this.processFilesAdded(files)
        }
        catch( error) {
          this.processingRunning = false;
          $('#myPleaseWait').modal('hide');
          this.progress(0,0,'js-progress-bar-exif');
          this.progress(0,0,'js-progress-bar-md5');

          // cancel all files
          this.r.cancel()
          this.displayGridPhotos();

          let msg = '<div style="text-align: justify;">'
          msg += 'Something goes wrong<BR>'
          msg += error
          msg += '<BR>'
          msg += 'All files have been cancelled'
          msg += '<BR>'
          msg+= 'Please make a screenshot and contact an administrator'
          msg += '<BR>'
          msg += '</div>'

          Swal({
            heightAuto: false,
            title: 'Critical error when processing',
            html: msg,
            type: 'error',
            showCancelButton: false,
            confirmButtonText: 'Ok !',
          })
        }
        finally {
          this.processingRunning = false;
        }

      })
      this.r.on('fileAdded', (file,event) => {
        //add keys for each file added
        //will be used for storing date found in exif and checksum calculated
        //will be used for UI in grid
        file.checksum = null;
        file.dateFind = null;
      })
    },

    progress: function (numerator,denominator, selector) {
      let width = '0%';
      if (denominator > 0) {
        if( ( numerator / denominator ) > 100 ) {
          width ='100%';
        }
        width = ((numerator / denominator) * 100).toFixed(2) + '%';
      }

      this.el.getElementsByClassName(selector)[0].style.width = width;
    },

    onShow: async function () {
      this.ui.modalUI.on("hidden.bs.modal", () => {
        if(this.processingRunning){
          this.ui.modalUI.modal('show');
        }
      });

      if (this.firstRendered === true) {
        this.r.assignBrowse(this.ui.addFile);

        this.r.assignDrop(this.ui.addFileDropZone);
        this.firstRendered = false;
      }
      if (this.previousModels !== null) {
        this.changeNbFilesValue();
        this.changeFilesSizeValue();
      }
      this.displayGridPhotos();
    },

    displayCancelButtonUI: function(nbRowsSelected) {
      if (nbRowsSelected == 0) {
        this.ui.cancelUpload[0].classList.add("hide")
        this.ui.legendCancelUpload[0].innerText = 'Remove selected file ';
        return;
      }
      if (nbRowsSelected ==  1) {
        this.ui.cancelUpload[0].classList.remove("hide")
        this.ui.legendCancelUpload[0].innerText = 'Remove selected file ';
      }
      if ( nbRowsSelected > 1) {
        this.ui.cancelUpload[0].classList.remove("hide")
        this.ui.legendCancelUpload[0].innerText = 'Remove selected files';
      }
    },

    displayGridPhotos: function () {
      let _this = this;
      var columnsDefs = [{
        field: 'fileName',
        headerName: 'Name',
        maxWidth: 500
      }, {
        field: 'dateFind',
        headerName: 'Exif Date',
        maxWidth: 350,
        cellRenderer: function(params) {
          if (params.value ===  undefined) {
            return 'Error'
          }
          return params.value
        }
      }, {
        field: 'checksum',
        headerName: 'Checksum',
        maxWidth: 500,
        cellRenderer: function(params) {
          if (params.value ===  undefined) {
            return 'Error'
          }
          return params.value
        }
      }, {
        field: 'size',
        headerName: 'Size',
        cellRenderer: function (params) {
            return (Number(params.value) / (1024 * 1024)).toFixed(2) + 'Mo';
        },
        maxWidth: 80
      }];

      var data = {}
      data = this.r.files.map(function (elem) {
        let dateFind = elem.dateFind;
        if (dateFind && 'dateString' in elem.dateFind) {
          dateFind  = elem.dateFind.dateString;
        }
        return {
          fileName: elem.fileName,
          size: elem.size,
          dateFind: dateFind,
          checksum: elem.checksum,
          referenceFile: elem
        }
      });
      this.gridViewPhotos = new GridView({
        // className: 'full-height',
        columns: columnsDefs,
        clientSide: true,
        displayRowIndex: true,
        gridOptions: {
          rowData: data,
          skipFocus: true,
          enableFilter: true,
          enableSorting: true,
          suppressRowClickSelection: true,
          rowSelection: 'multiple',
          onRowSelected: function() {
            let nbRowsSelected = 0;
            nbRowsSelected = this.api.getSelectedNodes().length;
            _this.displayCancelButtonUI(nbRowsSelected);
          },
          getRowStyle: function(params){
            // debugger
            if (params.data.dateFind === undefined || params.data.checksum === undefined) {
              return { background: 'red' };
          }
          }
        }
      })

      this.rgGridPhotos.show(this.gridViewPhotos);
    },

    cleanErrorFiles: function() {
      let filesToRemoved = [];
      let curFile = null;
      for(var i=0; i< this.r.files.length; i++){
        curFile = this.r.files[i];
        if( curFile.dateFind === undefined || curFile.checksum === undefined){
          filesToRemoved.push(curFile);
        }
      }
      for(var i=0; i< filesToRemoved.length; i++){
        this.r.removeFile(filesToRemoved[i]);
      }
    },

    onDestroy: function () {},

    validate: function () {
      if (this.r.files.length) {
        this.cleanErrorFiles();
        this.model.set('resumable', this.r);
        return this.model;
      } else {
        Swal({
          heightAuto: false,
          title: 'Warning',
          html: 'You need to add at least one file before going to the next step<BR>',
          type: 'warning',
          showCancelButton: false,
          confirmButtonText: 'OK'
        })
      }
    },

  });
});

