//radio
define([
  'jquery',
  'marionette',
  'ns_grid/grid.view',
  'moment',
  'sweetAlert',
  'sweetAlert2',
], function ($, Marionette, GridView, moment, Swal,Swal2) {

  'use strict';

  return Marionette.LayoutView.extend({
    tagName: 'release',
    template: 'app/modules/release/release_view.tpl.html',
    ui: {
      'releaseBtn' : '#release',
      'iconRelease' : '#iconbtnrelase'
    },
    regions: {
      rgGrid: '.js-rg-grid'
    },
    serializeData: function(){
      return {
        "station" : this.station,
        "release_method" : this.release_method
      };
    },
    handleClickOnReleaseMethod: function(event){
      this.closeToolTip();
      this.showLoading();
      let release_method_choose = event.target.getAttribute('value');
      this.processRelease(release_method_choose)
    },

    processRelease: function(release_method){
      var _this = this;
      let selectedRows = [];
      let rowModel = this.gridView.gridOptions.api.getModel();
      let error = null;
      for (var i = 0; i < rowModel.getRowCount(); i++) {
        var visibleRowNode = rowModel.getRow(i);
        if(visibleRowNode.selected){
          if(visibleRowNode.error || visibleRowNode.errorDuplicated){
            error = true;
          }
          selectedRows.push(visibleRowNode.data);
        }
      }
      if (error) {
        this.swal(
          {
            heightAuto: false,
            text:'unavailable sensor or duplicated sensor is equiped',
            title:'sensor error'
          },
          'error',
          null
        );
        this.hideLoading();
        return;
      }
      if (selectedRows.length == 0){
        this.swal(
          {
            heightAuto: false,
            text:'No individual selected',
            title:'error'
          },
          'error',
          null
        );
        this.hideLoading();
        return;
      }

      var indiv_list = []
      for (var i=0; i< selectedRows.length; i++){
        let curRow = selectedRows[i];
        let sensor_id = null;
        sensor_id = curRow.FK_Sensor;
        //if it's an object we need to get the value
        if (
          typeof curRow.FK_Sensor === 'object' &&
          curRow.FK_Sensor !== null &&
          !Array.isArray(curRow.FK_Sensor)
        ){
          sensor_id = curRow.FK_Sensor.value ? curRow.FK_Sensor.value : null;
        }
        let indiv_comment = curRow.Comments
        let release_comment= curRow.Release_Comments
        let data = {
          "Individual_ID" : curRow.ID,
          "Sensor_ID": sensor_id,
          "Individual_Comments": indiv_comment,
          "Release_Comments": release_comment
        }
        indiv_list.push(data)
      }
      var data = {
        "Release_Type_ID": Number(release_method),
        "Station_ID": this.station.ID,
        "Individual_Released": indiv_list
      }
      $.ajax({
        url: 'api/crud/release',
        method: 'POST',
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify(data),
        context:this
      }).done(function(resp, textStatus, jqXHR){
        _this.gridView.gridOptions.api.removeItems(_this.gridView.gridOptions.api.getSelectedNodes());
        _this.gridView.model.set('totalRecords', _this.gridView.gridOptions.api.rowModel.getRowCount());
        _this.gridView.ui.totalRecords.html(_this.gridView.model.get('totalRecords'));
        Swal2({
          type:'success',
          title:'Success',
          text:'Release created',
          icon: 'success',
          buttons: {
            cancel:'Keep same station',
            new_station: {
              text: 'Release on new station',
              value: 'new_station',
              className: 'btn-success'
            },
            view_station: {
              text: 'View station',
              value: 'view_station',
              className:'btn-success'
            }
          },
        }).then((value) => {
          switch (value) {
            case "view_station":
              Backbone.history.navigate(
                'stations/' + _this.station.ID,
                {trigger: true}
              );
              break;
            case "new_station":
              Backbone.history.navigate('release', {trigger:true});
              break;
            default:
              break;
          }
        });
      }).fail(function( jqXHR, textStatus, errorThrown){
        let options_for_swal = {
          heightAuto: false,
          text: "Something goes wrong... Please contact an administrator.",
          title: "Error undefined"
        }
        if (jqXHR.status==409) {
          let error_resp = jqXHR.responseJSON
          options_for_swal.text = error_resp.error_msg
          options_for_swal.title = error_resp.error_type+':'+error_resp.error_number
        }
        _this.swal(
          options_for_swal,
          'error',
          null
        );
      })
      .always(function() {
        _this.hideLoading();
      })
    },

    initialize: function(options) {

      this.stationID = options.stationID;
      this.station = {
        name: null,
        date: null
      };
      this.release_method = [
        {
          val: '',
          label: ''
        }
      ];
      this.columns = null;
      this.gridView = null;
      this.fetchDatas();
    },

    fetchDatas: async function() {
      this.blocking_error = false;
      this.blocking_error_msg = [];
      try {
        this.release_method = await this.fetchReleaseMethod();
      }
      catch (err) {
        this.blocking_error = True;
        this.blocking_error_msg.push("Cannot fetch Release Method");
      }
      try {
        this.station = await this.fetchStation();
      }
      catch (err) {
        this.blocking_error = True;
        this.blocking_error_msg.push("Cannot fetch Station");
      }
      try {
        this.columns = await this.fetchColumns();
        this.buildGrid();
      }
      catch (err) {
        this.blocking_error = True;
        this.blocking_error_msg.push("Cannot fetch Collumns for grid");
      }
      this.render();
    },

    fetchColumns: function() {
      return $.ajax({
        url: 'api/forms/grids/release'
      });

    },
    fetchReleaseMethod: function() {
      return $.ajax({
        url: 'api/release/getreleasemethod'
      });
    },
    fetchStation: function() {
      return $.ajax({
        url: 'stations/' + this.stationID
      });
    },
    onRender: function() {
      this.buildToolTip();
      if (this.gridView){
        this.rgGrid.show(this.gridView);
      }
    },

    buildGrid: function() {

      this.gridView = new GridView({
        clientSide: true,
        url: 'api/release/getindividualreleasable/',
        columns: this.columns,
        gridOptions: {
          enableFilter: true,
          // editType: 'fullRow',
          singleClickEdit : true,
          rowSelection: 'multiple',
          onCellValueChanged : this.checkAll.bind(this),
          overlayLoadingTemplate: '<span class="loading" ></span><span class="ag-overlay-loading-center">Loading .....</span>'
        },
        afterFetchColumns: function(options) {
          var colDefs = options.gridOptions.columnDefs;
          colDefs.map(function(colDef){
            if (colDef.field == 'FK_Sensor'){
              colDef.cellClassRules = {
                'error': function(params){
                  return params.node.error === true;},
                'ag-error-highlight': function(params){
                  return params.node.errorDuplicated === true;
                },
                'no-error':function(params){
                  return params.node.error === false;
                },
                'no-duplicated':function(params){
                  return params.node.errorDuplicated === false;
                }
              };
              }
          });
        }
      })
    },

    checkAll: async function (options) {
      /*
        checkAll is triggered by onCellValueChanged
        after each cells are edited
        that's could be confusing
        but we could check all constraints for all rows and cells
      */
      /*
        checkDuplicatedSensor will check all rows
      */
      this.checkDuplicatedSensor(options=options)
      /* checkSensorAvailability will only check cell FK_Sensor
        for the current edited row
      */
      options.node.error = false;
      try{
        await this.checkSensorAvailability(options=options)
      }
      catch(err){
        options.node.error = true;
      }
      options.api.refreshView();
    },

    checkSensorAvailability: function(options) {
      let format_date_from = 'DD/MM/YYYY HH:mm:ss';
      let format_date_to = 'DD/MM/YYYYTHH:mm:ss';
      let sensor_id = null;
      let date = moment(this.station.StationDate,format_date_from);
      sensor_id = options.data.FK_Sensor;
      //if it's an object we need to get the value
      if (
        typeof options.data.FK_Sensor === 'object' &&
        options.data.FK_Sensor !== null &&
        !Array.isArray(options.data.FK_Sensor)
      ){
        sensor_id = options.data.FK_Sensor.value ? options.data.FK_Sensor.value : null;
      }
      if (sensor_id === null) {
        Promise.resolve(true)
      }
      if (sensor_id) {
        let url = `api/sensors/${sensor_id}/isavailable?date=${date.format(format_date_to)}`
        return $.ajax({
          url: url
        })
      }
      return Promise.resolve(true)
    },

    checkDuplicatedSensor: function(options) {
      let rowsNode = options.api.rowModel.rowsToDisplay;
      let rows_by_sensor_id = {}

      for (let i=0; i < rowsNode.length; i++) {
        let nodeData = rowsNode[i].data;
        let sensor_id = null;
        rowsNode[i].errorDuplicated = false;
        // nodeData.FK_Sensor could be a value or a obj {value: integer, displayValue: string}
        sensor_id = nodeData.FK_Sensor;
        //if it's an object we need to get the value
        if (
          typeof nodeData.FK_Sensor === 'object' &&
          nodeData.FK_Sensor !== null &&
          !Array.isArray(nodeData.FK_Sensor)
        ){
          sensor_id = nodeData.FK_Sensor.value ? nodeData.FK_Sensor.value : null;
        }
        if (sensor_id === null) {
          continue;
        }
        //if sensor_id exist in rows_by_sensor_id
        //that's means we add it in previous iteration
        //so it's duplicated
        let sensor_id_is_duplicated = (sensor_id in rows_by_sensor_id);
        if (sensor_id_is_duplicated === false) {
            rows_by_sensor_id[sensor_id] = [];
        }
        rows_by_sensor_id[sensor_id].push(rowsNode[i]);
      }
      // will "walk" through the object (rows_by_sensor_id)
      // for each key (sensor_id)
      // if the array have more than one "row"
      // will iterate on each row to set them in errorDuplicated
      for (const cur_sensor_id in rows_by_sensor_id) {
        if (rows_by_sensor_id[cur_sensor_id].length > 1){
          rows_by_sensor_id[cur_sensor_id].forEach(row => {
            row.errorDuplicated = true;
          });
        }
      }
    },

    buildToolTip: function() {
      var _this = this;
      this.ui.releaseBtn.tooltipster({
        theme : 'tooltipList',
        position: 'bottom',
        interactive : true,
        trigger: "click",
        content : '',
        contentAsHTML : true,
        functionInit: function(instance, helper) {
          var content = $(helper.origin).find('.release_tooltip_content').detach();
          content.find('li').click(
            _this.handleClickOnReleaseMethod.bind(_this)
          )
          instance.content(content);
        }
      });
    },

    showToolTip: function() {
      this.ui.releaseBtn.tooltipster('open');
    },
    closeToolTip: function() {
      this.ui.releaseBtn.tooltipster('close');
    },
    showLoading: function() {
      this.ui.releaseBtn.prop('disabled', true);
      this.ui.iconRelease.removeClass();
      this.ui.iconRelease.addClass('loading');
      this.gridView.gridOptions.api.showLoadingOverlay();
    },
    hideLoading: function() {
      this.gridView.gridOptions.api.hideOverlay();
      this.ui.releaseBtn.prop('disabled', false);
      this.ui.iconRelease.removeClass();
      this.ui.iconRelease.addClass("icon reneco reneco-TRACK-to_release");
    },
    swal: function(opt, type, callback) {
      var btnColor;
      var confirmText;
      var showCancel;
      switch (type){
        case 'success':
          btnColor = 'green';
          confirmText = 'See Station';
          showCancel = true;
          break;
        case 'error':
          btnColor = 'rgb(147, 14, 14)';
          confirmText = 'Ok';
          showCancel = false;
          break;
        case 'warning':
          btnColor = 'orange';
          break;
        default:
          return;
          break;
      }
      Swal({
        heightAuto: false,
        title: opt.title || opt.responseText || 'error',
        text: opt.text || '',
        type: type,
        showCancelButton: showCancel,
        confirmButtonColor: btnColor,
        confirmButtonText: confirmText,
        cancelButtonColor: 'grey',
        cancelButtonText: 'New Release',
        closeOnConfirm: true,
      }).then( (result) => {
        if( 'value' in result && callback) {
          callback();
        }
      });
    }

  })
});