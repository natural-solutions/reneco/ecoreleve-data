CALL .\env\Scripts\activate
CALL python -m pip install pip==22.3.1 setuptools==66.1.1
CALL pip uninstall ecoreleve-server --yes
rem Installation en mode production (non editable) si la variable d'environnement NS_PYTHON_ENV existe et a pour valeur "prod"
IF "%NS_PYTHON_ENV%" == "prod" (
    CALL pip install .
) ELSE (
    CALL pip install -e .
)
CALL pip install pypiwin32
CALL pip install git+https://github.com/smarnach/pyexiftool.git
CALL .\localWheel_install.bat