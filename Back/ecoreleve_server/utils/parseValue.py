from sqlalchemy import select
from datetime import datetime

from .thesaurusLoad import thesaurusDictTraduction
from ..core import Main_Db_Base

import re

dictVal = {
    'null': None,
    '': None,
    'true': 1,
    'false': 0,
    'NULL': None,
    'None': None
}


def parser(value, handle_string_value=False):
    if handle_string_value:
        # string values equals None when:
        # empty string : ''
        # only spaces  : '    '
        if not value or value.isspace():
            return None
        return value

    for func in [dateParser, nullBitParser]:
        value = func(value)
    return value


def dateParser(stringDate):
    """ Date parsing tool.
        Change the formats here cause a changement in the whole application.
    """
    formats = ['%d/%m/%Y %H:%M:%S',
               '%d/%m/%Y%H:%M:%S',
               '%d/%m/%Y',
               '%H:%M:%S',
               '%Y-%m-%d %H:%M:%S']
    dateValue = stringDate
    for format_ in formats:
        try:
            dateValue = datetime.strptime(stringDate, format_)
            break
        except:
            pass
    return dateValue


def parseValue(value):
    oldValue = value
    try:
        newValue = dictVal[oldValue]
    except:
        newValue = oldValue
    return newValue


def nullBitParser(value):
    if isinstance(value, str) and value.isspace():
        value = None
    oldValue = value
    try:
        newValue = dictVal[oldValue]
    except:
        newValue = oldValue
    return newValue


def find(f, seq):
    """Return first item in sequence where f(item) == True."""
    for item in seq:
        if f(item):
            return item


def isNumeric(val):
    try:
        val = float(val)
        return True
    except:
        return False


def isEqual(val1, val2):
    return parseValue(val1) == parseValue(val2)


def formatValue(data, schema, session, request):
    individualPattern = re.compile("^FK_Individual(_[0-9]+)?$")
    for key in data:
        if key in schema:
            if schema[key]['type'] == 'AutocompTreeEditor':
                data[key] = formatThesaurus(
                    data=data[key],
                    request=request,
                    nodeID=schema[key]['options']['startId']
                )
            elif (schema[key]['type'] == 'ObjectPicker'
                    and not individualPattern.match(key)
                    and 'usedLabel' in schema[key]['options']):
                label = schema[key]['options']['usedLabel']
                data[key] = formatObjetPicker(
                    data=data[key],
                    key=key,
                    label=label,
                    session=session
                )
    return data


def formatThesaurus(data, request, nodeID=None):
    lng = request.authenticated_userid['userlanguage']
    try:
        if type(thesaurusDictTraduction.get(data, None)) is list and nodeID:
            displayValue = list(
                filter(lambda x: x['parentID'] == int(nodeID), thesaurusDictTraduction[data]))
            if displayValue:
                displayValue = displayValue[0]
            else:
                displayValue = thesaurusDictTraduction[data][0]
        else:
            displayValue = thesaurusDictTraduction[data]
            if type(displayValue) is list:
                displayValue = displayValue[0]
        data = {
            'displayValue': displayValue[lng],
            'value': data
        }
    except:
        # from traceback import print_exc
        # print_exc()
        data = {
            'displayValue': data,
            'value': data
        }
    return data


def formatObjetPicker(data, key, label, session):
    # format object Picker key to obtain generic object name (e.g. from FK_MonitoredSite to FK_MonitoredSite)
    # "_1" and "_2" are for the specific case of the "Nest and Brood" category of a monitored site :
    # Nest and Brood display two object Pickers (one for each parent) identified by "FK_Individual_1" and  "FK_Individual_2"
    autcompResult = getAutcompleteValues(
        ID=data,
        objName=key.replace('FK_', '').replace('_1', '').replace('_2', ''),
        NameValReturn=label,
        session=session
    )
    return {'displayValue': autcompResult,
            'value': data
            }


def getAutcompleteValues(ID, objName, NameValReturn, session):
    table = Main_Db_Base.metadata.tables[objName]

    query = select([table.c[NameValReturn]]).where(table.c['ID'] == ID)
    return session.execute(query).scalar()


def integerOrDefault(val,defaultVal,positive):
    '''

    will return an int or None

    '''
    try:
        res = int(val)
        if positive and res < 0:
            raise ValueError
    except ValueError:
        res = defaultVal
    return res
