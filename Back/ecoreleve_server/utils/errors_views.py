from pyramid.view import (
    view_config
)
from pyramid.httpexceptions import (
    HTTPUnauthorized,
)
from jwt import (
    PyJWTError
)


@view_config(context=PyJWTError)
def decode_failed(exception, request):
    return HTTPUnauthorized()
