from ..core.configuration_model.Business import BusinessRuleError
from traceback import print_exc
from sqlalchemy import event 
from sqlalchemy.exc import IntegrityError
from ecoreleve_server.modules.monitored_sites.monitored_site_resource import MonitoredSiteResource
from pyramid.events import NewRequest


def add_cors_headers_response_callback(event):
    def cors_headers(request, response):
        response.headers.update({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
            'Access-Control-Allow-Headers': 'Origin,\
                                            Content-Type,\
                                            Accept,\
                                            Authorization',
            'Access-Control-Allow-Credentials': 'true',
            'Access-Control-Max-Age': '1728000',
        })
    event.request.add_response_callback(cors_headers)


def session_callback(request):
    session = request.registry.dbmaker()

    @event.listens_for(session, 'before_flush')
    def receive_before_flush(session, flush_context, instances):
        '''
            Don't know why before_delete is replaced with before_flush
            but we for now we keep it here
        '''
        for instance_state, current_instance in session._deleted.items():
            if hasattr(current_instance, 'executeBusinessRules'):
                # create a binded connection with the ORM current_instance
                connection = session.get_bind(current_instance)
                current_instance.executeBusinessRules(
                    'before_delete',
                    connection
                )

    def cleanup(request):
        if request.exception is not None:
            session.rollback()
            session.close()
        else:
            try:
                session.commit()
            except BusinessRuleError as e:
                session.rollback()
                request.response.status_code = 409
                request.response.text = e.value
            except IntegrityError as e:
                context = getattr(request, 'context', None)
                if (context is not None):
                    if (isinstance(context, MonitoredSiteResource)):
                        request.response.status_code = 409
                        request.response.text = 'This name is already used for another Monitored site. Please change it.'
                        return
                raise e
            except Exception as e:
                print_exc()
                session.rollback()
                request.response.status_code = 500
            finally:
                session.close()
    request.add_finished_callback(cleanup)
    return session


def includeme(config):
    config.add_subscriber(add_cors_headers_response_callback, NewRequest)
    config.add_request_method(session_callback, name='dbsession', reify=True)
