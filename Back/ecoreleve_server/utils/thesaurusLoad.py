from sqlalchemy import select

from ..core import Main_Db_Base


thesaurusDictTraduction = {}


def loadThesaurusTrad(config):
    with Main_Db_Base.metadata.bind.connect() as connection:
        thesTable = Main_Db_Base.metadata.tables['ERDThesaurusTerm']
        query = select(thesTable.c)
        results = connection.execute(query).fetchall()

        for row in results:
            newTraduction = {
                'en': row['nameEn'],
                'fr': row['nameFr'],
                'parentID': row['parentID']
            }
            if thesaurusDictTraduction.get(row['fullPath'], None):
                thesaurusDictTraduction[row['fullPath']].append(newTraduction)
            else:
                thesaurusDictTraduction[row['fullPath']] = [newTraduction]
