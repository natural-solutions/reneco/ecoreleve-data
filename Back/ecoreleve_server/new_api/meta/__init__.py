from .meta_resources import (
    MetaResourceRoot,
    MetaResourceCollection,
    MetaResourceItem,
    MetaResourceCommon
)
from .meta_views import (
    GETView,
    POSTView,
    DELETEView,
    IGETView,
    IPOSTView,
    IDELETEView,
    IRESTView
)

__all__ = [
    "MetaResourceRoot",
    "MetaResourceCollection",
    "MetaResourceItem",
    "MetaResourceCommon",
    "GETView",
    "LOGINView",
    "POSTView",
    "DELETEView",
    "IGETView",
    "ILOGINView",
    "IPOSTView",
    "IDELETEView",
    "IRESTView"
]


def includeme(config):
    config.add_view(
        GETView,
        route_name='myApi',
        context=IGETView,
        request_method='GET',
        permission='read',
        name='',
        attr=None,
        renderer='json'
    )
    config.add_view(
        POSTView,
        route_name='myApi',
        context=IPOSTView,
        request_method='POST',
        permission='create',
        name='',
        attr=None
    )
    config.add_view(
        DELETEView,
        route_name='myApi',
        context=IDELETEView,
        request_method='DELETE',
        permission='delete',
        name='',
        attr=None
    )
