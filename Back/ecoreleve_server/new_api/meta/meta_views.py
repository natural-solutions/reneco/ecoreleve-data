from zope.interface import (
    Interface
)


class IGETView(Interface):
    pass


class IPOSTView(Interface):
    pass


class IDELETEView(Interface):
    pass


class IRESTView(IGETView, IPOSTView, IDELETEView):
    pass


class GETView:
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def __call__(self):
        return self.context.GET()


class POSTView:
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def __call__(self):
        return self.context.POST()


class DELETEView:
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def __call__(self):
        return self.context.DELETE()
