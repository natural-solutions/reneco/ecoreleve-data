from pyramid.interfaces import (
    ILocation
)
from zope.interface import (
    implementer
)
from ecoreleve_server.new_api.new_utils import (
    My_Method_Not_Allowed_Error
)
from .meta_views import (
    IRESTView
)


@implementer(ILocation, IRESTView)
class MetaResourceRoot:
    def __init__(self, name, parent, request):
        self.__name__ = name
        self.__parent__ = parent
        self.__request__ = request

    def __acl__(self):
        return []

    def __getitem__(self, name):
        '''
            usualy should be
            def __getitem(self,name):
                if name = 'MyRoute':
                    return MyRoute(name,self)
                else:
                    keyError
        '''
        raise KeyError

    def GET(self):
        raise My_Method_Not_Allowed_Error(reqObj=self.__request__)

    def POST(self):
        raise My_Method_Not_Allowed_Error(reqObj=self.__request__)

    def DELETE(self):
        raise My_Method_Not_Allowed_Error(reqObj=self.__request__)


@implementer(ILocation)
class MetaResourceCommon(MetaResourceRoot):
    def __init__(self, name, parent):
        MetaResourceRoot.__init__(
            self=self,
            name=name,
            parent=parent,
            request=parent.__request__
        )


@implementer(ILocation)
class MetaResourceCollection(MetaResourceCommon):

    __item_resource__ = None

    def __init__(self, name, parent):
        MetaResourceCommon.__init__(
            self=self,
            name=name,
            parent=parent
        )

    def __getitem__(self, name):
        try:
            int(name)
        except Exception as e:
            raise KeyError
        return self.__item_resource__(
                name=name,
                parent=self
            )

    def __acl__(self):
        return []


@implementer(ILocation)
class MetaResourceItem(MetaResourceCollection):
    def __init__(self, name, parent):
        MetaResourceCollection.__init__(
            self=self,
            name=name,
            parent=parent
        )

    def __acl__(self):
        return []
