from ecoreleve_server.core.init_db import (
    dbConfig
)
from .api import (
    api_factory
)


def includeme(config):
    config.add_route(
        "myApi",
        dbConfig.get('prefixapi') + '/api*traverse',
        factory=api_factory
    )
    config.include('.meta')
    config.include('.new_utils')
