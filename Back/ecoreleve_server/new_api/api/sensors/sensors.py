from datetime import datetime
from ecoreleve_server.new_api.meta import (
    MetaResourceCollection,
    MetaResourceItem,
    MetaResourceCommon
)
from ecoreleve_server.modules.observations.equipment_model import (
    checkEquip
)
from ecoreleve_server.modules.sensors.sensor_model import (
    Sensor
)
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPBadRequest,
    HTTPConflict
)
from pyramid.response import (
    Response
)


class IsAvailable(MetaResourceCommon):

    def GET(self):
        __format_date__ = '%d/%m/%YT%H:%M:%S'
        date_unparsed = self.__request__.GET.get('date', None)
        date = None
        error = {
            'type': None,
            'message': None
        }
        if not date_unparsed:
            error['type'] = 'missing_key'
            error['message'] = (
                'Need a date=value in the query string'
                f' with expected format for value:{__format_date__}'
            )
            return HTTPBadRequest(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body=error
            )
        try:
            date = datetime.strptime(
                date_unparsed,
                __format_date__
            )
        except ValueError:
            error['type'] = 'missing_key'
            error['message'] = (
                f'\'{date_unparsed}\' not a valid date,'
                f'expected format is {__format_date__}'
            )
            return HTTPBadRequest(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body=error
            )
        if date:
            availability = checkEquip(
                fk_sensor=self.__parent__.__name__,
                equipDate=date,
                session=self.__request__.dbsession
            )
            if availability is True:
                return Response(
                    json={
                        "availabe": True
                    }
                )
            else:
                error = {
                    'type': 'sensor_not_available',
                    'message': (
                        f'sensor {self.__parent__.__name__}'
                        f' not available at {date}'
                    )
                }
                return HTTPConflict(
                    headers={
                        "content-type": 'application/json',
                        "charset": 'utf-8'
                    },
                    json_body=error
                )


class SensorItem(MetaResourceItem):

    def __init__(self, name, parent):
        super().__init__(name, parent)
        self.check_item_exists()

    def check_item_exists(self):
        query = self.__request__.dbsession.query(Sensor)
        res = query.get(self.__name__)
        if not res:
            raise HTTPNotFound()

    def __getitem__(self, name):
        if name == 'isavailable':
            return IsAvailable(
                name=name,
                parent=self
            )
        else:
            raise KeyError


class SensorsCollection(MetaResourceCollection):

    __item_resource__ = SensorItem

    pass
