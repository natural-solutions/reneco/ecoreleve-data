from ecoreleve_server.new_api.meta import (
    MetaResourceRoot
)
from pyramid.security import (
    Allow,
    ALL_PERMISSIONS,
    Deny
)
from .crud import (
    Crud
)
from .release import (
    ReleaseForFront
)
from .forms import (
    Forms
)
from .sensors import (
    SensorsCollection
)
from .autocomplete import (
    Autocomplete
)
from .carto import (
    Carto
)
from .health_check import (
    HealthCheck
)


class Root(MetaResourceRoot):

    def __acl__(self):
        return [
            (Allow, 'group:admin', ALL_PERMISSIONS),
            (Allow, 'group:superUser', ALL_PERMISSIONS)
        ]

    def __getitem__(self, name):
        if name == 'crud':
            return Crud(name, self)
        if name == 'release':
            return ReleaseForFront(name, self)
        if name == 'forms':
            return Forms(name, self)
        if name == 'sensors':
            return SensorsCollection(name, self)
        if name == 'autocomplete':
            return Autocomplete(name, self)
        if name == 'carto':
            return Carto(name, self)
        if name == 'healthcheck':
            return HealthCheck(name, self)
        else:
            raise KeyError
