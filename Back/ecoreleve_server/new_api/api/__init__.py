from .root import (
    Root
)


def api_factory(request):

    return Root('', None, request)
