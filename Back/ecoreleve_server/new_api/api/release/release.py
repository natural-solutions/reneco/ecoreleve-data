from pyramid.httpexceptions import (
    HTTPConflict
)
from sqlalchemy import (
    case,
    func
)

from ecoreleve_server.new_api.meta import (
    MetaResourceCommon
)
from ecoreleve_server.core.configuration_model.synonyms import (
    Syn_TTopic,
    Syn_TTopicLibelle
)
from ecoreleve_server.core import (
    Main_Db_Base
)
import parse


class ReleasableIndividual(MetaResourceCommon):

    def GET(self):
        stmt = '''
            SET NOCOUNT ON;
        '''
        stmt += '''
            EXEC [UPDT_Is_Releasable_To_1] ?,?
        '''
        raw_connection = self.__request__.dbsession.get_bind(Syn_TTopic).raw_connection()
        rows_returned_in_sp = []
        debug = 0
        force_rollback = 0
        params = [debug, force_rollback]
        with raw_connection.cursor() as crs:
            try:
                crs.execute(stmt, params)

                while True:
                    if crs.description:
                        columns = [desc[0] for desc in crs.description]
                        rows_returned_in_sp.extend(
                            dict(zip(columns, row)) for row in crs
                        )

                    if not crs.nextset():
                        break
            except Exception as e:
                sqlstate = e.args[0]
                if sqlstate in ['42000', '23000']:
                    formatted_error = '[{0}] [{1}][{2}][{3}]{4} ({5}) ({6})'
                    error_msg = e.args[1]
                    result = parse.parse(
                        formatted_error,
                        error_msg
                    )
                    error_returned = {
                        'error_type': 'SQL_ERROR',
                        'error_number': result[5],
                        'error_msg': result[4]
                    }
                else:
                    error_returned = {
                        'error_type': 'ERROR_UNDEFINED',
                        'error_number': 0,
                        'error_msg': (
                            "Something goes wrong... "
                            "Please contact an administrator."
                        )
                    }
                return HTTPConflict(
                    headers={
                        "content-type": 'application/json',
                        "charset": 'utf-8'
                    },
                    json_body=error_returned
                )

        fields_for_front = {
            # 'Age': {
            #     'sql_type': 'String',
            #     'is_static': True
            # },
            'Box_ID': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Breeding ring kept after release': {
                'sql_type': 'ValueInt',
                'is_static': False
            },
            'Breeding_Ring_Code': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Chip_Code': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Comments': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Date_Sortie': {
                'sql_type': 'ValueDate',
                'is_static': False
            },
            'ID': {
                'sql_type': 'ValueInt',
                'is_static': True
            },
            'Mark_code_1': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Mark_code_2': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Origin': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Poids': {
                'sql_type': 'ValueFloat',
                'is_static': False
            },
            'Release_Comments': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Release_Ring_Code': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Sex': {
                'sql_type': 'ValueString',
                'is_static': False
            },
            'Species': {
                'sql_type': 'ValueString',
                'is_static': True
            },
            'FK_Sensor': {
                'sql_type': 'ValueInt',
                'is_static': False
            }
        }

        all_tables_in_db = Main_Db_Base.metadata.tables
        TIndividual = all_tables_in_db['Individual']
        TIndividualDynPropValuesNow = all_tables_in_db['IndividualDynPropValuesNow']

        static_properties = []
        dynamic_properties = []
        for key in fields_for_front:
            item = fields_for_front.get(key, {})
            is_static = item.get('is_static', None)
            if is_static is True:
                static_properties.append(key)
            if is_static is False:
                dynamic_properties.append(key)

        static_columns = []
        dynamic_columns = []

        for key in static_properties:
            col = TIndividual.c.get(key, None)
            if col is not None:
                static_columns.append(col)

        for key in dynamic_properties:
            fields = fields_for_front.get(key, {})
            key_type = fields.get('sql_type', None)
            col_where_value = TIndividualDynPropValuesNow.c.get(key_type)

            if key_type == 'ValueDate':
                col_where_value = func.format(
                    TIndividualDynPropValuesNow.c.get(key_type),
                    'yyyy-MM-dd 00:00:00'
                )

            agg_col = func.max(
                case(
                    [(
                        TIndividualDynPropValuesNow.c.Name == key,
                        col_where_value
                    )],
                    else_=None
                )
            ).label(key)
            dynamic_columns.append(agg_col)

        colsToRet = static_columns + dynamic_columns

        query = self.__request__.dbsession.query(TIndividual)
        query = query.join(
            TIndividualDynPropValuesNow,
            TIndividualDynPropValuesNow.c.FK_Individual
            ==
            TIndividual.c.ID
        )
        query = query.with_entities(*colsToRet)
        query = query.filter(
            TIndividual.c.Is_Releasable == 1
        )
        query = query.group_by(
            *static_columns
        )

        res = query
        response = []
        datas = []
        cpt = 0
        for row in res:
            cpt += 1
            datas.append(row._asdict())
        total_entries = {
            'total_entries': cpt
        }
        response = [
            total_entries,
            datas
        ]

        return response


class ReleaseMethod(MetaResourceCommon):

    def GET(self):
        __start_node_release_method__ = 229843
        userLng = self.__request__.authenticated_userid.get('userlanguage', 'fr').lower()
        colsToRet = [
            Syn_TTopic.TTop_PK_ID.label('val'),
            case(
                [(
                    Syn_TTopicLibelle.TLib_FK_TLan_ID == userLng,
                    Syn_TTopicLibelle.TLib_Name
                )],
                else_=Syn_TTopic.TTop_Name
            ).label('label')
        ]
        query = self.__request__.dbsession.query(Syn_TTopic)
        query = query.join(
            Syn_TTopicLibelle,
            (
                (Syn_TTopicLibelle.TLib_FK_TTop_ID == Syn_TTopic.TTop_PK_ID)
                &
                (Syn_TTopicLibelle.TLib_FK_TLan_ID == userLng)
            ),
            isouter=True
        )
        query = query.with_entities(*colsToRet)
        query = query.filter(
            Syn_TTopic.TTop_ParentID == __start_node_release_method__
        )
        res = query.all()
        return [row._asdict() for row in res]


class ReleaseForFront(MetaResourceCommon):

    def __getitem__(self, name):
        if name == 'getreleasemethod':
            return ReleaseMethod(name, self)
        if name == 'getindividualreleasable':
            return ReleasableIndividual(name, self)
        raise KeyError
