from http.client import HTTPResponse
from matplotlib.pyplot import table
from sqlalchemy import (
    case,
    cast,
    String,
    sql
)
from sqlalchemy.orm import (
    Query
)
from sqlalchemy.orm.exc import (
    MultipleResultsFound
)
from pyramid.security import (
    Allow
)
from pyramid.httpexceptions import (
    HTTPNotFound,
    HTTPConflict,
    HTTPBadRequest
)
from ecoreleve_server.modules.individuals.individual_model import (
    Individual
)
from ecoreleve_server.modules.monitored_sites.monitored_site_model import (
    MonitoredSite
)
from ecoreleve_server.modules.sensors.sensor_model import (
    Sensor
)
from ecoreleve_server.new_api.meta import (
    MetaResourceCommon
)
from ecoreleve_server.modules.regions import (
        AdministrativeArea
)

__LIMIT_ROWS_TO_RETURN__ = 30
__OPERATORS_CONF__ = [
    "startswith",
    "contains",
    "endswith"
]

class CartoAutocompleteItem(MetaResourceCommon):

    def build_query(self):
        query = self.__parent__.build_query()
        query = query.filter(
            AdministrativeArea.ID == self.__name__
        )
        return query
    def GET(self):
        query = self.build_query()
        res = query.one_or_none()
        if res is None:
            '''
                Impossible fk are materialized
            '''
            return {
                "ID": self.__name__,
                "Name": None,
                "fullpath": None
            }
        return res._asdict()


class CartoAutocomplete(MetaResourceCommon):

    def __getitem__(self, name):
        if name.isdigit() is True:
            return CartoAutocompleteItem(name, self)
        raise KeyError

    def build_query(self):
        dbsession = self.__request__.dbsession
        query = Query(
                entities=[
                    AdministrativeArea.ID,
                    AdministrativeArea.fullpath,
                    AdministrativeArea.Name
                ],
                session=dbsession
            )
        return query

    def GET(self):
        term = self.__request__.GET.get('term', None)
        operator = self.__request__.GET.get('operator', None)
        if term is None or operator is None:
            msg = ''
            if term is None:
                msg = 'term is missing'
            if operator is None:
                msg = 'operator is missing'
            if operator is None and term is None:
                msg = 'term and operator are missing'
            raise HTTPBadRequest(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body={
                    'error_type': 'BADREQUEST',
                    'error_number': 400,
                    'error_msg': (
                        f'Query string {msg}'
                    )
                }
            )
        if operator not in __OPERATORS_CONF__:
            raise HTTPBadRequest(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body={
                    'error_type': 'BADREQUEST',
                    'error_number': 400,
                    'error_msg': (
                        f"Query string 'operator={operator}' not authorize"
                        f"only ({__OPERATORS_CONF__})"
                    )
                }
            )

        query = self.build_query()

        if operator == 'startswith':
            query = query.filter(
                AdministrativeArea.fullpath.like(f'{term}%').collate('French_CS_AS')
            )
        if operator == 'contains':
            query = query.filter(
                AdministrativeArea.fullpath.like(f'%{term}%').collate('French_CS_AS')
            )
        if operator == 'endswith':
            query = query.filter(
                AdministrativeArea.fullpath.like(f'%{term}').collate('French_CS_AS')
            )
        query = query.order_by(
            AdministrativeArea.fullpath
        )
        query = query.limit(__LIMIT_ROWS_TO_RETURN__)

        res = query.all()

        toRet = []

        for row in res:
            toRet.append(row._asdict())

        return toRet


class ItemProps(MetaResourceCommon):
    def GET(self):
        dbsession = self.__request__.dbsession
        term = self.__request__.GET.get('term', None)
        operator = self.__request__.GET.get('operator', 'startswith')
        property_name = self.__name__
        current_model = self.__parent__.orm_model
        if term is None:
            raise HTTPBadRequest(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body={
                    'error_type': 'BADREQUEST',
                    'error_number': 400,
                    'error_msg': (
                        f'Query string "term" is missing'
                    )
                }
            )
        if operator not in __OPERATORS_CONF__:
            raise HTTPBadRequest(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body={
                    'error_type': 'BADREQUEST',
                    'error_number': 400,
                    'error_msg': (
                        f"Query string 'operator={operator}' not authorize"
                        f"only ({__OPERATORS_CONF__})"
                    )
                }
            )

        current_table = current_model.__table__
        is_table_column = hasattr(current_table.columns, property_name)
        if is_table_column is True:
            query = self.__parent__.build_query_for_table(
                property_name=property_name
            )
            if operator == 'startswith':
                query = query.filter(
                    current_table.c[property_name].startswith(f'{term}')
                )
            if operator == 'contains':
                query = query.filter(
                    current_table.c[property_name].contains(f'{term}')
                )
            if operator == 'endswith':
                query = query.filter(
                    current_table.c[property_name].endswith(f'{term}')
                )
            query = query.order_by(
                current_table.c[property_name]
            )
        if is_table_column is False:
            # For now we dont check if the property exist in dynprop
            cte_query = self.__parent__.build_query_for_dyn_prop(
                property_name=property_name
            )
            cte_query = cte_query.cte(name='cte_query')

            query = Query(
                entities=[
                    cte_query.c.value.label("value"),
                    cte_query.c.label.label("label")
                ],
                session=dbsession
            )
            if operator == 'startswith':
                query = query.filter(
                    cte_query.c.label.startswith(f'{term}')
                )
            if operator == 'contains':
                query = query.filter(
                    cte_query.c.label.contains(f'{term}')
                )
            if operator == 'endswith':
                query = query.filter(
                    cte_query.c.label.endswith(f'{term}')
                )
            query = query.order_by(
                cte_query.c.label
            )

        query = query.limit(__LIMIT_ROWS_TO_RETURN__)
        return [row._asdict() for row in query]


class ItemResource(MetaResourceCommon):

    def GET(self):
        dbsession = self.__request__.dbsession
        current_model = self.__parent__.orm_model
        id = self.__name__
        query = Query(
            entities=current_model,
            session=dbsession
        )
        res = query.get(id)
        if res is None:
            raise HTTPNotFound(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body={
                    'error_type': 'NOT_FOUND',
                    'error_number': 404,
                    'error_msg': f'{self.__parent__.__name__}/{id} not exist'
                }
            )
        property_name = self.__request__.GET.get('property', None)
        if property_name is None:
            raise HTTPBadRequest(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body={
                    'error_type': 'BAD_REQUEST',
                    'error_number': 400,
                    'error_msg': 'Missing property=value in query string'
                }
            )
        # .__table__ is an sqlalchemy property on a model
        # mapping the table in db
        current_table = current_model.__table__
        # maybe a weak point in algo
        # we suppose the property_name is in the "main" table
        # if the "resource" is a join between many table
        # the property could be valid but in anoter table
        is_table_column = hasattr(current_table.columns, property_name)
        if is_table_column is True:
            query = self.__parent__.build_query_for_table(
                property_name=property_name
            )
            query = query.filter(
                current_model.__table__.c['ID'] == id
            )
        if is_table_column is False:
            # For now we dont check if the property exist in dynprop
            dynPropValuesNowView = self.__parent__.dynPropValuesNowView
            query = self.__parent__.build_query_for_dyn_prop(
                property_name=property_name
            )
            query = query.filter(
                (dynPropValuesNowView.c.FK_Individual == id)
            )

        try:
            res = query.one_or_none()
            if res is None:
                '''
                None mean no row for id and that's mean the resource not exist
                if we make a query for table
                Should not happen as we query at the beggining
                For testing if the row exist
                A lock should be set on row in db

                But if we make a query for dyn prop that's could mean no row
                forthis property "yet".
                If the property is new and data are not migrated

                anyway we do not return a 404
                '''
                return {
                    "value": self.__name__,
                    "label": None
                }
            return res._asdict()
        except MultipleResultsFound:
            raise HTTPConflict(
                headers={
                    "content-type": 'application/json',
                    "charset": 'utf-8'
                },
                json_body={
                    'error_type': 'DATA_CONFLICTS',
                    'error_number': 409,
                    'error_msg': (
                        f'individuals/{self.__name__} '
                        f'multiple values for {property_name} '
                        f'at the same date'
                    )
                }
            )


class TableResource(MetaResourceCommon):

    def build_query_for_table(self, property_name):
        dbsession = self.__request__.dbsession
        current_model = self.orm_model
        # possible error
        # we assume the primary key of the "main" table
        # is on only one column nammed 'ID'
        query = Query(
            entities=[
                current_model.__table__.c['ID'].label('value'),
                current_model.__table__.c[property_name].label('label')
            ],
            session=dbsession
        )

        return query

    def build_query_for_dyn_prop(self, property_name):
        dbsession = self.__request__.dbsession
        query = Query(
            entities=[
                self.dynPropValuesNowView.c.FK_Individual.label('value'),
                case(
                    [
                        (
                            self.dynPropValuesNowView.c.TypeProp == 'String',
                            self.dynPropValuesNowView.c.ValueString
                        ),
                        (
                            self.dynPropValuesNowView.c.TypeProp == 'Date',
                            sql.func.CONVERT(
                                sql.literal_column('VARCHAR(500)'),
                                self.dynPropValuesNowView.c.ValueDate,
                                sql.literal_column('126')
                            )
                        ),
                        (
                            self.dynPropValuesNowView.c.TypeProp == 'Float',
                            cast(
                                self.dynPropValuesNowView.c.ValueFloat,
                                String(500)
                            )
                        ),
                        (
                            self.dynPropValuesNowView.c.TypeProp == 'Integer',
                            cast(
                                self.dynPropValuesNowView.c.ValueInt,
                                String(500)
                            )
                        ),
                    ],
                    else_=cast(None, String(500))
                ).label('label')
            ],
            session=dbsession
        )
        query = query.filter(
            (
                (
                    (self.dynPropValuesNowView.c.TypeProp == 'String')
                    &
                    (self.dynPropValuesNowView.c.ValueString.isnot(None))

                ).self_group()
                |
                (
                    (self.dynPropValuesNowView.c.TypeProp == 'Date')
                    &
                    (self.dynPropValuesNowView.c.ValueDate.isnot(None))
                ).self_group()
                |
                (
                    (self.dynPropValuesNowView.c.TypeProp == 'Float')
                    &
                    (self.dynPropValuesNowView.c.ValueFloat.isnot(None))
                ).self_group()
                |
                (
                    (self.dynPropValuesNowView.c.TypeProp == 'Integer')
                    &
                    (self.dynPropValuesNowView.c.ValueInt.isnot(None))
                ).self_group()
            )
        )
        query = query.filter(
            (self.dynPropValuesNowView.c.Name == property_name),
        )

        return query

    def __init__(self, name, parent):
        MetaResourceCommon.__init__(
            self=self,
            name=name,
            parent=parent
        )
        self.orm_model = self.__parent__.__ALLOWED__RESOURCES__.get(name)
        self.dynPropValuesNowView = getattr(
            self.orm_model,
            'LastDynamicValueViewClass',
            None
        )

    def __getitem__(self, name):
        if name.isdigit() is True:
            return ItemResource(name, self)
        return ItemProps(name, self)


class Autocomplete(MetaResourceCommon):

    __ALLOWED__RESOURCES__ = {
        'individuals': Individual,
        'monitoredSites': MonitoredSite,
        'sensors': Sensor,
        # 'carto': AdministrativeArea
    }

    def __getitem__(self, name):
        if name == 'carto':
            return CartoAutocomplete(
                name=name,
                parent=self)
        if name in self.__ALLOWED__RESOURCES__:
            return TableResource(
                name=name,
                parent=self
            )
        raise KeyError

    def __acl__(self):
        return [
            (Allow, 'group:admin', 'read'),
            (Allow, 'group:superUser', 'read'),
            (Allow, 'group:user', 'read'),
            (Allow, 'group:guest', 'read')
        ]


