import os
import json
import psutil
import datetime
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import sessionmaker
import configparser
from ecoreleve_server.new_api.meta import (
    MetaResourceRoot,
    MetaResourceCommon
)
from pyramid.security import (
    Allow,
    ALL_PERMISSIONS,
    Deny,
    Everyone
)

class HealthCheck(MetaResourceCommon):
    def __acl__(self):
        return [
            (Allow, Everyone, ALL_PERMISSIONS)
        ]

    def __init__(self, name, parent):
        self.__name__ = name
        self.__parent__ = parent
        self.healthy = None
        self.timestamp = None
        self.version = None
        self.dependencies = None
        self.database = None
        self.disk_space = None
        self.memory_usage = None
        self.cpu_usage = None
    
    def serialize_health_to_json(self):
        return {
            "healthy": self.healthy,
            "timestamp": self.timestamp,
            "version": self.version,
            "dependencies": self.dependencies,
            "database": self.database,
            "disk_space": self.disk_space,
            "memory_usage": self.memory_usage,
            "cpu_usage": self.cpu_usage
        }
    
    def get_disk_usage(self):
        drive_letter = 'C'  # Change this to the desired drive letter
        drive = psutil.disk_usage(drive_letter + ':')
        available_space_gb = drive.free / (1024 ** 3)
        available_space_mb = drive.free / (1024 ** 2)
        return f"{available_space_gb:.2f} GB ({available_space_mb:.2f} MB)"
    
    def get_memory_usage(self):
        memory_usage_bytes = psutil.Process(os.getpid()).memory_info().rss
        memory_usage_mb = memory_usage_bytes / (1024 ** 2)
        memory_usage_gb = memory_usage_bytes / (1024 ** 3)
        return f"{memory_usage_gb:.2f} GB ({memory_usage_mb:.2f} MB)"
    
    def get_cpu_usage(self):
        cpu_usage_percentage = psutil.cpu_percent(interval=1)
        return f"{cpu_usage_percentage:.2f}%"
    
    def is_database_reachable(self):
        folder = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(folder, "../../../../development.ini")

        config = configparser.ConfigParser()
        config.read(path)
        connection_string = config.get('app:main', 'sqlalchemy.maindb.url')
        
        try:
            engine = create_engine(connection_string)
            Session = sessionmaker(bind=engine)
            session = Session()
            session.execute("SELECT 1")  # Try executing a simple query
            session.close()
            return True
        except OperationalError:
            return False
    
    def GET(self):
        folder = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(folder, "../../../../../VERSION")

        self.healthy = True
        self.timestamp = datetime.datetime.utcnow().isoformat()
        self.dependencies = ["thesaurus"]

        try:
            with open(path, 'r') as version_file:
                self.version = version_file.read().strip()
            
            self.database = self.is_database_reachable()
            self.disk_space = self.get_disk_usage()
            self.memory_usage = self.get_memory_usage()
            self.cpu_usage = self.get_cpu_usage()

            if not self.database:
                self.healthy = False
        except Exception as e:
            self.healthy = None
        
        return self.serialize_health_to_json()
