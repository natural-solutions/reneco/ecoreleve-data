import parse
import uuid

from pyramid.httpexceptions import (
    HTTPConflict,
    HTTPNoContent
)

from ecoreleve_server.core.configuration_model.synonyms import Syn_TTopic
from ecoreleve_server.core.log_view import sendLog
from ecoreleve_server.new_api.meta import MetaResourceCollection

class Releases(MetaResourceCollection):

    def POST(self):
        data = self.__request__.json

        released = [
            (
                data.get("Release_Type_ID", None),
                data.get("Station_ID", None)
            )
        ]
        individual_released = []
        tmp_indiv_release = data.get("Individual_Released", [])
        for item in tmp_indiv_release:
            tmp_individual_released = (
                item.get("Individual_ID", None),
                item.get("Sensor_ID", None),
                item.get("Release_Comments", None),
                item.get("Individual_Comments", None),
            )
            individual_released.append(tmp_individual_released)

        # sqlserver have a limit of 2100 parameters
        # we used 2 parameters for the row of @Release
        # and 4 for each row of @Individual_Released
        # batch size is fixed to 500
        # We can't split the script each execute have his own scope
        # and we loose the variable declared before but
        # we find a workaround with temporary table
        # temporary table exist while the session/cursor is open
        # we can insert 500 rows by 500 rows
        # and build a script with a insert into select from temptable
        # no more limitations
        temporary_table_name = f'#release_{uuid.uuid4()}'
        stmt_temp = f'''
            SET NOCOUNT ON;
            CREATE TABLE [{temporary_table_name}] (
                Individual_ID INT,
                Sensor_ID INT,
                Release_Comments VARCHAR(255),
                Individual_Comments VARCHAR(255))
            '''
        batch_size = 500
        current_batch = []
        connection = self.__request__.dbsession.get_bind(Syn_TTopic).raw_connection()
        rows_returned_in_sp = []
        with connection.cursor() as crs:
            try:
                # Create temporary table
                crs.execute(stmt_temp)
                # Loop for inserting rows 500 by 500
                for i in range(0, len(individual_released), batch_size):
                    current_batch = individual_released[i:i+batch_size]
                    params_temp = []
                    stmt_temp = f'''
                        SET NOCOUNT ON;
                        INSERT INTO [{temporary_table_name}]
                        VALUES
                    '''
                    stmt_temp += ',\n'.join(['(?,?,?,?)']*len(current_batch))
                    for tuple in current_batch:
                        params_temp += [*tuple]
                    crs.execute(stmt_temp, params_temp)

                # create a single script
                params = []
                # remove message that show the number of rows
                stmt = '''
                    SET NOCOUNT ON;
                '''
                # Declare @Release and prepare params for insert
                stmt += '''
                    DECLARE @Release [Data_Type_Release]
                    INSERT INTO @Release (
                        [Release_Type_ID],
                        [Station_ID]
                    )
                    VALUES
                    '''

                stmt += ',\n'.join(['(?,?)']*len(released))
                for tuple in released:
                    params += [*tuple]
                # now we can retrieve all the individuals relased from the temp table
                # and insert into the variable @Individual_Released
                stmt += f'''
                    DECLARE @Individual_Released [Data_Type_Individual_Released]
                    INSERT INTO @Individual_Released
                    SELECT * FROM [{temporary_table_name}]
                    '''
                # add execute the stored procedure
                stmt += '''
                    EXEC [EXEC_Create_Release] @Release,@Individual_Released,?,?
                    '''

                debug = 0
                force_rollback = 0
                params += [debug, force_rollback]

                crs.execute(stmt, params)
                # Loop for consume any messages or select return by the sp
                while True:
                    if crs.description:
                        columns = [desc[0] for desc in crs.description]
                        rows_returned_in_sp.extend(
                            dict(zip(columns, row)) for row in crs
                        )

                    if not crs.nextset():
                        break
                crs.commit()
            except Exception as e:
                crs.rollback()
                sqlstate = e.args[0]
                if sqlstate in ['42000', '23000']:
                    formatted_error = '[{0}] [{1}][{2}][{3}]{4} ({5}) ({6})'
                    error_msg = e.args[1]
                    result = parse.parse(
                        formatted_error,
                        error_msg
                    )
                    error_returned = {
                        'error_type': 'SQL_ERROR',
                        'error_number': result[5],
                        'error_msg': result[4]
                    }
                else:
                    error_returned = {
                        'error_type': 'ERROR_UNDEFINED',
                        'error_number': 0,
                        'error_msg': (
                            "Something goes wrong... "
                            "Please contact an administrator."
                        )
                    }
                sendLog(
                    logLevel=1,
                    domaine=3,
                    request=self.__request__,
                )
                return HTTPConflict(
                    headers={
                        "content-type": 'application/json',
                        "charset": 'utf-8'
                    },
                    json_body=error_returned
                )

        return HTTPNoContent(
            headers={
                "content-type": 'application/json',
                "charset": 'utf-8'
            }
        )
