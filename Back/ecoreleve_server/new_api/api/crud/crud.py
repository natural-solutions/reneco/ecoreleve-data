from ecoreleve_server.new_api.meta import (
    MetaResourceCommon
)
from .release import (
    Releases
)


class Crud(MetaResourceCommon):

    def __getitem__(self, name):
        if name == 'release':
            return Releases(name, self)
        else:
            raise KeyError
