from ecoreleve_server.new_api.meta import (
    MetaResourceCommon
)
from .grids import (
    Grids
)


class Forms(MetaResourceCommon):

    def __getitem__(self, name):
        if name == 'grids':
            return Grids(name, self)
