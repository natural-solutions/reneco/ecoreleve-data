from ecoreleve_server.new_api.meta import (
    MetaResourceCommon
)
from ecoreleve_server.core.configuration_model.frontmodules import (
    FrontModules,
    ModuleGrids
)
import json


class ReleaseConf(MetaResourceCommon):

    def process_results_conf(self, result):

        def is_editable(grid_render_value):
            __BINARY_WEIGHT_GRIDRENDER_EDITABLE__ = 2
            toRet = False
            if (
                grid_render_value
                and
                grid_render_value > __BINARY_WEIGHT_GRIDRENDER_EDITABLE__
            ):
                toRet = True
            return toRet

        def is_hidden(grid_render_value):
            __BINARY_WEIGHT_GRIDRENDER_HIDDEN__ = 0
            toRet = True
            if (
                grid_render_value
                and
                grid_render_value > __BINARY_WEIGHT_GRIDRENDER_HIDDEN__
            ):
                toRet = False
            return toRet

        def get_filter(filter_type):
            __map_filter__ = {
                'Text': 'text',
                'Number': 'number',
                'DateTimePickerEditor': 'date'
            }

            return __map_filter__.get(filter_type, 'text')

        def json_loads_safe(data):
            try:
                return json.loads(data)
            except (ValueError, TypeError):
                return {}

        toRet = []

        for item in result:
            cell = getattr(item, 'CellType', None)
            editable = is_editable(getattr(item, 'GridRender', None))
            field = getattr(item, 'Name', None)
            filter = get_filter(getattr(item, 'FilterType', 'Text'))
            headerName = getattr(item, 'Label', None)
            hide = is_hidden(getattr(item, 'GridRender', None))
            grid_size = getattr(item, 'GridSize', '{}')
            grid_size_dict = json_loads_safe(data=grid_size)
            options = getattr(item, 'Options', '{}')
            options_dict = json_loads_safe(data=options)
            maxWidth = grid_size_dict.get('maxWidth', 0)
            minWidth = grid_size_dict.get('minWidth', 0)
            width = grid_size_dict.get('width', 0)

            toAdd = {
                'field': field,
                'headerName': headerName,
                'hide': hide,
                'editable': editable,
                'filter': filter,
                'cell': cell,
                'maxWidth': maxWidth,
                'minWidth': minWidth,
                'width': width,
                'options': options_dict
            }
            toRet.append(toAdd)

        return toRet

    def GET(self):
        colsToRet = [
            ModuleGrids.Name,
            ModuleGrids.Label,
            ModuleGrids.GridRender,
            ModuleGrids.GridSize,
            ModuleGrids.CellType,
            ModuleGrids.GridOrder,
            ModuleGrids.QueryName,
            ModuleGrids.Options,
            ModuleGrids.FilterType
        ]
        query = self.__request__.dbsession.query(ModuleGrids)
        query = query.join(
            FrontModules,
            FrontModules.ID == ModuleGrids.Module_ID
        )
        query = query.with_entities(*colsToRet)
        query = query.filter(
            FrontModules.Name == 'IndivReleaseGrid'
        )
        query = query.order_by(
            ModuleGrids.GridOrder
        )
        res = query.all()

        toRet = self.process_results_conf(result=res)
        return toRet
