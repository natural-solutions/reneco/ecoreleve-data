from ecoreleve_server.new_api.meta import (
    MetaResourceCommon
)
from .release import (
    ReleaseConf
)


class Grids(MetaResourceCommon):

    def __getitem__(self, name):
        if name == 'release':
            return ReleaseConf(name, self)
        raise KeyError
