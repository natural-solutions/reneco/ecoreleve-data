from asyncio import QueueEmpty
from geojson import Feature
from shapely import wkb
from pyramid.httpexceptions import (
    HTTPBadRequest
)
from sqlalchemy.orm import (
    Query
)
from ecoreleve_server.new_api.meta import (
    MetaResourceCommon
)
from ecoreleve_server.modules.regions import AdministrativeArea


class AdministrativeAreaItem(MetaResourceCommon):

    def build_query(self, type_):
        query = self.__parent__.build_query()
        query = query.filter(
            AdministrativeArea.ID == self.__name__
        )

    def GET(self):
        query = self.build_query()
        res = query.all()

        toRet = []
        for item in res:
            toRet.append(
                Feature(
                    id=getattr(item, 'ID'),
                    geometry=wkb.loads(getattr(item, 'valid_geom')),
                    properties={
                        'name': getattr(item, 'Name')
                    }
                )
            )
        response = {
            'geojson': toRet,
            'style': {
                'fillColor': '#d279d2',
                'color': '#ac39ac',
                'fillOpacity': 0.2,
                'weight': 2,
                'opacity': 0.7
            }
        }
        return response

class AdministrativeAreaCollection(MetaResourceCommon):

    def __getitem__(self, name):
        try:
            int(name)
        except Exception as e:
            raise KeyError
        return AdministrativeAreaItem(
                name=name,
                parent=self
            )

    def build_query(self):
        dbsession = self.__request__.dbsession
        query = Query(
            entities=[
                AdministrativeArea.ID,
                AdministrativeArea.Name,
                AdministrativeArea.valid_geom
                ],
            session=dbsession
        )

        return query

    def GET(self):
        params = self.__request__.params.mixed()

        if 'type' not in params:
            return HTTPBadRequest("missing type in query string")

        type_ = params.get('type','')
        query = self.build_query()
        query = query.filter(
            AdministrativeArea.type_ == type_
        )
        res = query.all()

        toRet = []
        for item in res:
            toRet.append(
                Feature(
                    id=getattr(item, 'ID'),
                    geometry=wkb.loads(getattr(item, 'valid_geom')),
                    properties={
                        'name': getattr(item, 'Name')
                    }
                )
            )

        response = {
            'geojson': toRet,
            'style': {
                'fillColor': '#d279d2',
                'color': '#ac39ac',
                'fillOpacity': 0.2,
                'weight': 2,
                'opacity': 0.7
            }
        }
        return response


class AdministrativeAreaTypes(MetaResourceCommon):

    def GET(self):
        # return ['Department','Municipality']
        # disable display of administrative area perf issues
        return []

class Carto(MetaResourceCommon):

    def __getitem__(self, name):
        if name == 'administrativearea':
            return AdministrativeAreaCollection(name, self)
        if name == 'getTypes':
            return AdministrativeAreaTypes(name, self)
        raise KeyError
