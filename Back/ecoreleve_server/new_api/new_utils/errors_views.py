from pyramid.httpexceptions import (
    HTTPMethodNotAllowed
)


def my_method_not_allowed_view(exception, request):
    """
    catch any MyNotImplementedError raised
    """
    print(
        'DEBUG HINT\n',
        'API called with request\n',
        f'METHOD : {exception.method}\n',
        f'URL : {exception.path_url}\n',
        f'QUERY STRING: {exception.query_string}\n',
        'this method is not yet implemented\n'
    )

    return HTTPMethodNotAllowed(
        headers={
            "content_type": 'application/json',
            "charset": 'utf-8',
        },
        body=f'{exception}'
        )
