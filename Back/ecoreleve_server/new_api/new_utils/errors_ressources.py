class My_Method_Not_Allowed_Error(Exception):
    def __init__(self, reqObj):
        self.method = getattr(reqObj, 'method')
        self.path_url = getattr(reqObj, 'path_url')
        self.query_string = getattr(reqObj, 'query_string')
        self.value = (
            f'VERB: {self.method}\n'
            f'URL: {self.path_url}\n'
            f'QUERY STRING: {self.query_string}\n'
        )

    def __str__(self):
        return self.value
