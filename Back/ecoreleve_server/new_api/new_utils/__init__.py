from .errors_ressources import (
    My_Method_Not_Allowed_Error
)
from .errors_views import (
    my_method_not_allowed_view
)


def includeme(config):
    config.add_view(
        my_method_not_allowed_view,
        context=My_Method_Not_Allowed_Error
    )
