import json
from collections import OrderedDict
from sqlalchemy import (
    select,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Numeric,
    String,
    Unicode,
    Sequence,
    orm,
    func,
    event,
    text,
    bindparam
)
from sqlalchemy.ext.hybrid import hybrid_property
from pyramid.view import view_config

from .. import Main_Db_Base

class BusinessRuleError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return self.value

    def __repr__(self):
        return self.value


@view_config(context=BusinessRuleError, renderer='json')
def businessError_view(exc, request):
    request.response.status_code = 409
    request.response.text = exc.value
    return request.response


class BusinessRules(Main_Db_Base):
    __tablename__ = 'BusinessRules'

    ID = Column(Integer, primary_key=True)
    name = Column(String(250))
    target = Column(String(250))
    targetType = Column(String(250))
    actionType = Column(String(250))
    executing = Column(String(250))
    params = Column(String(250))
    description = Column(String(250))
    errorValue = Column(String(250))

    __mapper_args__ = {
        'version_id_col': ID,
        'version_id_generator': False,
    }

    @hybrid_property
    def paramsJSON(self):
        return json.loads(self.params)

    @hybrid_property
    def targetTypes(self):
        if self.targetType:
            return json.loads(self.targetType)
        else:
            return []

    def raiseError(self):
        raise BusinessRuleError(self.errorValue)

    def buildQuery(self, entityDTO):
        ''' Build stored procedure statment, params:
                - entityDTO : dict()
            return text(query)'''
        paramsJSON = self.paramsJSON
        sqlParams = ' @result int; \n'
        declare_stmt = 'SET NOCOUNT ON;DECLARE ' + sqlParams
        params_stmt = ' :' + ', :'.join(paramsJSON) + ', @result OUTPUT; \n'
        bindparams = [bindparam(param, entityDTO.get(param, None))
                      for param in paramsJSON]
        # print('exec buisiness rule on {target}, type={type} executing: {executing} \n with data : {data}'.format(data=bindparams,
        #                                                                                                          executing=self.executing,
        #                                                                                                          target=self.target,
        #                                                                                                          type=self.targetType
        #                                                                                                          )
        #                                                                                                          )
        stmt = text(declare_stmt + ' EXEC ' + self.executing +
                    params_stmt + '\n SELECT @result;', bindparams=bindparams)
        return stmt

    def execute(self, entityDTO, connection):
        stmt = self.buildQuery(entityDTO)
        result = connection.execute(stmt).scalar()
        if result:
            self.raiseError()

        # with Main_Db_Base.metadata.bind.connect() as connection:
        #     stmt = self.buildQuery(entityDTO)
        #     result = connection.execute(stmt).scalar()
        #     if result:
        #         self.raiseError()
        # return result
