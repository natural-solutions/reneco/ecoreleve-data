from sqlalchemy import (
    Column,
    Integer,
    DateTime,
    Unicode,
    String,
    Boolean
    )
from sqlalchemy.dialects.mssql.base import BIT
from .. import Main_Db_Base


class Syn_TTopic(Main_Db_Base):
    __tablename__ = 'syn_TTopic'

    TTop_PK_ID = Column(Integer, primary_key=True)
    TTop_Name = Column(Unicode(200), nullable=False)
    TTop_Definition = Column(Unicode(900), nullable=True)
    TTop_Type = Column(Unicode(200), nullable=True)
    TTop_Date = Column(DateTime, nullable=True)
    TTop_FullPath = Column(Unicode(255), nullable=True)
    TTop_ParentID = Column(Integer, nullable=True)
    TTop_BranchOrder = Column(Integer, nullable=True)
    TTop_IsDeprecated = Column(BIT, nullable=False)
    TTop_OrderMode = Column(Integer, nullable=True)
    TTop_FK_TTop_TopConcept = Column(Integer, nullable=True)
    THis_TTop_FK_TTop_TopConcept = Column(Integer, nullable=True)
    THis_TTop_OrderMode = Column(Integer, nullable=True)
    __mapper_args__ = {
        'version_id_col': TTop_PK_ID,
        'version_id_generator': False,
    }

class Syn_TTopicLibelle(Main_Db_Base):
    __tablename__ = 'syn_TTopicLibelle'
    TLib_PK_ID = Column(Integer, primary_key=True)
    TLib_Name = Column(Unicode(200), nullable=False)
    TLib_FullPath = Column(Unicode(255), nullable=False)
    TLib_Definition = Column(Unicode(900), nullable=True)
    TLib_FK_TTop_ID = Column(Integer, nullable=False)
    TLib_FK_TLan_ID = Column(Unicode(2), nullable=False)
    __mapper_args__ = {
        'version_id_col': TLib_PK_ID,
        'version_id_generator': False,
    }
class Syn_TcameraTrap(Main_Db_Base):
    __tablename__ = 'syn_TcameraTrap'
    pk_id = Column(Integer, primary_key=True)
    fk_sensor = Column(Integer, nullable=False)
    path = Column(String(250), nullable=False)
    name = Column(String(250), nullable=False)
    extension = Column(String(250), nullable=False)
    checked = Column(Boolean, nullable=True)
    validated = Column(Integer, nullable=True)
    date_creation = Column(DateTime, nullable=True)
    date_uploaded = Column(DateTime)
    tags = Column(String, nullable=True)
    note = Column(Integer, nullable=False)
    stationId = Column(Integer, nullable=True)
    processed = Column(Integer, nullable=False)
    databaseTarget = Column(String(255), nullable=True)
    Original_Name = Column(String(255), nullable=True)
    Checksum = Column(String(255), nullable=True)
    FK_Import = Column(Integer)
    __mapper_args__ = {
        'version_id_col': pk_id,
        'version_id_generator': False,
    }