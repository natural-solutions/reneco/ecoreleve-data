from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData, engine_from_config
from sqlalchemy.orm import sessionmaker, scoped_session
from ecoreleve_server.modules import import_submodule
from pyramid.settings import asbool


def get_redis_con():
    try:
        import redis
        pool = redis.ConnectionPool(host='localhost', db=0)
        localRedis = redis.StrictRedis(connection_pool=pool)
    except:
        localRedis = None
    return localRedis


class myBase(object):
    __table_args__ = {'implicit_returning': False}


Main_Db_Base = declarative_base(cls=myBase)
Sensor_Db_Base = declarative_base(cls=myBase)
# BaseExport = declarative_base()
dbConfig = {}


# class Main_Db_Base(Base):
#     __abstract__ = True
#     metadata = MetaData()


# class Sensor_Db_Base(Sensor_Base):
#     __abstract__ = True
#     metadata = MetaData()


def load_db_config(global_config, settings):
    dbConfig['url'] = settings['sqlalchemy.maindb.url']
    dbConfig['wsThesaurus'] = {}
    dbConfig['wsThesaurus']['wsUrl'] = settings['wsThesaurus.wsUrl']
    dbConfig['wsThesaurus']['lng'] = settings['wsThesaurus.lng']
    dbConfig['data_schema'] = settings['data_schema']
    dbConfig['sensor_schema'] = settings['sensor_schema']
    dbConfig['dbLog.url'] = settings['dbLog.url']
    dbConfig['dbLog.schema'] = settings['dbLog.schema']
    if settings.get('prefixapi') is None:
        raise Exception('You mus\'t have this key (prefixapi) defined in your *.ini file')
    dbConfig['prefixapi'] = settings['prefixapi']
    dbConfig['is_referential_instance'] = asbool(
        global_config.get('is_referential_instance', False)
    )


# def initialize_session_export(settings):
#     engineExport = None
#     if 'loadExportDB' in settings and settings['loadExportDB'] == 'False':
#         print('''
#         /!\\================================/!\
#         WARNING :
#         Export DataBase NOT loaded, Export Functionality will not working
#         /!\\================================/!\\ \n''')
#     else:
#         engineExport = engine_from_config(
#             settings,
#             'sqlalchemy.Export.',
#             legacy_schema_aliasing=True
#         )
#         BaseExport.metadata.bind = engineExport
#         BaseExport.metadata.create_all(engineExport)
#         BaseExport.metadata.reflect(views=True, extend_existing=False)
#     return engineExport


def initialize_session(global_config, settings):
    load_db_config(global_config, settings)

    engine = engine_from_config(
        settings,
        'sqlalchemy.maindb.',
        legacy_schema_aliasing=False,
        implicit_returning=False,
        use_scope_identity=True
    )
    engine_sensordb = engine_from_config(
        settings,
        'sqlalchemy.sensordb.',
        legacy_schema_aliasing=False,
        implicit_returning=False,
        use_scope_identity=True
    )

    Main_Db_Base.metadata.bind = engine
    Sensor_Db_Base.metadata.bind = engine_sensordb
    factory = sessionmaker(
        binds={
            Main_Db_Base: engine,
            Sensor_Db_Base: engine_sensordb
        },
        autoflush=False
    )
    # factory.configure(
    #     binds={
    #         Main_Db_Base: engine,
    #         Sensor_Db_Base: engine_sensordb
    #     }
    # )
    dbConfig['dbSession'] = factory
    # dbConfig['dbSession'] = scoped_session(
    #     sessionmaker(
    #         bind=engine,
    #         autoflush=False
    #     )
    # )
    import_submodule()
    Main_Db_Base.metadata.create_all(engine)
    Main_Db_Base.metadata.reflect(views=True, extend_existing=False)

    return dbConfig['dbSession']
