"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""

from alembic import op
import sqlalchemy as sa
${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}


def upgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            WRITE YOUR SCRIPT!!!
            IF THIS SCRIPT IS NOT FOR REFERENTIAL
            REMOVE THE IF AND REINDENT OP.EXECUTE
            """
        )


def downgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            WRITE YOUR SCRIPT!!!
            IF THIS SCRIPT IS NOT FOR REFERENTIAL
            REMOVE THE IF AND REINDENT OP.EXECUTE
            """
        )
