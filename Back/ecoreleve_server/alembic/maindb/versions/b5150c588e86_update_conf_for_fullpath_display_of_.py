"""update conf for fullpath display of administrative area filter

Revision ID: b5150c588e86
Revises: aa6ea19ae1c7
Create Date: 2023-06-28 18:10:24.312885

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b5150c588e86'
down_revision = '127a88834c9a'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                UPDATE ModuleForms
                SET [Options] = '{"usedLabel":"fullpath","operator":"contains","triggerAt"\:3, "displayFullpath": true}'
                WHERE [module_id] = 2 AND [TypeObj] = 3 AND [Name] = 'FK_AdministrativeArea'
            """
        )


def downgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                UPDATE ModuleForms
                SET [Options] = '{"usedLabel":"fullpath","operator":"contains","triggerAt"\:3}'
                WHERE [module_id] = 2 AND [TypeObj] = 3 AND [Name] = 'FK_AdministrativeArea'
            """
        )
