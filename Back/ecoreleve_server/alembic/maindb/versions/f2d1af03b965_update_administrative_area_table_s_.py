"""update administrative area table's schema for differential process

Revision ID: f2d1af03b965
Revises: d34fbf05c5f3
Create Date: 2024-03-12 15:03:20.469614

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2d1af03b965'
down_revision = 'd34fbf05c5f3'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        -- Add unique constraint on Name (adm_code)
        ALTER TABLE AdministrativeArea
        ADD CONSTRAINT UQ_AdministrativeArea_Name UNIQUE (Name);

        -- Delete column data_modif and add the four other columns
        ALTER TABLE AdministrativeArea
        DROP COLUMN [date_modif];

        ALTER TABLE AdministrativeArea
        ADD [activation_date] [date] NULL,
            [inactivation_date] [date] NULL,
            [last_change_geometry_date] [date] NULL,
            [last_change_attribut_date] [date] NULL;
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        -- Add unique constraint on Name (adm_code)
        ALTER TABLE AdministrativeArea
        DROP CONSTRAINT UQ_AdministrativeArea_Name;

        -- Delete column data_modif and add the four other columns
        ALTER TABLE AdministrativeArea
        DROP COLUMN [activation_date], [inactivation_date], [last_change_geometry_date], [last_change_attribut_date];

        ALTER TABLE AdministrativeArea
        ADD [date_modif] [date] NULL;
        """
    )
