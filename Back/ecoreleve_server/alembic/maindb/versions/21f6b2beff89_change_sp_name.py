"""change sp name

Revision ID: 21f6b2beff89
Revises: e61fa2610f41
Create Date: 2023-11-15 11:17:39.680930

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '21f6b2beff89'
down_revision = 'e61fa2610f41'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
            UPDATE [BusinessRules]
            SET
            [name] = 'EXEC_DEL_Remove_Station_References_Before_Delete',
            [executing] = '[dbo].[EXEC_DEL_Remove_Station_References_Before_Delete]'
            WHERE
            [name] = 'Remove_FK_Station_References_Before_Delete'
            AND
            [executing] = '[dbo].[Remove_FK_Station_References_Before_Delete]'
        """
    )
    op.execute(
        """
            EXEC sp_rename 'Remove_FK_Station_References_Before_Delete', 'EXEC_DEL_Remove_Station_References_Before_Delete';
        """
    )


def downgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    op.execute(
        """
            EXEC sp_rename 'EXEC_DEL_Remove_Station_References_Before_Delete', 'Remove_FK_Station_References_Before_Delete';
        """
    )
    op.execute(
        """
            UPDATE [BusinessRules]
            SET
            [name] = 'Remove_FK_Station_References_Before_Delete',
            [executing] = '[dbo].[Remove_FK_Station_References_Before_Delete]'
            WHERE
            [name] = 'EXEC_DEL_Remove_Station_References_Before_Delete'
            AND
            [executing] = '[dbo].[EXEC_DEL_Remove_Station_References_Before_Delete]'
        """
    )
