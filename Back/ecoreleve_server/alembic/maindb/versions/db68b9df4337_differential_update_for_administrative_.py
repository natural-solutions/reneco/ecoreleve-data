"""differential update for administrative areas

Revision ID: db68b9df4337
Revises: f2d1af03b965
Create Date: 2024-03-20 18:01:56.310047

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'db68b9df4337'
down_revision = 'f2d1af03b965'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        CREATE PROCEDURE [dbo].[sp_AA_Update]
        -- ===================================================
        -- 
        -- ===================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN
                DECLARE	@return_value INT

                -- Run procedures
                EXEC @return_value = [dbo].[sp_AA_LayerDifference] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_AA_LayerDifference' = @return_value

                EXEC @return_value = [dbo].[sp_AA_TableFormatting] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_AA_TableFormatting' = @return_value

                EXEC @return_value = [dbo].[sp_AA_Compute] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_AA_Compute' = @return_value

                -- delete reneco administrative area table
                DROP TABLE reneco_administrativearea

                -- delete gdal table
                DROP TABLE spatial_ref_sys
                DROP TABLE geometry_columns

                -- delete To_Check table
                DROP TABLE To_Check

                -- manage debug/rollback mode
                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                    END
                    ;THROW
            END CATCH
        END
        """
    )
    op.execute(
        """
        CREATE PROCEDURE [dbo].[sp_AA_LayerDifference]
        -- ====================================================
        -- Identification of the FK_AdministrativeArea to be updated
        --
        -- The FK_AdministrativeArea will be set to NULL for all
        -- locations that need to be re-computed
        -- ====================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN

                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                END

                -- Declare all variables
                DECLARE @adm_code VARCHAR(500),
                    @adm_type VARCHAR(500),
                    @ctry_name VARCHAR(500),
                    @depa_name VARCHAR(500),
                    @muni_name VARCHAR(500),
                    @CodeToReset VARCHAR(500)

                -- Create temporary AA_update table: contain the fielwork area that
                -- have been activated or inactivated or with modified geom
                IF OBJECT_ID('tempdb..#AA_update') IS NOT NULL
                BEGIN 
                    DROP TABLE #AA_update
                END
                CREATE TABLE #AA_update (
                    [adm_code] VARCHAR(500),
                    [adm_type] VARCHAR(500),
                    [ctry_name] VARCHAR(500),
                    [depa_name] VARCHAR(500),
                    [muni_name] VARCHAR(500)
                )

                -- ToCheck Table: store the ID of the Fielwork areas that may be
                -- updated at the studied points (individual locations, stations,
                -- monitored sites)
                IF OBJECT_ID('To_Check') IS NOT NULL
                BEGIN
                    DROP TABLE To_Check
                END
                CREATE TABLE To_Check (
                    [IdToCheck] INT NOT NULL
                )

                -- Table des AA associées à un recalcul
                -- (seul les changement d'attribut ne conduisent pas à un recalcul)
                INSERT INTO #AA_update (
                    [adm_code],
                    [adm_type],
                    [ctry_name],
                    [depa_name],
                    [muni_name]
                )
                SELECT
                    UAA.[adm_code],
                    UAA.[adm_type],
                    UAA.[ctry_name],
                    UAA.[depa_name],
                    UAA.[muni_name]
                FROM reneco_administrativearea AS UAA
                LEFT JOIN AdministrativeArea AS AA ON UAA.[adm_code] = AA.[Name] 
                WHERE (AA.[inactivation_date] IS NULL AND UAA.[inacti_d] IS NOT NULL) 
                    OR (AA.[activation_date] IS NULL AND UAA.[acti_d] IS NOT NULL)
                    OR (AA.[last_change_geometry_date] IS NULL AND UAA.[geom_d] IS NOT NULL)
                    OR (AA.[last_change_geometry_date] < UAA.[geom_d])

                -- Identify Id corresponding to adm_code that need to be re-computed (stored in ToCheck)
                INSERT INTO To_Check ([IdToCheck]) 
                VALUES (0) -- by default, location with a FK_AdministrativeArea equal to 0 (meaning outside current AA) needs to be re-computed

                -- Iteration on the temporary table AA_update
                -- For each updated administrative area, position associated with the
                -- superior AA need to be re-computed
                -- If the AA is a country, no previous layer, else CodeToReset 
                -- is set as equal to the code of the containing layer
                -- (such as: municipality ⊂ department ⊂ country)
                WHILE (SELECT COUNT(*) From #AA_update) > 0
                BEGIN
                    SET @CodeToReset = NULL
                    
                    SELECT TOP 1
                        @adm_code = [adm_code],
                        @adm_type = [adm_type],
                        @ctry_name = [ctry_name],
                        @depa_name = [depa_name],
                        @muni_name = [muni_name]
                    FROM #AA_update
                                    
                    INSERT INTO To_Check (IdToCheck) 
                    SELECT ID
                    FROM AdministrativeArea
                    WHERE Name = @adm_code

                    IF @adm_type = 'department'
                    BEGIN	
                        SET @CodeToReset = (
                            SELECT [Name]
                            FROM AdministrativeArea
                            WHERE [type_] = 'country'
                                AND [Country] = @ctry_name
                        )
                    END
                    ELSE 
                    BEGIN
                        IF @adm_type = 'municipality'
                        BEGIN
                            SET @CodeToReset = (
                                SELECT [Name]
                                FROM AdministrativeArea
                                WHERE [type_] = 'department' 
                                    AND [Department] = @depa_name 
                            )
                            IF @CodeToReset IS NULL 
                            BEGIN
                                SET @CodeToReset = (
                                    SELECT [Name]
                                    FROM AdministrativeArea
                                    WHERE [type_] = 'country' 
                                        AND [Country] = @ctry_name 
                                )
                            END
                        END
                    END

                    IF @CodeToReset IS NOT NULL
                    BEGIN
                        INSERT INTO To_Check (IdToCheck)
                        SELECT ID
                        FROM AdministrativeArea
                        WHERE Name = @CodeToReset
                    END
                                    
                    -- Remove the processed administrative area
                    DELETE FROM #AA_update
                    WHERE [adm_code] =  @adm_code
                END

                IF @Debug = 1
                BEGIN
                    SELECT 'AA_update: should be empty'
                    SELECT *
                    FROM #AA_update

                    SELECT 'To_Check: all ID leading to set to NULL'
                    SELECT *
                    FROM To_Check
                END

                -- Set to NULL all locations for which the FA need to be re-compute
                PRINT('Individual locations - Set to NULL')
                UPDATE Individual_Location
                SET [FK_AdministrativeArea] = NULL
                WHERE [FK_AdministrativeArea] IN (
                    SELECT DISTINCT [IdToCheck] 
                    FROM To_Check
                )
                            
                PRINT('Station - Set to NULL')
                UPDATE Station
                SET [FK_AdministrativeArea] = NULL
                WHERE [FK_AdministrativeArea] IN (
                    SELECT DISTINCT [IdToCheck] FROM To_Check
                )
                
                PRINT('MonitoredSitePosition - Set to NULL')
                UPDATE MonitoredSitePosition
                SET [FK_AdministrativeArea] = NULL
                WHERE [FK_AdministrativeArea] IN (
                    SELECT DISTINCT [IdToCheck] 
                    FROM To_Check
                )

                IF OBJECT_ID('tempdb..#AA_update') IS NOT NULL
                BEGIN 
                    DROP TABLE #AA_update
                END
                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                PRINT('Error during administrative areas update - Layer Difference')
                PRINT(ERROR_MESSAGE())

                IF @@TRANCOUNT > 0
                BEGIN
                    PRINT 'Rollback'
                    ROLLBACK TRAN
                END
                ;THROW
            END CATCH
        END
        """
    )
    op.execute(
        """
        CREATE PROCEDURE [dbo].[sp_AA_TableFormatting]
        -- ===================================================
        -- Formatting new administrative area table
        --
        -- /!\ Il faut gérer les ID :
        -- si adm_code est déjà présent alors on reprend l'ID
        -- si adm_code n'est pas présent (i.e., nouvelle AA)
        --   alors il faut ordonner les lignes sans insérer
        --   l'ID et vérifier qu'il prend bien la suite de 
        --   l'incrémentation
        -- ===================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN

                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                END

                IF @Debug = 1
                BEGIN
                    SELECT *
                    FROM AdministrativeArea AA
                    JOIN reneco_administrativearea R ON R.[adm_code] = AA.[Name]
                END

                -- Delete lines corresponding to inactivated Admin Areas
                DELETE FROM AdministrativeArea
                WHERE [Name] IN (
                    SELECT [adm_code]
                    FROM reneco_administrativearea
                    WHERE inacti_d IS NOT NULL
                )

                -- Update Admin areas that have been modified (JOIN)
                IF OBJECT_ID('tempdb..#updated_AA') IS NOT NULL
                BEGIN 
                    DROP TABLE #updated_AA
                END
                CREATE TABLE #updated_AA (
                    [Country] NVARCHAR(255),
                    [Department] VARCHAR(255),
                    [Municipality] NVARCHAR(255),
                    [Name] VARCHAR(255),
                    [fullpath] VARCHAR(255),
                    [type_] VARCHAR(50),
                    [max_lat] DECIMAL(9,5),
                    [min_lat] DECIMAL(9,5),
                    [max_lon] DECIMAL(9,5),
                    [min_lon] DECIMAL(9,5),
                    [SHAPE_Leng] REAL,
                    [SHAPE_Area] REAL,
                    [valid_geom] GEOMETRY,
                    [geom] GEOMETRY,
                    [Centroid_Latitude] DECIMAL(9,5),
                    [Centroid_Longitude] DECIMAL(9,5),
                    [Center] VARCHAR(255),
                    [Data_Owner] VARCHAR(255),
                    [activation_date] DATE,
                    [inactivation_date] DATE,
                    [last_change_geometry_date] DATE,
                    [last_change_attribut_date] DATE
                )

                INSERT INTO #updated_AA (
                    [Country],
                    [Department],
                    [Municipality],
                    [Name],
                    [fullpath],
                    [type_],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [SHAPE_Leng],
                    [SHAPE_Area],
                    [valid_geom],
                    [geom],
                    [Centroid_Latitude],
                    [Centroid_Longitude],
                    [Center],
                    [Data_Owner],
                    [activation_date],
                    [inactivation_date],
                    [last_change_geometry_date],
                    [last_change_attribut_date]
                )
                SELECT 
                    [ctry_name],
                    [depa_name],
                    [muni_name],
                    FirstPart.[adm_code],
                    [fullpath],
                    [adm_type],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [Shape_Leng],
                    [Shape_Area],
                    [valid_geom],
                    [geom],
                    [centro_lat],
                    [centro_lon],
                    [center],
                    [data_owner],
                    [acti_d],
                    [inacti_d],
                    [attr_d],
                    [geom_d]
                FROM (
                    SELECT
                        [ctry_name],
                        [depa_name],
                        [muni_name],
                        [adm_code],
                        CASE 
                            WHEN [adm_type] = 'municipality' AND [ctry_name] IS NOT NULL AND [depa_name] IS NOT NULL AND [muni_name] IS NOT NULL
                                THEN CONCAT([ctry_name],'>',[depa_name],'>',[muni_name])
                            WHEN [adm_type] = 'department' AND [ctry_name] IS NOT NULL AND [depa_name] IS NOT NULL
                                THEN CONCAT([ctry_name],'>',[depa_name])
                            WHEN [adm_type] = 'country' AND [ctry_name] IS NOT NULL
                                THEN [ctry_name]
                            ELSE NULL
                        END AS [fullpath],
                        [adm_type],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                        [Shape_Leng],
                        [Shape_Area],
                        [centro_lat],
                        [centro_lon],
                        [center],
                        [data_owner],
                        [acti_d],
                        [inacti_d],
                        [attr_d],
                        [geom_d]
                    FROM reneco_administrativearea
                    GROUP BY [ctry_name], [depa_name], [muni_name], [adm_code], [adm_type], [acti_d], [inacti_d], [attr_d], [geom_d], [center], [data_owner], [centro_lat], [centro_lon], [shape_leng], [shape_area]
                ) AS FirstPart
                JOIN (
                    SELECT 
                        [adm_code],
                        [ogr_geometry] AS [geom],
                        [ogr_geometry].MakeValid() AS [valid_geom]
                    FROM reneco_administrativearea RA
                    JOIN AdministrativeArea AA ON AA.[Name] = RA.[adm_code]
                    WHERE [inacti_d] IS NULL
                ) AS SecondPart
                ON [FirstPart].[adm_code] = [SecondPart].[adm_code]

                UPDATE AdministrativeArea
                SET 
                    [Country] = UAA.[Country],
                    [Department] = UAA.[Department],
                    [Municipality] = UAA.[Municipality],
                    [Name] = UAA.[Name],
                    [fullpath] = UAA.[fullpath],
                    [type_] = UAA.[type_],
                    [max_lat] = UAA.[max_lat],
                    [min_lat] = UAA.[min_lat],
                    [max_lon] = UAA.[max_lon],
                    [min_lon] = UAA.[min_lon],
                    [SHAPE_Leng] = UAA.[Shape_Leng],
                    [SHAPE_Area] = UAA.[Shape_Area],
                    [valid_geom] = UAA.[valid_geom],
                    [geom] = UAA.[geom],
                    [Centroid_Latitude] = UAA.[Centroid_Latitude],
                    [Centroid_Longitude] = UAA.[Centroid_Longitude],
                    [Center] = UAA.[center],
                    [Data_Owner] = UAA.[data_owner],
                    [activation_date] = UAA.[activation_date],
                    [inactivation_date] = UAA.[inactivation_date],
                    [last_change_attribut_date] = UAA.[last_change_attribut_date],
                    [last_change_geometry_date] = UAA.[last_change_geometry_date]
                FROM #updated_AA AS UAA
                WHERE AdministrativeArea.[Name] = UAA.[Name]

                IF OBJECT_ID('tempdb..#updated_AA') IS NOT NULL
                BEGIN 
                    DROP TABLE #updated_AA
                END

                -- Insert new Admin areas with caution to the ID
                DECLARE @IdMax INT = (SELECT MAX(ID) FROM AdministrativeArea)
                DBCC CHECKIDENT (AdministrativeArea, RESEED, @IdMax)

                INSERT INTO AdministrativeArea (
                    [Country],
                    [Department],
                    [Municipality],
                    [Name],
                    [fullpath],
                    [type_],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [SHAPE_Leng],
                    [SHAPE_Area],
                    [valid_geom],
                    [geom],
                    [Centroid_Latitude],
                    [Centroid_Longitude],
                    [Center],
                    [Data_Owner],
                    [activation_date],
                    [inactivation_date],
                    [last_change_attribut_date],
                    [last_change_geometry_date]
                )
                SELECT 
                    [ctry_name],
                    [depa_name],
                    [muni_name],
                    FirstPart.[adm_code],
                    [fullpath],
                    [adm_type],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [Shape_Leng],
                    [Shape_Area],
                    [valid_geom],
                    [geom],
                    [centro_lat],
                    [centro_lon],
                    [center],
                    [data_owner],
                    [acti_d],
                    [inacti_d],
                    [attr_d],
                    [geom_d]
                FROM (
                    SELECT
                        [ctry_name],
                        [depa_name],
                        [muni_name],
                        [adm_code],
                        CASE 
                            WHEN [adm_type] = 'municipality' AND [ctry_name] IS NOT NULL AND [depa_name] IS NOT NULL AND [muni_name] IS NOT NULL
                                THEN CONCAT([ctry_name],'>',[depa_name],'>',[muni_name])
                            WHEN [adm_type] = 'department' AND [ctry_name] IS NOT NULL AND [depa_name] IS NOT NULL
                                THEN CONCAT([ctry_name],'>',[depa_name])
                            WHEN [adm_type] = 'country' AND [ctry_name] IS NOT NULL
                                THEN [ctry_name]
                            ELSE NULL
                        END AS [fullpath],
                        [adm_type],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                        [Shape_Leng],
                        [Shape_Area],
                        [centro_lat],
                        [centro_lon],
                        [center],
                        [data_owner],
                        [acti_d],
                        [inacti_d],
                        [attr_d],
                        [geom_d]
                    FROM reneco_administrativearea
                    GROUP BY [ctry_name], [depa_name], [muni_name], [adm_code], [adm_type], [acti_d], [inacti_d], [attr_d], [geom_d], [center], [data_owner], [centro_lat], [centro_lon], [shape_leng], [shape_area]
                ) AS FirstPart
                JOIN (
                    SELECT 
                        [adm_code],
                        [ogr_geometry] AS [geom],
                        [ogr_geometry].MakeValid() AS [valid_geom]
                    FROM reneco_administrativearea RA
                    LEFT JOIN AdministrativeArea AA ON AA.[Name] = RA.[adm_code]
                    WHERE AA.[Name] IS NULL
                        AND RA.[inacti_d] IS NULL
                ) AS SecondPart
                ON [FirstPart].[adm_code] = [SecondPart].[adm_code]
                ORDER BY [ctry_name] ASC, [depa_name] ASC, [muni_name] ASC, [FirstPart].[adm_code] ASC

                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                PRINT('Error during administrative areas update - Table Formatting')
                PRINT(ERROR_MESSAGE())

                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN
                END
                ;THROW
            END CATCH
        END
        """
    )
    op.execute(
        """
        CREATE PROCEDURE [dbo].[sp_AA_Compute]
        -- =====================================================
        -- Compute the FK_AdministrativeArea associated to
        -- each spatial point (individual location, station and
        -- monitored site position) which must be updated (NULL)
        -- =====================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            -- For each individual location, all the areas including it are found.
            -- Only the more precise (based on fullpath) is kept
            BEGIN TRY
            BEGIN TRAN

                -- Declare variable
                DECLARE @flag INT = 1
                WHILE @flag > 0
                BEGIN
                                
                    -- Create temporary table temptable to store the new administrative area
                    -- associated to a spatial point
                    IF OBJECT_ID('tempdb..#temptableindivlocation') IS NOT NULL
                    BEGIN
                        DROP TABLE #temptableindivlocation
                    END

                    SELECT TOP(50000) 
                        IL.[ID] AS [ID],
                        ISNULL(
                            (SELECT TOP 1 AA.[ID]
                            FROM Administrativearea AS AA
                            WITH(INDEX(IX_Administrativearea_valid_geom))
                            WHERE  
                                [min_lat] <= IL.[LAT] 
                                AND [max_lat] >= IL.[LAT] 
                                AND [min_lon] <= IL.[LON] 
                                AND [max_lon] >= IL.[LON]  
                                AND AA.[valid_geom].STIntersects(geometry::Point(IL.[LON], IL.[LAT], 4326)) = 1
                            ORDER BY AA.[fullpath] DESC), 
                            0
                        ) AS [FK_Administrativearea]
                    INTO #temptableindivlocation
                    FROM [Individual_Location] AS IL
                    WHERE 	
                        IL.[FK_AdministrativeArea] IS NULL
                        AND IL.[LAT] IS NOT NULL
                        AND IL.[LON] IS NOT NULL

                    UPDATE IL
                    SET IL.[FK_AdministrativeArea] = TT.[FK_AdministrativeArea]
                    FROM [Individual_Location] AS IL
                    JOIN #temptableindivlocation AS TT ON IL.[ID] = TT.[ID]

                    SELECT @flag = @@ROWCOUNT
                END

                -- For each station, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                SELECT @flag=1;
                WHILE @flag > 0
                BEGIN
                    IF OBJECT_ID('tempdb..#temptablestation') IS NOT NULL
                    BEGIN
                        DROP TABLE #temptablestation
                    END

                    SELECT TOP(50000) 
                        S.[ID] AS [ID],
                        ISNULL(
                            (SELECT TOP 1 
                                AA.[ID]
                            FROM Administrativearea AS AA
                            WITH(INDEX(IX_Administrativearea_valid_geom))
                            WHERE  [min_lat] <= S.[LAT] 
                                AND [max_lat] >= S.[LAT] 
                                AND [min_lon] <= S.[LON] 
                                AND [max_lon] >= S.[LON]  
                                AND AA.[valid_geom].STIntersects(geometry::Point(S.[LON], S.[LAT], 4326)) = 1
                            ORDER BY AA.[fullpath] DESC), 
                            0
                        ) AS [FK_Administrativearea]
                    INTO #temptablestation
                    FROM [Station] AS S
                    WHERE 	
                        S.[FK_AdministrativeArea] IS NULL
                        AND S.LAT IS NOT NULL
                        AND S.LON IS NOT NULL

                    UPDATE S
                    SET S.[FK_AdministrativeArea] = TT.[FK_AdministrativeArea]
                    FROM [Station] AS S
                    JOIN #temptablestation AS TT ON S.[ID] = TT.[ID]

                    SELECT @flag = @@ROWCOUNT
                END

                -- For each MonitoredSitePosition
                SELECT @flag=1;
                WHILE @flag > 0
                BEGIN
                    IF OBJECT_ID('tempdb..#temptableMonitoredSitePosition') IS NOT NULL
                    BEGIN
                        DROP TABLE #temptableMonitoredSitePosition
                    END

                    SELECT TOP(50000) 
                        MSP.[ID] AS [ID],
                        ISNULL(
                            (SELECT TOP 1 
                                AA.[ID]
                            FROM Administrativearea AS AA
                            WITH(INDEX(IX_Administrativearea_valid_geom))
                            WHERE  [min_lat] <= MSP.[LAT] 
                                AND [max_lat] >= MSP.[LAT] 
                                AND [min_lon] <= MSP.[LON] 
                                AND [max_lon] >= MSP.[LON]  
                                AND AA.[valid_geom].STIntersects(geometry::Point(MSP.[LON], MSP.[LAT], 4326)) = 1
                            ORDER BY AA.[fullpath] DESC), 
                            0
                        ) AS [FK_Administrativearea]
                    INTO #temptableMonitoredSitePosition
                    FROM [MonitoredSitePosition] AS MSP
                    WHERE
                        MSP.[FK_AdministrativeArea] IS NULL
                        AND MSP.LAT IS NOT NULL
                        AND MSP.LON IS NOT NULL

                    UPDATE MSP
                    SET MSP.[FK_AdministrativeArea] = TT.[FK_AdministrativeArea]
                    FROM [MonitoredSitePosition] AS MSP
                    JOIN #temptableMonitoredSitePosition AS TT ON MSP.[ID] = TT.[ID]

                    SELECT @flag = @@ROWCOUNT
                END

                -- clean
                IF OBJECT_ID('tempdb..#temptableindivlocation') IS NOT NULL
                BEGIN
                    DROP TABLE #temptableindivlocation
                END

                IF OBJECT_ID('tempdb..#temptablestation') IS NOT NULL
                BEGIN
                    DROP TABLE #temptablestation
                END

                IF OBJECT_ID('tempdb..#temptableMonitoredSitePosition') IS NOT NULL
                BEGIN
                    DROP TABLE #temptableMonitoredSitePosition
                END
                            
                --
                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                PRINT('Error during administrative areas update - Compute')
                PRINT(ERROR_MESSAGE())

                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN
                END
                ;THROW
            END CATCH
        END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        DROP PROCEDURE IF EXISTS dbo.sp_AA_Update;
        DROP PROCEDURE IF EXISTS dbo.sp_AA_LayerDifference;  
        DROP PROCEDURE IF EXISTS dbo.sp_AA_TableFormatting;  
        DROP PROCEDURE IF EXISTS dbo.sp_AA_Compute;  
        """
    )
