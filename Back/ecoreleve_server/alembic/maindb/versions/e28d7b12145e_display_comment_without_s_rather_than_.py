"""Display 'Comment' without 's' rather than 'Comment' in object forms

Revision ID: e28d7b12145e
Revises: 6e38083c9c4f
Create Date: 2022-11-15 14:37:21.043527

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e28d7b12145e'
down_revision = '6e38083c9c4f'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    
    op.execute(
        """
        -- STATION
        DECLARE @StationFormID INT = NULL

        SET @StationFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'StationForm'
        )

        UPDATE ModuleForms
        SET Label = 'Comment'
        WHERE 
            module_id = @StationFormID
            AND
            Name = 'Comments'

        -- INDIVIDUAL
        DECLARE @IndividualFormID INT = NULL

        SET @IndividualFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'IndivForm'
        )

        UPDATE ModuleForms
        SET Legend = 'Comment'
        WHERE 
            module_id = @IndividualFormID
            AND
            Legend = 'Comments'

        -- SENSOR 
        DECLARE @SensorFormID INT = NULL

        SET @SensorFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'SensorForm'
        )

        UPDATE ModuleForms
        SET Label = 'Comment'
        WHERE 
            module_id = @SensorFormID
            AND
            Name = 'Comments'

        -- MONITORED SITE
        DECLARE @MonitoredSiteFormID INT = NULL

        SET @MonitoredSiteFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'MonitoredSiteForm'
        )

        UPDATE ModuleForms
        SET Label = 'Comment'
        WHERE 
            module_id = @MonitoredSiteFormID
            AND
            Name = 'Comments'

        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        -- STATION 
        DECLARE @StationFormID INT = NULL

        SET @StationFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'StationForm'
        )

        UPDATE ModuleForms
        SET Label = 'Comments'
        WHERE 
            module_id = @StationFormID
            AND
            Name = 'Comments'

        -- INDIVIDUAL
        DECLARE @IndividualFormID INT = NULL

        SET @IndividualFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'IndivForm'
        )

        UPDATE ModuleForms
        SET Legend = 'Comments'
        WHERE 
            module_id = @IndividualFormID
            AND
            Legend = 'Comment'

        -- SENSOR 
        DECLARE @SensorFormID INT = NULL

        SET @SensorFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'SensorForm'
        )

        UPDATE ModuleForms
        SET Label = 'Comments'
        WHERE 
            module_id = @SensorFormID
            AND
            Name = 'Comments'

        -- MONITORED SITE
        DECLARE @MonitoredSiteFormID INT = NULL

        SET @MonitoredSiteFormID = (
            SELECT ID
            FROM FrontModules
            WHERE Name = 'MonitoredSiteForm'
        )

        UPDATE ModuleForms
        SET Label = 'Comments'
        WHERE 
            module_id = @MonitoredSiteFormID
            AND
            Name = 'Comments'

        """
    )
