"""add 'Nest and Brood' monitored site type

Revision ID: d8ec1b889ef2
Revises: e28d7b12145e
Create Date: 2022-11-08 15:01:00.914238

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd8ec1b889ef2'
down_revision = 'e28d7b12145e'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            -- Gestion du nouveau type
            INSERT INTO [MonitoredSiteType] (Name, Status)
            VALUES ('Nest and Brood', 4)

            -- Ajout du taxon (propriété dynamique)
            INSERT INTO [MonitoredSiteDynProp] (Name, TypeProp)
            VALUES ('Taxon', 'String')

            -- Ajout des correspondances entre type et propriété dynamique
            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM [MonitoredSiteType]
                WHERE Name LIKE 'Nest and Brood'
            )

            DECLARE @NestAndBroodProp INT = (
                SELECT [ID]
                FROM [MonitoredSiteDynProp]
                WHERE Name LIKE 'Taxon'
            )

            INSERT INTO [MonitoredSiteType_MonitoredSiteDynProp] (Required, FK_MonitoredSiteType, FK_MonitoredSiteDynProp)
            VALUES (1, @NestAndBroodType, @NestAndBroodProp)

            """
        )

    op.execute(
        """
        -- Ajout des parents avec contraintes de clés étrangères (propriétés fixes)
        ALTER TABLE [MonitoredSite]
        ADD
            FK_Individual_1 INTEGER
            CONSTRAINT [FK_MonitoredSite_FK_Individual_1_Individual] FOREIGN KEY (FK_Individual_1)
            REFERENCES [Individual] (ID)

        ALTER TABLE [MonitoredSite]
        ADD
            FK_Individual_2 INTEGER
            CONSTRAINT [FK_MonitoredSite_FK_Individual_2_Individual] FOREIGN KEY (FK_Individual_2)
            REFERENCES [Individual] (ID)
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        DECLARE @NestAndBroodType INT = (
            SELECT [ID]
            FROM [MonitoredSiteType]
            WHERE Name LIKE 'Nest and Brood'
        )
                                                                    
        DECLARE @NestAndBroodProp INT = (
            SELECT [ID]
            FROM [MonitoredSiteDynProp]
            WHERE Name LIKE 'Taxon'
        )
                                                                    
        DELETE FROM [MonitoredSiteType_MonitoredSiteDynProp]
        WHERE 
            FK_MonitoredSiteType = @NestAndBroodType
            AND FK_MonitoredSiteDynProp = @NestAndBroodProp

        DELETE FROM [MonitoredSiteType]
        WHERE Name LIKE 'Nest and Brood'

        DELETE FROM [MonitoredSiteDynProp]
        WHERE Name LIKE 'Taxon'

        DELETE FROM [MonitoredSiteDynPropValue]
        WHERE [FK_MonitoredSiteDynProp] = @NestAndBroodProp

        DELETE FROM [MonitoredSite] 
        WHERE [FK_MonitoredSiteType] = @NestAndBroodType

        ALTER TABLE [MonitoredSite] DROP CONSTRAINT [FK_MonitoredSite_FK_Individual_1_Individual]
        ALTER TABLE [MonitoredSite] DROP CONSTRAINT [FK_MonitoredSite_FK_Individual_2_Individual]

        ALTER TABLE [MonitoredSite] DROP COLUMN FK_Individual_1, FK_Individual_2;
        """
    )
