"""add start and end activity fields for sensors

Revision ID: 0e9a0f612125
Revises: 6cb2c71dd233
Create Date: 2024-10-22 16:20:31.339570

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0e9a0f612125'
down_revision = '6cb2c71dd233'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        DECLARE @module_id INT = (
            SELECT [ID]
            FROM FrontModules
            WHERE [Name] = 'SensorForm'
        )
        -- add link between sensor type and dynamic property frequency
        DECLARE @SensorDynProp_Frequency INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
        )

        CREATE TABLE #TempTable1 ([ID] INT)
        INSERT INTO #TempTable1 ([ID])
        SELECT [ID]
        FROM SensorType
        WHERE [Name] IN ('Satellite', 'GSM')

        INSERT INTO SensorType_SensorDynProp ([Required],[FK_SensorType],[FK_SensorDynProp]) (
            SELECT 0, *, @SensorDynProp_Frequency
            FROM #TempTable1
        )

        -- add new dynamic property for sensors
        SET IDENTITY_INSERT [SensorDynProp] ON
        INSERT INTO SensorDynProp ([ID], [Name], [TypeProp])
        VALUES
        (14, 'Begining_Activity', 'Time'),
        (15, 'End_Activity', 'Time')
        SET IDENTITY_INSERT [SensorDynProp] OFF

        -- add link between sensor type and dynamic property start/end activity
        DECLARE @SensorDynProp_Begining INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE Name = 'Begining_Activity'
        )

        DECLARE @SensorDynProp_End INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE Name = 'End_Activity'
        )

        CREATE TABLE #TempTable2 ([ID] INT)
        INSERT INTO #TempTable2 ([ID])
        SELECT [ID]
        FROM SensorType
        WHERE [Name] IN ('Satellite', 'GSM', 'UHF')

        INSERT INTO SensorType_SensorDynProp ([Required],[FK_SensorType],[FK_SensorDynProp]) (
            SELECT 0, *, @SensorDynProp_Begining
            FROM #TempTable2
            UNION
            SELECT 0, *, @SensorDynProp_End
            FROM #TempTable2
        )

        DROP TABLE #TempTable1
        DROP TABLE #TempTable2
        """
    )
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            CREATE TABLE #TempTable1 ([ID] INT)
            INSERT INTO #TempTable1 ([ID])
            SELECT [ID]
            FROM SensorType
            WHERE [Name] IN ('Satellite', 'GSM')

            CREATE TABLE #TempTable2 ([ID] INT)
            INSERT INTO #TempTable2 ([ID])
            SELECT [ID]
            FROM SensorType
            WHERE [Name] IN ('Satellite', 'GSM', 'UHF')

            -- add configuration for frequency
            INSERT INTO ModuleForms (
                [module_id],
                [TypeObj],
                [Name],
                [Label],
                [Required],
                [FieldSizeEdit],
                [FieldSizeDisplay],
                [InputType],
                [editorClass],
                [FormRender],
                [FormOrder],
                [Legend],
                [Options],
                [Validators],
                [displayClass],
                [EditClass],
                [Status],
                [Locked],
                [DefaultValue],
                [Rules],
                [Orginal_FB_ID]
            )
            SELECT @module_id, *, 'Transmitter_Frequency',	'Frequency', 0, 6, 6, 'Number', 'form-control', 7, 36, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
            FROM #TempTable1

            -- update form display in configuration
            UPDATE ModuleForms
            SET FormRender = 39
            WHERE module_id = @module_id AND [Name] = 'InitialLivespan' AND [TypeObj] IN (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] IN ('Satellite', 'UHF')
            )

            -- add configuration for start and end activity time
            INSERT INTO ModuleForms (
                [module_id],
                [TypeObj],
                [Name],
                [Label],
                [Required],
                [FieldSizeEdit],
                [FieldSizeDisplay],
                [InputType],
                [editorClass],
                [FormRender],
                [FormOrder],
                [Legend],
                [Options],
                [Validators],
                [displayClass],
                [EditClass],
                [Status],
                [Locked],
                [DefaultValue],
                [Rules],
                [Orginal_FB_ID]
            )
            SELECT @module_id, *, 'Begining_Activity',	'Beginning of activity', 0, 6, 6, 'DateTimePickerEditor', 'form-control', 39, 37, NULL, '{"format":"HH:mm:ss"}', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL
            FROM #TempTable2
            UNION
            SELECT @module_id, *, 'End_Activity',	'End of activity', 0, 6, 6, 'DateTimePickerEditor', 'form-control', 7, 38, NULL, '{"format":"HH:mm:ss"}', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL
            FROM #TempTable2

            DROP TABLE #TempTable1
            DROP TABLE #TempTable2
            """
        )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        -- delete values from database
        DELETE FROM SensorDynPropValue
        WHERE [FK_SensorDynProp] IN (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] IN ('Begining_Activity', 'End_Activity')
        )

        DELETE FROM SensorDynPropValue
        WHERE [FK_SensorDynProp] IN (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
        ) AND [FK_Sensor] IN (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] IN ('Satellite', 'GSM')
        )

        -- delete relationship between sensor type and dynamic properties
        DELETE FROM SensorType_SensorDynProp
        WHERE [FK_SensorDynProp] IN (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] IN ('Begining_Activity', 'End_Activity')
        )

        DELETE FROM SensorType_SensorDynProp
        WHERE [FK_SensorDynProp] IN (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
                AND [FK_SensorType] IN (
                    SELECT [ID]
                    FROM SensorType
                    WHERE [Name] IN ('Satellite', 'GSM')
                )
        )

        -- delete the dynamic property
        DELETE FROM SensorDynProp
        WHERE [Name] IN ('Begining_Activity', 'End_Activity') AND [TypeProp] = 'Time'
        """
    )
    
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            -- remove from configuration
            DECLARE @module_id INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE Name = 'SensorForm'
            )

            UPDATE ModuleForms
            SET FormRender = 7
            WHERE [module_id] = @module_id AND [Name] = 'InitialLivespan' AND [TypeObj] IN (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] IN ('Satellite', 'UHF')
            )

            DELETE FROM ModuleForms
            WHERE (
                [module_id] = @module_id 
                AND [Name] IN ('Begining_Activity', 'End_Activity')
            ) OR (
                [module_id] = @module_id 
                AND [Name] = 'Transmitter_Frequency'
                AND TypeObj IN (
                    SELECT [ID]
                    FROM SensorType
                    WHERE [Name] IN ('Satellite', 'GSM')
                )
            )
            """
        )
