"""limiting the length of comments

Revision ID: 6cb2c71dd233
Revises: e77a69510884
Create Date: 2024-10-22 15:01:12.576960

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6cb2c71dd233'
down_revision = 'e77a69510884'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT
            
            -- Sensors
            SET @module_id = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            UPDATE ModuleForms
            SET [Validators] = '[{"type":"maxLength"}]', [InputType] = 'TextArea'
            WHERE [module_id] = @module_id AND [Name] = 'Comments'

            -- Individuals
            SET @module_id = (
                SELECT [ID]
                FROM FrontModules
                WHERE Name = 'IndivForm'
            )

            UPDATE ModuleForms
            SET Validators = '[{"type":"maxLength"}]', InputType = 'TextArea'
            WHERE module_id = @module_id AND Name = 'Comments'
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT 
            
            -- Sensors
            SET @module_id = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            UPDATE ModuleForms
            SET [Validators] = NULL, [InputType] = 'Text'
            WHERE [module_id] = @module_id AND [Name] = 'Comments'

            -- Individuals
            SET @module_id = (
                SELECT [ID]
                FROM FrontModules
                WHERE Name = 'IndivForm'
            )

            UPDATE ModuleForms
            SET Validators = NULL, InputType = 'Text'
            WHERE module_id = @module_id AND Name = 'Comments'
            """
        )
