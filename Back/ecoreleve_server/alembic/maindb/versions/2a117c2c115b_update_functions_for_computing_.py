"""update functions for computing Fieldwork and Administrative areas

Revision ID: 2a117c2c115b
Revises: a9ecc31ea6f9
Create Date: 2023-12-05 15:58:10.910308

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2a117c2c115b'
down_revision = 'a9ecc31ea6f9'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER FUNCTION [dbo].[fn_GetRegionFromLatLon] (
            @lat decimal(9,5),
            @lon decimal(9,5)
        )
        RETURNS  INT 
        AS
        BEGIN
            -- Transforms lat and lon to a geometry object
            DECLARE @regionId INT;

            -- Select the Place the point is inside
            SET @regionId = ( 
                SELECT 
                TOP 1 
                F.[ID]
                FROM [FieldworkArea] AS F
                WITH(INDEX(IX_Fieldworkarea_valid_geom))
                WHERE 
                F.valid_geom.STIntersects(geometry::Point ( @lon, @lat, 4326)) = 1
                ORDER BY F.fullpath DESC
            )
            IF @regionId IS NULL
            BEGIN
                SET @regionId  = 0
            END

            RETURN @regionId
        END    
        """
    )

    op.execute(
        """
        ALTER   FUNCTION [dbo].[fn_GetAdministrativeAreaFromLatLon] (
            @lat decimal(9,5),
            @lon decimal(9,5)
        )
        RETURNS  INT
        AS
        BEGIN
        /*
            This query works because the geometries at the same level do not overlap
            and the geometries at level n+1 are necessarily included within the geometry at level n.
            Taking the longest fullpath thus gives us the most "precise" answer.
            For example,
            this point (43.29212, 5.37194) is located in the first district of Marseille,
            we don't want the France geom
            or the Provence-Alpes-Côte d'Azur geom,
            but rather the geom with fullpath France>Provence-Alpes-Côte d'Azur>Marseille, 1er arrondissement.
        */

            DECLARE @AdministrativeAreaID INT = (
                SELECT TOP 1 
                    F.[ID]
                FROM [AdministrativeArea] AS F
                WITH(INDEX(IX_AdministrativeArea_valid_geom))
                WHERE
                    F.valid_geom.STIntersects(geometry::Point ( @lon, @lat, 4326)) = 1
                ORDER BY F.[fullpath] DESC
            )

            IF @AdministrativeAreaID IS NULL
            BEGIN
                SET @AdministrativeAreaID  = 0
            END

            RETURN @AdministrativeAreaID
        END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER FUNCTION [dbo].[fn_GetRegionFromLatLon] (@lat decimal(9,5) , @lon decimal(9,5))
            
        RETURNS  int 
        AS
        BEGIN
        -- Transforms lat and lon to a geometry object
        declare @regionId int;

            -- Select the Place the point is inside
        SET @regionId = ( 
                SELECT 
                TOP 1 
                F.[ID]
                FROM [FieldworkArea] AS F
                WITH(INDEX(IX_Fieldworkarea_valid_geom))
                WHERE 
                F.valid_geom.STIntersects(geometry::Point ( @lon, @lat, 4326)) = 1
                ORDER BY F.fullpath DESC
            )
            IF @regionId IS NULL
                BEGIN
                    SET @regionId  = NULL
                END

            return @regionId
        return @regionId
        END
        """
    )
    
    op.execute(
        """
        ALTER   FUNCTION [dbo].[fn_GetAdministrativeAreaFromLatLon] (@lat decimal(9,5) , @lon decimal(9,5))
        RETURNS  INT
        AS
        BEGIN
        /*
            This query works because the geometries at the same level do not overlap
            and the geometries at level n+1 are necessarily included within the geometry at level n.
            Taking the longest fullpath thus gives us the most "precise" answer.
            For example,
            this point (43.29212, 5.37194) is located in the first district of Marseille,
            we don't want the France geom
            or the Provence-Alpes-Côte d'Azur geom,
            but rather the geom with fullpath France>Provence-Alpes-Côte d'Azur>Marseille, 1er arrondissement.
        */
        RETURN (
            SELECT TOP 1
            F.[ID]
            FROM [AdministrativeArea] AS F
            WITH(INDEX(IX_AdministrativeArea_valid_geom))
            WHERE
            F.valid_geom.STIntersects(geometry::Point ( @lon, @lat, 4326)) = 1
            ORDER BY F.[fullpath] DESC
            )
        END
        """
    )
