"""delete media unproperly uploaded

Revision ID: d2b864da81e8
Revises: 2a117c2c115b
Create Date: 2023-12-29 11:23:04.886703

Procedure to delete media file when it has not been
properly uploaded

Related to #3770
"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd2b864da81e8'
down_revision = '2a117c2c115b'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        CREATE PROCEDURE EXEC_Delete_Media_File
        -- ====================================================
        -- Delete one media file identified from the observation
        -- ID
        -- 
        -- This will delete the file even if it doesn't exist
        -- on the storage device
        -- ====================================================
            @ObservationID BIGINT,
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN

                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                END

                DECLARE @FileToDeleteID BIGINT

                SET @FileToDeleteID = (
                    SELECT  ODPV.[ID]
                    FROM Observation O
                    JOIN ObservationDynPropValue ODPV ON ODPV.[FK_Observation] = O.[ID]
                    WHERE O.[ID] = @ObservationID
                )

                PRINT(CONCAT('Observation: ', @ObservationID, ' File ID: ', @FileToDeleteID))

                DELETE FROM MediasFiles
                WHERE FK_DynPropValue = @FileToDeleteID
                
                DELETE FROM ObservationDynPropValue
                WHERE FK_Observation = @ObservationID

                DELETE FROM Observation
                WHERE ID = @ObservationID

                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
                PRINT('Successful media file removal')
            END TRY

            BEGIN CATCH
                PRINT('Error during the media file removal')
                PRINT(ERROR_MESSAGE())

                IF @@TRANCOUNT > 0
                BEGIN
                    PRINT 'Rollback'
                    ROLLBACK TRAN
                END
                ;THROW
            END CATCH
        END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        DROP PROCEDURE EXEC_Delete_Media_File
        """
    )
