"""Add a parameter to choose between updating only the FieldworkAreas table or updating it along with recalculating all positions during the update

Revision ID: db09bf2a11bd
Revises: dc2a3368ef7b
Create Date: 2025-02-17 13:15:50.000638

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'db09bf2a11bd'
down_revision = 'dc2a3368ef7b'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER PROCEDURE [dbo].[sp_FA_Update]
        -- ===================================================
        -- Compute the new fieldwork area layer associated to
        -- each spatial point (individual location or station)
        -- which must be updated
        --
        -- This stored procedure updates the Individual_Location
        -- table and the Station table and provides
        -- ===================================================
            @AllProcess INT = 0,
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN
                DECLARE	@return_value INT

                -- Run procedures
                IF @AllProcess = 1
                BEGIN
                    EXEC @return_value = [dbo].[sp_FA_LayerDifference] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_LayerDifference' = @return_value
                END

                EXEC @return_value = [dbo].[sp_FA_TableFormatting] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_FA_TableFormatting' = @return_value

                IF @AllProcess = 1
                BEGIN
                    EXEC @return_value = [dbo].[sp_FA_Compute] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_Compute' = @return_value
                END

                -- delete reneco Fieldwork area table
                IF OBJECT_ID('reneco_fieldworkarea') IS NOT NULL
                BEGIN
                    DROP TABLE reneco_fieldworkarea
                END

                -- delete gdal table
                IF OBJECT_ID('reneco_fieldworkarea') IS NOT NULL
                BEGIN
                    DROP TABLE spatial_ref_sys
                END

                -- delete gdal table
                IF OBJECT_ID('reneco_fieldworkarea') IS NOT NULL
                BEGIN
                    DROP TABLE geometry_columns
                END

                -- delete To_Check table
                IF OBJECT_ID('reneco_fieldworkarea') IS NOT NULL
                BEGIN
                    DROP TABLE To_Check
                END

                -- manage debug/rollback mode
                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                    END
                    ;THROW
            END CATCH
        END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER PROCEDURE [dbo].[sp_FA_Update]
        -- ===================================================
        -- Compute the new fieldwork area layer associated to
        -- each spatial point (individual location or station)
        -- which must be updated
        --
        -- This stored procedure updates the Individual_Location
        -- table and the Station table and provides
        -- ===================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN
                DECLARE	@return_value INT

                -- Run procedures
                EXEC @return_value = [dbo].[sp_FA_LayerDifference] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_FA_LayerDifference' = @return_value

                EXEC @return_value = [dbo].[sp_FA_TableFormatting] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_FA_TableFormatting' = @return_value

                EXEC @return_value = [dbo].[sp_FA_Compute] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_FA_Compute' = @return_value

                -- delete reneco Fieldwork area table
                DROP TABLE reneco_fieldworkarea

                -- delete gdal table
                DROP TABLE spatial_ref_sys

                -- delete gdal table
                DROP TABLE geometry_columns

                -- delete To_Check table
                DROP TABLE To_Check

                -- manage debug/rollback mode
                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                    END
                    ;THROW
            END CATCH
        END
        """
    )
