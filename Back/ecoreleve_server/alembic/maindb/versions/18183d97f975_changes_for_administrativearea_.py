"""changes_for_administrativearea_monitored_sites

Revision ID: 18183d97f975
Revises: e9c910706728
Create Date: 2023-06-05 22:25:20.525368

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '18183d97f975'
down_revision = 'e9c910706728'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        CREATE SPATIAL INDEX [IX_AdministrativeArea_valid_geom] ON [dbo].[AdministrativeArea]
            (
                [valid_geom]
            )USING  GEOMETRY_GRID
            WITH (BOUNDING_BOX =(-90, -180, 90, 180), GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM),
            CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        """
    )

    op.execute(
        """
        CREATE OR ALTER FUNCTION [dbo].[fn_GetAdministrativeAreaFromLatLon] (@lat decimal(9,5) , @lon decimal(9,5))
        RETURNS  INT
        AS
        BEGIN
        /*
            This query works because the geometries at the same level do not overlap
            and the geometries at level n+1 are necessarily included within the geometry at level n.
            Taking the longest fullpath thus gives us the most "precise" answer.
            For example,
            this point (43.29212, 5.37194) is located in the first district of Marseille,
            we don't want the France geom
            or the Provence-Alpes-Côte d'Azur geom,
            but rather the geom with fullpath France>Provence-Alpes-Côte d'Azur>Marseille, 1er arrondissement.
        */
        RETURN (
            SELECT TOP 1
            F.[ID]
            FROM [AdministrativeArea] AS F
            WITH(INDEX(IX_AdministrativeArea_valid_geom))
            WHERE
            F.valid_geom.STIntersects(geometry::Point ( @lon, @lat, 4326)) = 1
            ORDER BY F.[fullpath] DESC
            )
        END

        """
    )

    op.execute(
        """
        CREATE OR ALTER PROCEDURE [dbo].[UpdateStationRegion]
        @stationID int,
        @result int OUTPUT
        AS
        SET NOCOUNT ON
        SET @result = NULL
        BEGIN
        BEGIN TRY
            BEGIN TRAN
                DECLARE @lat DECIMAL(9,5), @lon DECIMAL(9,5), @FK_FieldworkArea INT, @FK_AdministrativeArea INT
                SELECT
                @lat = LAT,
                @lon = LON
                FROM [Station]
                WHERE [ID] = @stationID

                SELECT @FK_FieldworkArea = dbo.[fn_GetRegionFromLatLon] (@lat,@lon)
                SELECT @FK_AdministrativeArea = dbo.[fn_GetAdministrativeAreaFromLatLon] (@lat,@lon)

                UPDATE [Station]
                SET FK_FieldworkArea = @FK_FieldworkArea,
                    FK_AdministrativeArea = @FK_AdministrativeArea
                WHERE [ID] = @stationID

            IF @@TRANCOUNT > 0
                COMMIT TRAN
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN
                    SET @result = 1
                END
        END CATCH
        END
        """
    )

    op.execute(
        """
        ALTER TABLE [MonitoredSitePosition]
        ADD
        [FK_FieldWorkArea] INT NULL,
        [FK_AdministrativeArea] INT NULL,
        CONSTRAINT [FK_MonitoredSitePosition_FK_AdministrativeArea_AdministrativeArea]
            FOREIGN KEY ([FK_AdministrativeArea]) REFERENCES [AdministrativeArea](ID),
        CONSTRAINT [FK_MonitoredSitePosition_FK_FieldWorkArea_FieldWorkArea]
            FOREIGN KEY ([FK_FieldWorkArea]) REFERENCES [FieldWorkArea](ID)
        """
    )

    op.execute(
        """
            CREATE OR ALTER VIEW [dbo].[MonitoredSitePositionsNow] AS
            SELECT
            MSP.[ID],
            MSP.[LAT],
            MSP.[LON],
            MSP.[ELE],
            MSP.[Precision],
            MSP.[StartDate],
            MSP.[Comments],
            MSP.[FK_MonitoredSite],
            MSP.[ModificationDate],
            MSP.[FK_FieldWorkArea],
            MSP.[FK_AdministrativeArea]
            FROM [MonitoredSitePosition]  AS MSP
            WHERE
            NOT EXISTS(
            SELECT
            1
            FROM [MonitoredSitePosition] AS MSP2
            WHERE
            MSP.[FK_MonitoredSite] = MSP2.[FK_MonitoredSite]
            AND
            MSP2.[StartDate] > MSP.[StartDate]
            AND
            MSP2.[StartDate] <= GETDATE()
            )
            AND
            MSP.[StartDate] <= GETDATE()
        """
    )

    op.execute(
        """
        CREATE OR ALTER PROCEDURE [dbo].[Update_FK_Fieldworkaera_And_FK_AdministrativeArea_For_MonitoredSitePosition]
            @MonitoredSitePositionID int,
            @result int OUTPUT
            AS
            SET NOCOUNT ON
            SET @result = NULL
            BEGIN
            BEGIN TRY
                BEGIN TRAN
                    DECLARE @lat DECIMAL(9,5), @lon DECIMAL(9,5), @FK_FieldworkArea INT, @FK_AdministrativeArea INT
                    SELECT
                    @lat = LAT,
                    @lon = LON
                    FROM [MonitoredSitePosition]
                    WHERE [ID] = @MonitoredSitePositionID

                    SELECT @FK_FieldworkArea = dbo.[fn_GetRegionFromLatLon] (@lat,@lon)
                    SELECT @FK_AdministrativeArea = dbo.[fn_GetAdministrativeAreaFromLatLon] (@lat,@lon)

                    UPDATE [MonitoredSitePosition]
                    SET FK_FieldworkArea = @FK_FieldworkArea,
                        FK_AdministrativeArea = @FK_AdministrativeArea
                    WHERE [ID] = @MonitoredSitePositionID

                IF @@TRANCOUNT > 0
                    COMMIT TRAN
            END TRY
            BEGIN CATCH
                IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                        print('error')
                        SET @result = 1
                    END
            END CATCH
            END
        """
    )
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            INSERT INTO [dbo].[BusinessRules]
            (
            [name],
            [target],
            [targetType],
            [actionType],
            [executing],
            [params],
            [description],
            [errorValue])
            VALUES
            (
            'Update_MonitoredSitePosition_insert',
            'MonitoredSitePosition',
            NULL,
            'after_insert',
            'dbo.[Update_FK_Fieldworkaera_And_FK_AdministrativeArea_For_MonitoredSitePosition]',
            '["ID"]',
            'Mise à jour de la FK_FieldworkArea et de la FK_AdministrativeArea',
            'Something goes wrong when trying to update FK_FieldworkArea and FK_AdministrativeArea'),
            (
            'Update_MonitoredSitePosition_update',
            'MonitoredSitePosition',
            NULL,
            'after_update',
            'dbo.[Update_FK_Fieldworkaera_And_FK_AdministrativeArea_For_MonitoredSitePosition]',
            '["ID"]',
            'Mise à jour de la FK_FieldworkArea et de la FK_AdministrativeArea',
            'Something goes wrong when trying to update FK_FieldworkArea and FK_AdministrativeArea')
            """
        )

        op.execute(
            """
            INSERT INTO [dbo].[ModuleForms]
            (
            [module_id],[TypeObj],[Name],[Label],[Required],[FieldSizeEdit],[FieldSizeDisplay],[InputType],[editorClass],[FormRender],[FormOrder],[Legend],[Options])
            VALUES
            (12,NULL,'FK_FieldworkArea','Fieldwork Area',0,6,6,'FieldworkingAreaEditor','form-control',1,10,'Location Information','{}'),
            (12,NULL,'FK_AdministrativeArea','Administrative Area',0,6,6,'AdministrativeAreaEditor','form-control',1,11,'Location Information','{}')

            UPDATE [ModuleForms]
            SET [FormOrder] = 12
            WHERE
            [module_id] = 12
            AND
            [Name] = 'Comments'
            """
        )

        op.execute(
        """
            INSERT INTO [ModuleGrids]
            ([Module_ID]
            ,[Name]
            ,[Label]
            ,[GridRender]
            ,[GridSize]
            ,[CellType]
            ,[GridOrder]
            ,[Options]
            ,[FilterOrder]
            ,[FilterSize]
            ,[IsSearchable]
            ,[FilterRender]
            ,[FilterType])
            VALUES
            (13,
            'FieldworkArea@Name',
            'Fieldwork Area',
            2,
            '{"width"\:120,"maxWidth"\:350,"minWidth"\:100}',
            'string',
            7,
            '{"source": "autocomplete/fieldworkarea/Name/Name", "minLength"\:3}',
            7,
            2,
            1,
            4,
            'AutocompleteEditor'),
            (13,
            'AdministrativeArea@fullpath',
            'Administrative Area',
            2,
            '{"width"\:120,"maxWidth"\:350,"minWidth"\:100}',
            'string',
            8,
            '{"usedLabel":"fullpath","operator":"contains","triggerAt"\:3}',
            8,
            2,
            1,
            4,
            'AdministrativeAreaEditor')
        """
        )



def downgrade(**options_for_migration) -> None:
    op.execute(
        """
            DROP INDEX [IX_AdministrativeArea_valid_geom] ON [AdministrativeArea]
        """
    )

    op.execute(
        """
        DROP FUNCTION [fn_GetAdministrativeAreaFromLatLon]
        """
    )

    op.execute(
        """
        CREATE OR ALTER PROCEDURE [dbo].[UpdateStationRegion]
        @stationID int,
        @result int OUTPUT
        AS
        SET NOCOUNT ON
        SET @result = NULL

        BEGIN TRAN T1
        BEGIN TRY
        DECLARE @lat decimal(9,5), @lon decimal(9,5), @FK_FieldworkArea int

        SELECT @lat = LAT, @lon = LON
        FROM Station
        WHERE ID = @stationID


        SELECT @FK_FieldworkArea = dbo.[fn_GetRegionFromLatLon] (@lat,@lon)

        UPDATE Station SET FK_FieldworkArea = @FK_FieldworkArea
        WHERE ID = @stationID
        COMMIT TRAN T1
        END TRY
        BEGIN CATCH
            SELECT @result = 1;
        END CATCH
        """
    )

    op.execute(
        """
        ALTER TABLE [MonitoredSitePosition]
        DROP CONSTRAINT [FK_MonitoredSitePosition_FK_FieldWorkArea_FieldWorkArea]

        ALTER TABLE [MonitoredSitePosition]
        DROP COLUMN FK_FieldWorkArea

        ALTER TABLE [MonitoredSitePosition]
        DROP CONSTRAINT [FK_MonitoredSitePosition_FK_AdministrativeArea_AdministrativeArea]

        ALTER TABLE [MonitoredSitePosition]
        DROP COLUMN FK_AdministrativeArea
        """
    )

    op.execute(
    """
    CREATE OR ALTER VIEW [dbo].[MonitoredSitePositionsNow] AS
    SELECT * FROM MonitoredSitePosition p

        where not exists (select * from  MonitoredSitePosition  p2
            where p.FK_MonitoredSite = p2.FK_MonitoredSite
            AND p2.startdate > p.startdate and p2.startdate <= GETDATE())
            and p.StartDate  <= GETDATE()
    """
    )

    op.execute(
        """
        DROP PROCEDURE [Update_FK_Fieldworkaera_And_FK_AdministrativeArea_For_MonitoredSitePosition]
        """
    )
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:

        op.execute(
            """
            DELETE FROM [BusinessRules]
            WHERE [ID] IN (17,18)
            """
        )


        op.execute(
            """
            DELETE FROM [ModuleForms]
            WHERE
            [module_id] = 12
            AND
            [Name] IN ('FK_FieldworkArea','FK_AdministrativeArea')

            UPDATE [ModuleForms]
            SET [FormOrder] = 10
            WHERE
            [module_id] = 12
            AND
            [Name] = 'Comments'
            """
        )

        op.execute(
            """
            DELETE FROM [ModuleGrids]
            WHERE
            [Module_ID] = 13
            AND
            [Name] IN ('FieldworkArea@Name', 'AdministrativeArea@fullpath')
            """
        )

