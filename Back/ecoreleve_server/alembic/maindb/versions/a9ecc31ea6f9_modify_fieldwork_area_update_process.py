"""Modify fieldwork area update process

This revision modifies the pr_FA_TableFormatting.
Fields SHAPE_Leng and SHAPE_Area are not provided
in the Fieldwork Areas shapefile. So, their inserts
in the FieldworkArea table is no longer relevant 
and has been removed.

This revision modifies the pr_FA_TableFormatting also to get the country information for the shapefile and to store it in the Country field of the fieldworkarea.

This revision also modifies the pr_FA_Update.
The Foreign Keys no longer need to be deleted 
before to process the data update, as the 
FieldworkArea table is no longer delete.

Revision ID: a9ecc31ea6f9
Revises: 21f6b2beff89
Create Date: 2023-11-28 11:19:04.993338

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a9ecc31ea6f9'
down_revision = '21f6b2beff89'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER PROCEDURE [dbo].[sp_FA_TableFormatting]
        -- ===================================================
        -- Formatting new fieldwork area table
        --
        -- This stored procedure creates the Fieldworkarea 
        -- update table with the new layers and columns in the
        -- right formats (geom, fullpath etc)
        -- ===================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN

                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                END

                DELETE FROM FieldworkArea
                WHERE [ID] IN (
                    SELECT [eco_id]
                    FROM reneco_fieldworkarea
                )

                -- INSERT toutes les nouvelles données (NEW and UPDATED)
                -- i.e., seulement celles qui ne sont pas inactivées
                IF @Debug = 1
                BEGIN
                    SELECT *
                    FROM Fieldworkarea F
                    JOIN reneco_fieldworkarea R ON R.[eco_id] = F.[ID]
                END

                SET IDENTITY_INSERT Fieldworkarea ON
                INSERT INTO Fieldworkarea (
                    [ID],
                    [Country],
                    [Working_Area], 
                    [Working_Region], 
                    [Management_Unit], 
                    [Name], 
                    [fullpath], 
                    [type_], 
                    [max_lat], 
                    [min_lat], 
                    [max_lon],
                    [min_lon],
                    [geom], 
                    [valid_geom],
                    [activation_date],
                    [inactivation_date],
                    [last_change_attribut_date],
                    [last_change_geometry_date],
                    [Management_Status], 
                    [Communication_Name], 
                    [Center], 
                    [Data_Owner],
                    [Centroid_Latitude],
                    [Centroid_Longitude]
                )
                SELECT 
                    FirstPart.[ID],
                    [Country],
                    [Working_Area],
                    [Working_Region],
                    [Management_Unit],
                    [Name],
                    [fullpath],
                    [type_],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [geom],
                    [valid_geom],
                    [acti_d],
                    [inacti_d],
                    [attr_d],
                    [geom_d],
                    [mngt_statu],
                    [com_name],
                    [center],
                    [data_owner],
                    [centro_lat],
                    [centro_lon]
                FROM (
                    SELECT
                        [eco_id] AS [ID],
                        [country] AS [Country],
                        [w_area] AS [Working_Area],
                        [w_region] AS [Working_Region],
                        [mngt_unit] AS [Management_Unit],
                        CASE 
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN [mngt_unit]
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN [w_region]
                            WHEN [w_area] IS NOT NULL
                                THEN [w_area]
                            ELSE NULL
                        END AS [Name],
                        CASE 
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN CONCAT([w_area],'>',[w_region],'>',[mngt_unit])
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN CONCAT([w_area],'>',[w_region])
                            WHEN [w_area] IS NOT NULL
                                THEN [w_area]
                            ELSE NULL
                        END AS [fullpath],
                        CASE 
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN 'management unit'
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN 'working region'
                            WHEN [w_area] IS NOT NULL
                                THEN 'working area'
                            ELSE NULL
                        END AS [type_],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                        [acti_d],
                        [inacti_d],
                        [attr_d],
                        [geom_d],
                        [mngt_statu],
                        [com_name],
                        [center],
                        [data_owner],
                        [centro_lat],
                        [centro_lon]
                    FROM reneco_fieldworkarea
                    WHERE [inacti_d] IS NULL
                    GROUP BY [eco_id], [country], [w_area], [w_region], [mngt_unit], [acti_d], [inacti_d], [attr_d], [geom_d], [mngt_statu], [com_name], [center], [data_owner], [centro_lat], [centro_lon]
                ) AS FirstPart
                JOIN (
                    SELECT 
                        [eco_id] AS [ID],
                        [ogr_geometry] AS [geom],
                        [ogr_geometry].MakeValid() AS [valid_geom]
                    FROM reneco_fieldworkarea
                    WHERE [inacti_d] IS NULL
                ) AS SecondPart
                ON [FirstPart].[ID] = [SecondPart].[ID]
                ORDER BY [Country] ASC, [Working_Area] ASC, [Working_Region] ASC, [Management_Unit] ASC

                SET IDENTITY_INSERT Fieldworkarea OFF

                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                PRINT('Error during fieldwork areas update - Table Formatting')
                PRINT(ERROR_MESSAGE())

                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN
                END
                ;THROW
            END CATCH
        END
        """
    )

    op.execute(
        """
        ALTER PROCEDURE [dbo].[sp_FA_Update]
        -- ===================================================
        -- Compute the new fieldwork area layer associated to
        -- each spatial point (individual location or station)
        -- which must be updated
        --
        -- This stored procedure updates the Individual_Location
        -- table and the Station table and provides
        -- ===================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN
                DECLARE @return_value INT

                -- Run procedures
                EXEC @return_value = [dbo].[sp_FA_LayerDifference] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_FA_LayerDifference' = @return_value

                EXEC @return_value = [dbo].[sp_FA_TableFormatting] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_FA_TableFormatting' = @return_value

                EXEC @return_value = [dbo].[sp_FA_Compute] @Debug = 0, @ForceRollback = 0
                SELECT 'sp_FA_Compute' = @return_value

                -- delete reneco Fieldwork area table
                DROP TABLE reneco_fieldworkarea

                -- delete gdal table
                DROP TABLE spatial_ref_sys

                -- delete gdal table
                DROP TABLE geometry_columns

                -- delete To_Check table
                DROP TABLE To_Check

                -- manage debug/rollback mode
                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                    END
                    ;THROW
            END CATCH
        END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER PROCEDURE [dbo].[sp_FA_TableFormatting]
        -- ===================================================
        -- Formatting new fieldwork area table
        --
        -- This stored procedure creates the Fieldworkarea 
        -- update table with the new layers and columns in the
        -- right formats (geom, fullpath etc)
        -- ===================================================
            @Debug INT=1,
            @ForceRollback INT=1
        AS
        BEGIN
            BEGIN TRY
            BEGIN TRAN

                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                END

                DELETE FROM FieldworkArea
                WHERE [ID] IN (
                    SELECT [eco_id]
                    FROM reneco_fieldworkarea
                )

                -- INSERT toutes les nouvelles données (NEW and UPDATED)
                -- i.e., seulement celles qui ne sont pas inactivées
                IF @Debug = 1
                BEGIN
                    SELECT *
                    FROM Fieldworkarea F
                    JOIN reneco_fieldworkarea R ON R.[eco_id] = F.[ID]
                END

                SET IDENTITY_INSERT Fieldworkarea ON
                INSERT INTO Fieldworkarea (
                    [ID], 
                    [Working_Area], 
                    [Working_Region], 
                    [Management_Unit], 
                    [Name], 
                    [fullpath], 
                    [type_], 
                    [max_lat], 
                    [min_lat], 
                    [max_lon],
                    [min_lon], 
                    [SHAPE_Leng], 
                    [SHAPE_Area], 
                    [geom], 
                    [valid_geom],
                    [activation_date],
                    [inactivation_date],
                    [last_change_attribut_date],
                    [last_change_geometry_date],
                    [Management_Status], 
                    [Communication_Name], 
                    [Center], 
                    [Data_Owner],
                    [Centroid_Latitude],
                    [Centroid_Longitude]
                )
                SELECT 
                    FirstPart.[ID],
                    [Working_Area],
                    [Working_Region],
                    [Management_Unit],
                    [Name],
                    [fullpath],
                    [type_],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [SHAPE_Leng],
                    [SHAPE_Area],
                    [geom],
                    [valid_geom],
                    [acti_d],
                    [inacti_d],
                    [attr_d],
                    [geom_d],
                    [mngt_statu],
                    [com_name],
                    [center],
                    [data_owner],
                    [centro_lat],
                    [centro_lon]
                FROM (
                    SELECT
                        [eco_id] AS [ID],
                        [w_area] AS [Working_Area],
                        [w_region] AS [Working_Region],
                        [mngt_unit] AS [Management_Unit],
                        CASE 
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN [mngt_unit]
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN [w_region]
                            WHEN [w_area] IS NOT NULL
                                THEN [w_area]
                            ELSE NULL
                        END AS [Name],
                        CASE 
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN CONCAT([w_area],'>',[w_region],'>',[mngt_unit])
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN CONCAT([w_area],'>',[w_region])
                            WHEN [w_area] IS NOT NULL
                                THEN [w_area]
                            ELSE NULL
                        END AS [fullpath],
                        CASE 
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN 'management unit'
                            WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN 'working region'
                            WHEN [w_area] IS NOT NULL
                                THEN 'working area'
                            ELSE NULL
                        END AS [type_],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                        CONVERT(REAL,[SHAPE_Leng]) AS [SHAPE_Leng],
                        CONVERT(REAL,[SHAPE_Area])  AS [SHAPE_Area],
                        [acti_d],
                        [inacti_d],
                        [attr_d],
                        [geom_d],
                        [mngt_statu],
                        [com_name],
                        [center],
                        [data_owner],
                        [centro_lat],
                        [centro_lon]
                    FROM reneco_fieldworkarea
                    WHERE [inacti_d] IS NULL
                    GROUP BY [eco_id], [w_area], [w_region], [mngt_unit],[SHAPE_Leng], [SHAPE_Area], [acti_d], [inacti_d], [attr_d], [geom_d], [mngt_statu], [com_name], [center], [data_owner], [centro_lat], [centro_lon]
                ) AS FirstPart
                JOIN (
                    SELECT 
                        [eco_id] AS [ID],
                        [ogr_geometry] AS [geom],
                        [ogr_geometry].MakeValid() AS [valid_geom]
                    FROM reneco_fieldworkarea
                    WHERE [inacti_d] IS NULL
                ) AS SecondPart
                ON [FirstPart].[ID] = [SecondPart].[ID]
                ORDER BY [Working_Area] ASC, [Working_Region] ASC, [Management_Unit] ASC

                SET IDENTITY_INSERT Fieldworkarea OFF

                IF @@TRANCOUNT > 0
                BEGIN
                    IF @ForceRollback = 1
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        ;THROW 50001, N'FORCED ROLLBACK', 1;
                        ROLLBACK TRAN
                    END
                    ELSE
                    BEGIN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                        END
                        COMMIT TRAN
                    END
                END
            END TRY

            BEGIN CATCH
                PRINT('Error during fieldwork areas update - Table Formatting')
                PRINT(ERROR_MESSAGE())

                IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN
                END
                ;THROW
            END CATCH
        END
        """
    )

    op.execute(
            """
            ALTER PROCEDURE [dbo].[sp_FA_Update]
            -- ===================================================
            -- Compute the new fieldwork area layer associated to
            -- each spatial point (individual location or station)
            -- which must be updated
            --
            -- This stored procedure updates the Individual_Location
            -- table and the Station table and provides
            -- ===================================================
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    DECLARE @return_value INT

                    -- Remove Foreign Keys
                    IF OBJECT_ID('FK_Individual_Location_FK_FieldworkArea_Fieldworkarea') IS NOT NULL
                    BEGIN
                        ALTER TABLE Individual_Location
                        DROP CONSTRAINT FK_Individual_Location_FK_FieldworkArea_Fieldworkarea
                    END

                    IF OBJECT_ID('FK_Station_FK_FieldworkArea_Fieldworkarea') IS NOT NULL
                    BEGIN
                        ALTER TABLE Station
                        DROP CONSTRAINT FK_Station_FK_FieldworkArea_Fieldworkarea
                    END

                    IF OBJECT_ID('FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea') IS NOT NULL
                    BEGIN
                        ALTER TABLE MonitoredSitePosition
                        DROP CONSTRAINT FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea
                    END

                    -- Run procedures
                    EXEC @return_value = [dbo].[sp_FA_LayerDifference] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_LayerDifference' = @return_value

                    EXEC @return_value = [dbo].[sp_FA_TableFormatting] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_TableFormatting' = @return_value

                    EXEC @return_value = [dbo].[sp_FA_Compute] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_Compute' = @return_value

                    -- Add Foreign Keys
                    ALTER TABLE Individual_Location WITH NOCHECK 
                    ADD CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_Fieldworkarea] FOREIGN KEY([FK_FieldworkArea])
                    REFERENCES Fieldworkarea ([ID])
                    NOT FOR REPLICATION 

                    -- ALTER TABLE Individual_Location
                    -- CHECK CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_Fieldworkarea]

                    ALTER TABLE Station WITH NOCHECK 
                    ADD CONSTRAINT [FK_Station_FK_FieldworkArea_Fieldworkarea] 
                    FOREIGN KEY([FK_FieldworkArea]) 
                    REFERENCES Fieldworkarea ([ID])
                    NOT FOR REPLICATION 

                    -- ALTER TABLE Station
                    -- CHECK CONSTRAINT [FK_Station_FK_FieldworkArea_Fieldworkarea]

                    ALTER TABLE MonitoredSitePosition WITH NOCHECK 
                    ADD CONSTRAINT [FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea] 
                    FOREIGN KEY([FK_FieldworkArea]) 
                    REFERENCES Fieldworkarea ([ID])
                    NOT FOR REPLICATION 

                    -- ALTER TABLE MonitoredSitePosition 
                    -- CHECK CONSTRAINT [FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea]

                    -- delete reneco Fieldwork area table
                    DROP TABLE reneco_fieldworkarea

                    -- delete gdal table
                    DROP TABLE spatial_ref_sys

                    -- delete gdal table
                    DROP TABLE geometry_columns

                    -- delete To_Check table
                    DROP TABLE To_Check

                    -- manage debug/rollback mode
                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY

                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
            """
        )
