"""business rules station

Revision ID: e61fa2610f41
Revises: b5150c588e86
Create Date: 2023-10-09 14:01:09.417477

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e61fa2610f41'
down_revision = 'b5150c588e86'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:

    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                INSERT INTO [dbo].[BusinessRules]
                (
                    [name],
                    [target],
                    [targetType],
                    [actionType],
                    [executing],
                    [params],
                    [description],
                    [errorValue]
                )
                VALUES
                (
                    'Remove_FK_Station_References_Before_Delete',
                    'Station',
                    NULL,
                    'before_delete',
                    '[dbo].[Remove_FK_Station_References_Before_Delete]',
                    '["ID"]',
                    'Si plus aucune observations sur cette station, alors on supprime la station de la table [Release] et en cascade l''ensemble des [Individual_Released]',
                    'You cannot delete a station with an observation attached. You should delete all observation first'
                )
            """
        )

    op.execute(
        """
            CREATE PROCEDURE [dbo].[Remove_FK_Station_References_Before_Delete]
            @ID INT,
            @result INT OUTPUT
            AS
            SET NOCOUNT ON
            SET @result = NULL
            /*
            The API expected the stored procedure return 0 if it's ok and 1 any error (business logic or sql failed)
            */
            BEGIN TRAN
            BEGIN TRY
                --If still observations we throw an error to catch it
                IF EXISTS(SELECT 1
                            FROM [Observation] AS O
                            WHERE O.[FK_Station] = @ID)
                BEGIN
                    ;THROW 50001, N'There are still observations attached to this station', 1;
                END
                ELSE
                BEGIN
                    -- If no more observations we can remove references with release
                    -- Need to delete release references first
                    DELETE IR
                    FROM [Individual_Released] AS IR
                    JOIN [Release] AS R ON R.[ID] = IR.[Release_ID]
                    WHERE
                    R.[Station_ID] = @ID

                    DELETE FROM [Release]
                    WHERE [Station_ID] = @ID
                END

                    IF @@TRANCOUNT > 0
                        COMMIT TRAN
                        SET @result = 0
            END TRY
            BEGIN CATCH
                IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                        -- If any errror we  rollback the transaction and return 1
                        SET @result = 1
                    END
            END CATCH
        """
    )

def downgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                DELETE FROM [BusinessRules] WHERE [Name] = 'Remove_FK_Station_References_Before_Delete'
            """
        )

    op.execute(
        """
        DROP PROCEDURE [Remove_FK_Station_References_Before_Delete]
        """
    )