"""company and model fields are required for sensors

Revision ID: e7ceba7663f5
Revises: 02f8ba104388
Create Date: 2024-09-11 16:27:15.200284

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e7ceba7663f5'
down_revision = '02f8ba104388'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER TABLE Sensor
        ALTER COLUMN [Model] NVARCHAR(255) NOT NULL;

        ALTER TABLE Sensor
        ALTER COLUMN [Company] NVARCHAR(255) NOT NULL;
        """
    )
    
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT ID
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            UPDATE ModuleForms
            SET [Required] = 1
            WHERE [module_id] = @module_id
                AND ([Name] = 'Model'
                    OR [Name] = 'Company')
            """
        )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER TABLE Sensor
        ALTER COLUMN [Model] NVARCHAR(255) NULL;

        ALTER TABLE Sensor
        ALTER COLUMN [Company] NVARCHAR(255) NULL;
        """
    )

    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT ID
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            UPDATE ModuleForms
            SET [Required] = 0
            WHERE [module_id] = @module_id
                AND ([Name] = 'Model'
                    OR [Name] = 'Company')
            """
        )
