"""update fieldwork area process

Revision ID: 12a3c5b43973
Revises: 18183d97f975
Create Date: 2023-06-05 10:44:14.931706

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '12a3c5b43973'
down_revision = '18183d97f975'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:

    # ALTER Fieldworkarea
    op.execute(
        """
        ALTER TABLE [Fieldworkarea]
        DROP COLUMN [Status], [Country]

        ALTER TABLE [Fieldworkarea]
        ADD [activation_date] DATE NULL,
            [inactivation_date] DATE NULL,
            [last_change_geometry_date] DATE NULL,
            [last_change_attribut_date] DATE NULL
        """
    )

    # mise à jour de la procedure qui set a NULL
    op.execute(
        """
            ALTER PROCEDURE sp_FA_LayerDifference
            -- ====================================================
            -- Identification of the FK_FieldworkArea to be updated
            --
            -- The FK_FieldworkArea will be set to NULL for all
            -- locations that need to be re-computed
            -- ====================================================
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN

                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    -- Declare all variables
                    DECLARE @FA_ID INT,
                        @type VARCHAR(500),
                        @w_area VARCHAR(500),
                        @w_region VARCHAR(500),
                        @mngt_unit VARCHAR(500),
                        @IdToReset INT

                    -- Create temporary FA_update table: contain the fielwork area that
                    -- have been activated or inactivated or with modified geom
                    IF OBJECT_ID('tempdb..#FA_update') IS NOT NULL
                    BEGIN 
                        DROP TABLE #FA_update
                    END
                    CREATE TABLE #FA_update (
                        [eco_id] INT,
                        [type] VARCHAR(MAX),
                        [w_area] VARCHAR(MAX),
                        [w_region] VARCHAR(MAX),
                        [mngt_unit] VARCHAR(MAX),
                        [IdToReset] INT
                    )

                    -- ToCheck Table: store the ID of the Fielwork areas that may be
                    -- updated at the studied points (individual locations, stations,
                    -- monitored sites)
                    IF OBJECT_ID('To_Check') IS NOT NULL 
                    BEGIN
                        DROP TABLE To_Check
                    END
                    CREATE TABLE To_Check (
                        [IdToCheck] INT NOT NULL
                    )

                    -- Table des FA associées à un recalcul
                    -- (seul les changement d'attribut ne conduisent pas à un recalcul)
                    INSERT INTO #FA_update (
                        [eco_id],
                        [type],
                        [w_area],
                        [w_region],
                        [mngt_unit]
                    )
                    SELECT
                        UFA.[eco_id],
                        UFA.[type_area],
                        UFA.[w_area],
                        UFA.[w_region],
                        UFA.[mngt_unit]   
                    FROM reneco_fieldworkarea AS UFA
                    LEFT JOIN Fieldworkarea AS FA ON UFA.[eco_id] = FA.[ID] 
                    WHERE 
                        -- FA inactivated = devenu obsolete
                        (FA.[inactivation_date] IS NULL AND UFA.[inacti_d] IS NOT NULL) 
                        OR 
                        -- FA activated = nouvelle FA
                        (FA.[activation_date] IS NULL AND UFA.[acti_d] IS NOT NULL)
                        -- (UFA.[acti_d] IS NOT NULL AND UFA.[geom_d] IS NULL AND UFA.[attr_d] IS NULL)
                        OR
                        -- = modification de geom
                        (FA.[last_change_geometry_date] IS NULL AND UFA.[geom_d] IS NOT NULL)
                        OR
                        (FA.[last_change_geometry_date] < UFA.[geom_d])

                    -- Identification des ID de FA qui seront à recalculer
                    -- Stockage des ID dans ToCheck
                    INSERT INTO To_Check (IdToCheck) 
                    VALUES (0) -- by default, location with a FK_FieldworkArea equal to 0 (meaning outside current FA) needs to be re-computed

                    -- Iteration on the temporary table FA_UPDATE
                    -- For each updated fieldwork area, position associated with the
                    -- superior FA need to be re-computed
                    -- If the FA is a working area, no previous layer, else IdToReset 
                    -- is set as equal to the ID of the containing layer
                    -- (such as: management unit ⊂ working region ⊂ working area)
                    WHILE (SELECT COUNT(*) From #FA_update) > 0
                    BEGIN
                        SELECT TOP 1
                            @FA_ID = [eco_id],
                            @type = [type],
                            @w_area = [w_area],
                            @w_region = [w_region],
                            @mngt_unit = [mngt_unit]
                        FROM #FA_update
                            
                        INSERT INTO To_Check (IdToCheck) 
                        VALUES (@FA_ID)

                        IF @type = 'working region'
                        BEGIN	
                            SET @IdToReset = (
                                SELECT ID
                                FROM Fieldworkarea
                                WHERE [type_] = 'working area'
                                    AND [Working_Area] = @w_area
                            )
                        END
                        ELSE 
                        BEGIN
                            IF @type = 'management unit'
                            BEGIN
                                SET @IdToReset = (
                                    SELECT ID
                                    FROM Fieldworkarea
                                    WHERE [type_] = 'working region' 
                                        AND [Working_Region] = @w_region 
                                )
                                IF @IdToReset IS NULL 
                                BEGIN
                                    SET @IdToReset = (
                                        SELECT ID
                                        FROM Fieldworkarea
                                        WHERE [type_] = 'working area' 
                                            AND [Working_Area] = @w_area 
                                    )
                                END
                            END
                        END

                        IF @IdToReset IS NOT NULL
                        BEGIN
                            INSERT INTO To_Check (IdToCheck) 
                            VALUES (@IdToReset)
                        END
                            
                        -- Remove the processed fieldwork area
                        DELETE FROM #FA_update
                        WHERE [eco_id] =  @FA_ID
                    END

                    IF @Debug = 1
                    BEGIN
                        SELECT 'FA_Update: should be empty'
                        SELECT *
                        FROM #FA_UPDATE

                        SELECT 'To_Check: all ID leading to set to NULL'
                        SELECT *
                        FROM To_Check
                    END

                    -- Set to NULL all locations for which the FA need to be re-compute
                    UPDATE Individual_Location
                    SET [FK_FieldworkArea] = NULL
                    WHERE [FK_FieldworkArea] IN (
                        SELECT DISTINCT [IdToCheck] 
                        FROM To_Check
                    )
                    
                    UPDATE Station
                    SET [FK_FieldworkArea] = NULL
                    WHERE [FK_FieldworkArea] IN (
                        SELECT DISTINCT [IdToCheck] FROM To_Check
                    )

                    UPDATE MonitoredSitePosition
                    SET [FK_FieldworkArea] = NULL
                    WHERE [FK_FieldworkArea] IN (
                        SELECT DISTINCT [IdToCheck] 
                        FROM To_Check
                    )


                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY

                BEGIN CATCH
                    PRINT('Error during fieldwork areas update - Layer Difference')
                    PRINT(ERROR_MESSAGE())

                    IF @@TRANCOUNT > 0
                    BEGIN
                        PRINT 'Rollback'
                        ROLLBACK TRAN
                    END
                    ;THROW
                END CATCH
            END
        """
    )

    # mise à jour de la procedure import des données
    op.execute(
        """
            ALTER PROCEDURE sp_FA_TableFormatting
            -- ===================================================
            -- Formatting new fieldwork area table
            --
            -- This stored procedure creates the Fieldworkarea 
            -- update table with the new layers and columns in the
            -- right formats (geom, fullpath etc)
            -- ===================================================
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN

                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    DELETE FROM FieldworkArea
                    WHERE [ID] IN (
                        SELECT [eco_id]
                        FROM reneco_fieldworkarea
                    )

                    -- INSERT toutes les nouvelles données (NEW and UPDATED)
                    -- i.e., seulement celles qui ne sont pas inactivées
                    IF @Debug = 1
                    BEGIN
                        SELECT *
                        FROM Fieldworkarea F
                        JOIN reneco_fieldworkarea R ON R.[eco_id] = F.[ID]
                    END

                    SET IDENTITY_INSERT Fieldworkarea ON
                    INSERT INTO Fieldworkarea (
                        [ID], 
                        [Working_Area], 
                        [Working_Region], 
                        [Management_Unit], 
                        [Name], 
                        [fullpath], 
                        [type_], 
                        [max_lat], 
                        [min_lat], 
                        [max_lon],
                        [min_lon], 
                        [SHAPE_Leng], 
                        [SHAPE_Area], 
                        [geom], 
                        [valid_geom],
                        [activation_date],
                        [inactivation_date],
                        [last_change_attribut_date],
                        [last_change_geometry_date],
                        [Management_Status], 
                        [Communication_Name], 
                        [Center], 
                        [Data_Owner],
                        [Centroid_Latitude],
                        [Centroid_Longitude]
                    )
                    SELECT 
                        FirstPart.[ID],
                        [Working_Area],
                        [Working_Region],
                        [Management_Unit],
                        [Name],
                        [fullpath],
                        [type_],
                        [max_lat],
                        [min_lat],
                        [max_lon],
                        [min_lon],
                        [SHAPE_Leng],
                        [SHAPE_Area],
                        [geom],
                        [valid_geom],
                        [acti_d],
                        [inacti_d],
                        [attr_d],
                        [geom_d],
                        [mngt_statu],
                        [com_name],
                        [center],
                        [data_owner],
                        [centro_lat],
                        [centro_lon]
                    FROM (
                        SELECT
                            [eco_id] AS [ID],
                            [w_area] AS [Working_Area],
                            [w_region] AS [Working_Region],
                            [mngt_unit] AS [Management_Unit],
                            CASE 
                                WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                    THEN [mngt_unit]
                                WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                    THEN [w_region]
                                WHEN [w_area] IS NOT NULL
                                    THEN [w_area]
                                ELSE NULL
                            END AS [Name],
                            CASE 
                                WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                    THEN CONCAT([w_area],'>',[w_region],'>',[mngt_unit])
                                WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                    THEN CONCAT([w_area],'>',[w_region])
                                WHEN [w_area] IS NOT NULL
                                    THEN [w_area]
                                ELSE NULL
                            END AS [fullpath],
                            CASE 
                                WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                    THEN 'management unit'
                                WHEN [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                    THEN 'working region'
                                WHEN [w_area] IS NOT NULL
                                    THEN 'working area'
                                ELSE NULL
                            END AS [type_],
                            CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                            CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                            CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                            CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                            CONVERT(REAL,[SHAPE_Leng]) AS [SHAPE_Leng],
                            CONVERT(REAL,[SHAPE_Area])  AS [SHAPE_Area],
                            [acti_d],
                            [inacti_d],
                            [attr_d],
                            [geom_d],
                            [mngt_statu],
                            [com_name],
                            [center],
                            [data_owner],
                            [centro_lat],
                            [centro_lon]
                        FROM reneco_fieldworkarea
                        WHERE [inacti_d] IS NULL
                        GROUP BY [eco_id], [w_area], [w_region], [mngt_unit],[SHAPE_Leng], [SHAPE_Area], [acti_d], [inacti_d], [attr_d], [geom_d], [mngt_statu], [com_name], [center], [data_owner], [centro_lat], [centro_lon]
                    ) AS FirstPart
                    JOIN (
                        SELECT 
                            [eco_id] AS [ID],
                            [ogr_geometry] AS [geom],
                            [ogr_geometry].MakeValid() AS [valid_geom]
                        FROM reneco_fieldworkarea
                        WHERE [inacti_d] IS NULL
                    ) AS SecondPart
                    ON [FirstPart].[ID] = [SecondPart].[ID]
                    ORDER BY [Working_Area] ASC, [Working_Region] ASC, [Management_Unit] ASC

                    SET IDENTITY_INSERT Fieldworkarea OFF

                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY

                BEGIN CATCH
                    PRINT('Error during fieldwork areas update - Table Formatting')
                    PRINT(ERROR_MESSAGE())

                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                    END
                    ;THROW
                END CATCH
            END
        """
    )

    # mise à jour de la procédure qui calcule les intersections
    op.execute(
        """
            ALTER PROCEDURE sp_FA_Compute
            -- ===================================================
            -- Compute the new fieldwork area layer associated to
            -- each spatial point (individual location or station)
            -- which must be updated
            --
            -- This stored procedure updates the Individual_Location
            -- table and the Station table and provides
            -- ===================================================
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                -- For each individual location, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                BEGIN TRY
                BEGIN TRAN

                    -- Declare variable
                    DECLARE @flag INT = 1
                    WHILE @flag > 0
                    BEGIN
                        
                        -- Create temporary table temptable to store the new fieldwork area
                        -- associated to a spatial point
                        IF OBJECT_ID('tempdb..#temptableindivlocation') IS NOT NULL
                        BEGIN
                            DROP TABLE #temptableindivlocation
                        END

                        SELECT TOP(50000) 
                            IL.[ID] AS [ID],
                            ISNULL(
                                (SELECT TOP 1 F.[ID]
                                FROM Fieldworkarea AS F
                                WITH(INDEX(IX_Fieldworkarea_valid_geom))
                                WHERE  
                                    [min_lat] <= IL.[LAT] 
                                    AND [max_lat] >= IL.[LAT] 
                                    AND [min_lon] <= IL.[LON] 
                                    AND [max_lon] >= IL.[LON]  
                                    AND F.[valid_geom].STIntersects(geometry::Point(IL.[LON], IL.[LAT], 4326)) = 1
                                ORDER BY F.[fullpath] DESC), 
                                0
                            ) AS [FK_Fieldworkarea]
                        INTO #temptableindivlocation
                        FROM [Individual_Location] AS IL
                        WHERE 	
                            IL.[FK_FieldworkArea] IS NULL
                            AND IL.[LAT] IS NOT NULL
                            AND IL.[LON] IS NOT NULL

                        UPDATE IL
                        SET IL.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                        FROM [Individual_Location] AS IL
                        JOIN #temptableindivlocation AS TT ON IL.[ID] = TT.[ID]

                        SELECT @flag = @@ROWCOUNT
                    END

                    -- For each station, all the areas including it are found.
                    -- Only the more precise (based on fullpath) is kept
                    SELECT @flag=1;
                    WHILE @flag > 0
                    BEGIN
                        IF OBJECT_ID('tempdb..#temptablestation') IS NOT NULL
                        BEGIN
                            DROP TABLE #temptablestation
                        END

                        SELECT TOP(50000) 
                            S.[ID] AS [ID],
                            ISNULL(
                                (SELECT TOP 1 
                                    F.[ID]
                                FROM Fieldworkarea AS F
                                WITH(INDEX(IX_Fieldworkarea_valid_geom))
                                WHERE  [min_lat] <= S.[LAT] 
                                    AND [max_lat] >= S.[LAT] 
                                    AND [min_lon] <= S.[LON] 
                                    AND [max_lon] >= S.[LON]  
                                    AND F.[valid_geom].STIntersects(geometry::Point(S.[LON], S.[LAT], 4326)) = 1
                                ORDER BY F.[fullpath] DESC), 
                                0
                            ) AS [FK_Fieldworkarea]
                        INTO #temptablestation
                        FROM [Station] AS S
                        WHERE 	
                            S.[FK_FieldworkArea] IS NULL
                            AND S.LAT IS NOT NULL
                            AND S.LON IS NOT NULL

                        UPDATE S
                        SET S.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                        FROM [Station] AS S
                        JOIN #temptablestation AS TT ON S.[ID] = TT.[ID]

                        SELECT @flag = @@ROWCOUNT
                    END

                    -- For each MonitoredSitePosition
                    SELECT @flag=1;
                    WHILE @flag > 0
                    BEGIN
                        IF OBJECT_ID('tempdb..#temptableMonitoredSitePosition') IS NOT NULL
                        BEGIN
                            DROP TABLE #temptableMonitoredSitePosition
                        END

                        SELECT TOP(50000) 
                            MSP.[ID] AS [ID],
                            ISNULL(
                                (SELECT TOP 1 
                                    F.[ID]
                                FROM Fieldworkarea AS F
                                WITH(INDEX(IX_Fieldworkarea_valid_geom))
                                WHERE  [min_lat] <= MSP.[LAT] 
                                    AND [max_lat] >= MSP.[LAT] 
                                    AND [min_lon] <= MSP.[LON] 
                                    AND [max_lon] >= MSP.[LON]  
                                    AND F.[valid_geom].STIntersects(geometry::Point(MSP.[LON], MSP.[LAT], 4326)) = 1
                                ORDER BY F.[fullpath] DESC), 
                                0
                            ) AS [FK_Fieldworkarea]
                        INTO #temptableMonitoredSitePosition
                        FROM [MonitoredSitePosition] AS MSP
                        WHERE 	
                            MSP.[FK_FieldworkArea] IS NULL
                            AND MSP.LAT IS NOT NULL
                            AND MSP.LON IS NOT NULL

                        UPDATE MSP
                        SET MSP.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                        FROM [MonitoredSitePosition] AS MSP
                        JOIN #temptableMonitoredSitePosition AS TT ON MSP.[ID] = TT.[ID]

                        SELECT @flag = @@ROWCOUNT
                    END

                    -- clean
                    IF OBJECT_ID('tempdb..#temptableindivlocation') IS NOT NULL
                    BEGIN
                        DROP TABLE #temptableindivlocation
                    END

                    IF OBJECT_ID('tempdb..#temptablestation') IS NOT NULL
                    BEGIN
                        DROP TABLE #temptablestation
                    END

                    IF OBJECT_ID('tempdb..#temptableMonitoredSitePosition') IS NOT NULL
                    BEGIN
                        DROP TABLE #temptableMonitoredSitePosition
                    END
                    
                    --
                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY

                BEGIN CATCH
                    PRINT('Error during fieldwork areas update - Compute')
                    PRINT(ERROR_MESSAGE())

                    IF @@TRANCOUNT > 0
                    BEGIN
                        ROLLBACK TRAN
                    END
                    ;THROW
                END CATCH
            END
        """
    )
    
    # mise à jour de la procédure principale
    op.execute(
        """
            ALTER PROCEDURE sp_FA_Update
            -- ===================================================
            -- Compute the new fieldwork area layer associated to
            -- each spatial point (individual location or station)
            -- which must be updated
            --
            -- This stored procedure updates the Individual_Location
            -- table and the Station table and provides
            -- ===================================================
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    DECLARE	@return_value INT

                    -- Remove Foreign Keys
                    IF OBJECT_ID('FK_Individual_Location_FK_FieldworkArea_Fieldworkarea') IS NOT NULL
                    BEGIN
                        ALTER TABLE Individual_Location
                        DROP CONSTRAINT FK_Individual_Location_FK_FieldworkArea_Fieldworkarea
                    END

                    IF OBJECT_ID('FK_Station_FK_FieldworkArea_Fieldworkarea') IS NOT NULL
                    BEGIN
                        ALTER TABLE Station
                        DROP CONSTRAINT FK_Station_FK_FieldworkArea_Fieldworkarea
                    END

                    IF OBJECT_ID('FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea') IS NOT NULL
                    BEGIN
                        ALTER TABLE MonitoredSitePosition
                        DROP CONSTRAINT FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea
                    END

                    -- Run procedures
                    EXEC @return_value = [dbo].[sp_FA_LayerDifference] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_LayerDifference' = @return_value

                    EXEC @return_value = [dbo].[sp_FA_TableFormatting] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_TableFormatting' = @return_value

                    EXEC @return_value = [dbo].[sp_FA_Compute] @Debug = 0, @ForceRollback = 0
                    SELECT 'sp_FA_Compute' = @return_value

                    -- Add Foreign Keys
                    ALTER TABLE Individual_Location WITH NOCHECK 
                    ADD CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_Fieldworkarea] FOREIGN KEY([FK_FieldworkArea])
                    REFERENCES Fieldworkarea ([ID])
                    NOT FOR REPLICATION 

                    -- ALTER TABLE Individual_Location
                    -- CHECK CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_Fieldworkarea]

                    ALTER TABLE Station WITH NOCHECK 
                    ADD CONSTRAINT [FK_Station_FK_FieldworkArea_Fieldworkarea] 
                    FOREIGN KEY([FK_FieldworkArea]) 
                    REFERENCES Fieldworkarea ([ID])
                    NOT FOR REPLICATION 

                    -- ALTER TABLE Station
                    -- CHECK CONSTRAINT [FK_Station_FK_FieldworkArea_Fieldworkarea]

                    ALTER TABLE MonitoredSitePosition WITH NOCHECK 
                    ADD CONSTRAINT [FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea] 
                    FOREIGN KEY([FK_FieldworkArea]) 
                    REFERENCES Fieldworkarea ([ID])
                    NOT FOR REPLICATION 

                    -- ALTER TABLE MonitoredSitePosition 
                    -- CHECK CONSTRAINT [FK_MonitoredSitePosition_FK_FieldworkArea_Fieldworkarea]

                    -- delete reneco Fieldwork area table
                    DROP TABLE reneco_fieldworkarea

                    -- delete gdal table
                    DROP TABLE spatial_ref_sys

                    -- delete gdal table
                    DROP TABLE geometry_columns

                    -- delete To_Check table
                    DROP TABLE To_Check

                    -- manage debug/rollback mode
                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY

                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
        """
    )

    # creation de l'index spatial pour les admin area
    op.execute(
        """
            IF NOT EXISTS (
                SELECT * 
                FROM sys.indexes 
                WHERE name='IX_AdministrativeArea_valid_geom' AND object_id = OBJECT_ID('[dbo].[AdministrativeArea]')
            )
            BEGIN
                CREATE SPATIAL INDEX [IX_AdministrativeArea_valid_geom] ON [dbo].[AdministrativeArea]
                (
                    [valid_geom]
                )USING  GEOMETRY_GRID
                WITH (BOUNDING_BOX =(-90, -180, 90, 180), GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
                CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            END
        """
    )


def downgrade(**options_for_migration) -> None:

    op.execute(
        """
            ALTER TABLE FieldworkArea
            DROP COLUMN [last_change_geom_date], [last_change_attr_date], [activation_date], [inactivation_date]

            ALTER TABLE FieldworkArea
            ADD [Status] VARCHAR(255), [Country] NVARCHAR(255)
            
            UPDATE FieldworkArea
            SET [Status] = 'current'
        """
    )

    op.execute(
        """
            IF EXISTS (
                SELECT * 
                FROM sys.indexes 
                WHERE name='IX_AdministrativeArea_valid_geom' AND object_id = OBJECT_ID('[dbo].[AdministrativeArea]')
            )
            BEGIN
                DROP INDEX [IX_AdministrativeArea_valid_geom] ON [dbo].[AdministrativeArea]
            END
        """
    )

    op.execute(
        """
            ALTER PROCEDURE [dbo].[sp_FA_LayerDifference]
                -- ==================================================
                -- Difference between old (year n-1) and new (year n)
                -- spatial fieldwork area layers
                --
                -- This stored procedure checks for spatial fieldwork
                -- area layer changes between years n-1 and n.
                -- When difference, FK_Fieldworkarea attribute is set
                -- to Null in the Individual_Location and Station
                -- tables
                -- ==================================================
            AS
            BEGIN

                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;

                DECLARE @NbFA INTEGER
                DECLARE @NbTour INTEGER = 0 
                DECLARE @FA_ID VARCHAR(500)
                DECLARE @current_status VARCHAR(500)
                DECLARE @old_status VARCHAR(500)
                DECLARE @type VARCHAR(500)
                DECLARE @country VARCHAR(500)
                DECLARE @w_area VARCHAR(500)
                DECLARE @w_region VARCHAR(500)
                DECLARE @mngt_unit VARCHAR(500)
                DECLARE @IdToReset INTEGER
                DECLARE @flag INTEGER

                ---------------------------------------------------------------

                -- Create temporary FA_update table: contain the fielwork area that
                -- have been updated (@current_status <> @old_status)
                IF OBJECT_ID('tempdb..#FA_update') IS NOT NULL DROP TABLE #FA_update
                CREATE TABLE #FA_update (
                    [eco_id] integer,
                    [type] VARCHAR(MAX),
                    [current_st] VARCHAR(MAX),
                    [old_status] VARCHAR(MAX),
                    [country] VARCHAR(MAX),
                    [w_area] VARCHAR(MAX),
                    [w_region] VARCHAR(MAX),
                    [mngt_unit] VARCHAR(MAX),
                    [IdToReset] integer
                )

                -- ToCheck Table: store the ID of the Fielwork areas that may be
                -- updated at the studied points (individual locations or stations)
                IF OBJECT_ID('dbo.To_Check') IS NOT NULL DROP TABLE dbo.To_Check
                CREATE TABLE [dbo].[To_Check] ([IdToCheck] integer NOT NULL )


                -- Compare old (year n-1) and current (year n) fieldwork area
                -- status and select those that need to be updated (i.e. filter
                -- the layers current at n-1 but obsolete at n or those that did
                -- not exist at n-1 but do at n). They are inserted in the  
                -- temporary table FA_UPDATE.
                            
                INSERT INTO #FA_update (
                    [eco_id]
                    ,[type]
                    ,[current_st]
                    ,[old_status]
                    ,[country]
                    ,[w_area]
                    ,[w_region]
                    ,[mngt_unit]
                )
                SELECT
                    NFA.[eco_id]
                    ,NFA.[type_area]
                    ,NFA.[current_st]
                    ,PFA.[Status]
                    ,NFA.[country]
                    ,NFA.[w_area]
                    ,NFA.[w_region]
                    ,NFA.[mngt_unit]   
                FROM reneco_fieldworkarea AS NFA -- new fieldwork area
                            
                LEFT JOIN Fieldworkarea AS PFA -- past fieldwork area

                ON NFA.eco_id = PFA.ID 
                WHERE
                    (NFA.current_st = 'obsolete' AND PFA.[Status] = 'current') 
                    OR (PFA.[Status] is NULL)


                -- Iteration on the temporary table FA_UPDATE
                -- For each updated fieldwork area: if the FA is a country, then 
                -- IdToReset is set to zero, else IdToReset is set as equal to the
                -- ID of the previous layer (i.e. layer containing it such as:
                -- management unit ⊂ working region ⊂ working area ⊂ country)

                SELECT @NbFA = count(eco_id) 
                FROM #FA_update

                WHILE @NbTour < @NbFA
                BEGIN

                    -- select the first FA
                    SELECT TOP 1
                        @FA_ID = [eco_id],
                        @type = [type],
                        @current_status = [current_st],
                        @old_status = [old_status],
                        @country = [country],
                        @w_area = [w_area],
                        @w_region = [w_region],
                        @mngt_unit = [mngt_unit]
                    FROM #FA_update
                            
                            
                    BEGIN
                        BEGIN TRY
                                    
                            INSERT INTO [To_Check] (IdToCheck) VALUES (@FA_ID)

                            -- Depending on FA spatial layer, 
                            IF @type = 'country'
                            BEGIN
                                SET @IdToReset = 0 -- country change: insert 0
                            END

                            ELSE
                                IF @type = 'working area'
                                BEGIN	
                                    -- working_area change will insert
                                    SET @IdToReset = (
                                        SELECT ID
                                        FROM Fieldworkarea
                                        WHERE [type_] = 'country' 
                                            AND [Country] = @country 
                                            AND [Status] = 'current'
                                    )
                                END

                                ELSE
                                    IF @type = 'working region'
                                    BEGIN
                                        -- working_region change will insert
                                        SET @IdToReset = (
                                            SELECT ID 
                                            FROM Fieldworkarea
                                            WHERE [type_] = 'working area' 
                                                AND [Working_Area] = @w_area 
                                                AND [Status] = 'current'
                                        )
                                        IF @IdToReset IS NULL 
                                        BEGIN
                                            SET @IdToReset = (
                                                SELECT ID
                                                FROM Fieldworkarea
                                                WHERE [type_] = 'country' 
                                                    AND [Country] = @country 
                                                    AND [Status] = 'current'
                                            )
                                        END
                                    END

                                    ELSE -- @type = 'management unit'
                                    BEGIN
                                        -- management unit change will insert
                                        SET @IdToReset = (
                                            SELECT ID
                                            FROM Fieldworkarea
                                            WHERE [type_] = 'working region' 
                                                AND [Working_Region] = @w_region 
                                                AND [Status] = 'current'
                                        )
                                        IF @IdToReset IS NULL 
                                        BEGIN
                                            SET @IdToReset = (
                                                SELECT ID
                                                FROM Fieldworkarea
                                                WHERE [type_] = 'working area' 
                                                    AND [Working_Area] = @w_area 
                                                    AND [Status] = 'current'
                                            )
                                            IF @IdToReset IS NULL
                                            BEGIN
                                                SET @IdToReset = (
                                                    SELECT ID
                                                    FROM Fieldworkarea
                                                    WHERE [type_] = 'country' 
                                                        AND [Country] = @country 
                                                        AND [Status] = 'current'
                                                )
                                            END
                                        END
                                    END 
                            INSERT INTO [To_Check] (IdToCheck) VALUES (@IdToReset)
                                    
                        END TRY

                        BEGIN CATCH
                            PRINT('Error: ' + CAST(@FA_ID as VARCHAR))
                            PRINT(ERROR_MESSAGE())
                        END CATCH
                                    
                        -- remove the processed fieldwork area
                        DELETE FROM #FA_update
                        WHERE eco_id =  @FA_ID

                        SET @NbTour = @NbTour + 1
                    END
                END

                BEGIN TRY
                    SELECT @flag = 1

                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION

                        UPDATE Individual_Location
                        SET FK_FieldworkArea = NULL
                        WHERE FK_FieldworkArea IN (
                            SELECT DISTINCT [IdToCheck] FROM [To_Check]
                        )

                        SELECT @flag = @@ROWCOUNT
                        COMMIT TRANSACTION
                    END
                END TRY

                BEGIN CATCH
                    Print('Error')
                    print(ERROR_MESSAGE())
                END CATCH


                BEGIN TRY
                    SELECT @flag=1;
                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION

                        UPDATE Station
                        SET FK_FieldworkArea = NULL
                        WHERE FK_FieldworkArea IN (SELECT DISTINCT [IdToCheck] FROM [To_Check])

                        SELECT @flag = @@ROWCOUNT
                        COMMIT TRANSACTION
                    END
                END TRY
                BEGIN CATCH
                    PRINT('Error')
                    PRINT(ERROR_MESSAGE())
                END CATCH
            END
        """
    )

    op.execute(
        """
            ALTER PROCEDURE [dbo].[sp_FA_TableFormatting]
                -- ===================================================
                -- Formatting new fieldwork area table
                --
                -- This stored procedure creates the Fieldworkarea 
                -- update table with the new layers and columns in the
                -- right formats (geom, fullpath etc)
                -- ===================================================
            AS
            BEGIN

                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;

                --
                IF OBJECT_ID('Fieldworkarea_update') IS NOT NULL DROP TABLE Fieldworkarea_update
                CREATE TABLE Fieldworkarea_update( 	
                    [ID] [int] IDENTITY(1,1) primary key NOT NULL,
                    [Country] [nvarchar](255) NULL,
                    [Working_Area] [varchar](255) NULL,
                    [Working_Region] [nvarchar](255) NULL,
                    [Management_Unit] [varchar](255) NULL,
                    [Name] [varchar](255) NULL,
                    [fullpath] [varchar](255) NULL,
                    [type_] [varchar](50) NULL,
                    [max_lat] [decimal](9, 5) NULL,
                    [min_lat] [decimal](9, 5) NULL,
                    [max_lon] [decimal](9, 5) NULL,
                    [min_lon] [decimal](9, 5) NULL,
                    [SHAPE_Leng] [real] NULL,
                    [SHAPE_Area] [real] NULL,
                    [valid_geom] [geometry] NULL,
                    [geom] [geometry] NULL,
                    [Status] [varchar](255) NULL,
                    [Management_Status] [varchar](255) NULL,
                    [Communication_Name] [varchar](255) NULL,
                    [Center] [varchar](255) NULL,
                    [Data_Owner] [varchar](255) NULL,
                    [sponsor] [varchar](255) NULL,
                    [Centroid_Latitude] [DECIMAL](9,5) NULL,
                    [Centroid_Longitude] [DECIMAL](9,5) NULL
                )

                --
                DECLARE @tableFieldworAreaTemp TABLE( 	
                    [ID] [int] NOT NULL,
                    [Country] [nvarchar](255) NULL,
                    [Working_Area] [varchar](255) NULL,
                    [Working_Region] [nvarchar](255) NULL,
                    [Management_Unit] [varchar](255) NULL,
                    [Name] [varchar](255) NULL,
                    [fullpath] [varchar](255) NULL,
                    [type_] [varchar](50) NULL,
                    [max_lat] [decimal](9, 5) NULL,
                    [min_lat] [decimal](9, 5) NULL,
                    [max_lon] [decimal](9, 5) NULL,
                    [min_lon] [decimal](9, 5) NULL,
                    [SHAPE_Leng] [real] NULL,
                    [SHAPE_Area] [real] NULL,
                    [geom] [geometry] NULL,
                    [valid_geom] [geometry] NULL,
                    [current_st] [varchar](255) NULL,
                    [mngt_statu] [varchar](255) NULL,
                    [com_name] [varchar](255) NULL,
                    [center] [varchar](255) NULL,
                    [data_owner] [varchar](255) NULL,
                    [sponsor] [varchar](255) NULL,
                    [Centroid_Latitude] [DECIMAL](9,5) NULL,
                    [Centroid_Longitude] [DECIMAL](9,5) NULL
                )

                --
                INSERT INTO @tableFieldworAreaTemp (
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [current_st], [mngt_statu], [com_name],
                    [center], [data_owner]
                )
                SELECT 
                    [FirstPart].[ID],
                    [Country],
                    [Working_Area],
                    [Working_Region],
                    [Management_Unit],
                    [Name],
                    [fullpath],
                    [type_],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [SHAPE_Leng],
                    [SHAPE_Area],
                    [geom],
                    [valid_geom],
                    [current_st],
                    [mngt_statu],
                    [com_name],
                    [center],
                    [data_owner]
                FROM (
                    SELECT
                        [eco_id] AS [ID],
                        [country] AS [Country],
                        [w_area] AS [Working_Area],
                        [w_region] AS [Working_Region],
                        [mngt_unit] AS [Management_Unit],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN [mngt_unit]
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN [w_region]
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN [w_area]
                            WHEN [country] IS NOT NULL
                                THEN [country]
                            ELSE NULL
                        END AS [Name],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area],'>',[w_region],'>',[mngt_unit])
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area],'>',[w_region])
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area])
                            WHEN [country] IS NOT NULL
                                THEN [country]
                            ELSE NULL
                        END AS [fullpath],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN 'management unit'
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN 'working region'
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN 'working area'
                            WHEN [country] IS NOT NULL
                                THEN 'country'
                            ELSE NULL
                        END AS [type_],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                        CONVERT(REAL,[SHAPE_Leng]) AS [SHAPE_Leng],
                        CONVERT(REAL,[SHAPE_Area])  AS [SHAPE_Area],
                        [current_st],
                        [mngt_statu],
                        [com_name],
                        [center],
                        [data_owner]
                    FROM reneco_fieldworkarea
                    GROUP BY [eco_id], [country], [w_area], [w_region], [mngt_unit],[SHAPE_Leng], [SHAPE_Area], [current_st], [mngt_statu], [com_name], [center], [data_owner]
                ) AS FirstPart
                JOIN (
                    SELECT 
                        [eco_id] AS [ID],
                        [ogr_geometry] AS [geom],
                        [ogr_geometry].MakeValid() AS [valid_geom]
                    FROM reneco_fieldworkarea
                ) AS SecondPart
                ON [FirstPart].[ID] = [SecondPart].[ID]
                ORDER BY [country] ASC, [Working_Area] ASC, [Working_Region] ASC, [Management_Unit] ASC

                SET IDENTITY_INSERT Fieldworkarea_update ON;

                INSERT INTO Fieldworkarea_update (
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [Status], [Management_Status], [Communication_Name], [Center], [Data_Owner]
                )
                SELECT
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [current_st], [mngt_statu], [com_name], 
                    [center], [data_owner]
                FROM @tableFieldworAreaTemp

                -- Insert explicit values in the identity column is prohibited
                SET IDENTITY_INSERT Fieldworkarea_update OFF


                IF EXISTS (
                    SELECT * 
                    FROM sys.indexes 
                    WHERE name = 'IX_Fieldworkarea_valid_geom' AND object_id = OBJECT_ID('FieldworkArea')
                ) DROP INDEX [IX_Fieldworkarea_valid_geom] ON [FieldworkArea]

                CREATE SPATIAL INDEX [IX_Fieldworkarea_valid_geom] ON [FieldworkArea_update] (
                [valid_geom]
                ) USING  GEOMETRY_GRID 
                WITH (BOUNDING_BOX =(-90, -180, 90, 180), GRIDS = (LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
                CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            END
        """
    )

    op.execute(
        """
            ALTER PROCEDURE [dbo].[sp_FA_Compute]
                -- ===================================================
                -- Compute the new fieldwork area layer associated to
                -- each spatial point (individual location or station)
                -- which must be updated
                --
                -- This stored procedure updates the Individual_Location
                -- table and the Station table and provides
                -- ===================================================
            AS
            BEGIN

                SET NOCOUNT ON;
                
                -- Declare variable
                DECLARE @flag INT = 0
                
                -- For each individual location, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                BEGIN TRY
                    SELECT @flag = 1;
                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION
                            -- Create temporary table temptable to store the new fieldwork area
                            -- associated to a spatial point
                            IF OBJECT_ID('tempdb..#temptableindivlocation') IS NOT NULL
                            BEGIN
                                DROP TABLE #temptableindivlocation
                            END

                            SELECT TOP(50000) 
                                IL.[ID] AS [ID],
                                ISNULL(
                                    (
                                        SELECT TOP 1 F.[ID]
                                        FROM Fieldworkarea_update AS F
                                        WITH(INDEX(IX_Fieldworkarea_valid_geom))
                                        WHERE  
                                            F.[min_lat] <= IL.[LAT]
                                            AND F.[max_lat] >= IL.[LAT]
                                            AND F.[min_lon] <= IL.[LON] 
                                            AND F.[max_lon] >= IL.[LON]  
                                            AND F.[valid_geom].STIntersects(geometry::Point(IL.[LON], IL.[LAT], 4326)) = 1
                                            AND F.[Status] = 'current'
                                        ORDER BY F.[fullpath] DESC
                                    ), 
                                    0
                                ) AS [FK_Fieldworkarea]
                            INTO #temptableindivlocation
                            FROM Individual_Location AS IL
                            WHERE
                                IL.[FK_FieldworkArea] IS NULL
                                AND IL.[LAT] IS NOT NULL
                                AND IL.[LON] IS NOT NULL

                            UPDATE IL
                            SET IL.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                            FROM Individual_Location AS IL
                            JOIN #temptableindivlocation AS TT ON IL.[ID] = TT.[ID]

                            SELECT @flag = @@ROWCOUNT

                        COMMIT TRANSACTION
                    END
                END TRY

                BEGIN CATCH
                    Print('Error during fieldwork areas update')
                    print(ERROR_MESSAGE())
                END CATCH

                -- For each station, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                BEGIN TRY
                    SELECT @flag=1;
                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION
                            -- Create temporary table temptable to store the new fieldwork area
                            -- associated to a spatial point
                            IF OBJECT_ID('tempdb..#temptablestation') IS NOT NULL
                            BEGIN
                                DROP TABLE #temptablestation
                            END

                            SELECT TOP(50000) S.[ID] AS [ID],
                                ISNULL(
                                    (
                                        SELECT TOP 1 F.[ID]
                                        FROM Fieldworkarea_update AS F
                                        WITH(INDEX(IX_Fieldworkarea_valid_geom))
                                        WHERE  
                                            F.[min_lat] <= S.[LAT] 
                                            AND F.[max_lat] >= S.[LAT] 
                                            AND F.[min_lon] <= S.[LON]
                                            AND F.[max_lon] >= S.[LON]  
                                            AND F.[valid_geom].STIntersects(geometry::Point(S.[LON], S.[LAT], 4326)) = 1
                                            AND F.[Status] = 'current'
                                        ORDER BY F.[fullpath] DESC
                                    ), 
                                    0
                                ) AS [FK_Fieldworkarea]
                            INTO #temptablestation
                            FROM Station AS S
                            WHERE 	
                                S.[FK_FieldworkArea] IS NULL
                                AND S.[LAT] IS NOT NULL
                                AND S.[LON] IS NOT NULL

                            UPDATE S
                            SET S.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                            FROM Station AS S
                            JOIN #temptablestation AS TT ON S.[ID] = TT.[ID]

                            SELECT @flag = @@ROWCOUNT

                        COMMIT TRANSACTION
                    END
                END TRY

                BEGIN CATCH
                    Print('Error during fieldwork areas update')
                    print(ERROR_MESSAGE())
                END CATCH
            END
        """
    )

    op.execute(
            """
                ALTER PROCEDURE [dbo].[sp_FA_Update]
					-- ===================================================
					-- Compute the new fieldwork area layer associated to
					-- each spatial point (individual location or station)
					-- which must be updated
					--
					-- This stored procedure updates the Individual_Location
					-- table and the Station table and provides
					-- ===================================================
				AS
				BEGIN

					DECLARE	@return_value int

					-- Remove Foreign Keys
					ALTER TABLE [dbo].[Individual_Location] DROP CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_Fieldworkarea]
					ALTER TABLE [dbo].[Station] DROP CONSTRAINT [FK_Station_FK_FieldworkArea_Fieldworkarea]

					-- Run procedures
					EXEC	@return_value = [dbo].[sp_FA_LayerDifference]
					SELECT	'Return Value' = @return_value

					EXEC	@return_value = [dbo].[sp_FA_TableFormatting]
					SELECT	'Return Value' = @return_value

					EXEC	@return_value = [dbo].[sp_FA_Compute]
					SELECT	'Return Value' = @return_value

					-- delete previous (n-1) Fieldwork area table
					DROP TABLE Fieldworkarea

					-- rename the new fieldwork area table
					EXEC sp_rename @objname='Fieldworkarea_update', @newname='Fieldworkarea'

					-- Add Foreign Keys
					ALTER TABLE [dbo].[Individual_Location]  WITH NOCHECK ADD  CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_Fieldworkarea] FOREIGN KEY([FK_FieldworkArea])
					REFERENCES [dbo].[Fieldworkarea] ([ID])
					NOT FOR REPLICATION 

					ALTER TABLE [dbo].[Individual_Location] CHECK CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_Fieldworkarea]

					ALTER TABLE [dbo].[Station]  WITH NOCHECK ADD  CONSTRAINT [FK_Station_FK_FieldworkArea_Fieldworkarea] FOREIGN KEY([FK_FieldworkArea]) 
					REFERENCES [dbo].[Fieldworkarea] ([ID])
					NOT FOR REPLICATION 

					ALTER TABLE [dbo].[Station] CHECK CONSTRAINT [FK_Station_FK_FieldworkArea_Fieldworkarea]

					-- delete reneco Fieldwork area table
					DROP TABLE reneco_fieldworkarea

					-- delete gdal table
					DROP TABLE spatial_ref_sys

					-- delete gdal table
					DROP TABLE geometry_columns

					-- delete To_Check table
					DROP TABLE To_Check
				END
            """
        )