"""add country to station grid

Revision ID: e77a69510884
Revises: c067c84dd99b
Create Date: 2024-10-14 09:38:37.005132

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e77a69510884'
down_revision = 'c067c84dd99b'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @Module_ID INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'StationGrid'
            )

            INSERT INTO ModuleGrids (
                [Module_ID],
                [TypeObj],
                [Name],
                [Label],
                [GridRender],
                [GridSize],
                [CellType],
                [GridOrder],
                [QueryName],
                [Options],
                [FilterOrder],
                [FilterSize],
                [FilterClass],
                [IsSearchable],
                [FilterDefaultValue],
                [FilterRender],
                [FilterType],
                [Status],
                [ColumnParams]
            )
            VALUES (@Module_ID, NULL, 'AdministrativeArea@Country', 'Country', 2, '{"width"\:120,"maxWidth"\:350,"minWidth"\:100}', 'string', 56, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL)
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @Module_ID INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'StationGrid'
            )

            DELETE FROM ModuleGrids
            WHERE Module_ID = @Module_ID
                AND Label = 'Country'
            """
        )
