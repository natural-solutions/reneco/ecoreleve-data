"""Correct the legends in the forms

Revision ID: 3bff4ec7031c
Revises: 6db985a63cb6
Create Date: 2022-10-20 17:29:11.983020

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3bff4ec7031c'
down_revision = '6db985a63cb6'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
            UPDATE ModuleForms
            SET Legend = 'General Information'
            WHERE Legend LIKE 'General Infos'

            UPDATE ModuleForms
            SET Legend = 'Detailed Information'
            WHERE Legend LIKE 'Detailled Infos'

            UPDATE ModuleForms
            SET Legend = 'Location Information'
            WHERE Legend LIKE 'Location Infos'

            UPDATE ModuleForms
            SET Legend = 'Individual Information'
            WHERE Legend LIKE 'Individual Infos'
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
            UPDATE ModuleForms
            SET Legend = 'General Infos'
            WHERE Legend LIKE 'General Information'

            UPDATE ModuleForms
            SET Legend = 'Detailled Infos'
            WHERE Legend LIKE 'Detailed Information'

            UPDATE ModuleForms
            SET Legend = 'Location Infos'
            WHERE Legend LIKE 'Location Information'

            UPDATE ModuleForms
            SET Legend = 'Individual Infos'
            WHERE Legend LIKE 'Individual Information'
        """
    )
