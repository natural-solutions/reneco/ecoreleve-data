"""add beginning and end of activity for VHF

Revision ID: dc2a3368ef7b
Revises: acde60b00edd
Create Date: 2024-12-04 16:28:44.378482

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dc2a3368ef7b'
down_revision = 'acde60b00edd'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        DECLARE @module_id INT = (
            SELECT [ID]
            FROM FrontModules
            WHERE [Name] = 'SensorForm'
        )

        DECLARE @SensorDynProp_Frequency INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
        )

        DECLARE @SensorType_VHF INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'VHF'
        )
        IF NOT EXISTS ( SELECT 1 FROM [SensorType_SensorDynProp] WHERE [FK_SensorType] = @SensorType_VHF  AND [FK_SensorDynProp] = @SensorDynProp_Frequency)
        BEGIN
            INSERT INTO SensorType_SensorDynProp ([Required],[FK_SensorType],[FK_SensorDynProp])
            VALUES (0, @SensorType_VHF, @SensorDynProp_Frequency)
        END

        -- add link between sensor type and dynamic property start/end activity
        DECLARE @SensorDynProp_Begining INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE Name = 'Begining_Activity'
        )

        DECLARE @SensorDynProp_End INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE Name = 'End_Activity'
        )

        INSERT INTO SensorType_SensorDynProp ([Required],[FK_SensorType],[FK_SensorDynProp])
        VALUES 
            (0, @SensorType_VHF, @SensorDynProp_Begining),
            (0, @SensorType_VHF, @SensorDynProp_End)
        """
    )

    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            DECLARE @SensorType_VHF INT = (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] = 'VHF'
            )

            -- update form display in configuration
            UPDATE ModuleForms
            SET FormRender = 39
            WHERE module_id = @module_id AND [Name] = 'InitialLivespan' AND [TypeObj] IN (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] = 'VHF'
            )

            -- add configuration for start and end activity time
            INSERT INTO ModuleForms
            VALUES
                (@module_id, @SensorType_VHF, 'Begining_Activity',	'Beginning of activity', 0, 6, 6, 'DateTimePickerEditor', 'form-control', 39, 37, NULL, '{"format":"HH:mm:ss"}', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL),
                (@module_id, @SensorType_VHF, 'End_Activity',	'End of activity', 0, 6, 6, 'DateTimePickerEditor', 'form-control', 7, 38, NULL, '{"format":"HH:mm:ss"}', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL)
            """
        )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        -- delete values from database
        DELETE FROM SensorDynPropValue
        FROM SensorDynPropValue AS SDPV
        JOIN Sensor AS S ON S.[ID] = SDPV.[FK_Sensor]
        WHERE [FK_SensorDynProp] IN (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] IN ('Begining_Activity', 'End_Activity')
        ) AND S.[FK_SensorType] IN (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'VHF'
        )

        -- delete relationship between sensor type and dynamic properties
        DELETE FROM SensorType_SensorDynProp
        WHERE [FK_SensorDynProp] IN (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] IN ('Begining_Activity', 'End_Activity')
        ) AND [FK_SensorType] IN (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'VHF'
        )
        """
    )

    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            -- remove from configuration
            DECLARE @module_id INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE Name = 'SensorForm'
            )

            UPDATE ModuleForms
            SET FormRender = 7
            WHERE [module_id] = @module_id AND [Name] = 'InitialLivespan' AND [TypeObj] IN (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] = 'VHF'
            )

            DELETE FROM ModuleForms
            WHERE
                [module_id] = @module_id 
                AND [Name] IN ('Begining_Activity', 'End_Activity')
                AND TypeObj IN (
                    SELECT [ID]
                    FROM SensorType
                    WHERE [Name] = 'VHF'
                )
            """
        )
