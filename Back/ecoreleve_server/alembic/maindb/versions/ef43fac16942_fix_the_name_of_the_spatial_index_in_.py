"""fix the name of the spatial index in Fieldwork area process

Revision ID: ef43fac16942
Revises: 2c818db11faf
Create Date: 2022-12-23 10:36:30.707076

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ef43fac16942'
down_revision = '2c818db11faf'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
            ALTER PROCEDURE [dbo].[sp_FA_Compute]
                -- ===================================================
                -- Compute the new fieldwork area layer associated to
                -- each spatial point (individual location or station)
                -- which must be updated
                --
                -- This stored procedure updates the Individual_Location
                -- table and the Station table and provides
                -- ===================================================
            AS
            BEGIN

                SET NOCOUNT ON;
                
                -- Declare variable
                DECLARE @flag INT = 0
                
                -- For each individual location, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                BEGIN TRY
                    SELECT @flag = 1;
                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION
                            -- Create temporary table temptable to store the new fieldwork area
                            -- associated to a spatial point
                            IF OBJECT_ID('tempdb..#temptableindivlocation') IS NOT NULL
                            BEGIN
                                DROP TABLE #temptableindivlocation
                            END

                            SELECT TOP(50000) 
                                IL.[ID] AS [ID],
                                ISNULL(
                                    (
                                        SELECT TOP 1 F.[ID]
                                        FROM Fieldworkarea_update AS F
                                        WITH(INDEX(IX_Fieldworkarea_valid_geom))
                                        WHERE  
                                            F.[min_lat] <= IL.[LAT]
                                            AND F.[max_lat] >= IL.[LAT]
                                            AND F.[min_lon] <= IL.[LON] 
                                            AND F.[max_lon] >= IL.[LON]  
                                            AND F.[valid_geom].STIntersects(geometry::Point(IL.[LON], IL.[LAT], 4326)) = 1
                                            AND F.[Status] = 'current'
                                        ORDER BY F.[fullpath] DESC
                                    ), 
                                    0
                                ) AS [FK_Fieldworkarea]
                            INTO #temptableindivlocation
                            FROM Individual_Location AS IL
                            WHERE
                                IL.[FK_FieldworkArea] IS NULL
                                AND IL.[LAT] IS NOT NULL
                                AND IL.[LON] IS NOT NULL

                            UPDATE IL
                            SET IL.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                            FROM Individual_Location AS IL
                            JOIN #temptableindivlocation AS TT ON IL.[ID] = TT.[ID]

                            SELECT @flag = @@ROWCOUNT

                        COMMIT TRANSACTION
                    END
                END TRY

                BEGIN CATCH
                    Print('Error during fieldwork areas update')
                    print(ERROR_MESSAGE())
                END CATCH

                -- For each station, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                BEGIN TRY
                    SELECT @flag=1;
                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION
                            -- Create temporary table temptable to store the new fieldwork area
                            -- associated to a spatial point
                            IF OBJECT_ID('tempdb..#temptablestation') IS NOT NULL
                            BEGIN
                                DROP TABLE #temptablestation
                            END

                            SELECT TOP(50000) S.[ID] AS [ID],
                                ISNULL(
                                    (
                                        SELECT TOP 1 F.[ID]
                                        FROM Fieldworkarea_update AS F
                                        WITH(INDEX(IX_Fieldworkarea_valid_geom))
                                        WHERE  
                                            F.[min_lat] <= S.[LAT] 
                                            AND F.[max_lat] >= S.[LAT] 
                                            AND F.[min_lon] <= S.[LON]
                                            AND F.[max_lon] >= S.[LON]  
                                            AND F.[valid_geom].STIntersects(geometry::Point(S.[LON], S.[LAT], 4326)) = 1
                                            AND F.[Status] = 'current'
                                        ORDER BY F.[fullpath] DESC
                                    ), 
                                    0
                                ) AS [FK_Fieldworkarea]
                            INTO #temptablestation
                            FROM Station AS S
                            WHERE 	
                                S.[FK_FieldworkArea] IS NULL
                                AND S.[LAT] IS NOT NULL
                                AND S.[LON] IS NOT NULL

                            UPDATE S
                            SET S.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                            FROM Station AS S
                            JOIN #temptablestation AS TT ON S.[ID] = TT.[ID]

                            SELECT @flag = @@ROWCOUNT

                        COMMIT TRANSACTION
                    END
                END TRY

                BEGIN CATCH
                    Print('Error during fieldwork areas update')
                    print(ERROR_MESSAGE())
                END CATCH
            END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
            ALTER PROCEDURE [dbo].[sp_FA_Compute]
                -- ===================================================
                -- Compute the new fieldwork area layer associated to
                -- each spatial point (individual location or station)
                -- which must be updated
                --
                -- This stored procedure updates the Individual_Location
                -- table and the Station table and provides
                -- ===================================================
            AS
            BEGIN

                SET NOCOUNT ON;
                
                -- Declare variable
                DECLARE @flag INT = 0
                
                -- For each individual location, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                BEGIN TRY
                    SELECT @flag = 1;
                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION
                            -- Create temporary table temptable to store the new fieldwork area
                            -- associated to a spatial point
                            IF OBJECT_ID('tempdb..#temptableindivlocation') IS NOT NULL
                            BEGIN
                                DROP TABLE #temptableindivlocation
                            END

                            SELECT TOP(50000) IL.[ID] AS [ID],
                                ISNULL(
                                    (
                                        SELECT TOP 1 F.[ID]
                                        FROM Fieldworkarea_update AS F
                                        WITH(INDEX(SIndx_FielworkArea_geom))
                                        WHERE  
                                            F.[min_lat] <= IL.[LAT]
                                            AND F.[max_lat] >= IL.[LAT]
                                            AND F.[min_lon] <= IL.[LON] 
                                            AND F.[max_lon] >= IL.[LON]  
                                            AND F.[valid_geom].STIntersects(geometry::Point(IL.[LON], IL.[LAT], 4326)) = 1
                                            AND F.[Status] = 'current'
                                        ORDER BY F.[fullpath] DESC
                                    ), 
                                    0
                                ) AS [FK_Fieldworkarea]
                            INTO #temptableindivlocation
                            FROM Individual_Location AS IL
                            WHERE
                                IL.[FK_FieldworkArea] IS NULL
                                AND IL.[LAT] IS NOT NULL
                                AND IL.[LON] IS NOT NULL

                            UPDATE IL
                            SET IL.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                            FROM Individual_Location AS IL
                            JOIN #temptableindivlocation AS TT ON IL.[ID] = TT.[ID]

                            SELECT @flag = @@ROWCOUNT

                        COMMIT TRANSACTION
                    END
                END TRY

                BEGIN CATCH
                    Print('Error during fieldwork areas update')
                    print(ERROR_MESSAGE())
                END CATCH

                -- For each station, all the areas including it are found.
                -- Only the more precise (based on fullpath) is kept
                BEGIN TRY
                    SELECT @flag=1;
                    WHILE @flag > 0
                    BEGIN
                        BEGIN TRANSACTION
                            -- Create temporary table temptable to store the new fieldwork area
                            -- associated to a spatial point
                            IF OBJECT_ID('tempdb..#temptablestation') IS NOT NULL
                            BEGIN
                                DROP TABLE #temptablestation
                            END

                            SELECT TOP(50000) S.[ID] AS [ID],
                                ISNULL(
                                    (
                                        SELECT TOP 1 F.[ID]
                                        FROM Fieldworkarea_update AS F
                                        WITH(INDEX(SIndx_FielworkArea_geom))
                                        WHERE  
                                            F.[min_lat] <= S.[LAT] 
                                            AND F.[max_lat] >= S.[LAT] 
                                            AND F.[min_lon] <= S.[LON]
                                            AND F.[max_lon] >= S.[LON]  
                                            AND F.[valid_geom].STIntersects(geometry::Point(S.[LON], S.[LAT], 4326)) = 1
                                            AND F.[Status] = 'current'
                                        ORDER BY F.[fullpath] DESC
                                    ), 
                                    0
                                ) AS [FK_Fieldworkarea]
                            INTO #temptablestation
                            FROM Station AS S
                            WHERE 	
                                S.[FK_FieldworkArea] IS NULL
                                AND S.[LAT] IS NOT NULL
                                AND S.[LON] IS NOT NULL

                            UPDATE S
                            SET S.[FK_FieldworkArea] = TT.[FK_FieldworkArea]
                            FROM Station AS S
                            JOIN #temptablestation AS TT ON S.[ID] = TT.[ID]

                            SELECT @flag = @@ROWCOUNT

                        COMMIT TRANSACTION
                    END
                END TRY

                BEGIN CATCH
                    Print('Error during fieldwork areas update')
                    print(ERROR_MESSAGE())
                END CATCH
            END
        """
    )
