"""add_syn_and_sp_for_pull_refential_conf

Revision ID: 6db985a63cb6
Revises: 
Create Date: 2022-08-04 15:48:25.299459

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6db985a63cb6'
down_revision = None
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    referential_db = options_for_migration.get('referential_db', None)
    if options_for_migration.get('is_referential_instance', True) is False:
        op.execute(
            f"""
                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.BusinessRules')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.BusinessRules]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.BusinessRules] FOR {referential_db}.[dbo].[BusinessRules]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.fieldActivity')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.fieldActivity]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.fieldActivity] FOR {referential_db}.[dbo].[fieldActivity]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.FieldActivity_ProtocoleType')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.FieldActivity_ProtocoleType]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.FieldActivity_ProtocoleType] FOR {referential_db}.[dbo].[FieldActivity_ProtocoleType]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.FrontModules')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.FrontModules]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.FrontModules] FOR {referential_db}.[dbo].[FrontModules]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.IndividualDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.IndividualDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.IndividualDynProp] FOR {referential_db}.[dbo].[IndividualDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.IndividualType')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.IndividualType]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.IndividualType] FOR {referential_db}.[dbo].[IndividualType]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.IndividualType_IndividualDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.IndividualType_IndividualDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.IndividualType_IndividualDynProp] FOR {referential_db}.[dbo].[IndividualType_IndividualDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.Language')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.Language]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.Language] FOR {referential_db}.[dbo].[Language]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.ModuleForms')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.ModuleForms]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.ModuleForms] FOR {referential_db}.[dbo].[ModuleForms]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.ModuleFormsInternationalization')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.ModuleFormsInternationalization]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.ModuleFormsInternationalization] FOR {referential_db}.[dbo].[ModuleFormsInternationalization]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.ModuleGrids')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.ModuleGrids]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.ModuleGrids] FOR {referential_db}.[dbo].[ModuleGrids]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.MonitoredSiteDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.MonitoredSiteDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.MonitoredSiteDynProp] FOR {referential_db}.[dbo].[MonitoredSiteDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.MonitoredSiteType')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.MonitoredSiteType]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.MonitoredSiteType] FOR {referential_db}.[dbo].[MonitoredSiteType]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.MonitoredSiteType_MonitoredSiteDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.MonitoredSiteType_MonitoredSiteDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.MonitoredSiteType_MonitoredSiteDynProp] FOR {referential_db}.[dbo].[MonitoredSiteType_MonitoredSiteDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.ObservationDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.ObservationDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.ObservationDynProp] FOR {referential_db}.[dbo].[ObservationDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.ProtocoleType')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.ProtocoleType]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.ProtocoleType] FOR {referential_db}.[dbo].[ProtocoleType]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.ProtocoleType_ObservationDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.ProtocoleType_ObservationDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.ProtocoleType_ObservationDynProp] FOR {referential_db}.[dbo].[ProtocoleType_ObservationDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.SensorDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.SensorDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.SensorDynProp] FOR {referential_db}.[dbo].[SensorDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.SensorType')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.SensorType]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.SensorType] FOR {referential_db}.[dbo].[SensorType]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.SensorType_SensorDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.SensorType_SensorDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.SensorType_SensorDynProp] FOR {referential_db}.[dbo].[SensorType_SensorDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.StationDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.StationDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.StationDynProp] FOR {referential_db}.[dbo].[StationDynProp]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.StationType')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.StationType]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.StationType] FOR {referential_db}.[dbo].[StationType]

                IF EXISTS (SELECT * FROM sys.synonyms WHERE [name] = 'syn_Referentiel.StationType_StationDynProp')
                    BEGIN
                        DROP SYNONYM [dbo].[syn_Referentiel.StationType_StationDynProp]
                    END
                CREATE SYNONYM [dbo].[syn_Referentiel.StationType_StationDynProp] FOR {referential_db}.[dbo].[StationType_StationDynProp]
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_fieldActivity_ProtocoleType]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#FieldActivity_ProtocoleTypeTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #FieldActivity_ProtocoleTypeTMP
                        END

                        CREATE TABLE #FieldActivity_ProtocoleTypeTMP (
                            [FK_fieldActivity] INT NOT NULL,
                            [FK_ProtocoleType] INT NOT NULL,
                            [Order] INT NOT NULL
                            )

                        INSERT INTO #FieldActivity_ProtocoleTypeTMP([FK_fieldActivity],[FK_ProtocoleType],[Order])
                        SELECT
                        [FK_fieldActivity],
                        [FK_ProtocoleType],
                        [Order]
                        FROM [syn_Referentiel.FieldActivity_ProtocoleType]


                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeFieldActivity_ProtocoleType') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeFieldActivity_ProtocoleType
                        END

                        CREATE TABLE #tempMergeFieldActivity_ProtocoleType(
                            [action] VARCHAR(MAX),
                            [SourceFK_fieldActivity] INT,
                            [SourceFK_ProtocoleType] INT,
                            [SourceOrder] INT,
                            [TargetFK_fieldActivity] INT,
                            [TargetFK_ProtocoleType] INT,
                            [TargetOrder] INT
                        );

                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #FieldActivity_ProtocoleTypeTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [FK_fieldActivity],
                            [FK_ProtocoleType],
                            [Order]
                            FROM [FieldActivity_ProtocoleType]
                        END

                        PRINT('MERGE TABLE');
                        WITH ProtocoleTypeFiltered AS
                        (
                            SELECT
                            [FK_fieldActivity],
                            [FK_ProtocoleType],
                            [Order]
                            FROM [FieldActivity_ProtocoleType]
                        )
                        MERGE ProtocoleTypeFiltered AS [TARGET]
                        USING #FieldActivity_ProtocoleTypeTMP AS [SOURCE]
                        ON ( TARGET.[FK_fieldActivity] = SOURCE.[FK_fieldActivity] AND TARGET.[FK_ProtocoleType] = SOURCE.[FK_ProtocoleType] )
                        WHEN MATCHED AND TARGET.[Order] <> SOURCE.[Order] THEN
                            UPDATE SET
                            TARGET.[Order] = SOURCE.[Order]
                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([FK_fieldActivity], [FK_ProtocoleType], [Order])
                            VALUES ([FK_fieldActivity], [FK_ProtocoleType], [Order])
                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[FK_fieldActivity] AS [SourceFK_fieldActivity],
                        INSERTED.[FK_ProtocoleType] AS [SourceFK_ProtocoleType],
                        INSERTED.[Order]  AS [SourceOrder],
                        DELETED.[FK_fieldActivity] AS [TargetFK_fieldActivity],
                        DELETED.[FK_ProtocoleType] AS [TargetFK_ProtocoleType],
                        DELETED.[Order]  AS [TargetOrder]
                        INTO #tempMergeFieldActivity_ProtocoleType([action],[SourceFK_fieldActivity],[SourceFK_ProtocoleType],[SourceOrder],[TargetFK_fieldActivity],[TargetFK_ProtocoleType],[TargetOrder]);

                        IF @Debug = 1
                            BEGIN
                                SELECT 'RESULT MERGE'

                                SELECT * FROM #tempMergeFieldActivity_ProtocoleType
                            END

                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_fieldActivity]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN
                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#fieldActivityTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #fieldActivityTMP
                        END

                        CREATE TABLE #fieldActivityTMP(
                            [ID] [int] NOT NULL,
                            [Name] [nvarchar](250) NOT NULL
                        )

                        INSERT INTO #fieldActivityTMP([ID],[Name])
                        SELECT
                        [ID],
                        [Name]
                        FROM [syn_Referentiel.fieldActivity]

                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergefieldActivity') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergefieldActivity
                        END

                        CREATE TABLE #tempMergefieldActivity(
                            [action] VARCHAR(MAX),
                            [SourceID] [int],
                            [SourceName] [nvarchar](250),
                            [TargetID] [int],
                            [TargetName] [nvarchar](250)
                        )

                        IF @Debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #fieldActivityTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [Name]
                            FROM [fieldActivity]
                        END

                        PRINT('MERGE TABLE');

                        SET IDENTITY_INSERT [fieldActivity] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [fieldActivity]');

                        WITH fieldActivityFiltered AS
                        (
                            SELECT
                            [ID],
                            [Name]
                            FROM [fieldActivity]
                        )
                        MERGE fieldActivityFiltered AS [TARGET]
                        USING #FieldActivityTMP AS [SOURCE]
                        ON ( TARGET.[ID] = SOURCE.[ID] )
                        WHEN MATCHED THEN
                            UPDATE SET
                            TARGET.[Name] = SOURCE.[Name]
                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID], [Name])
                            VALUES ([ID], [Name])
                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceID],
                        INSERTED.[Name] AS [SourceName],
                        DELETED.[ID] AS [TargetID],
                        DELETED.[Name] AS [TargetName]
                        INTO #tempMergefieldActivity([action],[SourceID],[SourceName],[TargetID],[TargetName]);

                        SET IDENTITY_INSERT [fieldActivity] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [fieldActivity]');

                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_FrontModules]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#FrontModulesTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #FrontModulesTMP
                        END

                        CREATE TABLE #FrontModulesTMP(
                            [ID] [int] NOT NULL,
                            [Name] [nvarchar](250) NULL,
                            [TypeModule] [int] NULL,
                            [Comments] [nvarchar](2500) NULL
                        )

                        INSERT INTO #FrontModulesTMP([ID],[Name],[TypeModule],[Comments])
                        SELECT
                        [ID],
                        [Name],
                        [TypeModule],
                        [Comments]
                        FROM [syn_Referentiel.FrontModules]

                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeFrontModules') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeFrontModules
                        END

                        CREATE TABLE #tempMergeFrontModules(
                            [action] VARCHAR(MAX),
                            [SourceID] [int],
                            [SourceName] [nvarchar](250),
                            [SourceTypeModule] [int],
                            [SourceComments] [nvarchar](2500),
                            [TargetID] [int],
                            [TargetName] [nvarchar](250),
                            [TargetTypeModule] [int],
                            [TargetComments] [nvarchar](2500)
                        )

                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #FrontModulesTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [Name],
                            [TypeModule],
                            [Comments]
                            FROM [FrontModules]
                        END

                        SET IDENTITY_INSERT [FrontModules] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [FrontModules]');

                        PRINT('MERGE TABLE');
                        WITH FrontModulesFiltered AS
                        (
                            SELECT
                            [ID],
                            [Name],
                            [TypeModule],
                            [Comments]
                            FROM [FrontModules]
                        )
                        MERGE FrontModulesFiltered AS [TARGET]
                        USING #FrontModulesTMP AS [SOURCE]
                        ON ( TARGET.[ID] = SOURCE.[ID] )
                        WHEN MATCHED THEN
                            UPDATE SET
                            TARGET.[Name] = SOURCE.[Name],
                            TARGET.[TypeModule] = SOURCE.[TypeModule],
                            TARGET.[Comments] = SOURCE.[Comments]

                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID], [Name], [TypeModule],[Comments])
                            VALUES ([ID], [Name], [TypeModule],[Comments])
                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceID],
                        INSERTED.[Name] AS [SourceName],
                        INSERTED.[TypeModule]  AS [SourceTypeModule],
                        INSERTED.[Comments] AS [SourceComments],
                        DELETED.[ID] AS [TargetID],
                        DELETED.[Name]  AS [TargetName],
                        DELETED.[TypeModule]  AS [TargetTypeModule],
                        DELETED.[Comments]  AS [TargetComments]
                        INTO #tempMergeFrontModules([action],[SourceID],[SourceName],[SourceTypeModule],[SourceComments],[TargetID],[TargetName],[TargetTypeModule],[TargetComments]);

                        SET IDENTITY_INSERT [FrontModules] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [FrontModules]');

                        IF @Debug = 1
                        BEGIN
                            SELECT 'RESULT MERGE'

                            SELECT * FROM #FrontModulesTMP
                        END

                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_ModuleForms]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#ModuleFormsTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #ModuleFormsTMP
                        END

                        CREATE TABLE #ModuleFormsTMP (
                            [ID] INT NOT NULL,
                            [module_id] INT NULL,
                            [TypeObj] INT NULL,
                            [Name] NVARCHAR(250) NULL,
                            [Label] NVARCHAR(250) NULL,
                            [Required] BIT NULL,
                            [FieldSizeEdit] INT NOT NULL,
                            [FieldSizeDisplay] INT NOT NULL,
                            [InputType] NVARCHAR(100) NULL,
                            [editorClass] NVARCHAR(100) NULL,
                            [FormRender] INT NULL,
                            [FormOrder] INT NULL,
                            [Legend] NVARCHAR(500) NULL,
                            [Options] NVARCHAR(255) NULL,
                            [Validators] NVARCHAR(255) NULL,
                            [displayClass] NVARCHAR(150) NULL,
                            [EditClass] NVARCHAR(150) NULL,
                            [Status] INT NULL,
                            [Locked] BIT NULL,
                            [DefaultValue] VARCHAR(100) NULL,
                            [Rules] VARCHAR(550) NULL,
                            [Orginal_FB_ID] INT NULL
                            )

                        INSERT INTO #ModuleFormsTMP([ID],[module_id],[TypeObj],[Name],[Label],[Required],[FieldSizeEdit],[FieldSizeDisplay],[InputType],[editorClass],[FormRender],[FormOrder],[Legend],[Options],[Validators],[displayClass],[EditClass],[Status],[Locked],[DefaultValue],[Rules],[Orginal_FB_ID])
                        SELECT
                        [ID],
                        [module_id],
                        [TypeObj],
                        [Name],
                        [Label],
                        [Required],
                        [FieldSizeEdit],
                        [FieldSizeDisplay],
                        [InputType],
                        [editorClass],
                        [FormRender],
                        [FormOrder],
                        [Legend],
                        [Options],
                        [Validators],
                        [displayClass],
                        [EditClass],
                        [Status],
                        [Locked],
                        [DefaultValue],
                        [Rules],
                        [Orginal_FB_ID]
                        FROM [syn_Referentiel.ModuleForms]

                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeModuleForms') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeModuleForms
                        END

                        CREATE TABLE #tempMergeModuleForms(
                            [action] VARCHAR(MAX),
                            [SourceID] INT,
                            [Sourcemodule_id] INT,
                            [SourceTypeObj] INT,
                            [SourceName] NVARCHAR(250),
                            [SourceLabel] NVARCHAR(250),
                            [SourceRequired] BIT,
                            [SourceFieldSizeEdit] INT,
                            [SourceFieldSizeDisplay] INT,
                            [SourceInputType] NVARCHAR(100),
                            [SourceeditorClass] NVARCHAR(100),
                            [SourceFormRender] INT,
                            [SourceFormOrder] INT,
                            [SourceLegend] NVARCHAR(500),
                            [SourceOptions] NVARCHAR(255),
                            [SourceValidators] NVARCHAR(255),
                            [SourcedisplayClass] NVARCHAR(150),
                            [SourceEditClass] NVARCHAR(150),
                            [SourceStatus] INT,
                            [SourceLocked] BIT,
                            [SourceDefaultValue] VARCHAR(100),
                            [SourceRules] VARCHAR(550),
                            [SourceOrginal_FB_ID] INT,
                            [TargetID] INT,
                            [Targetmodule_id] INT,
                            [TargetTypeObj] INT,
                            [TargetName] NVARCHAR(250),
                            [TargetLabel] NVARCHAR(250),
                            [TargetRequired] BIT,
                            [TargetFieldSizeEdit] INT,
                            [TargetFieldSizeDisplay] INT,
                            [TargetInputType] NVARCHAR(100),
                            [TargeteditorClass] NVARCHAR(100),
                            [TargetFormRender] INT,
                            [TargetFormOrder] INT,
                            [TargetLegend] NVARCHAR(500),
                            [TargetOptions] NVARCHAR(255),
                            [TargetValidators] NVARCHAR(255),
                            [TargetdisplayClass] NVARCHAR(150),
                            [TargetEditClass] NVARCHAR(150),
                            [TargetStatus] INT,
                            [TargetLocked] BIT,
                            [TargetDefaultValue] VARCHAR(100),
                            [TargetRules] VARCHAR(550),
                            [TargetOrginal_FB_ID] INT
                        )

                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #ModuleFormsTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [module_id],
                            [TypeObj],
                            [Name],
                            [Label],
                            [Required],
                            [FieldSizeEdit],
                            [FieldSizeDisplay],
                            [InputType],
                            [editorClass],
                            [FormRender],
                            [FormOrder],
                            [Legend],
                            [Options],
                            [Validators],
                            [displayClass],
                            [EditClass],
                            [Status],
                            [Locked],
                            [DefaultValue],
                            [Rules],
                            [Orginal_FB_ID]
                            FROM [ModuleForms]
                        END


                        SET IDENTITY_INSERT [ModuleForms] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [ModuleForms]');

                        WITH ModuleFormsFiltered AS (
                            SELECT
                            [ID],
                            [module_id],
                            [TypeObj],
                            [Name],
                            [Label],
                            [Required],
                            [FieldSizeEdit],
                            [FieldSizeDisplay],
                            [InputType],
                            [editorClass],
                            [FormRender],
                            [FormOrder],
                            [Legend],
                            [Options],
                            [Validators],
                            [displayClass],
                            [EditClass],
                            [Status],
                            [Locked],
                            [DefaultValue],
                            [Rules],
                            [Orginal_FB_ID]
                            FROM [ModuleForms]
                        )
                        MERGE ModuleFormsFiltered AS [TARGET]
                        USING #ModuleFormsTMP AS [SOURCE]
                        ON ( [TARGET].[ID] = [SOURCE].[ID])

                        WHEN MATCHED THEN
                            UPDATE SET
                            TARGET.[module_id] = SOURCE.[module_id],
                            TARGET.[TypeObj] = SOURCE.[TypeObj],
                            TARGET.[Name] = SOURCE.[Name],
                            TARGET.[Label] = SOURCE.[Label],
                            TARGET.[Required] = SOURCE.[Required],
                            TARGET.[FieldSizeEdit] = SOURCE.[FieldSizeEdit],
                            TARGET.[FieldSizeDisplay] = SOURCE.[FieldSizeDisplay],
                            TARGET.[InputType] = SOURCE.[InputType],
                            TARGET.[editorClass] = SOURCE.[editorClass],
                            TARGET.[FormRender] = SOURCE.[FormRender],
                            TARGET.[FormOrder] = SOURCE.[FormOrder],
                            TARGET.[Legend] = SOURCE.[Legend],
                            TARGET.[Options] = SOURCE.[Options],
                            TARGET.[Validators] = SOURCE.[Validators],
                            TARGET.[displayClass] = SOURCE.[displayClass],
                            TARGET.[EditClass] = SOURCE.[EditClass],
                            TARGET.[Status] = SOURCE.[Status],
                            TARGET.[Locked] = SOURCE.[Locked],
                            TARGET.[DefaultValue] = SOURCE.[DefaultValue],
                            TARGET.[Rules] = SOURCE.[Rules],
                            TARGET.[Orginal_FB_ID] = SOURCE.[Orginal_FB_ID]

                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID],[module_id],[TypeObj],[Name],[Label],[Required],[FieldSizeEdit],[FieldSizeDisplay],[InputType],[editorClass],[FormRender],[FormOrder],[Legend],[Options],[Validators],[displayClass],[EditClass],[Status],[Locked],[DefaultValue],[Rules],[Orginal_FB_ID]) 
                            VALUES ([ID],[module_id],[TypeObj],[Name],[Label],[Required],[FieldSizeEdit],[FieldSizeDisplay],[InputType],[editorClass],[FormRender],[FormOrder],[Legend],[Options],[Validators],[displayClass],[EditClass],[Status],[Locked],[DefaultValue],[Rules],[Orginal_FB_ID])

                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceID],
                        INSERTED.[module_id] AS [Sourcemodule_id],
                        INSERTED.[TypeObj] AS [SourceTypeObj],
                        INSERTED.[Name] AS [SourceName],
                        INSERTED.[Label] AS [SourceLabel],
                        INSERTED.[Required] AS [SourceRequired],
                        INSERTED.[FieldSizeEdit] AS [SourceFieldSizeEdit],
                        INSERTED.[FieldSizeDisplay] AS [SourceFieldSizeDisplay],
                        INSERTED.[InputType] AS [SourceInputType],
                        INSERTED.[editorClass] AS [SourceeditorClass],
                        INSERTED.[FormRender] AS [SourceFormRender],
                        INSERTED.[FormOrder] AS [SourceFormOrder],
                        INSERTED.[Legend] AS [SourceLegend],
                        INSERTED.[Options] AS [SourceOptions],
                        INSERTED.[Validators] AS [SourceValidators],
                        INSERTED.[displayClass] AS [SourcedisplayClass],
                        INSERTED.[EditClass] AS [SourceEditClass],
                        INSERTED.[Status] AS [SourceStatus],
                        INSERTED.[Locked] AS [SourceLocked],
                        INSERTED.[DefaultValue] AS [SourceDefaultValue],
                        INSERTED.[Rules] AS [SourceRules],
                        INSERTED.[Orginal_FB_ID] AS [SourceOrginal_FB_ID],
                        DELETED.[ID] AS [TargetID],
                        DELETED.[module_id] AS [Targetmodule_id],
                        DELETED.[TypeObj] AS [TargetTypeObj],
                        DELETED.[Name] AS [TargetName],
                        DELETED.[Label] AS [TargetLabel],
                        DELETED.[Required] AS [TargetRequired],
                        DELETED.[FieldSizeEdit] AS [TargetFieldSizeEdit],
                        DELETED.[FieldSizeDisplay] AS [TargetFieldSizeDisplay],
                        DELETED.[InputType] AS [TargetInputType],
                        DELETED.[editorClass] AS [TargeteditorClass],
                        DELETED.[FormRender] AS [TargetFormRender],
                        DELETED.[FormOrder] AS [TargetFormOrder],
                        DELETED.[Legend] AS [TargetLegend],
                        DELETED.[Options] AS [TargetOptions],
                        DELETED.[Validators] AS [TargetValidators],
                        DELETED.[displayClass] AS [TargetdisplayClass],
                        DELETED.[EditClass] AS [TargetEditClass],
                        DELETED.[Status] AS [TargetStatus],
                        DELETED.[Locked] AS [TargetLocked],
                        DELETED.[DefaultValue] AS [TargetDefaultValue],
                        DELETED.[Rules] AS [TargetRules],
                        DELETED.[Orginal_FB_ID] AS [TargetOrginal_FB_ID]
                        INTO #tempMergeModuleForms([action],[SourceID],[Sourcemodule_id],[SourceTypeObj],[SourceName],[SourceLabel],[SourceRequired],[SourceFieldSizeEdit],[SourceFieldSizeDisplay],[SourceInputType],[SourceeditorClass],[SourceFormRender],[SourceFormOrder],[SourceLegend],[SourceOptions],[SourceValidators],[SourcedisplayClass],[SourceEditClass],[SourceStatus],[SourceLocked],[SourceDefaultValue],[SourceRules],[SourceOrginal_FB_ID],[TargetID],[Targetmodule_id],[TargetTypeObj],[TargetName],[TargetLabel],[TargetRequired],[TargetFieldSizeEdit],[TargetFieldSizeDisplay],[TargetInputType],[TargeteditorClass],[TargetFormRender],[TargetFormOrder],[TargetLegend],[TargetOptions],[TargetValidators],[TargetdisplayClass],[TargetEditClass],[TargetStatus],[TargetLocked],[TargetDefaultValue],[TargetRules],[TargetOrginal_FB_ID]);

                        SET IDENTITY_INSERT [ModuleForms] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [ModuleForms]');

                        IF @Debug = 1
                        BEGIN
                            SELECT 'RESULT MERGE'

                            SELECT * FROM #tempMergeModuleForms
                        END

                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_ModuleFormsInternationalization]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#ModuleFormsInternationalizationTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #ModuleFormsInternationalizationTMP
                        END

                        CREATE TABLE #ModuleFormsInternationalizationTMP (
                            [ID] [INT] NOT NULL,
                            [Fk_ProtocoleType] [INT] NOT NULL,
                            [Fk_Language] [VARCHAR] (2) NOT NULL,
                            [Property_Name] [VARCHAR] (255) NULL,
                            [Description] [VARCHAR] (300) NULL,
                            [Label] [VARCHAR] (255) NULL,
                            [Help] [VARCHAR] (255) NULL,
                            [Keywords] [VARCHAR] (255) NULL
                            )

                        INSERT INTO #ModuleFormsInternationalizationTMP([ID],[Fk_ProtocoleType],[Fk_Language],[Property_Name],[Description],[Label],[Help],[Keywords])
                        SELECT
                        [ID],
                        [Fk_ProtocoleType],
                        [Fk_Language],
                        [Property_Name],
                        [Description],
                        [Label],
                        [Help],
                        [Keywords]
                        FROM [syn_Referentiel.ModuleFormsInternationalization]


                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeModuleFormsInternationalization') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeModuleFormsInternationalization
                        END

                        CREATE TABLE #tempMergeModuleFormsInternationalization(
                            [action] VARCHAR(MAX),
                            [SourceID] INT,
                            [SourceFk_ProtocoleType] INT,
                            [SourceFk_Language] VARCHAR(2),
                            [SourceProperty_Name] VARCHAR(255),
                            [SourceDescription] VARCHAR(300),
                            [SourceLabel] VARCHAR(255),
                            [SourceHelp] VARCHAR(255),
                            [SourceKeywords] VARCHAR(255),
                            [TargetID] INT,
                            [TargetFk_ProtocoleType] INT,
                            [TargetFk_Language] VARCHAR(2),
                            [TargetProperty_Name] VARCHAR(255),
                            [TargetDescription] VARCHAR(300),
                            [TargetLabel] VARCHAR(255),
                            [TargetHelp] VARCHAR(255),
                            [TargetKeywords] VARCHAR(255)
                        );

                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #ModuleFormsInternationalizationTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [Fk_ProtocoleType],
                            [Fk_Language],
                            [Property_Name],
                            [Description],
                            [Label],
                            [Help],
                            [Keywords]
                            FROM [ModuleFormsInternationalization]
                        END

                        PRINT('MERGE TABLE');
                        SET IDENTITY_INSERT [ModuleFormsInternationalization] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [ModuleFormsInternationalization]');
                        WITH ModuleFormsInternationalizationFiltered AS (
                            SELECT
                            [ID],
                            [Fk_ProtocoleType],
                            [Fk_Language],
                            [Property_Name],
                            [Description],
                            [Label],
                            [Help],
                            [Keywords]
                            FROM [ModuleFormsInternationalization]
                        )
                        MERGE ModuleFormsInternationalizationFiltered AS [TARGET]
                        USING #ModuleFormsInternationalizationTMP AS [SOURCE]
                        ON ( [TARGET].[ID] = [SOURCE].[ID] )

                        WHEN MATCHED THEN
                            UPDATE SET
                            [TARGET].[Fk_ProtocoleType] = [SOURCE].[Fk_ProtocoleType],
                            [TARGET].[Fk_Language] = [SOURCE].[Fk_Language],
                            [TARGET].[Property_Name] = [SOURCE].[Property_Name],
                            [TARGET].[Description] = [SOURCE].[Description],
                            [TARGET].[Label] = [SOURCE].[Label],
                            [TARGET].[Help] = [SOURCE].[Help],
                            [TARGET].[Keywords] = [SOURCE].[Keywords]
                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID],[Fk_ProtocoleType],[Fk_Language],[Property_Name],[Description],[Label],[Help],[Keywords])
                            VALUES ([SOURCE].[ID],[SOURCE].[Fk_ProtocoleType],[SOURCE].[Fk_Language],[SOURCE].[Property_Name],[SOURCE].[Description],[SOURCE].[Label],[SOURCE].[Help],[SOURCE].[Keywords])
                        WHEN NOT MATCHED BY SOURCE THEN
                        DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceID],
                        INSERTED.[Fk_ProtocoleType] AS [SourceFk_ProtocoleType],
                        INSERTED.[Fk_Language] AS [SourceFk_Language],
                        INSERTED.[Property_Name] AS [SourceProperty_Name],
                        INSERTED.[Description] AS [SourceDescription],
                        INSERTED.[Label] AS [SourceLabel],
                        INSERTED.[Help] AS [SourceHelp],
                        INSERTED.[Keywords] AS [SourceKeywords],
                        DELETED.[ID] AS [TargetID],
                        DELETED.[Fk_ProtocoleType] AS [TargetFk_ProtocoleType],
                        DELETED.[Fk_Language] AS [TargetFk_Language],
                        DELETED.[Property_Name] AS [TargetProperty_Name],
                        DELETED.[Description] AS [TargetDescription],
                        DELETED.[Label] AS [TargetLabel],
                        DELETED.[Help] AS [TargetHelp],
                        DELETED.[Keywords] AS [TargetKeywords]
                        INTO #tempMergeModuleFormsInternationalization([action],[SourceID],[SourceFk_ProtocoleType],[SourceFk_Language],[SourceProperty_Name],[SourceDescription],[SourceLabel],[SourceHelp],[SourceKeywords],[TargetID],[TargetFk_ProtocoleType],[TargetFk_Language],[TargetProperty_Name],[TargetDescription],[TargetLabel],[TargetHelp],[TargetKeywords]);

                        SET IDENTITY_INSERT [ModuleFormsInternationalization] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [ModuleFormsInternationalization]');

                        IF @Debug = 1
                        BEGIN
                            SELECT 'RESULT MERGE'

                            SELECT * FROM #tempMergeModuleFormsInternationalization
                        END

                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_ModuleGrids]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#ModuleGridsTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #ModuleGridsTMP
                        END

                        CREATE TABLE #ModuleGridsTMP (
                            [ID] [int] NOT NULL,
                            [Module_ID] [int] NULL,
                            [TypeObj] [int] NULL,
                            [Name] [nvarchar](255) NULL,
                            [Label] [nvarchar](255) NULL,
                            [GridRender] [int] NULL,
                            [GridSize] [varchar](250) NULL,
                            [CellType] [nvarchar](255) NULL,
                            [GridOrder] [int] NULL,
                            [QueryName] [nvarchar](255) NULL,
                            [Options] [nvarchar](255) NULL,
                            [FilterOrder] [int] NULL,
                            [FilterSize] [int] NULL,
                            [FilterClass] [varchar](250) NULL,
                            [IsSearchable] [bit] NULL,
                            [FilterDefaultValue] [varchar](250) NULL,
                            [FilterRender] [int] NULL,
                            [FilterType] [varchar](250) NULL,
                            [Status] [int] NULL,
                            [ColumnParams] [varchar](250) NULL,
                            )

                        INSERT INTO #ModuleGridsTMP([ID], [Module_ID], [TypeObj], [Name], [Label], [GridRender], [GridSize], [CellType], [GridOrder], [QueryName], [Options], [FilterOrder], [FilterSize], [FilterClass], [IsSearchable], [FilterDefaultValue], [FilterRender], [FilterType], [Status], [ColumnParams])
                        SELECT
                        [ID],
                        [Module_ID],
                        [TypeObj],
                        [Name],
                        [Label],
                        [GridRender],
                        [GridSize],
                        [CellType],
                        [GridOrder],
                        [QueryName],
                        [Options],
                        [FilterOrder],
                        [FilterSize],
                        [FilterClass],
                        [IsSearchable],
                        [FilterDefaultValue],
                        [FilterRender],
                        [FilterType],
                        [Status],
                        [ColumnParams]
                        FROM [syn_Referentiel.ModuleGrids]


                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeModuleGrids') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeModuleGrids
                        END

                        CREATE TABLE #tempMergeModuleGrids(
                            [action] VARCHAR(MAX),
                            [SourceID] INT,
                            [SourceModule_ID] INT,
                            [SourceTypeObj] INT,
                            [SourceName] NVARCHAR(255),
                            [SourceLabel] NVARCHAR(255),
                            [SourceGridRender] INT,
                            [SourceGridSize] VARCHAR(255),
                            [SourceCellType] NVARCHAR(255),
                            [SourceGridOrder] INT,
                            [SourceQueryName] NVARCHAR(255),
                            [SourceOptions] NVARCHAR(255),
                            [SourceFilterOrder] INT,
                            [SourceFilterSize] INT,
                            [SourceFilterClass] VARCHAR(255),
                            [SourceIsSearchable] BIT,
                            [SourceFilterDefaultValue] VARCHAR(255),
                            [SourceFilterRender] INT,
                            [SourceFilterType] VARCHAR(255),
                            [SourceStatus] INT,
                            [SourceColumnParams] VARCHAR(255),
                            [TargetID] INT,
                            [TargetModule_ID] INT,
                            [TargetTypeObj] INT,
                            [TargetName] NVARCHAR(255),
                            [TargetLabel] NVARCHAR(255),
                            [TargetGridRender] INT,
                            [TargetGridSize] VARCHAR(255),
                            [TargetCellType] NVARCHAR(255),
                            [TargetGridOrder] INT,
                            [TargetQueryName] NVARCHAR(255),
                            [TargetOptions] NVARCHAR(255),
                            [TargetFilterOrder] INT,
                            [TargetFilterSize] INT,
                            [TargetFilterClass] VARCHAR(255),
                            [TargetIsSearchable] BIT,
                            [TargetFilterDefaultValue] VARCHAR(255),
                            [TargetFilterRender] INT,
                            [TargetFilterType] VARCHAR(255),
                            [TargetStatus] INT,
                            [TargetColumnParams] VARCHAR(255)
                        );

                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #ModuleGridsTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [Module_ID],
                            [TypeObj],
                            [Name],
                            [Label],
                            [GridRender],
                            [GridSize],
                            [CellType],
                            [GridOrder],
                            [QueryName],
                            [Options],
                            [FilterOrder],
                            [FilterSize],
                            [FilterClass],
                            [IsSearchable],
                            [FilterDefaultValue],
                            [FilterRender],
                            [FilterType],
                            [Status],
                            [ColumnParams]
                            FROM [ModuleGrids]
                        END

                        PRINT('MERGE TABLE');
                        SET IDENTITY_INSERT [ModuleGrids] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [ModuleGrids]');

                        WITH ModuleGridsFiltered AS (
                            SELECT
                            [ID],
                            [Module_ID],
                            [TypeObj],
                            [Name],
                            [Label],
                            [GridRender],
                            [GridSize],
                            [CellType],
                            [GridOrder],
                            [QueryName],
                            [Options],
                            [FilterOrder],
                            [FilterSize],
                            [FilterClass],
                            [IsSearchable],
                            [FilterDefaultValue],
                            [FilterRender],
                            [FilterType],
                            [Status],
                            [ColumnParams]
                            FROM [ModuleGrids]
                        )
                        MERGE ModuleGridsFiltered AS [TARGET]
                        USING #ModuleGridsTMP AS [SOURCE]
                        ON ( [TARGET].[ID] = [SOURCE].[ID] )

                        WHEN MATCHED THEN
                            UPDATE SET
                            [TARGET].[Module_ID] = [SOURCE].[Module_ID],
                            [TARGET].[TypeObj] = [SOURCE].[TypeObj],
                            [TARGET].[Name] = [SOURCE].[Name],
                            [TARGET].[Label] = [SOURCE].[Label],
                            [TARGET].[GridRender] = [SOURCE].[GridRender],
                            [TARGET].[GridSize] = [SOURCE].[GridSize],
                            [TARGET].[CellType] = [SOURCE].[CellType],
                            [TARGET].[GridOrder] = [SOURCE].[GridOrder],
                            [TARGET].[QueryName] = [SOURCE].[QueryName],
                            [TARGET].[Options] = [SOURCE].[Options],
                            [TARGET].[FilterOrder] = [SOURCE].[FilterOrder],
                            [TARGET].[FilterSize] = [SOURCE].[FilterSize],
                            [TARGET].[FilterClass] = [SOURCE].[FilterClass],
                            [TARGET].[IsSearchable] = [SOURCE].[IsSearchable],
                            [TARGET].[FilterDefaultValue] = [SOURCE].[FilterDefaultValue],
                            [TARGET].[FilterRender] = [SOURCE].[FilterRender],
                            [TARGET].[FilterType] = [SOURCE].[FilterType],
                            [TARGET].[Status] = [SOURCE].[Status],
                            [TARGET].[ColumnParams] = [SOURCE].[ColumnParams]
                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID], [Module_ID], [TypeObj], [Name], [Label], [GridRender], [GridSize], [CellType], [GridOrder], [QueryName], [Options], [FilterOrder], [FilterSize], [FilterClass], [IsSearchable], [FilterDefaultValue], [FilterRender], [FilterType], [Status], [ColumnParams])
                            VALUES ([SOURCE].[ID], [SOURCE].[Module_ID], [SOURCE].[TypeObj], [SOURCE].[Name], [SOURCE].[Label], [SOURCE].[GridRender], [SOURCE].[GridSize], [SOURCE].[CellType], [SOURCE].[GridOrder], [SOURCE].[QueryName], [SOURCE].[Options], [SOURCE].[FilterOrder], [SOURCE].[FilterSize], [SOURCE].[FilterClass], [SOURCE].[IsSearchable], [SOURCE].[FilterDefaultValue], [SOURCE].[FilterRender], [SOURCE].[FilterType], [SOURCE].[Status], [SOURCE].[ColumnParams])
                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceID],
                        INSERTED.[Module_ID] AS [SourceModule_ID],
                        INSERTED.[TypeObj] AS [SourceTypeObj],
                        INSERTED.[Name] AS [SourceName],
                        INSERTED.[Label] AS [SourceLabel],
                        INSERTED.[GridRender] AS [SourceGridRender],
                        INSERTED.[GridSize] AS [SourceGridSize],
                        INSERTED.[CellType] AS [SourceCellType],
                        INSERTED.[GridOrder] AS [SourceGridOrder],
                        INSERTED.[QueryName] AS [SourceQueryName],
                        INSERTED.[Options] AS [SourceOptions],
                        INSERTED.[FilterOrder] AS [SourceFilterOrder],
                        INSERTED.[FilterSize] AS [SourceFilterSize],
                        INSERTED.[FilterClass] AS [SourceFilterClass],
                        INSERTED.[IsSearchable] AS [SourceIsSearchable],
                        INSERTED.[FilterDefaultValue] AS [SourceFilterDefaultValue],
                        INSERTED.[FilterRender] AS [SourceFilterRender],
                        INSERTED.[FilterType] AS [SourceFilterType],
                        INSERTED.[Status] AS [SourceStatus],
                        INSERTED.[ColumnParams] AS [SourceColumnParams],
                        DELETED.[ID] AS [TargetID],
                        DELETED.[Module_ID] AS [TargetModule_ID],
                        DELETED.[TypeObj] AS [TargetTypeObj],
                        DELETED.[Name] AS [TargetName],
                        DELETED.[Label] AS [TargetLabel],
                        DELETED.[GridRender] AS [TargetGridRender],
                        DELETED.[GridSize] AS [TargetGridSize],
                        DELETED.[CellType] AS [TargetCellType],
                        DELETED.[GridOrder] AS [TargetGridOrder],
                        DELETED.[QueryName] AS [TargetQueryName],
                        DELETED.[Options] AS [TargetOptions],
                        DELETED.[FilterOrder] AS [TargetFilterOrder],
                        DELETED.[FilterSize] AS [TargetFilterSize],
                        DELETED.[FilterClass] AS [TargetFilterClass],
                        DELETED.[IsSearchable] AS [TargetIsSearchable],
                        DELETED.[FilterDefaultValue] AS [TargetFilterDefaultValue],
                        DELETED.[FilterRender] AS [TargetFilterRender],
                        DELETED.[FilterType] AS [TargetFilterType],
                        DELETED.[Status] AS [TargetStatus],
                        DELETED.[ColumnParams] AS [TargetColumnParams]
                        INTO #tempMergeModuleGrids([action],[SourceID],[SourceModule_ID],[SourceTypeObj],[SourceName],[SourceLabel],[SourceGridRender],[SourceGridSize],[SourceCellType],[SourceGridOrder],[SourceQueryName],[SourceOptions],[SourceFilterOrder],[SourceFilterSize],[SourceFilterClass],[SourceIsSearchable],[SourceFilterDefaultValue],[SourceFilterRender],[SourceFilterType],[SourceStatus],[SourceColumnParams],[TargetID],[TargetModule_ID],[TargetTypeObj],[TargetName],[TargetLabel],[TargetGridRender],[TargetGridSize],[TargetCellType],[TargetGridOrder],[TargetQueryName],[TargetOptions],[TargetFilterOrder],[TargetFilterSize],[TargetFilterClass],[TargetIsSearchable],[TargetFilterDefaultValue],[TargetFilterRender],[TargetFilterType],[TargetStatus],[TargetColumnParams]);

                        SET IDENTITY_INSERT [ModuleGrids] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [ModuleGrids]');

                        IF @Debug = 1
                        BEGIN
                            SELECT 'RESULT MERGE'

                            SELECT * FROM #tempMergeModuleGrids
                        END

                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_ObservationDynProp]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#ObservationDynPropTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #ObservationDynPropTMP
                        END

                        CREATE TABLE #ObservationDynPropTMP (
                            [ID] INT NULL,
                            [Name] NVARCHAR(250) NULL,
                            [TypeProp] NVARCHAR(250) NULL
                        )

                        INSERT INTO #ObservationDynPropTMP([ID],[Name],[TypeProp])
                        SELECT
                        [ID],
                        [Name],
                        [TypeProp]
                        FROM [syn_Referentiel.ObservationDynProp]



                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeObservationDynProp') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeObservationDynProp
                        END

                        CREATE TABLE #tempMergeObservationDynProp(
                        [action] VARCHAR(MAX),
                        [SourceId] INT,
                        [sourceName] VARCHAR(MAX),
                        [sourceTypeProp] VARCHAR(MAX),
                        [TargetId] INT,
                        [TargetName] VARCHAR(MAX),
                        [TargetTypeProp] VARCHAR(MAX)
                        );

                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #ObservationDynPropTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [Name],
                            [TypeProp]
                            FROM [ObservationDynProp]
                        END


                        SET IDENTITY_INSERT [ObservationDynProp] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [ObservationDynProp]');

                        WITH ObservationDynPropFiltered AS (
                            SELECT
                            [ID],
                            [Name],
                            [TypeProp]
                            FROM [ObservationDynProp]
                        )
                        MERGE ObservationDynPropFiltered AS [TARGET]
                        USING #ObservationDynPropTMP AS [SOURCE]
                        ON ( [TARGET].[ID] = [SOURCE].[ID])

                        WHEN MATCHED THEN
                            UPDATE SET
                            TARGET.[Name] = SOURCE.[Name],
                            TARGET.[TypeProp] = SOURCE.[TypeProp]

                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID],[Name],[TypeProp])
                            VALUES ([ID],[Name],[TypeProp])

                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceId],
                        INSERTED.[name] AS [sourceName],
                        INSERTED.[TypeProp] AS [sourceTypeProp],
                        DELETED.[ID] AS [TargetId],
                        DELETED.[name] AS [TargetName],
                        DELETED.[TypeProp] AS [TargetTypeProp]
                        INTO #tempMergeObservationDynProp([action],[SourceId],[sourceName],[sourceTypeProp],[TargetId],[TargetName],[TargetTypeProp]);

                        SET IDENTITY_INSERT [ObservationDynProp] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [ObservationDynProp]');

                        IF @Debug = 1
                        BEGIN
                            SELECT 'RESULT MERGE'

                            SELECT * FROM #tempMergeObservationDynProp
                        END

                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_ProtocoleType]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#ProtocoleTypeTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #ProtocoleTypeTMP
                        END

                        CREATE TABLE #ProtocoleTypeTMP(
                            [ID] [int] NOT NULL,
                            [Name] [nvarchar](250) NULL,
                            [Status] [int] NULL,
                            [OriginalId] [nvarchar](250) NULL,
                            [obsolete] [bit] NULL
                        )

                        INSERT INTO #ProtocoleTypeTMP([ID],[Name],[Status],[OriginalId],[obsolete])
                        SELECT
                        [ID],
                        [Name],
                        [Status],
                        [OriginalId],
                        [obsolete]
                        FROM [syn_Referentiel.ProtocoleType]

                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeProtocoleType') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeProtocoleType
                        END

                        CREATE TABLE #tempMergeProtocoleType(
                            [action] VARCHAR(MAX),
                            [SourceID] [int],
                            [SourceName] [nvarchar](250),
                            [SourceStatus] [int],
                            [SourceOriginalId] [nvarchar](250),
                            [Sourceobsolete] [bit],
                            [TargetID] [int],
                            [TargetName] [nvarchar](250),
                            [TargetStatus] [int],
                            [TargetOriginalId] [nvarchar](250),
                            [Targetobsolete] [bit]
                        )


                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #ProtocoleTypeTMP

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [Name],
                            [Status],
                            [OriginalId],
                            [obsolete]
                            FROM [ProtocoleType]
                        END

                        SET IDENTITY_INSERT [ProtocoleType] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [ProtocoleType]');

                        PRINT('MERGE TABLE');
                        WITH ProtocoleTypeFiltered AS
                        (
                            SELECT
                            [ID],
                            [Name],
                            [Status],
                            [OriginalId],
                            [obsolete]
                            FROM [ProtocoleType]
                        )
                        MERGE ProtocoleTypeFiltered AS [TARGET]
                        USING #ProtocoleTypeTMP AS [SOURCE]
                        ON ( TARGET.[ID] = SOURCE.[ID] )
                        WHEN MATCHED THEN
                            UPDATE SET
                            TARGET.[Name] = SOURCE.[Name],
                            TARGET.[Status] = SOURCE.[Status],
                            TARGET.[OriginalId] = SOURCE.[OriginalId],
                            TARGET.[obsolete] = SOURCE.[obsolete]

                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID], [Name], [Status], [OriginalId], [obsolete])
                            VALUES ([ID], [Name], [Status], [OriginalId], [obsolete])
                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceID],
                        INSERTED.[Name] AS [SourceName],
                        INSERTED.[Status]  AS [SourceStatus],
                        INSERTED.[OriginalId] AS [SourceOriginalId],
                        INSERTED.[obsolete] AS [Sourceobsolete],
                        DELETED.[ID] AS [TargetID],
                        DELETED.[Name]  AS [TargetName],
                        DELETED.[Status]  AS [TargetStatus],
                        DELETED.[OriginalId]  AS [TargetOriginalId],
                        DELETED.[obsolete] AS [Targetobsolete]
                        INTO #tempMergeProtocoleType([action],[SourceID],[SourceName],[SourceStatus],[SourceOriginalId],[Sourceobsolete],[TargetID],[TargetName],[TargetStatus],[TargetOriginalId],[Targetobsolete]);

                        SET IDENTITY_INSERT [ProtocoleType] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [ProtocoleType]');
                        IF @Debug = 1
                        BEGIN
                            SELECT 'RESULT MERGE'

                            SELECT * FROM #tempMergeProtocoleType
                        END


                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
            """
                CREATE PROCEDURE [dbo].[MERGE_ProtocoleType_ObservationDynProp]
                    @Debug INT=1,
                    @ForceRollback INT=1
                AS
                BEGIN
                    BEGIN TRY
                    BEGIN TRAN

                        IF @Debug = 1
                        BEGIN
                            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                        END

                        IF OBJECT_ID('tempdb..#ProtocoleType_ObservationDynPropTMP') IS NOT NULL
                        BEGIN
                            DROP TABLE #ProtocoleType_ObservationDynPropTMP
                        END

                        CREATE TABLE #ProtocoleType_ObservationDynPropTMP(
                            [ID] [int] NOT NULL,
                            [Required] [int] NOT NULL,
                            [FK_ProtocoleType] [int] NULL,
                            [FK_ObservationDynProp] [int] NULL,
                            [Locked] [bit] NULL,
                            [LinkedTable] [varchar](255) NULL,
                            [LinkedField] [varchar](255) NULL,
                            [LinkedID] [varchar](255) NULL,
                            [LinkSourceID] [varchar](255) NULL
                        )

                        INSERT INTO #ProtocoleType_ObservationDynPropTMP([ID],[Required],[FK_ProtocoleType],[FK_ObservationDynProp],[Locked],[LinkedTable],[LinkedField],[LinkedID],[LinkSourceID])
                        SELECT
                        [ID],
                        [Required],
                        [FK_ProtocoleType],
                        [FK_ObservationDynProp],
                        [Locked],
                        [LinkedTable],
                        [LinkedField],
                        [LinkedID],
                        [LinkSourceID]
                        FROM [syn_Referentiel.ProtocoleType_ObservationDynProp]

                        ---- MERGE TABLE ---
                        IF OBJECT_ID('tempdb..#tempMergeProtocoleType_ObservationDynProp') IS NOT NULL
                        BEGIN
                            DROP TABLE #tempMergeProtocoleType_ObservationDynProp
                        END

                        CREATE TABLE #tempMergeProtocoleType_ObservationDynProp(
                            [action] VARCHAR(MAX),
                            [SourceID] [int],
                            [SourceRequired] [int],
                            [SourceFK_ProtocoleType] [int],
                            [SourceFK_ObservationDynProp] [int],
                            [SourceLocked] [bit],
                            [SourceLinkedTable] [varchar](255),
                            [SourceLinkedField] [varchar](255),
                            [SourceLinkedID] [varchar](255),
                            [SourceLinkSourceID] [varchar](255),
                            [TargetID] [int],
                            [TargetRequired] [int],
                            [TargetFK_ProtocoleType] [int],
                            [TargetFK_ObservationDynProp] [int],
                            [TargetLocked] [bit],
                            [TargetLinkedTable] [varchar](255),
                            [TargetLinkedField] [varchar](255),
                            [TargetLinkedID] [varchar](255),
                            [TargetLinkSourceID] [varchar](255)
                        )

                        IF @debug = 1
                        BEGIN
                            SELECT 'DATA TO MERGE'
                            SELECT
                            *
                            FROM #tempMergeProtocoleType_ObservationDynProp

                            SELECT 'WILL BE MERGED WITH TARGET'
                            SELECT
                            [ID],
                            [Required],
                            [FK_ProtocoleType],
                            [FK_ObservationDynProp],
                            [Locked],
                            [LinkedTable],
                            [LinkedField],
                            [LinkedID],
                            [LinkSourceID]
                            FROM [ProtocoleType_ObservationDynProp]
                        END

                        SET IDENTITY_INSERT [ProtocoleType_ObservationDynProp] ON;
                        PRINT('SET IDENTITY_INSERT ON FOR [ProtocoleType_ObservationDynProp]');

                        WITH ProtocoleType_ObservationDynPropFiltered AS
                        (
                            SELECT
                            [ID],
                            [Required],
                            [FK_ProtocoleType],
                            [FK_ObservationDynProp],
                            [Locked],
                            [LinkedTable],
                            [LinkedField],
                            [LinkedID],
                            [LinkSourceID]
                            FROM [ProtocoleType_ObservationDynProp]
                        )
                        MERGE ProtocoleType_ObservationDynPropFiltered AS [TARGET]
                        USING #ProtocoleType_ObservationDynPropTMP AS [SOURCE]
                        ON ( TARGET.[ID] = SOURCE.[ID] )
                        WHEN MATCHED THEN
                            UPDATE SET
                            TARGET.[Required] = SOURCE.[Required],
                            TARGET.[FK_ProtocoleType] = SOURCE.[FK_ProtocoleType],
                            TARGET.[FK_ObservationDynProp] = SOURCE.[FK_ObservationDynProp],
                            TARGET.[Locked] = SOURCE.[Locked],
                            TARGET.[LinkedTable] = SOURCE.[LinkedTable],
                            TARGET.[LinkedField] = SOURCE.[LinkedField],
                            TARGET.[LinkedID] = SOURCE.[LinkedID],
                            TARGET.[LinkSourceID] = SOURCE.[LinkSourceID]

                        WHEN NOT MATCHED BY TARGET THEN
                            INSERT ([ID], [Required], [FK_ProtocoleType], [FK_ObservationDynProp], [Locked], [LinkedTable], [LinkedField], [LinkedID], [LinkSourceID])
                            VALUES ([ID], [Required], [FK_ProtocoleType], [FK_ObservationDynProp], [Locked], [LinkedTable], [LinkedField], [LinkedID], [LinkSourceID])
                        WHEN NOT MATCHED BY SOURCE THEN
                            DELETE

                        OUTPUT $action AS [action],
                        INSERTED.[ID] AS [SourceID],
                        INSERTED.[Required] AS [SourceRequired],
                        INSERTED.[FK_ProtocoleType]  AS [SourceFK_ProtocoleType],
                        INSERTED.[FK_ObservationDynProp] AS [SourceFK_ObservationDynProp],
                        INSERTED.[Locked] AS [SourceLocked],
                        INSERTED.[LinkedTable] AS [SourceLinkedTable],
                        INSERTED.[LinkedField]  AS [SourceLinkedField],
                        INSERTED.[LinkedID] AS [SourceLinkedID],
                        INSERTED.[LinkSourceID] AS [SourceLinkSourceID],
                        DELETED.[ID] AS [TargetID],
                        DELETED.[Required]  AS [TargetRequired],
                        DELETED.[FK_ProtocoleType]  AS [TargetFK_ProtocoleType],
                        DELETED.[FK_ObservationDynProp]  AS [TargetFK_ObservationDynProp],
                        DELETED.[Locked] AS [TargetLocked],
                        DELETED.[LinkedTable]  AS [TargetLinkedTable],
                        DELETED.[LinkedField]  AS [TargetLinkedField],
                        DELETED.[LinkedID]  AS [TargetLinkedID],
                        DELETED.[LinkSourceID] AS [TargetLinkSourceID]
                        INTO #tempMergeProtocoleType_ObservationDynProp([action],[SourceID],[SourceRequired],[SourceFK_ProtocoleType],[SourceFK_ObservationDynProp],[SourceLocked],[SourceLinkedTable],[SourceLinkedField],[SourceLinkedID],[SourceLinkSourceID],[TargetID],[TargetRequired],[TargetFK_ProtocoleType],[TargetFK_ObservationDynProp],[TargetLocked],[TargetLinkedTable],[TargetLinkedField],[TargetLinkedID],[TargetLinkSourceID]);

                        SET IDENTITY_INSERT [ProtocoleType_ObservationDynProp] OFF;
                        PRINT('SET IDENTITY_INSERT OFF FOR [ProtocoleType_ObservationDynProp]');

                        IF @Debug = 1
                        BEGIN
                            SELECT 'RESULT MERGE'

                            SELECT * FROM #tempMergeProtocoleType_ObservationDynProp
                        END


                        IF @@TRANCOUNT > 0
                        BEGIN
                            IF @ForceRollback = 1
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                ;THROW 50001, N'FORCED ROLLBACK', 1;
                                ROLLBACK TRAN
                            END
                            ELSE
                            BEGIN
                                IF @Debug = 1
                                BEGIN
                                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                                END
                                COMMIT TRAN
                            END
                        END
                    END TRY
                    BEGIN CATCH
                        IF @@TRANCOUNT > 0
                            BEGIN
                                ROLLBACK TRAN
                            END
                            ;THROW
                    END CATCH
                END
            """
        )

        op.execute(
        """
            CREATE PROCEDURE [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables]
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    --- Disable all constraints for database
                    EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

                    --- ORDER IS  NOT IMPORTANT  BECAUSE WE DISABLE ALL CONSTRAINTS ---
                    EXEC [MERGE_fieldActivity_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_fieldActivity]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_FrontModules]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleForms]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleGrids]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleFormsInternationalization]  @Debug = 0, @ForceRollback = 0

                    --- Enable all constraints for database and launch check  ---
                    EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
        """
    )
def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', True) is False:
        op.execute(
            """
                DROP SYNONYM [dbo].[syn_Referentiel.BusinessRules]

                DROP SYNONYM [dbo].[syn_Referentiel.fieldActivity]

                DROP SYNONYM [dbo].[syn_Referentiel.FieldActivity_ProtocoleType]

                DROP SYNONYM [dbo].[syn_Referentiel.FrontModules]

                DROP SYNONYM [dbo].[syn_Referentiel.IndividualDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.IndividualType]

                DROP SYNONYM [dbo].[syn_Referentiel.IndividualType_IndividualDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.Language]

                DROP SYNONYM [dbo].[syn_Referentiel.ModuleForms]

                DROP SYNONYM [dbo].[syn_Referentiel.ModuleFormsInternationalization]

                DROP SYNONYM [dbo].[syn_Referentiel.ModuleGrids]

                DROP SYNONYM [dbo].[syn_Referentiel.MonitoredSiteDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.MonitoredSiteType]

                DROP SYNONYM [dbo].[syn_Referentiel.MonitoredSiteType_MonitoredSiteDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.ObservationDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.ProtocoleType]

                DROP SYNONYM [dbo].[syn_Referentiel.ProtocoleType_ObservationDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.SensorDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.SensorType]

                DROP SYNONYM [dbo].[syn_Referentiel.SensorType_SensorDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.StationDynProp]

                DROP SYNONYM [dbo].[syn_Referentiel.StationType]

                DROP SYNONYM [dbo].[syn_Referentiel.StationType_StationDynProp]

            """
        )

        op.execute(
        """
        IF OBJECT_ID('[MERGE_fieldActivity_ProtocoleType]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_fieldActivity_ProtocoleType]

        IF OBJECT_ID('[MERGE_fieldActivity]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_fieldActivity]

        IF OBJECT_ID('[MERGE_FrontModules]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_FrontModules]

        IF OBJECT_ID('[MERGE_ModuleForms]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_ModuleForms]

        IF OBJECT_ID('[MERGE_ModuleGrids]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_ModuleGrids]

        IF OBJECT_ID('[MERGE_ProtocoleType_ObservationDynProp]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_ProtocoleType_ObservationDynProp]

        IF OBJECT_ID('[MERGE_ObservationDynProp]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_ObservationDynProp]

        IF OBJECT_ID('[MERGE_ProtocoleType]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_ProtocoleType]

        IF OBJECT_ID('[MERGE_ModuleFormsInternationalization]', 'P') IS NOT NULL
        DROP PROCEDURE [MERGE_ModuleFormsInternationalization]

        IF OBJECT_ID('[EXEC_MERGE_All_Referential_Configurations_Tables]', 'P') IS NOT NULL
        DROP PROCEDURE [EXEC_MERGE_All_Referential_Configurations_Tables]
        """
    )
