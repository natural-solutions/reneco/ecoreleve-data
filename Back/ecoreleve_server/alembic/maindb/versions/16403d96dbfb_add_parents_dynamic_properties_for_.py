"""add parents' dynamic properties for monitored "nest and brood" sites

Revision ID: 16403d96dbfb
Revises: e655cdb0841f
Create Date: 2024-02-05 14:13:50.265819

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '16403d96dbfb'
down_revision = 'e655cdb0841f'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @MonitoredSiteFormObject INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE Name = 'MonitoredSiteForm'
            );

            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM MonitoredSiteType
                WHERE Name LIKE 'Nest and Brood'
            );

            INSERT INTO ModuleForms
            VALUES
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Transmitter_Shape_FK_Individual_1', 'Transmitter Shape', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 14, 'Parentage Information', 204113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Transmitter_Model_FK_Individual_1', 'Transmitter Model', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 17, 'Parentage Information', 204114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Transmitter_Company_FK_Individual_1', 'Transmitter Company', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 20, 'Parentage Information', 1204132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_position_1_FK_Individual_1', 'Ring position 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 23, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_color_1_FK_Individual_1', 'Ring color 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 26, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_position_2_FK_Individual_1', 'Ring position 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 29, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_color_2_FK_Individual_1', 'Ring color 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 32, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Microchip_FK_Individual_1', 'Microchip', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 35, 'Parentage Information', 204112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Position_1_FK_Individual_1', 'Mark Position 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 38, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Color_1_FK_Individual_1', 'Mark Color 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 41, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Position_2_FK_Individual_1', 'Mark Position 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 44, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Color_2_FK_Individual_1', 'Mark Color 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 39, 47, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Transmitter_Shape_FK_Individual_2', 'Transmitter Shape', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 15, 'Parentage Information', 204113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Transmitter_Model_FK_Individual_2', 'Transmitter Model', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 18, 'Parentage Information', 204114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Transmitter_Company_FK_Individual_2', 'Transmitter Company', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 21, 'Parentage Information', 1204132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_position_1_FK_Individual_2', 'Ring position 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 24, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_color_1_FK_Individual_2', 'Ring color 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 27, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_position_2_FK_Individual_2', 'Ring position 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 30, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Ring_color_2_FK_Individual_2', 'Ring color 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 33, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Microchip_FK_Individual_2', 'Microchip', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 36, 'Parentage Information', 204112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Position_1_FK_Individual_2', 'Mark Position 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 39, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Color_1_FK_Individual_2', 'Mark Color 1', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 42, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Position_2_FK_Individual_2', 'Mark Position 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 45, 'Parentage Information', 204107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
            (@MonitoredSiteFormObject, @NestAndBroodType, 'Mark_Color_2_FK_Individual_2', 'Mark Color 2', 0, 4, 4, 'AutocompTreeEditor', 'form-control displayInput', 7, 48, 'Parentage Information', 204108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @MonitoredSiteFormObject INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE Name = 'MonitoredSiteForm'
            );

            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM MonitoredSiteType
                WHERE Name LIKE 'Nest and Brood'
            );

            DELETE FROM ModuleForms
            WHERE [module_id] = @MonitoredSiteFormObject 
                AND [TypeObj] = @NestAndBroodType 
                AND [Name] Like '%_FK_Individual_%';
            """
        )
