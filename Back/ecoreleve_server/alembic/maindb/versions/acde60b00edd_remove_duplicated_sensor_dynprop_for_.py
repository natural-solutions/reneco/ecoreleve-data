"""remove duplicated sensor dynprop for gsm and satellite

Revision ID: acde60b00edd
Revises: 0e9a0f612125
Create Date: 2024-12-04 16:12:50.676659

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'acde60b00edd'
down_revision = '0e9a0f612125'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        DECLARE @ID_Frequency INT = (
            SELECT [ID] 
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
        )

        DECLARE @ID_Satellite INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'Satellite'
        )

        DECLARE @ID_GSM INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'GSM'
        )

        DELETE FROM SensorType_SensorDynProp
        WHERE [ID] = (
            SELECT TOP 1 [ID]
            FROM SensorType_SensorDynProp
            WHERE [FK_SensorType] = @ID_Satellite AND [FK_SensorDynProp] = @ID_Frequency
        )

        DELETE FROM SensorType_SensorDynProp
        WHERE [ID] = (
            SELECT TOP 1 [ID]
            FROM SensorType_SensorDynProp
            WHERE [FK_SensorType] = @ID_GSM AND [FK_SensorDynProp] = @ID_Frequency
        )
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        DECLARE @ID_Frequency INT = (
            SELECT [ID] 
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
        )

        DECLARE @ID_Satellite INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'Satellite'
        )

        DECLARE @ID_GSM INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'GSM'
        )

        INSERT INTO SensorType_SensorDynProp
        VALUES 
            (0, @ID_GSM, @ID_Frequency),
            (0, @ID_Satellite, @ID_Frequency)
        """
    )
