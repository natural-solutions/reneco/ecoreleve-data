"""fieldworkarea not required for station without coordinate

Revision ID: 8fe9d30d13bf
Revises: 3c76a26a85c6
Create Date: 2023-06-20 10:45:11.183340

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8fe9d30d13bf'
down_revision = '3c76a26a85c6'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            UPDATE [ModuleForms]
            SET [Required] = 0
            WHERE
            [module_id] = 2
            AND
            [TypeObj] = 3
            AND
            [Name] = 'FK_FieldworkArea'
            """
        )

    if options_for_migration.get('is_referential_instance', True) is False:
        op.execute(
            """
            ALTER PROCEDURE [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables]
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    --- Disable all constraints for database
                    EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

                    --- ORDER IS  NOT IMPORTANT  BECAUSE WE DISABLE ALL CONSTRAINTS ---
                    EXEC [MERGE_fieldActivity_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_fieldActivity]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_FrontModules]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleForms]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleGrids]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleFormsInternationalization]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0

                    --- Enable all constraints for database and launch check  ---
                    DECLARE @TEMPORARY_TABLE_CONSTRAINT TABLE( [STMT] VARCHAR(MAX), [NB] INT)
                    INSERT INTO @TEMPORARY_TABLE_CONSTRAINT([STMT],[NB])
                    SELECT
                    STRING_AGG(
                        CASE
                            -- special case when a location not in any fieldworkaera, fk is set to 0
                            -- we need to "disable" the fk constraint
                            WHEN A.[CK_Name] IN('FK_Station_FK_FieldworkArea_Fieldworkarea','FK_Individual_Location_FK_FieldworkArea_Fieldworkarea', 'FK_MonitoredSitePosition_FK_FieldworkArea_FieldworkArea')
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' NOCHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- special case when a location not in any administrativearea, fk is set to 0
                            -- we need to "disable" the fk constraint
                            WHEN A.[CK_Name] IN('FK_Individual_Location_FK_AdministrativeArea_AdministrativeArea','FK_Station_FK_AdministrativeArea_AdministrativeArea', 'FK_MonitoredSitePosition_FK_AdministrativeArea_AdministrativeArea')
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' NOCHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- special case for merge replication
                            -- we need to enable the check on id used but we didn't force the check
                            -- force the check raise an error as if narc try to use id not in his range
                            WHEN A.[CK_Name] like 'repl_identity_range_%'
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' WITH NOCHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- enable the constraint and force the check
                            ELSE N''+'ALTER TABLE '+A.[Table_Name]+' WITH CHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                        END,
                        ';'
                    ) AS [STMT],
                    ROW_NUMBER() OVER(ORDER BY [CK_Name], [Table_Name]) AS [NB]
                    FROM (
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.check_constraints
                        UNION ALL
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.foreign_keys
                    ) AS A
                    GROUP BY [CK_Name], [Table_Name]

                    DECLARE @stmt NVARCHAR(MAX) = NULL;
                    DECLARE @nb INT = NULL;

                    SELECT TOP 1 @stmt = [stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    WHILE EXISTS(SELECT * FROM @TEMPORARY_TABLE_CONSTRAINT)
                    BEGIN
                        EXEC sp_executesql @stmt
                        DELETE TOP(1) FROM @TEMPORARY_TABLE_CONSTRAINT WHERE [NB]=@nb;
                        SELECT TOP 1 @stmt=[stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    END

                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
            """
        )


def downgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            UPDATE [ModuleForms]
            SET [Required] = 1
            WHERE
            [module_id] = 2
            AND
            [TypeObj] = 3
            AND
            [Name] = 'FK_FieldworkArea'
            """
        )

    if options_for_migration.get('is_referential_instance', True) is False:
        op.execute(
            """
            ALTER PROCEDURE [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables]
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    --- Disable all constraints for database
                    EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

                    --- ORDER IS  NOT IMPORTANT  BECAUSE WE DISABLE ALL CONSTRAINTS ---
                    EXEC [MERGE_fieldActivity_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_fieldActivity]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_FrontModules]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleForms]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleGrids]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleFormsInternationalization]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0

                    --- Enable all constraints for database and launch check  ---
                    DECLARE @TEMPORARY_TABLE_CONSTRAINT TABLE( [STMT] VARCHAR(MAX), [NB] INT)
                    INSERT INTO @TEMPORARY_TABLE_CONSTRAINT([STMT],[NB])
                    SELECT
                    STRING_AGG(
                        CASE
                            -- special case when a location not in any fieldworkaera, fk is set to 0
                            -- we need to "disable" the fk constraint
                            WHEN A.[CK_Name] IN('FK_Station_FK_FieldworkArea_Fieldworkarea','FK_Individual_Location_FK_FieldworkArea_Fieldworkarea')
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' NOCHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- special case for merge replication
                            -- we need to enable the check on id used but we didn't force the check
                            -- force the check raise an error as if narc try to use id not in his range
                            WHEN A.[CK_Name] like 'repl_identity_range_%'
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' WITH NOCHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- enable the constraint and force the check
                            ELSE N''+'ALTER TABLE '+A.[Table_Name]+' WITH CHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                        END,
                        ';'
                    ) AS [STMT],
                    ROW_NUMBER() OVER(ORDER BY [CK_Name], [Table_Name]) AS [NB]
                    FROM (
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.check_constraints
                        UNION ALL
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.foreign_keys
                    ) AS A
                    GROUP BY [CK_Name], [Table_Name]

                    DECLARE @stmt NVARCHAR(MAX) = NULL;
                    DECLARE @nb INT = NULL;

                    SELECT TOP 1 @stmt = [stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    WHILE EXISTS(SELECT * FROM @TEMPORARY_TABLE_CONSTRAINT)
                    BEGIN
                        EXEC sp_executesql @stmt
                        DELETE TOP(1) FROM @TEMPORARY_TABLE_CONSTRAINT WHERE [NB]=@nb;
                        SELECT TOP 1 @stmt=[stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    END

                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
            """
        )
