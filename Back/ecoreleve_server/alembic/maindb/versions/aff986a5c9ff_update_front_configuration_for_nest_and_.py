"""Update front configuration for 'Nest and Brood' monitored site type

Revision ID: aff986a5c9ff
Revises: d8ec1b889ef2
Create Date: 2022-11-08 15:01:51.835605

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'aff986a5c9ff'
down_revision = 'd8ec1b889ef2'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            -- ID du formulaire des sites monitorés
            DECLARE @NestAndBroodObject INT = (
                SELECT ID
                FROM [FrontModules]
                WHERE Name LIKE 'MonitoredSiteForm'
            );

            -- ID du type de site monitoré "nest and brood"
            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM [MonitoredSiteType]
                WHERE Name LIKE 'Nest and Brood'
            );
                                                                        
            -- Ajout des nouveaux champs
            INSERT INTO [ModuleForms] (
                [module_id],
                [TypeObj],
                [Name],
                [Label],
                [Required],
                [FieldSizeEdit],
                [FieldSizeDisplay],
                [InputType],
                [editorClass],
                [FormRender],
                [FormOrder],
                [Legend],
                [Options],
                [Validators],
                [displayClass],
                [EditClass],
                [Status],
                [Locked],
                [DefaultValue],
                [Rules],
                [Orginal_FB_ID]
            )
            VALUES 
                (@NestAndBroodObject, @NestAndBroodType, 'FK_Individual_1', 'Parent 1', 0, 4, 4, 'ObjectPicker', '', 7, 11, 'Parentage Information', '{"usedLabel":"ID","operator":"startswith","triggerAt"\:1}', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL),
                (@NestAndBroodObject, @NestAndBroodType, 'FK_Individual_2', 'Parent 2', 0, 4, 4, 'ObjectPicker', '', 7, 12, 'Parentage Information', '{"usedLabel":"ID","operator":"startswith","triggerAt"\:1}', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL),
                (@NestAndBroodObject, @NestAndBroodType, 'Taxon', 'Taxon', 1, 4, 4, 'AutocompTreeEditor', 'form-control', 7, 13, 'Parentage Information', '204089', NULL, NULL, NULL, 1, 1, NULL, NULL, NULL),
                (@NestAndBroodObject, @NestAndBroodType, 'Category', 'Category', 1, 6, 6, 'AutocompTreeEditor', 'form-control', 1, 2, 'General Information', '223070', NULL, NULL, NULL, 1, 1, 'Nest and brood', NULL, NULL);
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @NestAndBroodObject INT = (
                SELECT [ID]
                FROM [FrontModules]
                WHERE Name LIKE 'MonitoredSiteForm'
            );

            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM [MonitoredSiteType]
                WHERE Name LIKE 'Nest and Brood'
            );
                                                                        
            DELETE FROM [ModuleForms]
            WHERE  [module_id] = @NestAndBroodObject
                AND [TypeObj] = @NestAndBroodType
            """
        )
