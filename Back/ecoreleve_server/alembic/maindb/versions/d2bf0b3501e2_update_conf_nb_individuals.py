"""update_conf_nb_individuals

Revision ID: d2bf0b3501e2
Revises: 71216c2da2b4
Create Date: 2023-01-25 15:28:57.785276

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd2bf0b3501e2'
down_revision = '71216c2da2b4'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                UPDATE [ModuleForms]
                SET [Rules] = '{"operator":"count","target":"Nb_Individuals","source":"Release_Individual"}'
                WHERE
                [TypeObj] = 216
                AND
                [Name] = 'Nb_Individuals'
                AND
                [module_id] = 1
            """
        )


def downgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                UPDATE [ModuleForms]
                SET [Rules] = NULL
                WHERE
                [TypeObj] = 216
                AND
                [Name] = 'Nb_Individuals'
                AND
                [module_id] = 1
            """
        )
