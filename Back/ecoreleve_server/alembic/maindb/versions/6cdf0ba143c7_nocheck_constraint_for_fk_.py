"""nocheck constraint for FK_Fieldworkareas in IndividualLocations

Revision ID: 6cdf0ba143c7
Revises: ccbe4c5a6794
Create Date: 2024-03-21 12:50:54.823767

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6cdf0ba143c7'
down_revision = 'ccbe4c5a6794'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER TABLE Individual_Location NOCHECK CONSTRAINT [FK_Individual_Location_FK_FieldworkArea_FieldworkArea]
        """
    )


def downgrade(**options_for_migration) -> None:
     pass
