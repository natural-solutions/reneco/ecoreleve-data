"""add frequency field to GSM and satellite sensors

Revision ID: c067c84dd99b
Revises: ae051f622101
Create Date: 2024-09-24 11:28:18.913957

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c067c84dd99b'
down_revision = 'ae051f622101'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:

    op.execute(
        """
        DECLARE @Type_GSM_ID INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'GSM'
        )

        DECLARE @Type_Satellite_ID INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'Satellite'
        )

        DECLARE @DynProp_Frequency_ID INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
        )

        INSERT INTO SensorType_SensorDynProp ([Required],[FK_SensorType],[FK_SensorDynProp])
        VALUES 
            (0, @Type_GSM_ID, @DynProp_Frequency_ID),
            (0, @Type_Satellite_ID, @DynProp_Frequency_ID)
        """
    )

    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @sensor_moduleID INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            DECLARE @Type_GSM_ID INT = (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] = 'GSM'
            )

            DECLARE @Type_Satellite_ID INT = (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] = 'Satellite'
            )

            INSERT INTO ModuleForms (
                [module_id],
                [TypeObj],
                [Name],
                [Label],
                [Required],
                [FieldSizeEdit],
                [FieldSizeDisplay],
                [InputType],
                [editorClass],
                [FormRender],
                [FormOrder],
                [Legend],
                [Options],
                [Validators],
                [displayClass],
                [EditClass],
                [Status],
                [Locked],
                [DefaultValue],
                [Rules],
                [Orginal_FB_ID]
            )
            VALUES
                (@sensor_moduleID, @Type_Satellite_ID, 'Transmitter_Frequency', 'Frequency', 0, 6, 6, 'Number', 'form-control', 7, 37, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
                (@sensor_moduleID, @Type_GSM_ID, 'Transmitter_Frequency', 'Frequency', 0, 6, 6, 'Number', 'form-control', 7, 37, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
            """
        )


def downgrade(**options_for_migration) -> None:
    
    op.execute(
        """
        DECLARE @Type_GSM_ID INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'GSM'
        )

        DECLARE @Type_Satellite_ID INT = (
            SELECT [ID]
            FROM SensorType
            WHERE [Name] = 'Satellite'
        )

        DECLARE @DynProp_Frequency_ID INT = (
            SELECT [ID]
            FROM SensorDynProp
            WHERE [Name] = 'Transmitter_Frequency'
        )

        DELETE SensorDynPropValue
        FROM SensorDynPropValue SDPV
        JOIN Sensor S ON S.[ID] = SDPV.[FK_Sensor]
        WHERE (SDPV.[FK_SensorDynProp] = @DynProp_Frequency_ID AND S.[FK_SensorType] = @Type_GSM_ID)
            OR (SDPV.[FK_SensorDynProp] = @DynProp_Frequency_ID AND S.[FK_SensorType] = @Type_Satellite_ID)

        DELETE FROM SensorType_SensorDynProp
        WHERE ([FK_SensorType] = @Type_GSM_ID AND [FK_SensorDynProp] = @DynProp_Frequency_ID)
            OR ([FK_SensorType] = @Type_Satellite_ID AND [FK_SensorDynProp] = @DynProp_Frequency_ID)
        """
    )

    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @sensor_moduleID INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'SensorForm'
            )

            DECLARE @Type_GSM_ID INT = (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] = 'GSM'
            )

            DECLARE @Type_Satellite_ID INT = (
                SELECT [ID]
                FROM SensorType
                WHERE [Name] = 'Satellite'
            )

            DELETE FROM ModuleForms
            WHERE 
                [module_id] = 16  
                AND ([TypeObj] = @Type_Satellite_ID OR [TypeObj] = @Type_GSM_ID) 
                AND [Name] = 'Transmitter_Frequency'
            """
        )
