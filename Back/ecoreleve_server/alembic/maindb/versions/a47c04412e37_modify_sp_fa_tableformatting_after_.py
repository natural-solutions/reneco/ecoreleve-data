"""modify sp_FA_TableFormatting after update prod

Revision ID: a47c04412e37
Revises: 4079ac44a872
Create Date: 2022-12-14 14:11:50.482319

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a47c04412e37'
down_revision = '4079ac44a872'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
            ALTER PROCEDURE sp_FA_TableFormatting
                -- ===================================================
                -- Formatting new fieldwork area table
                --
                -- This stored procedure creates the Fieldworkarea 
                -- update table with the new layers and columns in the
                -- right formats (geom, fullpath etc)
                -- ===================================================
            AS
            BEGIN

                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;

                --
                IF OBJECT_ID('Fieldworkarea_update') IS NOT NULL DROP TABLE Fieldworkarea_update
                CREATE TABLE Fieldworkarea_update( 	
                    [ID] [int] IDENTITY(1,1) primary key NOT NULL,
                    [Country] [nvarchar](255) NULL,
                    [Working_Area] [varchar](255) NULL,
                    [Working_Region] [nvarchar](255) NULL,
                    [Management_Unit] [varchar](255) NULL,
                    [Name] [varchar](255) NULL,
                    [fullpath] [varchar](255) NULL,
                    [type_] [varchar](50) NULL,
                    [max_lat] [decimal](9, 5) NULL,
                    [min_lat] [decimal](9, 5) NULL,
                    [max_lon] [decimal](9, 5) NULL,
                    [min_lon] [decimal](9, 5) NULL,
                    [SHAPE_Leng] [real] NULL,
                    [SHAPE_Area] [real] NULL,
                    [valid_geom] [geometry] NULL,
                    [geom] [geometry] NULL,
                    [Status] [varchar](255) NULL,
                    [Management_Status] [varchar](255) NULL,
                    [Communication_Name] [varchar](255) NULL,
                    [Center] [varchar](255) NULL,
                    [Data_Owner] [varchar](255) NULL,
                    [sponsor] [varchar](255) NULL,
                    [Centroid_Latitude] [DECIMAL](9,5) NULL,
                    [Centroid_Longitude] [DECIMAL](9,5) NULL
                )

                --
                DECLARE @tableFieldworAreaTemp TABLE( 	
                    [ID] [int] NOT NULL,
                    [Country] [nvarchar](255) NULL,
                    [Working_Area] [varchar](255) NULL,
                    [Working_Region] [nvarchar](255) NULL,
                    [Management_Unit] [varchar](255) NULL,
                    [Name] [varchar](255) NULL,
                    [fullpath] [varchar](255) NULL,
                    [type_] [varchar](50) NULL,
                    [max_lat] [decimal](9, 5) NULL,
                    [min_lat] [decimal](9, 5) NULL,
                    [max_lon] [decimal](9, 5) NULL,
                    [min_lon] [decimal](9, 5) NULL,
                    [SHAPE_Leng] [real] NULL,
                    [SHAPE_Area] [real] NULL,
                    [geom] [geometry] NULL,
                    [valid_geom] [geometry] NULL,
                    [current_st] [varchar](255) NULL,
                    [mngt_statu] [varchar](255) NULL,
                    [com_name] [varchar](255) NULL,
                    [center] [varchar](255) NULL,
                    [data_owner] [varchar](255) NULL,
                    [sponsor] [varchar](255) NULL,
                    [Centroid_Latitude] [DECIMAL](9,5) NULL,
                    [Centroid_Longitude] [DECIMAL](9,5) NULL
                )

                --
                INSERT INTO @tableFieldworAreaTemp (
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [current_st], [mngt_statu], [com_name],
                    [center], [data_owner]
                )
                SELECT 
                    [FirstPart].[ID],
                    [Country],
                    [Working_Area],
                    [Working_Region],
                    [Management_Unit],
                    [Name],
                    [fullpath],
                    [type_],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [SHAPE_Leng],
                    [SHAPE_Area],
                    [geom],
                    [valid_geom],
                    [current_st],
                    [mngt_statu],
                    [com_name],
                    [center],
                    [data_owner]
                FROM (
                    SELECT
                        [eco_id] AS [ID],
                        [country] AS [Country],
                        [w_area] AS [Working_Area],
                        [w_region] AS [Working_Region],
                        [mngt_unit] AS [Management_Unit],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN [mngt_unit]
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN [w_region]
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN [w_area]
                            WHEN [country] IS NOT NULL
                                THEN [country]
                            ELSE NULL
                        END AS [Name],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area],'>',[w_region],'>',[mngt_unit])
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area],'>',[w_region])
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area])
                            WHEN [country] IS NOT NULL
                                THEN [country]
                            ELSE NULL
                        END AS [fullpath],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN 'management unit'
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN 'working region'
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN 'working area'
                            WHEN [country] IS NOT NULL
                                THEN 'country'
                            ELSE NULL
                        END AS [type_],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                        CONVERT(REAL,[SHAPE_Leng]) AS [SHAPE_Leng],
                        CONVERT(REAL,[SHAPE_Area])  AS [SHAPE_Area],
                        [current_st],
                        [mngt_statu],
                        [com_name],
                        [center],
                        [data_owner]
                    FROM reneco_fieldworkarea
                    GROUP BY [eco_id], [country], [w_area], [w_region], [mngt_unit],[SHAPE_Leng], [SHAPE_Area], [current_st], [mngt_statu], [com_name], [center], [data_owner]
                ) AS FirstPart
                JOIN (
                    SELECT 
                        [eco_id] AS [ID],
                        [ogr_geometry] AS [geom],
                        [ogr_geometry].MakeValid() AS [valid_geom]
                    FROM reneco_fieldworkarea
                ) AS SecondPart
                ON [FirstPart].[ID] = [SecondPart].[ID]
                ORDER BY [country] ASC, [Working_Area] ASC, [Working_Region] ASC, [Management_Unit] ASC

                SET IDENTITY_INSERT Fieldworkarea_update ON;

                INSERT INTO Fieldworkarea_update (
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [Status], [Management_Status], [Communication_Name], [Center], [Data_Owner]
                )
                SELECT
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [current_st], [mngt_statu], [com_name], 
                    [center], [data_owner]
                FROM @tableFieldworAreaTemp

                -- Insert explicit values in the identity column is prohibited
                SET IDENTITY_INSERT Fieldworkarea_update OFF


                IF EXISTS (
                    SELECT * 
                    FROM sys.indexes 
                    WHERE name = 'IX_Fieldworkarea_valid_geom' AND object_id = OBJECT_ID('FieldworkArea')
                ) DROP INDEX [IX_Fieldworkarea_valid_geom] ON [FieldworkArea]

                CREATE SPATIAL INDEX [IX_Fieldworkarea_valid_geom] ON [FieldworkArea_update] (
                [valid_geom]
                ) USING  GEOMETRY_GRID 
                WITH (BOUNDING_BOX =(-90, -180, 90, 180), GRIDS = (LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
                CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
            ALTER PROCEDURE sp_FA_TableFormatting
                -- ===================================================
                -- Formatting new fieldwork area table
                --
                -- This stored procedure creates the Fieldworkarea 
                -- update table with the new layers and columns in the
                -- right formats (geom, fullpath etc)
                -- ===================================================
            AS
            BEGIN

                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;

                --
                IF OBJECT_ID('Fieldworkarea_update') IS NOT NULL DROP TABLE Fieldworkarea_update
                CREATE TABLE Fieldworkarea_update( 	
                    [ID] [int] IDENTITY(1,1) primary key NOT NULL,
                    [Country] [nvarchar](255) NULL,
                    [Working_Area] [varchar](255) NULL,
                    [Working_Region] [nvarchar](255) NULL,
                    [Management_Unit] [varchar](255) NULL,
                    [Name] [varchar](255) NULL,
                    [fullpath] [varchar](255) NULL,
                    [type_] [varchar](50) NULL,
                    [max_lat] [decimal](9, 5) NULL,
                    [min_lat] [decimal](9, 5) NULL,
                    [max_lon] [decimal](9, 5) NULL,
                    [min_lon] [decimal](9, 5) NULL,
                    [SHAPE_Leng] [real] NULL,
                    [SHAPE_Area] [real] NULL,
                    [valid_geom] [geometry] NULL,
                    [geom] [geometry] NULL,
                    [Status] [varchar](255) NULL,
                    [Management_Status] [varchar](255) NULL,
                    [Communication_Name] [varchar](255) NULL,
                    [Center] [varchar](255) NULL,
                    [Data_Owner] [varchar](255) NULL,
                    [sponsor] [varchar](255) NULL,
                    [Centroid_Latitude] [DECIMAL](9,5) NULL,
                    [Centroid_Longitude] [DECIMAL](9,5) NULL
                )

                --
                DECLARE @tableFieldworAreaTemp TABLE( 	
                    [ID] [int] NOT NULL,
                    [Country] [nvarchar](255) NULL,
                    [Working_Area] [varchar](255) NULL,
                    [Working_Region] [nvarchar](255) NULL,
                    [Management_Unit] [varchar](255) NULL,
                    [Name] [varchar](255) NULL,
                    [fullpath] [varchar](255) NULL,
                    [type_] [varchar](50) NULL,
                    [max_lat] [decimal](9, 5) NULL,
                    [min_lat] [decimal](9, 5) NULL,
                    [max_lon] [decimal](9, 5) NULL,
                    [min_lon] [decimal](9, 5) NULL,
                    [SHAPE_Leng] [real] NULL,
                    [SHAPE_Area] [real] NULL,
                    [geom] [geometry] NULL,
                    [valid_geom] [geometry] NULL,
                    [current_st] [varchar](255) NULL,
                    [mngt_statu] [varchar](255) NULL,
                    [com_name] [varchar](255) NULL,
                    [center] [varchar](255) NULL,
                    [data_owner] [varchar](255) NULL,
                    [sponsor] [varchar](255) NULL,
                    [Centroid_Latitude] [DECIMAL](9,5) NULL,
                    [Centroid_Longitude] [DECIMAL](9,5) NULL
                )

                --
                INSERT INTO @tableFieldworAreaTemp (
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [current_st], [mngt_statu], [com_name],
                    [center], [data_owner]
                )
                SELECT 
                    [FirstPart].[ID],
                    [Country],
                    [Working_Area],
                    [Working_Region],
                    [Management_Unit],
                    [Name],
                    [fullpath],
                    [type_],
                    [max_lat],
                    [min_lat],
                    [max_lon],
                    [min_lon],
                    [SHAPE_Leng],
                    [SHAPE_Area],
                    [geom],
                    [valid_geom],
                    [current_st],
                    [mngt_statu],
                    [com_name],
                    [center],
                    [data_owner]
                FROM (
                    SELECT
                        [eco_id] AS [ID],
                        [country] AS [Country],
                        [w_area] AS [Working_Area],
                        [w_region] AS [Working_Region],
                        [mngt_unit] AS [Management_Unit],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN [mngt_unit]
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN [w_region]
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN [w_area]
                            WHEN [country] IS NOT NULL
                                THEN [country]
                            ELSE NULL
                        END AS [Name],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area],'>',[w_region],'>',[mngt_unit])
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area],'>',[w_region])
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN CONCAT([country],'>',[w_area])
                            WHEN [country] IS NOT NULL
                                THEN [country]
                            ELSE NULL
                        END AS [fullpath],
                        CASE 
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL AND [mngt_unit] IS NOT NULL
                                THEN 'management unit'
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL AND [w_region] IS NOT NULL
                                THEN 'working region'
                            WHEN [country] IS NOT NULL AND [w_area] IS NOT NULL
                                THEN 'working area'
                            WHEN [country] IS NOT NULL
                                THEN 'country'
                            ELSE NULL
                        END AS [type_],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
                        CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
                        CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
                        CONVERT(REAL,[SHAPE_Leng]) AS [SHAPE_Leng],
                        CONVERT(REAL,[SHAPE_Area])  AS [SHAPE_Area],
                        [current_st],
                        [mngt_statu],
                        [com_name],
                        [center],
                        [data_owner]
                    FROM reneco_fieldworkarea
                    GROUP BY [eco_id], [country], [w_area], [w_region], [mngt_unit],[SHAPE_Leng], [SHAPE_Area], [current_st], [mngt_statu], [com_name], [center], [data_owner]
                ) AS FirstPart
                JOIN (
                    SELECT 
                        [eco_id] AS [ID],
                        [ogr_geometry] AS [geom],
                        [ogr_geometry].MakeValid() AS [valid_geom]
                    FROM reneco_fieldworkarea
                ) AS SecondPart
                ON [FirstPart].[ID] = [SecondPart].[ID]
                ORDER BY [country] ASC, [Working_Area] ASC, [Working_Region] ASC, [Management_Unit] ASC

                SET IDENTITY_INSERT Fieldworkarea_update ON;

                INSERT INTO Fieldworkarea_update (
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [Status], [Management_Status], [Communication_Name], [Center], [Data_Owner]
                )
                SELECT
                    [ID], [Country], [Working_Area], [Working_Region], [Management_Unit], 
                    [Name], [fullpath], [type_], [max_lat], [min_lat], [max_lon], [min_lon], 
                    [SHAPE_Leng], [SHAPE_Area], [geom], [valid_geom], [current_st], [mngt_statu], [com_name], 
                    [center], [data_owner]
                FROM @tableFieldworAreaTemp

                -- Insert explicit values in the identity column is prohibited
                SET IDENTITY_INSERT Fieldworkarea_update OFF


                IF EXISTS (
                    SELECT * 
                    FROM sys.indexes 
                    WHERE name='SIndx_FielworkArea_geom' AND object_id = OBJECT_ID('FieldworkArea')
                ) DROP INDEX [SIndx_FielworkArea_geom] ON [FieldworkArea]

                CREATE SPATIAL INDEX [SIndx_FielworkArea_geom] ON [FieldworkArea_update] (
                [valid_geom]
                ) USING  GEOMETRY_GRID 
                WITH (BOUNDING_BOX =(-90, -180, 90, 180), GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
                CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
            END
        """
    )
