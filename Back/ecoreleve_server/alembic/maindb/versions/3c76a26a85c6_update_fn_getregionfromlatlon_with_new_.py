"""update fn_GetRegionFromLatLon with new Fieldwork area schema

Revision ID: 3c76a26a85c6
Revises: 12a3c5b43973
Create Date: 2023-06-13 17:14:04.425305

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3c76a26a85c6'
down_revision = '12a3c5b43973'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
            ALTER FUNCTION [dbo].[fn_GetRegionFromLatLon] (@lat decimal(9,5) , @lon decimal(9,5))
            
            RETURNS  int 
            AS
            BEGIN
            -- Transforms lat and lon to a geometry object
            declare @regionId int;

                -- Select the Place the point is inside
            SET @regionId = ( 
                    SELECT 
                    TOP 1 
                    F.[ID]
                    FROM [FieldworkArea] AS F
                    WITH(INDEX(IX_Fieldworkarea_valid_geom))
                    WHERE 
                    F.valid_geom.STIntersects(geometry::Point ( @lon, @lat, 4326)) = 1
                    ORDER BY F.fullpath DESC
                )
                IF @regionId IS NULL
                    BEGIN
                        SET @regionId  = NULL
                    END

                return @regionId
            return @regionId
            END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
            ALTER FUNCTION [dbo].[fn_GetRegionFromLatLon] (@lat decimal(9,5) , @lon decimal(9,5))

            RETURNS  int 
            AS
            BEGIN
            -- Transforms lat and lon to a geometry object
            declare @regionId int;

                -- Select the Place the point is inside
            SET @regionId = ( 
                    SELECT 
                    TOP 1 
                    F.[ID]
                    FROM [FieldworkArea] AS F
                    WITH(INDEX(IX_Fieldworkarea_valid_geom))
                    WHERE 
                    F.valid_geom.STIntersects(geometry::Point ( @lon, @lat, 4326)) = 1
                    AND 
                    F.[Status] = 'current'
                    ORDER BY F.fullpath DESC
                )
                IF @regionId IS NULL
                    BEGIN
                        SET @regionId  = NULL
                    END

                return @regionId
            return @regionId
            END
        """
    )
