"""Remove station prefixe in AllIndivLocationWithStations and modify ID type from VARCHAR to INT 

Revision ID: 6e38083c9c4f
Revises: 3bff4ec7031c
Create Date: 2022-11-08 17:36:46.503891

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6e38083c9c4f'
down_revision = '3bff4ec7031c'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER VIEW [allIndivLocationWithStations] AS (
            SELECT 
                IL.[ID],
                [LAT],
                [LON],
                [Date],
                IL.[type_],
                FAr.[Name] AS FieldworkArea,
                IL.[FK_Individual] AS FK_Individual,
                NULL AS fieldActivity_Name,
                IL.[Precision] as precision
            FROM [Individual_Location] IL
            LEFT JOIN FieldworkArea FAr on FAr.[ID] = IL.[FK_FieldworkArea]

            UNION ALL 
            
            SELECT  
                O.[FK_Station] AS ID,
                [LAT],
                [LON],
                StationDate AS Date,
                'station' AS type_,
                FAr.[Name] AS FieldworkArea,
                O.[FK_Individual] AS FK_Individual,
                FAc.[Name] AS fieldActivity_Name,
                S.[precision]
            FROM Station S
            LEFT JOIN FieldworkArea FAr ON FAr.[ID] = S.[FK_FieldworkArea]
            LEFT JOIN fieldActivity FAc ON FAc.[ID] = S.[fieldActivityId]
            JOIN Observation O ON O.[FK_Station] = S.[ID]
            GROUP BY 
                O.[FK_Station],
                [LAT],
                [LON],
                StationDate,
                FAr.Name,
                O.FK_Individual,
                FAc.Name,
                S.precision
        )
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER View [allIndivLocationWithStations] as (
            SELECT convert(varchar,l.[ID]) as ID
                ,[LAT]
                ,[LON]
                ,[Date]
                ,l.[type_]
                ,r.Name as FieldworkArea
                ,l.FK_Individual as FK_Individual
                ,NuLL as fieldActivity_Name
                ,l.Precision as precision
            FROM [Individual_Location] l
            LEFT JOIN FieldworkArea r on r.ID = l.FK_FieldworkArea

            UNion ALL 
            SELECT  
                'sta_'+convert(varchar,o.FK_Station) as ID
                ,[LAT]
                ,[LON]
                ,StationDate as Date
                ,'station' as type_
                ,r.Name as FieldworkArea
                ,o.FK_Individual as FK_Individual
                ,fa.Name as fieldActivity_Name
                ,s.precision

            FROM Station s
            LEFT JOIN FieldworkArea r on r.ID = s.FK_FieldworkArea
            LEFT JOIN fieldActivity fa on fa.ID = s.fieldActivityId
            JOIN Observation o ON o.FK_Station = s.ID
            group by  
                o.FK_Station
                ,[LAT]
                ,[LON]
                ,StationDate 
                ,r.Name
                ,o.FK_Individual
                ,fa.Name
                ,s.precision
        )
        """
    )
