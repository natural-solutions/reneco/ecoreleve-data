"""add new dynamic properties referencing parents in monitored sites of type nest and broods

Revision ID: d34fbf05c5f3
Revises: 16403d96dbfb
Create Date: 2024-02-27 18:17:18.538290

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd34fbf05c5f3'
down_revision = '16403d96dbfb'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            CREATE TABLE #ListOfDynProp (DynProp NVARCHAR(255));

            INSERT INTO #ListOfDynProp 
            VALUES ('Transmitter_Shape_FK_Individual_1'),
                ('Transmitter_Model_FK_Individual_1'),
                ('Transmitter_Company_FK_Individual_1'),
                ('Ring_position_1_FK_Individual_1'),
                ('Ring_color_1_FK_Individual_1'),
                ('Ring_position_2_FK_Individual_1'),
                ('Ring_color_2_FK_Individual_1'),
                ('Microchip_FK_Individual_1'),
                ('Mark_Position_1_FK_Individual_1'),
                ('Mark_Color_1_FK_Individual_1'),
                ('Mark_Position_2_FK_Individual_1'),
                ('Mark_Color_2_FK_Individual_1'),
                ('Transmitter_Shape_FK_Individual_2'),
                ('Transmitter_Model_FK_Individual_2'),
                ('Transmitter_Company_FK_Individual_2'),
                ('Ring_position_1_FK_Individual_2'),
                ('Ring_color_1_FK_Individual_2'),
                ('Ring_position_2_FK_Individual_2'),
                ('Ring_color_2_FK_Individual_2'),
                ('Microchip_FK_Individual_2'),
                ('Mark_Position_1_FK_Individual_2'),
                ('Mark_Color_1_FK_Individual_2'),
                ('Mark_Position_2_FK_Individual_2'),
                ('Mark_Color_2_FK_Individual_2');

            DECLARE @DynProp NVARCHAR(255);
            DECLARE @MonitoredSiteDynProp INT;

            -- "nest and brood" monitored site id
            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM MonitoredSiteType
                WHERE Name LIKE 'Nest and Brood'
            );

            WHILE EXISTS (SELECT * FROM #ListOfDynProp)
            BEGIN
                SELECT TOP 1 @DynProp = DynProp
                FROM #ListOfDynProp
                
                -- add new dynamic property for monitored sites
                INSERT INTO MonitoredSiteDynProp
                VALUES (@DynProp, 'String')

                -- monitored site "Experimentation" dynamic property id
                SELECT @MonitoredSiteDynProp = [ID]
                FROM MonitoredSiteDynProp
                WHERE Name = @DynProp

                -- add link between monitored site type and dynamic property
                INSERT INTO [MonitoredSiteType_MonitoredSiteDynProp] (Required, FK_MonitoredSiteType, FK_MonitoredSiteDynProp)
                VALUES (0, @NestAndBroodType, @MonitoredSiteDynProp)

                DELETE #ListOfDynProp
                WHERE DynProp = @DynProp
            END

            DROP TABLE #ListOfDynProp
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            CREATE TABLE #ListOfDynProp (DynProp NVARCHAR(255));

            INSERT INTO #ListOfDynProp 
            VALUES ('Transmitter_Shape_FK_Individual_1'),
                ('Transmitter_Model_FK_Individual_1'),
                ('Transmitter_Company_FK_Individual_1'),
                ('Ring_position_1_FK_Individual_1'),
                ('Ring_color_1_FK_Individual_1'),
                ('Ring_position_2_FK_Individual_1'),
                ('Ring_color_2_FK_Individual_1'),
                ('Microchip_FK_Individual_1'),
                ('Mark_Position_1_FK_Individual_1'),
                ('Mark_Color_1_FK_Individual_1'),
                ('Mark_Position_2_FK_Individual_1'),
                ('Mark_Color_2_FK_Individual_1'),
                ('Transmitter_Shape_FK_Individual_2'),
                ('Transmitter_Model_FK_Individual_2'),
                ('Transmitter_Company_FK_Individual_2'),
                ('Ring_position_1_FK_Individual_2'),
                ('Ring_color_1_FK_Individual_2'),
                ('Ring_position_2_FK_Individual_2'),
                ('Ring_color_2_FK_Individual_2'),
                ('Microchip_FK_Individual_2'),
                ('Mark_Position_1_FK_Individual_2'),
                ('Mark_Color_1_FK_Individual_2'),
                ('Mark_Position_2_FK_Individual_2'),
                ('Mark_Color_2_FK_Individual_2');

            DECLARE @DynProp NVARCHAR(255);
            DECLARE @MonitoredSiteDynProp INT;
            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM MonitoredSiteType
                WHERE Name LIKE 'Nest and Brood'
            );

            WHILE EXISTS (SELECT * FROM #ListOfDynProp)
            BEGIN
                SELECT TOP 1 @DynProp = DynProp
                FROM #ListOfDynProp
                
                SELECT @MonitoredSiteDynProp = [ID]
                FROM MonitoredSiteDynProp
                WHERE [Name] = @DynProp


                DELETE FROM MonitoredSiteDynPropValue
                WHERE [FK_MonitoredSiteDynProp] = @MonitoredSiteDynProp
                        
                DELETE FROM MonitoredSiteType_MonitoredSiteDynProp
                WHERE 
                    [FK_MonitoredSiteType] = @NestAndBroodType
                    AND [FK_MonitoredSiteDynProp] = @MonitoredSiteDynProp

                DELETE FROM MonitoredSiteDynProp
                WHERE [Name] = @DynProp

                DELETE #ListOfDynProp
                WHERE DynProp = @DynProp
            END

            DROP TABLE #ListOfDynProp
            """
        )
