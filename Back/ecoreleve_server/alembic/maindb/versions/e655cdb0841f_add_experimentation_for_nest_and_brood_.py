"""add experimentation for nest and brood monitored site type

Revision ID: e655cdb0841f
Revises: d2b864da81e8
Create Date: 2024-02-01 20:16:14.545116

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e655cdb0841f'
down_revision = 'd2b864da81e8'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            -- "nest and brood" monitored site id
            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM MonitoredSiteType
                WHERE Name LIKE 'Nest and Brood'
            );

            -- add new dynamic property for monitored sites
            INSERT INTO MonitoredSiteDynProp
            VALUES( 'Experimentation', 'String')

            -- monitored site "Experimentation" dynamic property id
            DECLARE @MonitoredSiteDynProp INT = (
                SELECT [ID]
                FROM MonitoredSiteDynProp
                WHERE Name = 'Experimentation'
            ) 

            -- add link between monitored site type and dynamic property
            INSERT INTO [MonitoredSiteType_MonitoredSiteDynProp] (Required, FK_MonitoredSiteType, FK_MonitoredSiteDynProp)
            VALUES (1, @NestAndBroodType, @MonitoredSiteDynProp)

            -- monitored site form id
            DECLARE @MonitoredSiteFormObject INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE Name = 'MonitoredSiteForm'
            ) 

            -- add configuration for experimentation field
            INSERT INTO ModuleForms
            VALUES (12, 2, 'Experimentation', 'Experimentation', 0, 4, 4, 'AutocompTreeEditor', 'form-control', '7', 4,'General Information', 2014983, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @NestAndBroodType INT = (
                SELECT [ID]
                FROM MonitoredSiteType
                WHERE [Name] = 'Nest and Brood'
            )
            
            DECLARE @MonitoredSiteDynProp INT = (
                SELECT [ID]
                FROM MonitoredSiteDynProp
                WHERE [Name] = 'Experimentation'
            )

            DECLARE @MonitoredSiteFormObject INT = (
                SELECT [ID]
                FROM FrontModules
                WHERE [Name] = 'MonitoredSiteForm'
            ) 

            DELETE FROM MonitoredSiteDynPropValue
            WHERE [FK_MonitoredSiteDynProp] = @MonitoredSiteDynProp
            
            DELETE FROM MonitoredSiteType_MonitoredSiteDynProp
            WHERE 
                [FK_MonitoredSiteType] = @NestAndBroodType
                AND [FK_MonitoredSiteDynProp] = @MonitoredSiteDynProp

            DELETE FROM MonitoredSiteDynProp
            WHERE [Name] = 'Experimentation'

            DELETE FROM ModuleForms
            WHERE [Name] = 'Experimentation' and [module_id] = @MonitoredSiteFormObject
            """
        )
