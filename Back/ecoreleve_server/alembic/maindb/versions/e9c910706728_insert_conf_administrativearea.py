"""insert conf administrativearea

Revision ID: e9c910706728
Revises: d2bf0b3501e2
Create Date: 2022-11-24 18:20:09.728788

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e9c910706728'
down_revision = 'd2bf0b3501e2'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:

    op.execute(
        """
            ALTER TABLE [Individual_Location]
            ADD [FK_AdministrativeArea] INT NULL,
            CONSTRAINT [FK_Individual_Location_FK_AdministrativeArea_AdministrativeArea]
                FOREIGN KEY ([FK_AdministrativeArea]) REFERENCES [AdministrativeArea](ID)
        """
    )
    op.execute(
        """
        ALTER VIEW [dbo].[allIndivLocationWithStations] AS (
        SELECT
        IL.[ID],
        IL.[LAT],
        IL.[LON],
        IL.[Date],
        IL.[type_],
        FAR.[Name] AS [FieldworkArea],
        AA.[fullpath] AS [AdministrativeArea],
        IL.[FK_Individual] AS FK_Individual,
        NULL AS [fieldActivity_Name],
        IL.[Precision] AS [precision]
        FROM [Individual_Location] AS IL
        LEFT JOIN [FieldworkArea] AS FAR ON FAR.[ID] = IL.[FK_FieldworkArea]
        LEFT JOIN [AdministrativeArea] AS AA ON AA.[ID] = IL.[FK_AdministrativeArea]

        UNION ALL

        SELECT
        O.[FK_Station] AS [ID],
        S.[LAT],
        S.[LON],
        S.[StationDate] AS [Date],
        'station' AS [type_],
        FAR.[Name] AS [FieldworkArea],
        AA.[fullpath] AS [AdministrativeArea],
        O.[FK_Individual] AS [FK_Individual],
        FAC.[Name] AS [fieldActivity_Name],
        S.[precision]
        FROM [Station] AS S
        LEFT JOIN [FieldworkArea] AS FAR ON FAR.[ID] = S.[FK_FieldworkArea]
        LEFT JOIN [fieldActivity] AS FAC ON FAC.[ID] = S.[fieldActivityId]
        JOIN [Observation] AS O ON O.[FK_Station] = S.[ID]
        LEFT JOIN [AdministrativeArea] AS AA ON AA.[ID] = S.[FK_AdministrativeArea]
        GROUP BY
            O.[FK_Station],
            S.[LAT],
            S.[LON],
            S.[StationDate],
            FAR.[Name],
            O.[FK_Individual],
            FAC.[Name],
            AA.[fullpath],
            S.[precision]
        )
        """
    )
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                INSERT INTO [ModuleForms]
                (
                [module_id],[TypeObj],[Name],[Label],[Required],[FieldSizeEdit],[FieldSizeDisplay],[InputType],[editorClass],[FormRender],[FormOrder],[Legend],[Options])
                VALUES
                (2,1,'FK_AdministrativeArea','Administrative Area',0,6,6,'AdministrativeAreaEditor','form-control',1,500,'Location Information','{"usedLabel":"fullpath","operator":"contains","triggerAt"\:3}'),
                (2,3,'FK_AdministrativeArea','Administrative Area',1,6,6,'AdministrativeAreaEditor','form-control',7,500,'Location Information','{"usedLabel":"fullpath","operator":"contains","triggerAt"\:3}'),
                (2,4,'FK_AdministrativeArea','Administrative Area',0,6,6,'AdministrativeAreaEditor','form-control',1,500,'Location Information','{"usedLabel":"fullpath","operator":"contains","triggerAt"\:3}')
            """
        )
        op.execute(
            """
                INSERT INTO [ModuleGrids]
                ([Module_ID]
                ,[Name]
                ,[Label]
                ,[GridRender]
                ,[GridSize]
                ,[CellType]
                ,[GridOrder]
                ,[Options]
                ,[FilterOrder]
                ,[FilterSize]
                ,[IsSearchable]
                ,[FilterRender]
                ,[FilterType])
                VALUES
                (3,
                'AdministrativeArea@fullpath',
                'Administrative Area',
                2,
                '{"width"\:120,"maxWidth"\:350,"minWidth"\:100}',
                'string',
                55,
                '{"usedLabel":"fullpath","operator":"contains","triggerAt"\:3}',
                50,
                2,
                1,
                4,
                'AdministrativeAreaEditor')
            """
        )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
        ALTER TABLE [Individual_Location]
        DROP CONSTRAINT [FK_Individual_Location_FK_AdministrativeArea_AdministrativeArea]

        ALTER TABLE [Individual_Location]
        DROP COLUMN FK_AdministrativeArea
        """
    )

    op.execute(
        """
        ALTER VIEW [dbo].[allIndivLocationWithStations] AS (
        SELECT
        IL.[ID],
        IL.[LAT],
        IL.[LON],
        IL.[Date],
        IL.[type_],
        FAR.[Name] AS [FieldworkArea],
        IL.[FK_Individual] AS FK_Individual,
        NULL AS [fieldActivity_Name],
        IL.[Precision] AS [precision]
        FROM [Individual_Location] AS IL
        LEFT JOIN [FieldworkArea] AS FAR ON FAR.[ID] = IL.[FK_FieldworkArea]

        UNION ALL

        SELECT
        O.[FK_Station] AS [ID],
        S.[LAT],
        S.[LON],
        S.[StationDate] AS [Date],
        'station' AS [type_],
        FAR.[Name] AS [FieldworkArea],
        O.[FK_Individual] AS [FK_Individual],
        FAC.[Name] AS [fieldActivity_Name],
        S.[precision]
        FROM [Station] AS S
        LEFT JOIN [FieldworkArea] AS FAR ON FAR.[ID] = S.[FK_FieldworkArea]
        LEFT JOIN [fieldActivity] AS FAC ON FAC.[ID] = S.[fieldActivityId]
        JOIN [Observation] AS O ON O.[FK_Station] = S.[ID]
        GROUP BY
            O.[FK_Station],
            S.[LAT],
            S.[LON],
            S.[StationDate],
            FAR.[Name],
            O.[FK_Individual],
            FAC.[Name],
            S.[precision]
        )
        """
    )
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
                DELETE FROM [ModuleForms]
                WHERE
                [module_id] = 2
                AND
                [TypeObj] IN (1,3,4)
                AND
                [Name] = 'FK_AdministrativeArea'
            """
        )
        op.execute(
            """
                DELETE FROM [ModuleGrids]
                WHERE
                [Module_ID] = 3
                AND
                [Name] = 'AdministrativeArea@fullpath'
            """
        )
