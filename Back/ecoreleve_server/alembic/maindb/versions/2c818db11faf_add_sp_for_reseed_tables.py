"""add sp for reseed tables

Revision ID: 2c818db11faf
Revises: a47c04412e37
Create Date: 2022-12-23 10:20:32.704461

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2c818db11faf'
down_revision = 'a47c04412e37'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    op.execute(
        """
            CREATE PROCEDURE [dbo].[EXEC_Reseed_all_tables]
            -- Stored procedure to reseed all tables. To execute after backup restauration.
            -- The current identity value is checked for each table and the value is changed if needed.
                @DEBUG INT=1,
                @FORCEROLLBACK INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    IF @DEBUG = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    -- Retrieve seed state for all tables
                    DECLARE @TEMPORARY_TABLE_RESEED TABLE ([RESEED_STMT] VARCHAR(MAX), [NB] INT)
                    INSERT INTO @TEMPORARY_TABLE_RESEED ([RESEED_STMT], [NB])
                    SELECT
                        'DBCC CHECKIDENT(''' + [TABLE_NAME] + ''', RESEED )',
                        ROW_NUMBER() OVER(ORDER BY [TABLE_NAME])
                    FROM
                        INFORMATION_SCHEMA.TABLES
                    WHERE
                        OBJECTPROPERTY(OBJECT_ID([TABLE_NAME]), 'TableHasIdentity') = 1
                        AND [TABLE_TYPE] = 'BASE TABLE'


                    -- Execute the DBCC CHECKINDENT command for each table
                    DECLARE @STMT NVARCHAR(MAX) = NULL;
                    DECLARE @NB INT = NULL;

                    WHILE EXISTS(SELECT * FROM @TEMPORARY_TABLE_RESEED)
                    BEGIN
                        SELECT TOP (1) @STMT = [RESEED_STMT], @NB = [NB] FROM @TEMPORARY_TABLE_RESEED ORDER BY [NB]
                        PRINT(@STMT)
                        EXEC SP_EXECUTESQL @STMT
                        DELETE TOP(1) FROM @TEMPORARY_TABLE_RESEED WHERE [NB] = @NB
                    END
                    
                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @FORCEROLLBACK = 1
                        BEGIN
                            IF @DEBUG = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @DEBUG = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
        """
    )


def downgrade(**options_for_migration) -> None:
    op.execute(
        """
            IF OBJECT_ID('EXEC_Reseed_all_tables', 'P') IS NOT NULL
            BEGIN
                DROP PROCEDURE EXEC_Reseed_all_tables
            END
        """
    )
