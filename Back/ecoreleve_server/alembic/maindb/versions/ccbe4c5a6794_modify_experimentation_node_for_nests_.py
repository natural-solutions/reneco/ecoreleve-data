"""modify experimentation node for nests and broods

Revision ID: ccbe4c5a6794
Revises: db68b9df4337
Create Date: 2024-03-21 12:42:23.757690

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ccbe4c5a6794'
down_revision = 'db68b9df4337'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT ID
                FROM FrontModules
                WHERE Name = 'MonitoredSiteForm'
            )

            DECLARE @TypeObj INT = (
                SELECT ID
                FROM MonitoredSiteType
                WHERE Name = 'Nest and Brood'
            )

            UPDATE ModuleForms
            SET Options = '2017197'
            WHERE module_id = 12
            AND TypeObj =2
            AND Name = 'Experimentation'
            AND Options = '2014983'
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT ID
                FROM FrontModules
                WHERE Name = 'MonitoredSiteForm'
            )

            DECLARE @TypeObj INT = (
                SELECT ID
                FROM MonitoredSiteType
                WHERE Name = 'Nest and Brood'
            )

            UPDATE ModuleForms
            SET Options = '2014983'
            WHERE module_id = 12
            AND TypeObj =2
            AND Name = 'Experimentation'
            AND Options = '2017197'
            """
        )
