"""add history tab for dynamic properties of monitored sites

Revision ID: ae051f622101
Revises: e7ceba7663f5
Create Date: 2024-09-13 17:05:51.622897

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ae051f622101'
down_revision = 'e7ceba7663f5'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            UPDATE FrontModules
            SET [Name] = 'MonitoredSiteGridLocationHistory'
            WHERE [Name] = 'MonitoredSiteGridHistory'
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            UPDATE FrontModules
            SET [Name] = 'MonitoredSiteGridHistory'
            WHERE [Name] = 'MonitoredSiteGridLocationHistory'
            """
        )
