"""missing sp for businessrules

Revision ID: 127a88834c9a
Revises: 8fe9d30d13bf
Create Date: 2023-06-22 17:11:13.381110

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '127a88834c9a'
down_revision = '8fe9d30d13bf'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', True) is False:
        op.execute(
            """
            CREATE OR ALTER PROCEDURE [dbo].[MERGE_BusinessRules]
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    IF OBJECT_ID('tempdb..#BusinessRulesTMP') IS NOT NULL
                    BEGIN
                        DROP TABLE #BusinessRulesTMP
                    END

                    CREATE TABLE #BusinessRulesTMP(
                        [ID] [int] NOT NULL,
                        [name] [varchar](250) NULL,
                        [target] [varchar](250) NULL,
                        [targetType] [varchar](250) NULL,
                        [actionType] [varchar](250) NULL,
                        [executing] [varchar](250) NULL,
                        [params] [varchar](250) NULL,
                        [description] [varchar](250) NULL,
                        [errorValue] [varchar](250) NULL
                    )

                    INSERT INTO #BusinessRulesTMP(
                        [ID],
                        [name],
                        [target],
                        [targetType],
                        [actionType],
                        [executing],
                        [params],
                        [description],
                        [errorValue]
                    )
                    SELECT
                    [ID],
                    [name],
                    [target],
                    [targetType],
                    [actionType],
                    [executing],
                    [params],
                    [description],
                    [errorValue]
                    FROM [syn_Referentiel.BusinessRules]

                    ---- MERGE TABLE ---
                    IF OBJECT_ID('tempdb..#tempMergeBusinessRules') IS NOT NULL
                    BEGIN
                        DROP TABLE #tempMergeBusinessRules
                    END

                    CREATE TABLE #tempMergeBusinessRules(
                        [action] VARCHAR(MAX),
                        [SourceID] [int],
                        [Sourcename] [varchar](250),
                        [Sourcetarget] [varchar](250),
                        [SourcetargetType] [varchar](250),
                        [SourceactionType] [varchar](250),
                        [Sourceexecuting] [varchar](250),
                        [Sourceparams] [varchar](250),
                        [Sourcedescription] [varchar](250),
                        [SourceerrorValue] [varchar](250),
                        [TargetID] [int],
                        [Targetname] [varchar](250),
                        [Targettarget] [varchar](250),
                        [TargettargetType] [varchar](250),
                        [TargetactionType] [varchar](250),
                        [Targetexecuting] [varchar](250),
                        [Targetparams] [varchar](250),
                        [Targetdescription] [varchar](250),
                        [TargeterrorValue] [varchar](250)
                    )

                    IF @Debug = 1
                    BEGIN
                        SELECT 'DATA TO MERGE'
                        SELECT
                        *
                        FROM #BusinessRulesTMP

                        SELECT 'WILL BE MERGED WITH TARGET'
                        SELECT
                        [ID],
                        [name],
                        [target],
                        [targetType],
                        [actionType],
                        [executing],
                        [params],
                        [description],
                        [errorValue]
                        FROM [BusinessRules]
                    END

                    SET IDENTITY_INSERT [BusinessRules] ON;
                    IF @Debug = 1
                    BEGIN
                        PRINT('SET IDENTITY_INSERT ON FOR [BusinessRules]');
                        PRINT('MERGE TABLE');
                    END

                    ;WITH BusinessRulesFiltered AS
                    (
                        SELECT
                        [ID],
                        [name],
                        [target],
                        [targetType],
                        [actionType],
                        [executing],
                        [params],
                        [description],
                        [errorValue]
                        FROM [BusinessRules]
                    )
                    MERGE BusinessRulesFiltered AS [TARGET]
                    USING #BusinessRulesTMP AS [SOURCE]
                    ON ( TARGET.[ID] = SOURCE.[ID] )
                    WHEN MATCHED THEN
                        UPDATE SET
                        TARGET.[name] = SOURCE.[name],
                        TARGET.[target] = SOURCE.[target],
                        TARGET.[targetType] = SOURCE.[targetType],
                        TARGET.[actionType] = SOURCE.[actionType],
                        TARGET.[executing] = SOURCE.[executing],
                        TARGET.[params] = SOURCE.[params],
                        TARGET.[description] = SOURCE.[description],
                        TARGET.[errorValue] = SOURCE.[errorValue]
                    WHEN NOT MATCHED BY TARGET THEN
                        INSERT ([ID], [Name], [target], [targetType], [actionType], [executing], [params], [description], [errorValue])
                        VALUES ([ID], [Name], [target], [targetType], [actionType], [executing], [params], [description], [errorValue])
                    WHEN NOT MATCHED BY SOURCE THEN
                        DELETE

                    OUTPUT $action AS [action],
                    INSERTED.[ID] AS [SourceID],
                    INSERTED.[name] AS [Sourcename],
                    INSERTED.[target] AS [Sourcetarget],
                    INSERTED.[targetType] AS [SourcetargetType],
                    INSERTED.[actionType] AS [SourceactionType],
                    INSERTED.[executing] AS [Sourceexecuting],
                    INSERTED.[params] AS [Sourceparams],
                    INSERTED.[description] AS [Sourcedescription],
                    INSERTED.[errorValue] AS [SourceerrorValue],
                    DELETED.[ID] AS [TargetID],
                    DELETED.[name] AS [Targetname],
                    DELETED.[target] AS [Targettarget],
                    DELETED.[targetType] AS [TargettargetType],
                    DELETED.[actionType] AS [TargetactionType],
                    DELETED.[executing] AS [Targetexecuting],
                    DELETED.[params] AS [Targetparams],
                    DELETED.[description] AS [Targetdescription],
                    DELETED.[errorValue] AS [TargeterrorValue]
                    INTO #tempMergeBusinessRules([action], [SourceID], [Sourcename], [Sourcetarget], [SourcetargetType], [SourceactionType], [Sourceexecuting], [Sourceparams], [Sourcedescription], [SourceerrorValue], [TargetID], [Targetname], [Targettarget], [TargettargetType], [TargetactionType], [Targetexecuting], [Targetparams], [Targetdescription], [TargeterrorValue]);

                    SET IDENTITY_INSERT [BusinessRules] OFF;
                    IF @Debug = 1
                    BEGIN
                        PRINT('SET IDENTITY_INSERT OFF FOR [BusinessRules]');
                        SELECT 'RESULT MERGE'
                        SELECT * FROM #tempMergeBusinessRules
                    END

                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
            """
        )

        op.execute(
            """
            ALTER PROCEDURE [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables]
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    --- Disable all constraints for database
                    EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

                    --- ORDER IS  NOT IMPORTANT  BECAUSE WE DISABLE ALL CONSTRAINTS ---
                    EXEC [MERGE_fieldActivity_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_fieldActivity]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_FrontModules]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleForms]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleGrids]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleFormsInternationalization]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_BusinessRules] @Debug = 0, @ForceRollback = 0

                    --- Enable all constraints for database and launch check  ---
                    DECLARE @TEMPORARY_TABLE_CONSTRAINT TABLE( [STMT] VARCHAR(MAX), [NB] INT)
                    INSERT INTO @TEMPORARY_TABLE_CONSTRAINT([STMT],[NB])
                    SELECT
                    STRING_AGG(
                        CASE
                            -- special case when a location not in any fieldworkaera, fk is set to 0
                            -- we need to "disable" the fk constraint
                            WHEN A.[CK_Name] IN('FK_Station_FK_FieldworkArea_Fieldworkarea','FK_Individual_Location_FK_FieldworkArea_Fieldworkarea', 'FK_MonitoredSitePosition_FK_FieldworkArea_FieldworkArea')
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' NOCHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- special case when a location not in any administrativearea, fk is set to 0
                            -- we need to "disable" the fk constraint
                            WHEN A.[CK_Name] IN('FK_Individual_Location_FK_AdministrativeArea_AdministrativeArea','FK_Station_FK_AdministrativeArea_AdministrativeArea', 'FK_MonitoredSitePosition_FK_AdministrativeArea_AdministrativeArea')
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' NOCHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- special case for merge replication
                            -- we need to enable the check on id used but we didn't force the check
                            -- force the check raise an error as if narc try to use id not in his range
                            WHEN A.[CK_Name] like 'repl_identity_range_%'
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' WITH NOCHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- enable the constraint and force the check
                            ELSE N''+'ALTER TABLE '+A.[Table_Name]+' WITH CHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                        END,
                        ';'
                    ) AS [STMT],
                    ROW_NUMBER() OVER(ORDER BY [CK_Name], [Table_Name]) AS [NB]
                    FROM (
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.check_constraints
                        UNION ALL
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.foreign_keys
                    ) AS A
                    GROUP BY [CK_Name], [Table_Name]

                    DECLARE @stmt NVARCHAR(MAX) = NULL;
                    DECLARE @nb INT = NULL;

                    SELECT TOP 1 @stmt = [stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    WHILE EXISTS(SELECT * FROM @TEMPORARY_TABLE_CONSTRAINT)
                    BEGIN
                        EXEC sp_executesql @stmt
                        DELETE TOP(1) FROM @TEMPORARY_TABLE_CONSTRAINT WHERE [NB]=@nb;
                        SELECT TOP 1 @stmt=[stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    END

                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
            """
        )


def downgrade(**options_for_migration) -> None:
    # DONT FORGET TO REMOVE THE IF WHEN THE SCRIPT IS NOT FOR REFERENTIAL
    if options_for_migration.get('is_referential_instance', True) is False:
        op.execute(
            """
            IF OBJECT_ID('MERGE_BusinessRules', 'P') IS NOT NULL
            BEGIN
                DROP PROCEDURE MERGE_BusinessRules
            END
            """
        )
        op.execute(
            """
            CREATE OR ALTER PROCEDURE [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables]
                @Debug INT=1,
                @ForceRollback INT=1
            AS
            BEGIN
                BEGIN TRY
                BEGIN TRAN
                    IF @Debug = 1
                    BEGIN
                        PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
                    END

                    --- Disable all constraints for database
                    EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

                    --- ORDER IS  NOT IMPORTANT  BECAUSE WE DISABLE ALL CONSTRAINTS ---
                    EXEC [MERGE_fieldActivity_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_fieldActivity]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_FrontModules]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleForms]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleGrids]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ProtocoleType_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_ModuleFormsInternationalization]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType]  @Debug = 0, @ForceRollback = 0
                    EXEC [MERGE_MonitoredSiteType_MonitoredSiteDynProp]  @Debug = 0, @ForceRollback = 0

                    --- Enable all constraints for database and launch check  ---
                    DECLARE @TEMPORARY_TABLE_CONSTRAINT TABLE( [STMT] VARCHAR(MAX), [NB] INT)
                    INSERT INTO @TEMPORARY_TABLE_CONSTRAINT([STMT],[NB])
                    SELECT
                    STRING_AGG(
                        CASE
                            -- special case when a location not in any fieldworkaera, fk is set to 0
                            -- we need to "disable" the fk constraint
                            WHEN A.[CK_Name] IN('FK_Station_FK_FieldworkArea_Fieldworkarea','FK_Individual_Location_FK_FieldworkArea_Fieldworkarea', 'FK_MonitoredSitePosition_FK_FieldworkArea_FieldworkArea')
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' NOCHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- special case when a location not in any administrativearea, fk is set to 0
                            -- we need to "disable" the fk constraint
                            WHEN A.[CK_Name] IN('FK_Individual_Location_FK_AdministrativeArea_AdministrativeArea','FK_Station_FK_AdministrativeArea_AdministrativeArea', 'FK_MonitoredSitePosition_FK_AdministrativeArea_AdministrativeArea')
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' NOCHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- special case for merge replication
                            -- we need to enable the check on id used but we didn't force the check
                            -- force the check raise an error as if narc try to use id not in his range
                            WHEN A.[CK_Name] like 'repl_identity_range_%'
                                THEN N''+'ALTER TABLE '+A.[Table_Name]+' WITH NOCHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                            -- enable the constraint and force the check
                            ELSE N''+'ALTER TABLE '+A.[Table_Name]+' WITH CHECK CHECK CONSTRAINT '+A.[CK_Name]+';'
                        END,
                        ';'
                    ) AS [STMT],
                    ROW_NUMBER() OVER(ORDER BY [CK_Name], [Table_Name]) AS [NB]
                    FROM (
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.check_constraints
                        UNION ALL
                        SELECT
                        [Name] AS [CK_Name],
                        OBJECT_NAME(parent_object_id) AS [Table_Name]
                        FROM sys.foreign_keys
                    ) AS A
                    GROUP BY [CK_Name], [Table_Name]

                    DECLARE @stmt NVARCHAR(MAX) = NULL;
                    DECLARE @nb INT = NULL;

                    SELECT TOP 1 @stmt = [stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    WHILE EXISTS(SELECT * FROM @TEMPORARY_TABLE_CONSTRAINT)
                    BEGIN
                        EXEC sp_executesql @stmt
                        DELETE TOP(1) FROM @TEMPORARY_TABLE_CONSTRAINT WHERE [NB]=@nb;
                        SELECT TOP 1 @stmt=[stmt], @nb=[NB] FROM @TEMPORARY_TABLE_CONSTRAINT ORDER BY [NB]
                    END

                    IF @@TRANCOUNT > 0
                    BEGIN
                        IF @ForceRollback = 1
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            ;THROW 50001, N'FORCED ROLLBACK', 1;
                            ROLLBACK TRAN
                        END
                        ELSE
                        BEGIN
                            IF @Debug = 1
                            BEGIN
                                PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                            END
                            COMMIT TRAN
                        END
                    END
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN
                        END
                        ;THROW
                END CATCH
            END
            """
        )
