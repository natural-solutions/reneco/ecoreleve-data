"""add filters for parents in monitored sites

Revision ID: 02f8ba104388
Revises: 6cdf0ba143c7
Create Date: 2024-09-11 15:50:09.902619

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '02f8ba104388'
down_revision = '6cdf0ba143c7'
branch_labels = None
depends_on = None


def upgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT ID
                FROM FrontModules
                WHERE Name = 'MonitoredSiteGrid'
            )

            INSERT INTO ModuleGrids
            VALUES 
                (@module_id, NULL, 'FK_Individual_1', 'Parent 1', 0, '{"width"\:120,"maxWidth"\:350,"minWidth"\:100}', 'string', 0, NULL, '{"source": "autocomplete/individuals/ID", "minLength"\:3}', 1, 2, NULL, 'True', NULL, 4, 'AutocompleteEditor', NULL, NULL),
                (@module_id, NULL, 'FK_Individual_2', 'Parent 2', 0, '{"width"\:120,"maxWidth"\:350,"minWidth"\:100}', 'string', 0, NULL, '{"source": "autocomplete/individuals/ID", "minLength"\:3}', 1, 2, NULL, 'True', NULL, 4, 'AutocompleteEditor', NULL, NULL)
            """
        )


def downgrade(**options_for_migration) -> None:
    if options_for_migration.get('is_referential_instance', False) is True:
        op.execute(
            """
            DECLARE @module_id INT = (
                SELECT ID
                FROM FrontModules
                WHERE Name = 'MonitoredSiteGrid'
            )

            DELETE FROM ModuleGrids
            WHERE module_id = @module_id
                AND Name LIKE 'FK_Individual_%'
            """
        )