from imp import lock_held
import os
from logging.config import fileConfig

from sqlalchemy import engine_from_config
from sqlalchemy import pool

from alembic import context
from pyramid.settings import asbool
from pyramid.paster import get_appsettings
from ecoreleve_server.core.init_db import Main_Db_Base

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Main_Db_Base.metadata
settings = get_appsettings(
    config_uri=config.config_file_name,
    options=os.environ
)
target_metadata = Main_Db_Base.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline(options_for_migration) -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = settings["sqlalchemy.maindb.url"]
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations(**options_for_migration)


def run_migrations_online(options_for_migration) -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        settings,
        prefix="sqlalchemy.maindb."
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations(**options_for_migration)
            try:
                # execute sp with connection.execute() have limitations
                # the sp called must not have any output or any print
                # we use NOCOUNT ON; for hidding implicit return
                # and we use if condition to print only in case of debug
                
                # we only try to call the sp [EXEC_MERGE_All_Referential_Configurations_Tables]
                # if we are not on a referential instance  

                # For a more comprehensive overview
                # The SP will pull configuration from the referential db and merge it on the "local" db
                # so we dont call it if:
                # 1)We are on a referential instance
                # 2)We are on a subrscriber instance
                # 3)We are on a readonly instance

                # the method run_migrations_online will be only call on 
                # referential and non subscriber and non readonly 
                # so we only need to check if it's not a referential
                if(options_for_migration.get("is_referential_instance", False) is False) :
                    connection.execute("""
                        SET NOCOUNT ON;
                        IF OBJECT_ID('[EXEC_MERGE_All_Referential_Configurations_Tables]', 'P') IS NOT NULL
                        BEGIN
                            EXEC [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables] @Debug = 0, @ForceRollback = 0
                        END
                    """)
            except Exception as e:
                raise e


alembic_settings = settings.loader.get_settings('alembic')
if bool(alembic_settings) is False:
    print('NO SECTION FOR ALEMBIC IN .INI FILE')
    print('Pls check your config file')

referential_is_defined = settings.global_conf.get(
    'is_referential_instance',
    None
)
if referential_is_defined is None:
    print("Please define is_referential_instance in [DEFAULT] section")

if bool(alembic_settings) is True and referential_is_defined is not None:
    is_a_merge_subscriber = asbool(
        alembic_settings.get('is_a_merge_subscriber', True)
    )
    is_a_read_only_instance = asbool(
        alembic_settings.get('is_a_read_only_instance', True)
    )

    is_referential_instance = asbool(
        alembic_settings.global_conf.get('is_referential_instance', False)
    )


    if is_a_merge_subscriber is True:
        print(
            f'is_a_merge_subscriber set to {is_a_merge_subscriber} interpreted as TRUE '
            'Alembic will not apply any revision for this instance'
        )
    if is_a_read_only_instance is True:
        print(
            f'is_a_read_only_instance set to {is_a_read_only_instance} interpreted as TRUE '
            'Alembic will not apply any revision for this instance'
        )

    play_migration = not (is_a_merge_subscriber or is_a_read_only_instance)

    if play_migration is True:
        referential_db = settings.get('databaseNameReferentialdb', None)
        options_for_migration = {
            "referential_db": referential_db,
            "is_referential_instance": is_referential_instance
        }
        if context.is_offline_mode():
            run_migrations_offline(options_for_migration)
        else:
            run_migrations_online(options_for_migration)
