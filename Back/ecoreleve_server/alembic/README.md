# Alembic in ecoreleve

Ecoreleve has two databases:

- Main:
  - The main db is for storage all object
- Sensor

## Commands


## Create new revision

alembic -c development.ini --name maindb revision -m "new rev for maindb"
alembic -c development.ini --name sensordb revision -m "new rev for sensordb"

### Upgrade to last version

alembic -c development.ini --name maindb upgrade head
alembic -c development.ini --name sensordb upgrade head

### Downgrade

alembic -c development.ini --name maindb downgrade base
alembic -c development.ini --name sensordb downgrade base

## Offline mode

alembic -c development.ini --name maindb upgrade head --sql
alembic -c development.ini --name sensordb upgrade head --sql
alembic -c development.ini --name maindb downgrade base --sql
alembic -c development.ini --name sensordb downgrade base --sql

RTFM!! For offline mod
https://alembic.sqlalchemy.org/en/latest/offline.html
