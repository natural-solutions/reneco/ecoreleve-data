from sqlalchemy import (
    Column,
    Integer,
    Sequence,
    String,
    DateTime,
    func,
    ForeignKeyConstraint,
    PrimaryKeyConstraint,
    UniqueConstraint
)
from ecoreleve_server.core import Main_Db_Base


class Photos(Main_Db_Base):

    __tablename__ = "Photos"
    Id = Column(Integer, Sequence("Photos__id_seq"), primary_key=True)
    Path = Column(String(250), nullable=False)
    FileName = Column(String(250), nullable=False)
    Date = Column(DateTime, nullable=False)
    Fk_MonitoredSite = Column(Integer, nullable=False)
    old_id = Column(Integer, nullable=False)
    Statut = Column(Integer, nullable=True)
    Note = Column(Integer, nullable=False, default=5)

    __mapper_args__ = {
        'version_id_col': Id,
        'version_id_generator': False,
    }

class MediasFiles(Main_Db_Base):

    __tablename__ = "MediasFiles"
    __table_args__ = (
        PrimaryKeyConstraint(
            "Id", name="PK_MediasFiles_Id", mssql_clustered=True
        ),
        ForeignKeyConstraint(
            ["FK_DynPropValue"],
            ["ObservationDynPropValue.ID"],
            name="FK_MediasFiles_ObservationDynPropValue_ID",
            ondelete="cascade",
        ),
        UniqueConstraint("GUID", name="UQ_MediasFiles_GUID"),
    )
    Id = Column(Integer)
    GUID = Column(String(255), nullable=False)
    Original_Name = Column(String(250), nullable=False)
    Extension = Column(String(4), nullable=True)
    Date_Uploaded = Column(DateTime, server_default=func.now(), nullable=False)
    Creator = Column(Integer, nullable=False)
    FK_DynPropValue = Column(
        Integer,
        nullable=True,
    )

    __mapper_args__ = {
        'version_id_col': Id,
        'version_id_generator': False,
    }