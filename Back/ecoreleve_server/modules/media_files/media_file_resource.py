import os
import errno
import shutil
from ecoreleve_server.core import RootCore, dbConfig
from ecoreleve_server.core.base_resource import CustomResource
from ..permissions import context_permissions
from .media_file_model import MediasFiles

import uuid


class MediasFilesResource(CustomResource):

    __acl__ = context_permissions["mediasfiles"]
    model = MediasFiles

    def upload(self):
        if (
            self.request.POST
            and "fileBin" in self.request.POST
            and "FK_Station" in self.request.POST
        ):
            FK_Station = self.request.POST["FK_Station"]
            fileBin = self.request.POST["fileBin"]
            fileName = fileBin.filename
            try:
                newGUID = str(uuid.uuid4())
                splittedName = fileName.split(".")
                creator = self.request.authenticated_userid["sub"]
                if len(splittedName) > 1:
                    extension = splittedName[-1]
                else:
                    extension = None
                newMedia = self.model(
                    GUID=newGUID,
                    Original_Name=fileName,
                    Extension=extension,
                    Creator=creator
                )
                self.session.add(newMedia)
                self.session.flush()
                self.session.refresh(newMedia)
                self.createFile(
                    FK_Station=FK_Station,
                    fileBin=fileBin,
                    newFileName=newGUID,
                    extension=extension,
                )
                url = f"{FK_Station}/{newMedia.GUID}"
                if extension:
                    url = f"{url}.{extension}"
                return {"url": url}
            except Exception as e:
                raise e

    def createFile(self, FK_Station, fileBin, newFileName, extension):
        fileName = newFileName
        if extension:
            fileName = f"{newFileName}.{extension}"
        absolutePath = os.path.join(dbConfig["mediasFiles"]["path"], FK_Station)
        absolutePathForFile = os.path.join(
            dbConfig["mediasFiles"]["path"], FK_Station, fileName
        )
        if not os.path.isfile(absolutePathForFile):
            # write in the file
            try:
                if not os.path.isdir(absolutePath):
                    os.mkdir(absolutePath)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise e
            try:
                with open(absolutePathForFile, "wb") as output_file:
                    shutil.copyfileobj(fileBin.file, output_file)
                output_file.close()
            except Exception as error:
                raise error


RootCore.children.append(("mediasfiles", MediasFilesResource))
