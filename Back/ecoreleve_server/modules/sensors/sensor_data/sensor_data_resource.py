import os
import datetime
import numpy as np
import json
import pandas as pd
from pyramid.httpexceptions import (
    HTTPNotImplemented,
    HTTPNotFound,
    HTTPConflict,
    HTTPOk,
    HTTPSuccessful
)
from traceback import print_exc
from exiftool import fsencode
from sqlalchemy import func, select, bindparam, text, Table, join, or_, and_, update, asc
from sqlalchemy.orm import joinedload, raiseload, load_only
from sqlalchemy.orm.exc import (
    MultipleResultsFound
)
from ecoreleve_server.core.base_resource import CustomResource
from ecoreleve_server.core import RootCore, Main_Db_Base, dbConfig
from ecoreleve_server.core.configuration_model.synonyms import Syn_TcameraTrap
from ecoreleve_server.utils.distance import haversine
from ecoreleve_server.utils.data_toXML import data_to_XML
from ecoreleve_server.utils.ocr_detect import OCR_parser
from ecoreleve_server.utils.quality_detect import imgProcess
from ecoreleve_server.utils.parseValue import parser

from ecoreleve_server.modules.permissions import context_permissions
from ecoreleve_server.modules.import_module.importArgos import uploadFileArgos
from ecoreleve_server.modules.import_module.importGSM import uploadFilesGSM
from ecoreleve_server.modules.import_module.importRFID import uploadFileRFID
from ecoreleve_server.modules.import_module.importCameraTrap import *
from ecoreleve_server.modules.statistics import graphDataDate
from ecoreleve_server.modules.monitored_sites import MonitoredSite
from ecoreleve_server.modules.sensors.sensor_model import Sensor
from ecoreleve_server.modules.observations import Equipment
from ecoreleve_server.modules.users import User
from ecoreleve_server.modules.media_files.media_file_model import Photos, MediasFiles
from ecoreleve_server.modules.stations.station_model import Station
from ecoreleve_server.modules.observations.observation_model import Observation

from . import CamTrap, ArgosGps, Gsm, Rfid, MetaData, Tags, PhotosTags
from lxml import etree
import xml.etree.ElementTree as ET
import uuid
import pathlib
import shutil
import hashlib
from PIL import Image
from ecoreleve_server import mySubExif

ArgosDatasWithIndiv = Table(
    'VArgosData_With_EquipIndiv', Main_Db_Base.metadata, autoload=True)
GsmDatasWithIndiv = Table('VGSMData_With_EquipIndiv',
                          Main_Db_Base.metadata, autoload=True)
DataRfidWithSite = Table('VRfidData_With_equipSite',
                         Main_Db_Base.metadata, autoload=True)
DataRfidasFile = Table('V_dataRFID_as_file',
                       Main_Db_Base.metadata, autoload=True)
DataCamTrapFile = Table('V_dataCamTrap_With_equipSite',
                        Main_Db_Base.metadata, autoload=True)
viewDict = {'gsm': GsmDatasWithIndiv,
            'argos': ArgosDatasWithIndiv,
            'rfid': DataRfidasFile,
            'camtrap': DataCamTrapFile
            }

'''
    When you see
    session._Session__binds[Main_Db_Base]
    it's for retrieve the "good" engine for executing query
    it's an hack because we can't access to .__binds dict in session
    .__binds is a dict where key are Base Class and value are the instanciate engine

    .__binds = {
        Main_Db_Base: Engine(),
        Sensor_Db_Base: Engine()
    }

'''


class SensorDatasBySessionItem(CustomResource):

    item = None
    models = {'gsm': Gsm,
              'argos': ArgosGps,
              'rfid': Rfid,
              'camtrap': CamTrap}

    def __init__(self, ref, parent):
        CustomResource.__init__(self, ref, parent)
        self.type_ = parent.type_
        self.itemID = ref
        self.viewTable = parent.viewTable

        self.item = self.session.query(self.models.get(self.type_)).get(ref)

    def retrieve(self):
        if not self.item:
            self.request.response.status_code = 404
            return self.request.response
        else:
            #todo fix json
            return self.item.as_dict()

    def patch(self):
        data = self.request.json_body #potentially new props
        cmdTags = ''
        # '-XMP-Photoshop:Headline=Picture of'+metaData['monitoredSite'].Category+''+metaData['monitoredSite'].Name+',Lon:'+metaData['monitoredSite'].Lon+',Lat:'+metaData['monitoredSite'].Lat+', on'+metaData.pictureCreationDate+'showing'+tagList
        for item in data:
            if(item not in ['pk_id', 'fk_sensor', 'path', 'name', 'extension', 'date_creation', 'date_uploaded']):
                tmp = data.get(item)
                if(item == 'tags' and tmp):
                    listTags = tmp.split(",")
                    XMLTags = "<TAGS>"
                    for tag in listTags:
                        XMLTags += "<TAG>" + str(tag) + "</TAG>"
                    XMLTags += "</TAGS>"
                    tmp = XMLTags
                setattr(self.item, item, tmp)
                #TODO INSERT TAG AS METADATA
        self.request.response.status_code = 204
        return self.request.response

    def XMLToStr(self, xmlStr):
        strForReplace = ''
        for strToRemove in ['<TAGS>','</TAGS>','</TAG>']:
            if strToRemove in xmlStr:
                xmlStr = xmlStr.replace(strToRemove , strForReplace)
        firstTagFind = 0
        for strToReplace in ['<TAG>']:
            if strToReplace in xmlStr :
                if not firstTagFind:
                    firstTagFind = 1
                else:
                    strForReplace = ','
                xmlStr = xmlStr.replace(strToRemove , strForReplace)
        return xmlStr
    
    def strToXML(self, strVal):
        listTags = strVal.split(",")
        XMLTags = None
        if len(listTags) > 0 :
            XMLTags = "<TAGS>"
            for tag in listTags :
                XMLTags += "<TAG>" + str(tag) + "</TAG>"
            XMLTags += "</TAGS>"
        return XMLTags


class DATASubDatasBySession(CustomResource):
    children = [('{int}', SensorDatasBySessionItem)]

    def __init__(self, ref, parent):
        CustomResource.__init__(self, ref, parent)
        self.type_ = parent.type_
        self.viewTable = parent.viewTable

    def retrieve(self):
        return self.__parent__.getDatas()
    
    def patch(self):
        pass

    def create(self):
        return self.__parent__.create()


class SensorDatasBySession(CustomResource):

    item = SensorDatasBySessionItem
    children = [('datas', DATASubDatasBySession)]

    def __init__(self, ref, parent):
        CustomResource.__init__(self, ref, parent)
        self.type_ = parent.type_
        self.sessionID = ref
        if ref == '0':
            self.sessionID = None
        self.viewTable = parent.viewTable
        # self.__acl__ = parent.__acl__

        # self.actions = {
        #     'datas': self.getDatas,
        #     'updateMany' : self.updateMany
        #     }

    def updateMany(self): #update if diff
        if self.type_ == 'camtrap':
            seqIds = []
            datas = self.request.json_body #potentially new props
            for item in datas:
                idTmp = item['pk_id']
                if idTmp:
                    seqIds.append(idTmp)
            allItems = self.session.query(CamTrap).filter(CamTrap.pk_id.in_(seqIds)).all()

            for bddItem in allItems:
                for elemRequest in datas:
                    if elemRequest['pk_id'] == bddItem.pk_id :
                        if elemRequest['tags']:
                            tabTags = elemRequest['tags'].split(',')
                            xmlTags = etree.Element('TAGS')
                            for newTag in tabTags:
                                tag = etree.SubElement(xmlTags,"TAG")
                                tag.text = newTag
                            xmlTagsStr = etree.tostring(xmlTags).decode()
                            if bddItem.tags != xmlTagsStr:
                                bddItem.tags = xmlTagsStr
                        else:
                            bddItem.tags = None
                        bddItem.validated = elemRequest['validated']
                        if elemRequest['stationId']:
                            bddItem.stationId = elemRequest['stationId']

                        break
            # self.bulk_save_objects(allItems)
            # s.bulk_save_objects(objects)    

        self.request.response.status_code = 204
        return self.request.response

    def getDatas(self):
        if self.type_ == 'camtrap':
            if self.request.method == 'GET':
                joinTable = join(Syn_TcameraTrap, self.viewTable,
                                Syn_TcameraTrap.pk_id == self.viewTable.c['pk_id'])
                query = select([Syn_TcameraTrap]).select_from(joinTable)
                query = query.where(self.viewTable.c['sessionID'] == self.sessionID)
                query = query.where(or_(self.viewTable.c['checked'] == 0, self.viewTable.c['checked'] == None))
                query = query.order_by(asc(self.viewTable.c['date_creation']))
                data = self.session.execute(query).fetchall()
                return self.handleResult(data)

        else:
            query = select([self.viewTable]
                           ).where(self.viewTable.c['sessionID'] == self.sessionID
                                   ).where(or_(self.viewTable.c['checked'] == 0, self.viewTable.c['checked'] == None))
        if self.type_ in ['gsm', 'argos']:
            query = query.order_by(self.viewTable.c['date'].desc())
        query = self.handleQuery(query)
        data = self.session._Session__binds[Main_Db_Base].execute(query).fetchall()
        return self.handleResult(data)

    def handleQuery(self, query):
        fk_sensor = self.request.params.mixed().get('FK_Sensor', None)
        if not self.sessionID and fk_sensor:
            newQuery = query.where(self.viewTable.c['FK_Sensor'] == fk_sensor)
        else:
            newQuery = query
        return newQuery

    def handleResult(self, data):
        if self.type_ in ['gsm', 'argos']:
            if 'geo' in self.request.params:
                geoJson = []
                for row in data:
                    geoJson.append({'type': 'Feature', 'id': row['PK_id'], 'properties': {
                                   'type': row['type'], 'Date': row['date']}, 'geometry': {'type': 'Point', 'coordinates': [row['lat'], row['lon']]}})
                result = {'type': 'FeatureCollection', 'features': geoJson}

            else:
                df = pd.DataFrame.from_records(
                    data, columns=data[0].keys(), coerce_float=True)
                X1 = df.iloc[:-1][['lat', 'lon']].values
                X2 = df.iloc[1:][['lat', 'lon']].values
                df['dist'] = np.append(haversine(X1, X2), 0).round(3)
                # Compute the speed
                df['speed'] = (df['dist'] / ((df['date'] - df['date'].shift(-1)
                                              ).fillna(1) / np.timedelta64(1, 'h'))).round(3)
                df['date'] = df['date'].apply(
                    lambda row: np.datetime64(row).astype(datetime.datetime))
                # Fill NaN
                df.fillna(value={'ele': -999}, inplace=True)
                df.fillna(value={'speed': 0}, inplace=True)
                df.replace(to_replace={'speed': np.inf},
                           value={'speed': 9999}, inplace=True)
                df.fillna(value=0, inplace=True)
                # dataResult = [dict(row) for row in data]
                dataResult = df.to_dict('records')
                result = [{'total_entries': len(dataResult)}]
                result.append(dataResult)

        elif self.type_ == 'camtrap':
            result = []
            for row in data:
                tmp = dict(row.items())
                varchartmp = tmp['path'].split('\\')
                tmp['path'] = "/imgcamtrap/" + str(varchartmp[len(varchartmp) - 2]) + "/"
                tmp['name'] = tmp['name'].replace(" ", "%20")
                tmp['id'] = tmp['pk_id']
                tmp['date_creation'] = str(tmp['date_creation'])
                tmp['date_creation'] = tmp['date_creation']
                if(str(tmp['tags']) != 'None'):
                    strTags = tmp['tags'].replace("<TAGS>", "")
                    strTags = strTags.replace("<TAG>", "")
                    strTags = strTags.replace("</TAGS>", "")
                    strTags = strTags.replace("</TAG>", ",")
                    strTags = strTags[:len(strTags) - 1]  # del the last ","
                    if(strTags != 'None'):
                        tmp['tags'] = strTags
                    else:
                        tmp['tags'] = ""
                result.append(tmp)
            #result = data
        else:
            result = data
        return result

    def retrieve(self):
        queryStmt = select([Equipment]).where(
            Equipment.ID == self.sessionID)
        data = self.session.get_bind(Equipment).execute(queryStmt.limit(1)).fetchall()
        return dict(data[0])

    def patch(self):
        # here patch method
        pass

    def create(self):
        return self.manual_validate()

    def manual_validate(self):
        # global graphDataDate
        params = self.request.params.mixed()
        user = self.request.authenticated_userid['sub']

        data = json.loads(params['data'])

        procStockDict = {
            'argos': '[sp_validate_Argos_GPS]',
            'gsm': '[sp_validate_GSM]'
        }

        try:
            if self.sessionID:
                ptt = params['id_ptt']
                ind_id = params['id_indiv']
                xml_to_insert = data_to_XML(data)
                stmt = text(""" DECLARE @nb_insert int , @exist int, @error int;
                    exec """ + dbConfig['data_schema'] + """.""" + procStockDict[self.type_]
                            + """ :id_list, :ind_id , :user , :ptt, @nb_insert OUTPUT, @exist OUTPUT , @error OUTPUT;
                        SELECT @nb_insert, @exist, @error; """
                            ).bindparams(bindparam('id_list', xml_to_insert),
                                         bindparam('ind_id', ind_id),
                                         bindparam('ptt', ptt),
                                         bindparam('user', user))
                nb_insert, exist, error = self.session._Session__binds[Main_Db_Base].execute(stmt).fetchone()
                self.session.commit()

                graphDataDate['pendingSensorData'] = None
                graphDataDate['indivLocationData'] = None
                return {'inserted': nb_insert, 'existing': exist, 'errors': error}
            else:
                return self.error_response(None)
        except Exception as err:
            print_exc()
            return self.error_response(err)

    def error_response(self, err):
        if err is not None:
            msg = err.args[0] if err.args else ""
            response = 'Problem occurs : ' + str(type(err)) + ' = ' + msg
        else:
            response = 'No individual equiped'
        self.request.response.status_code = 500
        return response


class SensorDatasByType(CustomResource):

    item = SensorDatasBySession
    children = [('{int}', SensorDatasBySession)]
    dictFuncImport = {
        'argos': uploadFileArgos,
        'gsm': uploadFilesGSM,
        'rfid': uploadFileRFID,
    }
    __acl__ = context_permissions['SensorDatasByType']

    def __init__(self, ref, parent):
        CustomResource.__init__(self, ref, parent)
        self.type_ = ref
        self.viewTable = viewDict[ref]

        self.queryType = {'gsm': self.queryWithIndiv,
                          'argos': self.queryWithIndiv,
                          'rfid': self.queryWithSite,
                          'camtrap': self.queryWithSite}

    def retrieve(self):
        criteria = self.request.params.get('criteria', None)
        if criteria == '[]' :
            criteria = None

        queryStmt = self.queryType[self.type_]()
        queryStmt = self.handleCriteria(queryStmt, criteria)
        data = self.session._Session__binds[Main_Db_Base].execute(queryStmt).fetchall()
        dataResult = [dict(row) for row in data]
        result = [{'total_entries': len(dataResult)}]
        result.append(dataResult)
        return result

    def handleCriteria(self, queryStmt, criteria=None):
        # apply other criteria
        if self.type_ in ['gsm', 'argos'] and not criteria:
            queryStmt = queryStmt.order_by(self.viewTable.c['FK_ptt'].asc())

        return queryStmt

    def queryWithSite(self):
        if(self.type_ in ['camtrap']):
            queryStmt = select([self.viewTable.c['sessionID'],
                                self.viewTable.c['UnicIdentifier'],
                                self.viewTable.c['fk_sensor'],
                                self.viewTable.c['site_name'],
                                self.viewTable.c['site_type'],
                                self.viewTable.c['StartDate'],
                                self.viewTable.c['EndDate'],
                                self.viewTable.c['FK_MonitoredSite'],
                                func.count(self.viewTable.c['sessionID']).label(
                                    'nb_photo'),
                                func.sum(self.viewTable.c['processed']).label('processed')
                                ]
                               )
            queryStmt = queryStmt.where(self.viewTable.c['checked'] == None)
            queryStmt = queryStmt.group_by(self.viewTable.c['sessionID'],
                                          self.viewTable.c['UnicIdentifier'],
                                          self.viewTable.c['fk_sensor'],
                                          self.viewTable.c['site_name'],
                                          self.viewTable.c['site_type'],
                                          self.viewTable.c['StartDate'],
                                          self.viewTable.c['EndDate'],
                                          self.viewTable.c['FK_MonitoredSite']
                                          )
        else:
            queryStmt = select(self.viewTable.c)

        return queryStmt

    def queryWithIndiv(self):
        selectStmt = select([self.viewTable.c['FK_Individual'],
                             self.viewTable.c['sessionID'],
                             self.viewTable.c['Survey_type'],
                             self.viewTable.c['FK_ptt'],
                             self.viewTable.c['FK_Sensor'],
                             self.viewTable.c['StartDate'],
                             self.viewTable.c['EndDate'],
                             func.count().label('nb'),
                             func.max(self.viewTable.c['date']).label(
                                 'max_date'),
                             func.min(self.viewTable.c['date']).label('min_date')])

        queryStmt = selectStmt.where(self.viewTable.c['checked'] == 0
                                     ).group_by(self.viewTable.c['FK_Individual'],
                                                self.viewTable.c['Survey_type'],
                                                self.viewTable.c['FK_ptt'],
                                                self.viewTable.c['StartDate'],
                                                self.viewTable.c['EndDate'],
                                                self.viewTable.c['FK_Sensor'],
                                                self.viewTable.c['sessionID'],
                                                )
        return queryStmt

    def create(self):
        return self.dictFuncImport[self.type_](self.request)


    def buildMetaDataInfoFromErdData(self, name):
        metaDataInfo = {}
        metaDataInfo['monitoredSite'] = {}
        metaDataInfo['image'] = {}
        metaDataInfo['user'] = {}
        metaDataInfo['session'] = {}
        metaDataInfo['misc'] = {}


        user = self.session.query(User).get(self.request.authenticated_userid['sub'])
        
        metaDataInfo['user']['TUse_FirstName'] = user.Firstname
        metaDataInfo['user']['TUse_LastName'] = user.Lastname
        # print(self.request)
        metaDataInfo['image']['photoId'] = 0
        metaDataInfo['image']['name'] = name
        metaDataInfo['image']['fkSensor'] = int(self.request.POST['id'])
        metaDataInfo['image']['dateTimeOriginalPhoto'] = ''
        metaDataInfo['image']['dateInsertSQL'] = ''
        metaDataInfo['image']['lastTransformationDate'] = ''
        metaDataInfo['image']['dateTimeCreationPhoto'] = ''
        metaDataInfo['image']['lastDateWriteInPhoto'] = ''
        metaDataInfo['image']['shootId'] = -1
        
        metaDataInfo['session']['startDate'] = str(self.request.POST['startDate'])
        metaDataInfo['session']['endDate'] = str(self.request.POST['endDate'])

        metaDataInfo['misc']['regionAnPlaceMonitoredSite'] = ''
        metaDataInfo['misc']['projectName'] = ''

        monitoredSite = self.session.query(MonitoredSite).get(self.request.POST['monitoredSiteId'])

        sitePosition = monitoredSite.GetLastPositionWithDate(parser(str(self.request.POST['endDate'])))
        metaDataInfo['monitoredSite']['Name'] = monitoredSite.Name
        metaDataInfo['monitoredSite']['LAT'] = sitePosition['LAT']
        metaDataInfo['monitoredSite']['LON'] = sitePosition['LON']
        metaDataInfo['monitoredSite']['Precision'] = sitePosition['Precision']
        metaDataInfo['monitoredSite']['ELE'] = sitePosition['ELE']

        return metaDataInfo

    def uploadFileCamTrapResumable(self):

        def get_md5(pathFile):
            hash_md5 = hashlib.md5()
            with open(pathFile, "rb") as f:
                for chunk in iter(lambda: f.read(4096), b""):
                    hash_md5.update(chunk)
            return hash_md5.hexdigest()
        if mySubExif is None:
            return HTTPNotImplemented("ExifTool is not available we can't process the request")

        flagDate = False
        flagError = False
        pathPrefix = dbConfig['camTrap']['path']
        pathPost = str(self.request.POST['path'])
        jetLag = {}
        jetLag['operator'] = str( self.request.POST['jetLagOperator'] )
        jetLag['hours'] = str(self.request.POST['jetLagHours'])
        fk_sensor = int(self.request.POST['id'])
        messageDate = ""
        #TODO make all test on temporary file before store it (better way)

        uri = pathPrefix + '\\' + pathPost
        newGUID = str(uuid.uuid4())
        original_name = self.request.POST['resumableFilename']
        extType = original_name.split('.')
        extension = str(extType[len(extType) - 1])
        newName = f'{newGUID}.{extension}'
        inputFile = self.request.POST['file'].file
        if(int(self.request.POST['resumableChunkNumber']) == 1 and int(self.request.POST['resumableCurrentChunkSize']) == int(self.request.POST['resumableTotalSize']) and str(extType[len(extType) - 1]) != ".zip"):
            pathFile = f'{pathPrefix}\\{pathPost}\\{newName}'
            if not os.path.isfile(pathFile):
                with open(pathFile, 'wb') as output_file:
                    shutil.copyfileobj(inputFile, output_file)
                output_file.close()

            py_checksum = get_md5(pathFile)

            jetLagArray = jetLag['hours'].split(':')
            if jetLag['operator'] == '+':
                operator = 1
            if jetLag['operator'] == '-':
                operator = -1
            exifDate = dateFromExif(pathFile)
            dateOrigine = datetime.datetime.strptime(str(exifDate) , "%Y-%m-%d %H:%M:%S")
            dateDecall = dateOrigine + datetime.timedelta(hours = operator*int(jetLagArray[0]) , minutes = operator*int(jetLagArray[1]), seconds = operator*int(jetLagArray[2]) )
            datePhoto = dateDecall
            user = self.request.authenticated_userid['sub']
            resultProcessing = imgProcess(self.request.POST['file'].file)
            defaultTags = 'Poor quality'
            if resultProcessing == 1 :
                defaultTags = 'Standard quality'
            if(checkDate(datePhoto, str(self.request.POST['startDate']), str(self.request.POST['endDate']))):
                resOCR = OCR_parser(pathFile)
                temp = resOCR['temp'] or 'N/A'
                hygro = resOCR['hygro'] or 'N/A'

                metaDataInfo = self.buildMetaDataInfoFromErdData(
                    name=newName
                )
                cmdMetaDataInfo = buildCmdMetaDatasAtImport(self,metaDataInfo)
                cmdMetaDataInfo.append('-XMP-dc:Subject= { "temperature" : '+str(temp)+', "hygrometry" : '+str(hygro)+ '}')

                try:
                    AddPhotoOnSQL(
                        dbsession=self.request.dbsession,
                        fk_sensor=fk_sensor,
                        path=str(uri),
                        name=newName,
                        extension=str(extType[len(extType) - 1]),
                        date_creation=datePhoto,
                        startDate=str(self.request.POST['startDate']),
                        endDate=str(self.request.POST['endDate']),
                        user=user,
                        cmdMetaData=cmdMetaDataInfo,
                        defaultTags=defaultTags,
                        checksum=py_checksum,
                        original_name=original_name
                    )

                    resExif = ''
                    try:
                        # sourceFile = str(uri)+'\\'+str(self.request.POST['resumableFilename'])
                        sourceFile = pathFile
                        cmdMetaDataInfo.append(sourceFile)
                        cmdMetaDataInfo.append('-overwrite_original')


                        params = map(fsencode , cmdMetaDataInfo)

                        resExif = mySubExif.execute( *params )
                    except Exception as e:
                        print_exc()
                        print("ERROR",e)
                        raise e

                    resizePhoto(pathFile)
                    messageDate = "ok"
                except Exception as e:
                    print_exc()
                    flagError = True
                    os.remove(pathFile)
                    messageDate = 'something goes wrong when insert in database'
            else:
                os.remove(pathFile)
                flagDate = True
                # messageDate = 'Date not valid (' + str(datePhoto) + ') <BR>'
                messageDate = 'Date not valid'
        else:
            if not os.path.isfile(pathFile):
                # calculate the position of cursor
                position = int(self.request.POST['resumableChunkNumber'])
                # write in the file
                with open(uri + '\\' + str(self.request.POST['resumableIdentifier']) + "_" + str(position), 'wb') as output_file:
                    shutil.copyfileobj(inputFile, output_file)
                output_file.close()

        if(flagDate or flagError):
            self.request.response.status_code = 415
            self.request.response.text = messageDate
        else:
            self.request.response.status_code = 200
            self.request.response.text = 'ok'

        return self.request.response

    def concatChunk(self):
        flagSuppression = False
        res = {}
        message = ""
        timeConcat = ""
        messageConcat = ""
        messageUnzip = ""
        nbInZip = 0
        nbInserted = 0
        pathPrefix = dbConfig['camTrap']['path']
        self.request.response.status_code = 200
        self.request.response.text = 'ok'
        # create folder
        if(int(self.request.POST['action']) == 0):
            pathPost = str(self.request.POST['path'])
            if not os.path.exists(pathPrefix + '\\' + pathPost):
                os.makedirs(pathPrefix + '\\' + pathPost)
                self.request.response.status_code = 200
            if not os.path.exists(pathPrefix + '\\' + pathPost + '\\thumbnails\\'):
                os.makedirs(pathPrefix + '\\' + pathPost + '\\thumbnails\\')
                self.request.response.status_code = 200
        return self.request.response
        ''' no more zip file upload but i keep it in case of '''
        # else:
        #     # print(" il faut que la date de l'exif soit entre le " +
        #     #       str(request.POST['startDate']) + " et le " + str(request.POST['endDate']))
        #     pathPost = str(self.request.POST['path'])
        #     fk_sensor = int(self.request.POST['id'])
        #     name = str(self.request.POST['file'])
        #     debutTime = time.time()
        #     # print("name " + str(name))
        #     # print("on veut reconstruire le fichier" + str(name) + " qui se trouve dans " +
        #     #       str(request.POST['path']) + " en :" + str(request.POST['taille']) + " fichiers")

        #     txtConcat = str(name).split('.')
        #     # print("avant text")
        #     if not os.path.isfile(pathPrefix + '\\' + pathPost + '\\' + str(txtConcat[0]) + str('.txt')):
        #         with open(pathPrefix + '\\' + pathPost + '\\' + str(txtConcat[0]) + str('.txt'), 'a') as socketConcat:
        #             # print("fichier socket ok")
        #             # print("%s\n%s\n" % (str(request.POST['taille']), str('0')))
        #             socketConcat.write("%s\n%s\n" % (self.request.POST['taille'], 0))
        #             # si le fichier n'existe pas on va le reconstruire
        #             if not os.path.isfile(pathPrefix + '\\' + pathPost + '\\' + str(name)):
        #                 # on ouvre le fichier comme destination
        #                 with open(pathPrefix + '\\' + pathPost + '\\' + str(name), 'wb') as destination:
        #                     # on va parcourir l'ensemble des chunks
        #                     for i in range(1, int(self.request.POST['taille']) + 1):
        #                         # si le chunk est present
        #                         if os.path.isfile(pathPrefix + '\\' + pathPost + '\\' + str(self.request.POST['name']) + '_' + str(i)):
        #                             shutil.copyfileobj(open(pathPrefix + '\\' + pathPost + '\\' + str(
        #                                 self.request.POST['name']) + '_' + str(i), 'rb'), destination)  # on le concat dans desitnation
        #                             socketConcat.write("%s\n" % (i))
        #                             socketConcat.flush()
        #                             if (i == 30):
        #                                 with open(pathPrefix + '\\' + pathPost + '\\' + str(txtConcat[0]) + str('.txt'), 'r') as testConcat:
        #                                     first = testConcat.readline()
        #                                     for last in testConcat:
        #                                         avantDer = last
        #                                 testConcat.close()
        #                         else:  # si il n'est pas present
        #                             flagSuppression = True
        #                             self.request.response.status_code = 510
        #                             message = "Chunk file number : '" + \
        #                                 str(i) + "' missing try to reupload the file '" + \
        #                                 str(self.request.POST['file']) + "'"
        #                             break  # break the for
        #                 destination.close()
        #             else:
        #                 self.request.response.status_code = 510
        #                 message = "File : '" + \
        #                     str(name) + "' is already on the server <BR>"
        #             if (flagSuppression):
        #                 # supprime le fichier destination et on force la sortie
        #                 os.remove(pathPrefix + '\\' + pathPost + '\\' + str(name))
        #             else:
        #                 socketConcat.close()
        #                 os.remove(pathPrefix + '\\' + pathPost + '\\' +
        #                           str(txtConcat[0]) + str('.txt'))
        #                 # on va parcourir l'ensemble des chunks
        #                 for i in range(1, int(self.request.POST['taille']) + 1):
        #                     os.remove(pathPrefix + '\\' + pathPost + '\\' +
        #                               str(self.request.POST['name']) + '_' + str(i))
        #         socketConcat.close()

        #     # destination.close()
        #     finTime = time.time()
        #     timeConcat = str(finTime - debutTime)
        #     # print("durée :" + timeConcat)
        #     # file concat ok now unzip
        #     if(message == ""):
        #         if(self.request.POST['type'] == "application/x-zip-compressed"):
        #             debutTime = time.time()
        #             # print(" on commence la décompression ")
        #             messageUnzip, nbInZip, nbInserted = unzip(pathPrefix + '\\' + pathPost + '\\' + str(
        #                 name), pathPrefix + '\\' + pathPost + '\\', fk_sensor, str(self.request.POST['startDate']), str(self.request.POST['endDate']))
        #             # print(messageUnzip)
        #             if(messageUnzip != ""):
        #                 self.request.response.status_code = 510
        #             # print("fin decompression ")
        #             finTime = time.time()
        #             # print("durée :" + str(finTime - debutTime))
        #         else:
        #             extType = self.request.POST['file'].split('.')
        #             destfolder = pathPrefix + '\\' + pathPost + '\\'
        #             # datePhoto = dateFromExif(destfolder + str(name))
        #             exifDate = dateFromExif(destfolder + str(name))
        #             dateOrigine = datetime.datetime.strptime(str(exifDate) , "%Y-%m-%d %H:%M:%S")
        #             dateDecall = dateOrigine + datetime.timedelta(hours = operator*int(jetLagArray[0]) , minutes = operator*int(jetLagArray[1]), seconds = operator*int(jetLagArray[2]) )
        #             datePhoto = dateDecall.strftime("%Y-%m-%d %H:%M:%S")
        #             if(checkDate(datePhoto, jetLag, str(self.request.POST['startDate']), str(self.request.POST['endDate']))):
        #                 AddPhotoOnSQL(fk_sensor, destfolder, name, str(
        #                     extType[len(extType) - 1]), datePhoto)
        #                 resizePhoto(str(destfolder) + str(name))
        #             else:
        #                 os.remove(destfolder + str(name))
        #                 # flagDate = True
        #                 self.request.response.status_code = 510
        #                 messageConcat = 'Date not valid (' + \
        #                     str(datePhoto) + ') <BR>'

        #             #AddPhotoOnSQL(fk_sensor,destfolder,name, str(extType[len(extType)-1]) , dateFromExif (destfolder+str(name)))
        # # os.remove(pathPrefix+'\\'+pathPost+'\\'+str(txtConcat[0])+str('.txt')) #supprime le fichier destination et on force la sortie
        # # os.remove(pathPrefix+'\\'+pathPost+'\\'+str(name)) #supprime le fichier
        # # destination et on force la sortie
        # res = {
        #     'message': message,
        #     'messageConcat': messageConcat,
        #     'messageUnzip': messageUnzip,
        #     'timeConcat': timeConcat,
        #     'nbInZip': nbInZip,
        #     'nbInserted': nbInserted
        # }
        # return res

    def check_chunk_file(self):
        '''
            will check if the md5hash calculated by de front end for a file
            and the path calculated with the equipment infos are in db
            means the same file was uploaded for the same session
        '''
        pathPrefix = dbConfig['camTrap']['path'] or None
        pathSuffix = self.request.GET.get('path', None)
        checksum = self.request.GET.get('checksum', None)

        if pathPrefix is None or pathSuffix is None:
            return

        query = self.request.dbsession.query(CamTrap)
        query = query.filter(
            CamTrap.path == f'{pathPrefix}\\{pathSuffix}\\',
            CamTrap.Checksum == checksum
        )
        res = query.first()

        if res is None:
            self.request.response.status_code = 204
        else:
            self.request.response.status_code = 200
            self.request.response.text = 'exist'

        return self.request.response

    def auto_validation(self):
        # global graphDataDate
        if self.type_ == 'camtrap':
            return self.validateCamTrap()

        param = self.request.params.mixed()
        freq = param['frequency']
        listToValidate = json.loads(param['toValidate'])
        user = self.request.authenticated_userid['sub']

        if freq == 'all':
            freq = 1

        Total_nb_insert = 0
        Total_exist = 0
        Total_error = 0

        if listToValidate == 'all':
            Total_nb_insert, Total_exist, Total_error = self.auto_validate_ALL_stored_procGSM_Argos(
                user, self.type_, freq)
        else:
            if self.type_ == 'rfid':
                for row in listToValidate:
                    equipID = row['equipID']
                    sensor = row['FK_Sensor']
                    if equipID == 'null' or equipID is None:
                        equipID = None
                    else:
                        equipID = int(equipID)
                    nb_insert, exist, error = self.auto_validate_proc_stocRfid(
                        equipID, sensor, freq, user)
                    self.session.commit()
                    Total_exist += exist
                    Total_nb_insert += nb_insert
                    Total_error += error
            else:
                for row in listToValidate:
                    ind_id = row['FK_Individual']
                    ptt = row['FK_ptt']

                    try:
                        ind_id = int(ind_id)
                    except TypeError:
                        ind_id = None

                    nb_insert, exist, error = self.auto_validate_stored_procGSM_Argos(
                        ptt, ind_id, user, self.type_, freq)
                    self.session.commit()

                    Total_exist += exist
                    Total_nb_insert += nb_insert
                    Total_error += error

        # graphDataDate['pendingSensorData'] = None
        # graphDataDate['indivLocationData'] = None
        return {'inserted': Total_nb_insert, 'existing': Total_exist, 'errors': Total_error}

    def auto_validate_ALL_stored_procGSM_Argos(self, user, type_, freq):
        procStockDict = {
            'argos': '[sp_auto_validate_ALL_Argos_GPS]',
            'gsm': '[sp_auto_validate_ALL_GSM]',
            'rfid': '[sp_validate_ALL_rfid]'
        }

        stmt = text(""" DECLARE @nb_insert int , @exist int , @error int;
        exec """ + dbConfig['data_schema'] + """.""" + procStockDict[type_] + """ :user ,:freq , @nb_insert OUTPUT, @exist OUTPUT, @error OUTPUT;
        SELECT @nb_insert, @exist, @error; """
                    ).bindparams(bindparam('user', user), bindparam('freq', freq))
        nb_insert, exist, error = self.session._Session__binds[Main_Db_Base].execute(stmt).fetchone()
        return nb_insert, exist, error

    def auto_validate_proc_stocRfid(self, equipID, sensor, freq, user):
        if equipID is None:
            stmt = update(DataRfidWithSite).where(and_(DataRfidWithSite.c[
                'FK_Sensor'] == sensor, DataRfidWithSite.c['equipID'] == equipID)).values(checked=1)
            self.session._Session__binds[Main_Db_Base].execute(stmt)
            nb_insert = exist = error = 0
        else:
            stmt = text(""" DECLARE @nb_insert int , @exist int , @error int;
                exec """ + dbConfig['data_schema']
                        + """.[sp_validate_rfid]  :equipID,:freq, :user , @nb_insert OUTPUT, @exist OUTPUT, @error OUTPUT;
                SELECT @nb_insert, @exist, @error; """
                        ).bindparams(bindparam('equipID', equipID),
                                     bindparam('user', user),
                                     bindparam('freq', freq))
            nb_insert, exist, error = self.session._Session__binds[Main_Db_Base].execute(stmt).fetchone()

        return nb_insert, exist, error

    def auto_validate_stored_procGSM_Argos(self, ptt, ind_id, user, type_, freq):
        procStockDict = {
            'argos': '[sp_auto_validate_Argos_GPS]',
            'gsm': '[sp_auto_validate_GSM]'
        }

        if type_ == 'argos':
            table = ArgosDatasWithIndiv
        elif type_ == 'gsm':
            table = GsmDatasWithIndiv

        if ind_id is None:
            stmt = update(table).where(and_(table.c['FK_Individual'] == None,
                                            table.c['FK_ptt'] == ptt)
                                       ).where(table.c['checked'] == 0).values(checked=1)
            self.session._Session__binds[Main_Db_Base].execute(stmt)
            nb_insert = exist = error = 0
        else:
            stmt = text(""" DECLARE @nb_insert int , @exist int , @error int;
            exec """ + dbConfig['data_schema'] + """.""" + procStockDict[type_]
                        + """ :ptt , :ind_id , :user ,:freq , @nb_insert OUTPUT, @exist OUTPUT, @error OUTPUT;
            SELECT @nb_insert, @exist, @error; """
                        ).bindparams(bindparam('ind_id', ind_id),
                                     bindparam('user', user),
                                     bindparam('freq', freq),
                                     bindparam('ptt', ptt))
            nb_insert, exist, error = self.session._Session__binds[Main_Db_Base].execute(stmt).fetchone()

        return nb_insert, exist, error

    def deletePhotoOnSQL(self, fk_sensor):
        currentPhoto = self.session.query(CamTrap).get(fk_sensor)
        self.session.delete(currentPhoto)
        return True

    def find_unequipment_session_to_validate(
        self,
        startDate=None,
        fk_sensor=None,
        fk_monitoredSite=None
    ):
        # A sensor could be equipped/unequipped on the same monitored site many time
        # And for a Id of equippement we can't directly find the unequippement row
        # we have to find all unequippement rows > to the startdate, order by date
        # and take the first one
        Q_unequipment_session_to_validate = self.session.query(Equipment)
        Q_unequipment_session_to_validate = Q_unequipment_session_to_validate.filter(
            (Equipment.StartDate > startDate),
            (Equipment.FK_Sensor == fk_sensor),
            (Equipment.FK_MonitoredSite == fk_monitoredSite),
            (Equipment.Deploy == 0)
        )
        Q_unequipment_session_to_validate = Q_unequipment_session_to_validate.order_by(
            Equipment.StartDate
        )
        return Q_unequipment_session_to_validate.first()

    def find_cameratrap_session(
        self,
        database_target=None,
        fk_sensor=None,
        starting_date_photos=None,
        ending_date_photos=None
    ):
        Q_cameraTrap_photos = self.session.query(CamTrap)
        Q_cameraTrap_photos = Q_cameraTrap_photos.filter(
            (CamTrap.databaseTarget == database_target),
            (CamTrap.fk_sensor == fk_sensor ),
            (CamTrap.date_creation >= starting_date_photos),
            (CamTrap.date_creation <= ending_date_photos),
            (CamTrap.checked == None)
        )
        return Q_cameraTrap_photos.all()

    def find_tags_in_db(
        self,
        unique_tags=[]
    ):
        if len(unique_tags) == 0:
            return []

        Q_tags_in_db = self.session.query(Tags)
        Q_tags_in_db = Q_tags_in_db.filter(
            Tags.Label.in_(unique_tags)
        )
        return Q_tags_in_db.all()

    def find_all_pending_stations(
        self,
        stations=[]
    ):
        Q_pending_stations = self.session.query(Station)
        Q_pending_stations = Q_pending_stations.filter(
            Station.ID.in_(stations)
        )
        return Q_pending_stations.all()

    def update_all_pending_stations_to_standard(
        self,
        stations_in_db=[]
    ):
        for station in stations_in_db:
            station._type_id = 1

        self.session.add_all(stations_in_db)
        self.session.flush()

    def create_media_file_protocole(
        self,
        stations_in_db=[],
        index_station_id_mediasfiles={}
    ):
        all_observations = []
        protocole_mediafile = self.session.query(Observation.TypeClass).get(239)

        for station in stations_in_db:
            new_observation = Observation(session=self.session, request=self.request)
            new_observation.FK_Station=station.ID
            new_observation._type_id=protocole_mediafile.ID
            medias_files = index_station_id_mediasfiles.get(station.ID)
            relative_link = f'{station.ID}\\{medias_files.GUID}'
            if medias_files.Extension:
                relative_link = f'{relative_link}.{medias_files.Extension}'
            new_observation.values = {
                'Mediafile': relative_link
            }
            all_observations.append(new_observation)

        self.session.add_all(all_observations)
        self.session.flush()
        return all_observations

    def update_all_cameratrap_session_to_checked(self, cameratrap_session=[]):
        for cameratrap_photo in cameratrap_session:
            cameratrap_photo.checked = 1

        self.session.add_all(cameratrap_session)
        self.session.flush()

    def rollback_files(
        self,
        medias_files_to_remove_if_rollback=[],
        cameraTrap_path=None,
        path_working_directory=None
    ):
        # remove media files created
        for media_file in medias_files_to_remove_if_rollback:
            try:
                pathlib.Path(media_file).unlink()
            except FileNotFoundError as e:
                pass
            except Exception as e:
                raise e
        absolute_path_working_dir_validated = f'{cameraTrap_path}\\{path_working_directory}\\validated'
        shutil.rmtree(absolute_path_working_dir_validated, ignore_errors=True)

    def copy_file_for_mediasfiles(
        self,
        index_station_id_mediasfiles={},
        index_station_id_cameratrap_photo_id={},
        index_camtrap_id_photo={},
        media_files_path=None
    ):
        medias_files_path_created=[]
        for stationid in index_station_id_mediasfiles:
            try:
                medias_files = index_station_id_mediasfiles.get(stationid)
                photos_files_id = index_station_id_cameratrap_photo_id.get(stationid)
                photos_files = index_camtrap_id_photo.get(photos_files_id)
                dstFolder = f'{media_files_path}\\{stationid}'
                pathlib.Path(dstFolder).mkdir(parents=True, exist_ok=True)
                srcFile=f'{photos_files.Path}\\{photos_files.FileName}'
                dstFile = f'{dstFolder}\\{medias_files.GUID}'
                if medias_files.Extension:
                    dstFile = f'{dstFile}.{medias_files.Extension}'

                shutil.copy(
                    src=f'{srcFile}',
                    dst=f'{dstFile}'
                )
                medias_files_path_created.append(f'{dstFile}')
            except Exception as e:
                raise e

        return medias_files_path_created

    def create_mediasfiles_rows(
        self,
        stations_in_db={},
        index_station_id_cameratrap_photo_id={},
        index_camtrap_id_photo={},
        dateExecution=None
    ):
        index_station_id_mediasfiles={}
        all_mediaFiles = []

        for station in stations_in_db:
            cameratrap_photo_id = index_station_id_cameratrap_photo_id.get(station.ID)
            photo = index_camtrap_id_photo.get(cameratrap_photo_id)
            curFileName = photo.FileName
            curExtension = None
            splittedName = curFileName.split('.')
            if len(splittedName) > 1:
                curExtension = splittedName[-1]

            newMediasFiles = MediasFiles(
                GUID=str(uuid.uuid4()),
                Original_Name=photo.FileName,
                Extension=curExtension,
                Date_Uploaded=dateExecution,
                Creator=self.request.authenticated_userid['sub']
            )
            index_station_id_mediasfiles[station.ID] = newMediasFiles
            all_mediaFiles.append(newMediasFiles)

        self.session.add_all(all_mediaFiles)
        self.session.flush()
        return index_station_id_mediasfiles


    def validateCamTrap(self):
        # supression des photos rejete
        '''
        last step session is in last state so we build metadata
        call stored procedure
        if ok
        build metadata
        resize photos
        remove photos
        '''
        data = self.request.params.mixed()
        fkMonitoredSite = data['fk_MonitoredSite']
        fkEquipmentId = data['fk_EquipmentId']
        fkSensor = data['fk_Sensor']
        medias_files_to_remove_if_rollback = []
        try:
            equipment_session_to_validate = self.session.query(Equipment).get(fkEquipmentId)
            if equipment_session_to_validate is None:
                return HTTPNotFound('')

            unequipment_session_to_validate = self.find_unequipment_session_to_validate(
                startDate=equipment_session_to_validate.StartDate,
                fk_sensor=equipment_session_to_validate.FK_Sensor,
                fk_monitoredSite=equipment_session_to_validate.FK_MonitoredSite
            )

            if unequipment_session_to_validate is None:
                return HTTPConflict('No end date found for this session')

            starting_date_photos = equipment_session_to_validate.StartDate
            ending_date_photos = unequipment_session_to_validate.StartDate
            fk_sensor = equipment_session_to_validate.FK_Sensor
            fk_monitoredSite = equipment_session_to_validate.FK_MonitoredSite
            sourceDatabaseName = dbConfig.get('data_schema')
            sensor = self.session.query(Sensor).get(fk_sensor)
            monitored_site = self.session.query(MonitoredSite).get(fk_monitoredSite)

            path_working_directory = (
                f'{sensor.UnicIdentifier}'
                '_'
                f'{starting_date_photos:%Y-%m-%d}'
                '_'
                f'{ending_date_photos:%Y-%m-%d}'
                '_'
                f'{monitored_site.Name}'
            )

            if sourceDatabaseName is None:
                return HTTPConflict('No database name to filter camtrap')

            cameratrap_session = self.find_cameratrap_session(
                database_target=sourceDatabaseName,
                fk_sensor=fk_sensor,
                starting_date_photos=starting_date_photos,
                ending_date_photos=ending_date_photos
            )

            STATUS_CAMERATRAP_FILE = {
                0: None, # 0:'not_see'
                1: None, # 1:'see',
                2: True, # 2:'validated',
                4: False # 4:'refused'
            }

            all_tags = []
            all_photos = []
            all_cameratrap_photos = {
                True: [],
                False: []
            }
            all_stations = []
            index_tags_label_id = {}
            index_tags_id_label = {}
            index_station_id_cameratrap_photo_id = {}
            index_camtrap_id = {}
            index_camtrap_id_photo = {}
            index_photo_id_camtrap_id = {}
            index_photo_id = {}
            index_station_id_mediasfiles = {}
            cameraTrap_path = dbConfig['camTrap']['path']
            media_files_path = dbConfig['mediasFiles']['path']
            dateExecution = datetime.datetime.now()

            # create list new photos orm and list all_tags
            # will create dict with id as key in order to use it as "index" for retrieve data
            # index_camtrap_id_photo
            # index_tags_label_id
            # index_station_id_cameratrap_photo_id
            for cameratrap_photo in cameratrap_session:
                photos_validated = STATUS_CAMERATRAP_FILE.get(cameratrap_photo.validated)
                if photos_validated is None:
                    return HTTPConflict(f'Session is not finished {cameratrap_photo.name} is not validated or refused ')

                # only validated photo will be created in db mother
                if photos_validated is True:
                    all_cameratrap_photos[True].append(cameratrap_photo)
                    if cameratrap_photo.pk_id not in index_camtrap_id:
                        index_camtrap_id[cameratrap_photo.pk_id] = cameratrap_photo
                    cur_tags = []
                    cur_photo = Photos(
                        Path=cameratrap_photo.path,
                        FileName=cameratrap_photo.name,
                        Date=dateExecution,
                        Fk_MonitoredSite=fk_monitoredSite,
                        old_id=cameratrap_photo.pk_id,
                        Statut=cameratrap_photo.validated,
                        Note=cameratrap_photo.note
                    )
                    if cameratrap_photo.pk_id not in index_camtrap_id_photo:
                        index_camtrap_id_photo[cameratrap_photo.pk_id] = cur_photo
                    all_photos.append(cur_photo)
                    # if tags not null we parse them for creation
                    if cameratrap_photo.tags is not None:
                        try:
                            tags = ET.fromstring(cameratrap_photo.tags)
                            for tag in tags:
                                value = tag.text
                                if value not in index_tags_label_id:
                                    index_tags_label_id[value] = 0
                                cur_tags.append(value)
                        except ET.ParseError as e :
                            pass
                    if cameratrap_photo.stationId is not None:
                        if cameratrap_photo.stationId not in index_station_id_cameratrap_photo_id:
                            index_station_id_cameratrap_photo_id[cameratrap_photo.stationId] = cameratrap_photo.pk_id

                    all_tags.append(cur_tags)
                if photos_validated is False:
                    all_cameratrap_photos[False].append(cameratrap_photo)

            list_unique_tags = [ label for label in index_tags_label_id ]
            tags_in_db = self.find_tags_in_db(unique_tags=list_unique_tags)

            for tag in tags_in_db:
                label_in_db = tag.Label
                id_in_db = tag.ID
                index_tags_label_id[label_in_db] = id_in_db
                if id_in_db not in index_tags_id_label:
                    index_tags_id_label[id_in_db] = label_in_db

            tags_to_create = [ label for label in index_tags_label_id if index_tags_label_id[label] == 0 ]

            new_tags = []
            for label in tags_to_create:
                new_tag = Tags(Label=label)
                new_tags.append(new_tag)

            # add flush new tags for getting id
            self.session.add_all(new_tags)
            self.session.flush()

            for new_tag in new_tags:
                new_label_in_db = new_tag.Label
                new_id_in_db = new_tag.ID
                index_tags_label_id[new_label_in_db] = new_id_in_db
                if new_id_in_db not in index_tags_id_label:
                    index_tags_id_label[new_id_in_db] = new_label_in_db

            # add and flush new photos for getting id
            self.session.add_all(all_photos)
            self.session.flush()

            photos_tags = []
            for index, photo in enumerate(all_photos):
                tags = all_tags[index]
                index_photo_id[photo.Id] = photo
                if photo.Id not in index_photo_id_camtrap_id:
                    index_photo_id_camtrap_id[photo.Id] = photo.old_id
                for label in tags:
                    id_tag_in_db = index_tags_label_id.get(label)
                    new_photos_tags = PhotosTags(
                        Fk_Tags=id_tag_in_db,
                        Fk_Photos=photo.Id
                    )
                    photos_tags.append(new_photos_tags)

            # add and flush new photos tags
            self.session.add_all(photos_tags)
            self.session.flush()
            # from here we have created all photos, tags and photostags rows
            # now we gonna change type of stations
            # create mediafile rows
            # copy media files
            # create protocole mediafile

            # start update stationType
            all_stations_id = [ station_id for station_id in index_station_id_cameratrap_photo_id]
            all_stations = self.find_all_pending_stations(
                stations=all_stations_id
            )
            self.update_all_pending_stations_to_standard(
                stations_in_db=all_stations
            )
            # end update stationType
            # start create mediafile rows
            index_station_id_mediasfiles = self.create_mediasfiles_rows(
                stations_in_db=all_stations,
                index_station_id_cameratrap_photo_id=index_station_id_cameratrap_photo_id,
                index_camtrap_id_photo=index_camtrap_id_photo,
                dateExecution=dateExecution
            )
            # end create mediafile rows
            # start copy files for mediafiles
            medias_files_to_remove_if_rollback = self.copy_file_for_mediasfiles(
                index_station_id_mediasfiles=index_station_id_mediasfiles,
                index_station_id_cameratrap_photo_id=index_station_id_cameratrap_photo_id,
                index_camtrap_id_photo=index_camtrap_id_photo,
                media_files_path=media_files_path
            )
            # end copy files for mediafiles
            # start create observation for protocole media files
            all_observations = self.create_media_file_protocole(
                stations_in_db=all_stations,
                index_station_id_mediasfiles=index_station_id_mediasfiles
            )
            # end create observation for protocole media files
            # start update mediafile fk with ObservationDynPropValue.ID
            self.update_fk_for_medias_files_with_observationdynpropvalues(
                all_observations=all_observations,
                index_station_id_mediasfiles=index_station_id_mediasfiles
            )
            # end update mediafile fk
            # start all rows in session checked
            self.update_all_cameratrap_session_to_checked(cameratrap_session=cameratrap_session)
            # end all rows in sessions checked

        except Exception as e:
            self.session.rollback()
            self.rollback_files(
                medias_files_to_remove_if_rollback=medias_files_to_remove_if_rollback,
                cameraTrap_path=cameraTrap_path,
                path_working_directory=path_working_directory
            )
            print(e)
            return HTTPConflict('Internal error: Session could not be validated, Contact an admin')

        # handle files
        # we gonna resize validated photos by note and remove refused
        # This step "could" fail but we can't rollback file's modifications in a simple way
        # Need more work for that and refact of import and folder logic
        # clues:
        #       handle imported file in another nested folder "To_Validate"
        #       move files IN CURRENT SESSION to "validated"
        #       clean To_Validate WITH DATAS IN CURRENT SESSION!!!
        try:
            self.resize_photos(photos=all_photos)
            self.remove_files(cameratrap_session=cameratrap_session)
        except Exception as e:
            pass

        return {
            'nbValidated': len(all_cameratrap_photos[True]),
            'nbRefused': len(all_cameratrap_photos[False]),
            'nbStationsCreated': len(all_stations_id)
        }

    def update_fk_for_medias_files_with_observationdynpropvalues(
        self,
        all_observations=[],
        index_station_id_mediasfiles={}
    ):
        all_medias_files_to_update = []
        for observation in all_observations:
            cur_station_id = observation.Station.ID
            cur_medias_files = index_station_id_mediasfiles.get(cur_station_id)
            cur_all_observationDynPropValues = observation._dynamicValues
            expected_ValueString = f'{cur_station_id}\\{cur_medias_files.GUID}'
            if cur_medias_files.Extension:
                expected_ValueString = f'{expected_ValueString}.{cur_medias_files.Extension}'
            for observationdynprop in  cur_all_observationDynPropValues:
                curValueString = observationdynprop.ValueString
                if curValueString == expected_ValueString:
                    cur_medias_files.FK_DynPropValue = observationdynprop.ID
                    all_medias_files_to_update.append(cur_medias_files)

        self.session.add_all(all_medias_files_to_update)
        self.session.flush()

    def remove_files(self, cameratrap_session):
        for cameratrap_file in cameratrap_session:
            if cameratrap_file.validated == 4:
                cur_cametrap_file_path = f'{cameratrap_file.path}{cameratrap_file.name}'
                cur_cametrap_file_path_thumbnail = f'{cameratrap_file.path}thumbnails\\{cameratrap_file.name}'
                try:
                    pathlib.Path(cur_cametrap_file_path).unlink()
                    pathlib.Path(cur_cametrap_file_path_thumbnail).unlink()
                except FileNotFoundError as e:
                    pass
                except Exception as e:
                    raise e

    def resize_photos(
        self,
        photos=[]
    ):
        NOTE_PERCENT = {
            None: 0.25,
            0: 0.25,
            1: 0.25,
            2: 0.25,
            3: 0.75,
            4: 0.75,
            5: 1
        }
        try:
            for photo in photos:
                absolute_path = f'{photo.Path}{photo.FileName}'
                curNote = photo.Note
                size_in_percent = NOTE_PERCENT.get(curNote)

                img = Image.open(absolute_path)
                exif_datas = img.info['exif']
                new_width = int(float(img.size[0]) * size_in_percent)
                new_hsize = int(float(img.size[1]) * size_in_percent)
                img = img.resize((new_width,new_hsize), Image.ANTIALIAS)
                img.save(absolute_path, exif=exif_datas)
        except Exception as e:
            print(e)
            raise e

class SensorDatas(CustomResource):

    item = SensorDatasByType
    children = [('argos', SensorDatasByType),
                ('gsm', SensorDatasByType),
                ('rfid', SensorDatasByType),
                ('camtrap', SensorDatasByType)
    ]


RootCore.children.append(('sensorDatas', SensorDatas))
