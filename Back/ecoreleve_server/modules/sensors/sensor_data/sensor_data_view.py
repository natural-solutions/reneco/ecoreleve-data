from pyramid.view import (
    view_config,
    view_defaults
)
from ecoreleve_server.core.base_view import CRUDCommonView
from .sensor_data_resource import (
    SensorDatasByType,
    SensorDatasBySession,
    SensorDatasBySessionItem
)

route_prefix = 'sensors/'


@view_defaults(context=SensorDatasBySessionItem)
class SensorDatasBySessionItemView(CRUDCommonView):

    @view_config(renderer='json', request_method='PATCH', permission='update')
    def getDatasPatch(self):
        return self.context.patch()


@view_defaults(context=SensorDatasBySession)
class SensorDatasBySessionView(CRUDCommonView):

    @view_config(name='updateMany', renderer='json', permission='update')
    def updateMany(self):
        return self.context.updateMany()


@view_defaults(context=SensorDatasByType)
class SensorDatasByTypeView(CRUDCommonView):

    @view_config(name='getChunck', renderer='json', permission='create')
    def checkChunk(self):
        return self.context.checkChunk()

    @view_config(name='validate', renderer='json', permission='create')
    def auto_validation(self):
        return self.context.auto_validation()

    @view_config(name='concat', renderer='json', permission='create')
    def concatChunk(self):
        return self.context.concatChunk()

    @view_config(
        name='resumable',
        renderer='json',
        request_method='POST',
        permission='create'
    )
    def uploadFileCamTrapResumable(self):
        return self.context.uploadFileCamTrapResumable()

    @view_config(
        name='resumable',
        renderer='json',
        request_method='GET',
        permission='read'
    )
    def check_chunk_file(self):
        return self.context.check_chunk_file()