# from ecoreleve_server.core.base_resource import *
from ecoreleve_server.core import Main_Db_Base
from ecoreleve_server.core.base_resource import (
    DynamicValueResource,
    DynamicValuesResource
)
from collections import OrderedDict
from ecoreleve_server.modules.permissions import context_permissions
from ..sensor_model import Sensor
from sqlalchemy import (
    select,
    join,
    desc
)
from sqlalchemy.orm import (
    Query
)

SensorDynPropValue = Sensor.DynamicValuesClass


class SensorValueResource(DynamicValueResource):
    model = SensorDynPropValue

    __acl__ = context_permissions['sensors_history']

    def retrieve(self):
        pass


class SensorValuesResource(DynamicValuesResource):
    model = SensorDynPropValue
    children = [('{int}', SensorValueResource)]

    def retrieve(self):
        from ecoreleve_server.utils.parseValue import formatThesaurus

        propertiesTable = Main_Db_Base.metadata.tables[self.__parent__.objectDB.TypeClass.PropertiesClass.__tablename__]
        dynamicValuesTable = Main_Db_Base.metadata.tables[self.__parent__.objectDB.DynamicValuesClass.__tablename__]
        labelTable = Main_Db_Base.metadata.tables["ModuleForms"]
        frontModulesTable = Main_Db_Base.metadata.tables["FrontModules"]

        query = Query(
            entities=[
                dynamicValuesTable,
                propertiesTable.c['Name'].label('CodeName'),
                labelTable.c['Label'].label('Name')
            ],
            session=self.request.dbsession
        )

        query = query.join(
            Sensor,
            Sensor.ID == dynamicValuesTable.c.FK_Sensor
        )

        query = query.join(
            propertiesTable,
            propertiesTable.c.ID == dynamicValuesTable.c.FK_SensorDynProp
        )

        query = query.join(
            labelTable,
            (
            (labelTable.c.Name == propertiesTable.c.Name)
                &
                (
                    (labelTable.c.TypeObj == Sensor._type_id)
                    |
                    (labelTable.c.TypeObj.__eq__(None))
                )
            )
        )

        query = query.join(
            frontModulesTable,
            frontModulesTable.c.ID == labelTable.c.Module_ID
        )

        query = query.filter(
            frontModulesTable.c.Name == "SensorForm",
            Sensor.ID == self.__parent__.objectDB.ID
        )

        query = query.order_by(
            desc(dynamicValuesTable.c['StartDate'])
        )

        result = query.all()
        response = []

        for row in result:
            curRow = OrderedDict(row._asdict())
            dictRow = {}
            for key in curRow:
                if curRow[key] is not None:
                    if key == 'ValueString' in key and curRow[key] is not None:
                        try:
                            thesauralValueObj = formatThesaurus(
                                data=curRow[key],
                                request=self.request
                            )
                            dictRow['value'] = thesauralValueObj['displayValue']
                        except:
                            dictRow['value'] = curRow[key]
                    elif key in ['ValueDate','ValueFloat','ValueInt']:
                        dictRow['value'] = curRow[key]
                    elif 'FK' not in key:
                        dictRow[key] = curRow[key]
                if key == 'Name' and (curRow[key] is None or curRow[key] == ''):
                    dictRow['Name'] = curRow['CodeName']
            dictRow['StartDate'] = curRow[
                'StartDate'].strftime('%Y-%m-%d %H:%M:%S')
            response.append(dictRow)

        return response
