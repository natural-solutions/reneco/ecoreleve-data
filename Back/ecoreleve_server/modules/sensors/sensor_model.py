
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Numeric,
    String,
    Sequence,
    orm,
    func)
from sqlalchemy.orm import relationship

from ecoreleve_server.core import Main_Db_Base, HasDynamicProperties, GenericType


class Sensor (HasDynamicProperties, Main_Db_Base):

    __tablename__ = 'Sensor'

    moduleFormName = 'SensorForm'
    moduleGridName = 'SensorFilter'

    ID = Column(Integer, Sequence('Sensor__id_seq'), primary_key=True)
    UnicIdentifier = Column(String(250))
    Model = Column(String(250), nullable=False)
    Company = Column(String(250), nullable=False)
    SerialNumber = Column(String(250))
    creationDate = Column(DateTime, nullable=False, default=func.now())
    OldID = Column(Integer, nullable=True)
    Original_ID = Column(String(50), nullable=True)
    # FK_SensorType = Column(Integer, ForeignKey('SensorType.ID'))

    Equipments = relationship('Equipment')
    Locations = relationship('Individual_Location')

    __mapper_args__ = {
        'version_id_col': ID,
        'version_id_generator': False,
    }