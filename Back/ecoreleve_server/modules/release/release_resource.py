import json
from datetime import datetime
from sqlalchemy import (
    text,
    and_
)
import operator
from traceback import print_exc
from collections import Counter


from ecoreleve_server.core.base_resource import CustomResource
from ecoreleve_server.utils.parseValue import (
    isNumeric,
    formatThesaurus
)
from ecoreleve_server.core import RootCore
from ..permissions import context_permissions
from ..observations import Observation
from ..individuals import Individual
from ..stations import Station
from ..observations.equipment_model import checkEquip, set_equipment
from ..individuals.individual_resource import IndividualsResource

from ecoreleve_server.core.configuration_model.frontmodules import (
    ModuleGrids,
    FrontModules,
    ModuleForms
)

ProtocoleType = Observation.TypeClass


class ReleaseIndividualsResource(IndividualsResource):

    moduleGridName = 'IndivReleaseGrid'  # will be used for fetching conf
    __acl__ = context_permissions['release']
    listProtosNameToInstanciate = [
                'Vertebrate_Group',
                'Vertebrate_Individual',
                'Bird_Biometry',
                'Release_Group',
                'Release_Individual',
                'Individual_Equipment'
                ]
    objProtoToInstanciate = {}

    def getFilter(self, type_=None, moduleName=None):
        return []

    def handleCriteria(self, params):
        criteria = [{
            'Column': 'LastImported',
            'Operator': '=',
            'Value': True
        }]
        params['criteria'].extend(criteria)
        return params

    def handleResult(self, data):
        for row in data[1]:
            if 'Date_Sortie' in row and row['Date_Sortie'] is not None:
                row['Date_Sortie'] = row['Date_Sortie'].strftime(
                    '%Y-%m-%d %H:%M:%S')
        return data

    def retrieve(self):
        return self.search(paging=False)

    def createObjProtoToInstanciate(self, listProtosName):
        self.objProtoToInstanciate = {}  # reset the object
        for item in listProtosName:
            self.objProtoToInstanciate[item] = {
                'id': None,
                'propsList': []
            }

    def getConfForProtocole(self):

        self.createObjProtoToInstanciate(self.listProtosNameToInstanciate)

        query = self.session.query(
            ProtocoleType.ID.label("idProto"),
            ProtocoleType.Name.label("nameProto"),
            ModuleForms.Name.label("nameDynProp")
        )
        query = query.join(
            ProtocoleType,
            ProtocoleType.ID == ModuleForms.TypeObj
        )
        query = query.join(
            FrontModules,
            FrontModules.ID == ModuleForms.Module_ID
        )
        query = query.filter(
            ProtocoleType.Name.in_(self.listProtosNameToInstanciate)
        )
        query = query.order_by(
            ProtocoleType.ID,
            ModuleForms.Name
        )

        rows = query.all()

        for item in rows:
            cur_proto = self.objProtoToInstanciate[item.nameProto]

            if cur_proto['id'] is None and item.idProto is not None:
                cur_proto['id'] = item.idProto

            if item.nameDynProp not in cur_proto['propsList']:
                cur_proto['propsList'].append(item.nameDynProp)

    def getConfForValidate(self):

        query = self.session.query(ModuleGrids)
        query = query.join(FrontModules)
        query = query.filter(
            and_(
                ModuleGrids.Module_ID == FrontModules.ID,
                FrontModules.Name == self.moduleGridName
                )
        )
        self.confInDB = query.all()

        if len(self.confInDB) <= 0:
            print("someting goes wrong")

        self.keysUpdatable = []
        self.defaultKeys = ['ID']

        self.keysUpdatable.extend(self.defaultKeys)

        for item in self.confInDB:
            if (item.GridRender > 2):
                propsName = item.Name
                if propsName.lower() == 'Unicidentifier'.lower():
                    propsName = 'FK_Sensor'
                self.keysUpdatable.append(propsName)

        if (len(self.keysUpdatable) <= 0):
            print("nothing to update")

    def updateAllStartDate(self, indiv, date, properties):
        for prop in properties:
            existingDynPropValues = list(filter(
                lambda p: p.fk_property == prop['ID'],
                indiv._dynamicValues
                )
            )
            if existingDynPropValues:
                curValueProperty = max(
                    existingDynPropValues,
                    key=operator.attrgetter('StartDate')
                )
                curValueProperty.StartDate = date

    def create(self):
        session = self.session
        request = self.request
        data = request.params.mixed()

        if 'StationID' not in data and 'IndividualList' not in data:
            if data == {}:
                data = request.json_body
            if 'FK_Sensor' in data and data['FK_Sensor'] not in (None, ''):
                # TODO pls remove POST Logic from front for checking
                # if sensor is avaiable .... this should be an endpoint or
                # a GET on sensors/ID ressource with search options
                return isavailableSensor(request, data)
            return

        self.getConfForValidate()

        sta_id = int(data['StationID'])
        indivListFromData = json.loads(data['IndividualList'])
        releaseMethod = data['releaseMethod']
        curStation = session.query(Station).get(sta_id)
        taxon = False

        indivList = []
        for row in indivListFromData:
            rowTmp = {}

            for key, value in row.items():
                if key in self.keysUpdatable:
                    rowTmp[key] = value

            indivList.append(rowTmp)

        def getnewObs(typeID, station):
            newObs = Observation()
            newObs.type_id = typeID
            newObs.FK_Station = station.ID
            newObs.Station = station
            return newObs

        self.getConfForProtocole()

        vertebrateIndList = []
        biometryList = []
        releaseIndList = []
        equipmentIndList = []

        binaryDict = {
            9: 'nb_adult_indeterminate',
            10: 'nb_adult_male',
            12: 'nb_adult_female',
            17: 'nb_juvenile_indeterminate',
            18: 'nb_juvenile_male',
            20: 'nb_juvenile_female',
            33: 'nb_indeterminate',
            36: 'nb_indeterminate',
            34: 'nb_indeterminate'
        }

        """ Return sex, age repartition of released individuals

            binary ponderation female : 4, male :2 , indeterminateSex : 1,
            adult:8, juvenile : 16, indeterminateAge : 32
        """
        def MoF_AoJ(obj):
            curSex = None
            binP = 0
            # find key case
            sexKey = None
            ageKey = None
            ##
            # need to fix this , all config must be the same ....
            ##
            if 'sex' in obj and 'Sex' in obj:
                raise Exception(
                    'INDERTERMINATE KEYs sex and Sex check your config'
                )
            if 'age' in obj and 'Age' in obj:
                raise Exception(
                    'INDERTERMINATE KEYs age and Age check your config'
                )

            if 'sex' in obj:
                sexKey = 'sex'
            if 'Sex' in obj:
                sexKey = 'Sex'

            if 'age' in obj:
                ageKey = 'age'
            if 'Age' in obj:
                ageKey = 'Age'

            if sexKey is None:
                curSex == 'Indeterminate'
                binP += 1
            else:
                if (
                    obj[sexKey] is None
                    or
                    obj[sexKey] not in ['male', 'mâle', 'female', 'femelle']
                ):
                    curSex == 'Indeterminate'
                    binP += 1
                elif (
                    obj[sexKey].lower() == 'male'
                    or
                    obj[sexKey].lower() == 'mâle'
                ):
                    curSex = 'male'
                    binP += 2
                elif (
                    obj[sexKey].lower() == 'female'
                    or
                    obj[sexKey].lower() == 'femelle'
                ):
                    curSex = 'female'
                    binP += 4

            if ageKey is None:
                curSex == 'Indeterminate'
                binP += 32
            else:
                if (
                    obj[ageKey] is None
                    or
                    obj[ageKey] not in [
                        'adult',
                        'adulte',
                        'junévile',
                        'juvenile'
                    ]
                ):
                    curSex == 'Indeterminate'
                    binP += 32
                elif (
                    obj[ageKey].lower() == 'adult'
                    or
                    obj[ageKey].lower() == 'adulte'
                ):
                    curSex = 'Adult'
                    binP += 8
                elif (
                    obj[ageKey].lower() == 'juvenile'
                    or
                    obj[ageKey].lower() == 'juvénile'
                ):
                    curSex = 'Juvenile'
                    binP += 16

            return binaryDict[binP]

        try:
            errorEquipment = None
            binList = []
            for indiv in indivList:
                tmpIndiv = indiv
                curIndiv = session.query(Individual).get(indiv['ID'])
                if not taxon:
                    taxon = curIndiv.Species

                tmpIndiv['taxon'] = taxon
                if 'species' in tmpIndiv:
                    del tmpIndiv['species']

                tmpIndiv['__useDate__'] = curStation.StationDate
                tmpIndiv['type_id'] = curIndiv.values['type_id']
                curIndiv.values = tmpIndiv
                self.updateAllStartDate(
                    curIndiv, curStation.StationDate, curIndiv.properties
                )

                curIndiv.enable_business_ruler = False
                binList.append(MoF_AoJ(curIndiv.values))

                curIndiv.values['FK_Individual'] = curIndiv.ID
                curIndiv.values['FK_Station'] = sta_id
                if 'poids' in curIndiv.values:
                    curIndiv.values['weight'] = curIndiv.values['poids']
                if 'Poids' in curIndiv.values:
                    curIndiv.values['weight'] = curIndiv.values['Poids']
                if 'Release_Comments' in curIndiv.values:
                    curIndiv.values['Comments'] = curIndiv.values['Release_Comments']

                listPropsCurIndiv = [x for x in list(curIndiv.values)]
                curIndivDict = curIndiv.values

                curProtoID = self.objProtoToInstanciate['Vertebrate_Individual']['id']
                listPropsCurProto = self.objProtoToInstanciate['Vertebrate_Individual']['propsList']

                curVertebrateInd = getnewObs(curProtoID, curStation)
                curValues = {}
                for propsProto in listPropsCurProto:
                    for propsIndiv in listPropsCurIndiv:
                        if propsProto.lower() == propsIndiv.lower():
                            curValues[propsProto] = curIndivDict[propsIndiv]

                curVertebrateInd.values = curValues
                curVertebrateInd.Comments = None
                vertebrateIndList.append(curVertebrateInd)

                curProtoID = self.objProtoToInstanciate['Bird_Biometry']['id']
                listPropsCurProto = self.objProtoToInstanciate['Bird_Biometry']['propsList']
                curBiometry = getnewObs(curProtoID, curStation)

                curValues = {}
                for propsProto in listPropsCurProto:
                    for propsIndiv in listPropsCurIndiv:
                        if propsIndiv == 'Poids':
                            curValues['Weight'] = curIndivDict.get('Poids')
                        if propsProto.lower() == propsIndiv.lower():
                            curValues[propsProto] = curIndivDict[propsIndiv]

                curBiometry.values = curValues
                curBiometry.Comments = None
                biometryList.append(curBiometry)

                curProtoID = self.objProtoToInstanciate['Release_Individual']['id']
                listPropsCurProto = self.objProtoToInstanciate['Release_Individual']['propsList']
                curReleaseInd = getnewObs(curProtoID, curStation)

                curValues = {}
                for propsProto in listPropsCurProto:
                    for propsIndiv in listPropsCurIndiv:
                        if propsIndiv == 'Breeding ring kept after release':
                            curValues['Breeding_Ring_Kept_After_Release'] = curIndivDict.get('Breeding ring kept after release')
                        if propsProto.lower() == propsIndiv.lower():
                            curValues[propsProto] = curIndivDict[propsIndiv]

                curReleaseInd.values = curValues
                releaseIndList.append(curReleaseInd)

                sensor_id = curIndiv.values.get(
                    'FK_Sensor') or curIndiv.values.get('fk_sensor')

                if sensor_id:
                    try:
                        curProtoID = self.objProtoToInstanciate['Individual_Equipment']['id']
                        listPropsCurProto = self.objProtoToInstanciate['Individual_Equipment']['propsList']
                        curEquipmentInd = getnewObs(curProtoID, curStation)

                        # TODO we should fetch it as default conf or create 'special rules' for release
                        #      if we have to fill input with spécific values for automatique instanciate protocole with release
                        curValues = {
                            'Survey_Type': 'Post-relâcher',
                            'Monitoring_Status': 'Suivi',
                            'Sensor_Status': 'Sortie de stock>Mise en service'
                        }
                        for propsProto in listPropsCurProto:
                            for propsIndiv in listPropsCurIndiv:
                                if propsProto.lower() == propsIndiv.lower():
                                    curValues[propsProto] = curIndivDict[propsIndiv]

                        curEquipmentInd.values = curValues
                        # set_equipment(curEquipmentInd, curStation)
                        equipmentIndList.append(curEquipmentInd)
                        # equipInfo = {
                        #     'FK_Individual': curIndiv.values['FK_Individual'],
                        #     'FK_Sensor': sensor_id,
                        #     'Survey_type': 'post-relâcher',
                        #     'Monitoring_Status': 'suivi',
                        #     'Sensor_Status': 'sortie de stock>mise en service',
                        #     'FK_Station': curStation.ID,
                        #     '__useDate__': curStation.StationDate
                        # }
                        # curEquipmentInd.values = equipInfo
                        # set_equipment(curEquipmentInd, curStation)
                        # equipmentIndList.append(curEquipmentInd)
                    except Exception as e:
                        if e.__class__.__name__ == 'ErrorAvailable':
                            sensor_available = 'is' if e.value[
                                'sensor_available'] else 'is not'
                            tpl = 'SensorID {0} {1} available for equipment'.format(
                                equipInfo['FK_Sensor'], sensor_available)
                            if errorEquipment is None:
                                errorEquipment = tpl
                            else:
                                errorEquipment += ',   ' + tpl
                        else:
                            print_exc()
                            errorEquipment = e.__class__.__name__

            dictVertGrp = dict(Counter(binList))
            dictVertGrp['Nb_Total'] = len(releaseIndList)
            dictVertGrp['Taxon'] = taxon

            obsVertGrpFilteredId = list(filter(
                lambda o: o.type_id == self.objProtoToInstanciate['Vertebrate_Group']['id'], curStation.Observations))
            obsReleaseGrpFilteredId = list(
                filter(lambda o: o.type_id == self.objProtoToInstanciate['Release_Group']['id'], curStation.Observations))

            if len(obsVertGrpFilteredId) > 0:
                for obsId in obsVertGrpFilteredId:
                    curObs = session.query(Observation).get(obsId.ID)
                    if curObs.values.get('Taxon') == taxon:
                        vertebrateGrp = curObs
                        break
                    else:
                        vertebrateGrp = None
            else:
                vertebrateGrp = None

            if len(obsReleaseGrpFilteredId) > 0:
                for obsId in obsReleaseGrpFilteredId:
                    curObs = session.query(Observation).get(obsId.ID)
                    if curObs.values.get('Taxon') == taxon and curObs.values.get('Release_Method') == releaseMethod:
                        releaseGrp = curObs
                        break
                    else:
                        releaseGrp = None
            else:
                releaseGrp = None

            if vertebrateGrp:
                for prop, val in dictVertGrp.items():
                    if isNumeric(val):
                        vertebrateGrp.setValue(
                            prop, int(val) + int(vertebrateGrp.values.get(prop, 0)))
            else:
                vertebrateGrp = Observation()
                vertebrateGrp.session = session
                dictVertGrp.update({
                    'FK_Station': sta_id,
                    'type_id': self.objProtoToInstanciate['Vertebrate_Group']['id']
                })
                vertebrateGrp.values = dictVertGrp

            if releaseGrp:
                releaseGrp.setValue('Nb_Individuals', int(
                    curObs.values.get('Nb_Individuals')) + len(releaseIndList))
            else:
                releaseGrp = Observation()
                releaseGrp.session = session
                releaseGrp.values = {
                    'type_id': self.objProtoToInstanciate['Release_Group']['id'],
                    'FK_Station': sta_id,
                    'Taxon': taxon,
                    'Release_Method': releaseMethod,
                    'Nb_Individuals': len(releaseIndList)
                }

            releaseGrp.Observation_children.extend(releaseIndList)
            vertebrateGrp.Observation_children.extend(vertebrateIndList)

            if errorEquipment is not None:
                session.rollback()
                request.response.status_code = 510
                message = errorEquipment

            else:
                session.add(vertebrateGrp)
                session.add(releaseGrp)
                session.add_all(biometryList)
                session.add_all(equipmentIndList)
                message = {'release': len(releaseIndList)}

        except Exception as e:
            print_exc()
            self.request.response.status_code = 500
            session.rollback()
            message = str(type(e))
        finally:
            Individual.enable_business_ruler = True

        return message


class ReleaseResource(CustomResource):

    model = None
    __acl__ = context_permissions['release']
    children = [('individuals', ReleaseIndividualsResource)]

    def getReleaseMethod(self):
        userLng = self.request.authenticated_userid['userlanguage'].lower()
        if not userLng:
            userLng = 'fr'
        query = text("""SELECT TTop_FullPath as val, TTop_Name as label"""
                     + """ FROM syn_TTopic th
            JOIN [ModuleForms] f on th.TTop_ParentID = f.Options
            where Name = 'release_method' """)
        result = self.session.execute(query).fetchall()
        result = [dict(row) for row in result]
        if userLng != 'fr':
            for row in result:
                row['label'] = formatThesaurus(
                    data=row['val'],
                    request=self.request
                )['displayValue']
        return result


RootCore.children.append(('release', ReleaseResource))


def isavailableSensor(request, data):
    availability = checkEquip(
        fk_sensor=data['FK_Sensor'],
        equipDate=datetime.strptime(
            data['sta_date'],
            '%d/%m/%Y %H:%M:%S'
        ),
        session=request.dbsession
    )
    if availability is True:
        return
    else:
        request.response.status_code = 510
        return 'sensor not available'
