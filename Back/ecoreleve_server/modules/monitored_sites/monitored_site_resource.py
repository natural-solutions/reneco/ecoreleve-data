import json
from sqlalchemy import select, desc, join
from sqlalchemy.exc import IntegrityError
from sqlalchemy import select, desc, join, and_, Table, union
from collections import OrderedDict

from ecoreleve_server.core import RootCore, Main_Db_Base
from ecoreleve_server.core.base_resource import DynamicObjectResource, DynamicObjectCollectionResource
from ecoreleve_server.core.base_collection import Query_engine
from . import MonitoredSite
from .monitored_site_collection import MonitoredSiteCollection
from ..sensors import Sensor
from ..stations import Station
from ..field_activities import fieldActivity
from ..permissions import context_permissions
from .monitored_sites_location_history import MonitoredSiteLocationHistoryResource


SensorType = Sensor.TypeClass


@Query_engine(Main_Db_Base.metadata.tables['MonitoredSitePosition'])
class PositionCollection:
    pass


class MonitoredSiteResource(DynamicObjectResource):

    model = MonitoredSite
    children=[('locationHistory', MonitoredSiteLocationHistoryResource)]

    def update(self):
        try:
            response = DynamicObjectResource.update(self)
        except IntegrityError as e:
            self.session.rollback()
            response = self.request.response
            response.status_code = 510
            response.text = "IntegrityError"
        return response

    def getDynPropertiesHistory(self):
        all_tables_in_db = Main_Db_Base.metadata.tables
        TMonitoredSiteDynProp = all_tables_in_db['MonitoredSiteDynProp']
        TMonitoredSiteDynPropValue = all_tables_in_db['MonitoredSiteDynPropValue']
        TIndividualDynProp = all_tables_in_db['IndividualDynProp']
        TIndividualDynPropValue = all_tables_in_db['IndividualDynPropValue']

        # tables
        joinTable = join(
            MonitoredSite, 
            TMonitoredSiteDynPropValue, 
            MonitoredSite.ID == TMonitoredSiteDynPropValue.c['FK_MonitoredSite']
        )
        joinTable = join(
            joinTable, 
            TMonitoredSiteDynProp,
            TMonitoredSiteDynProp.c['ID'] == TMonitoredSiteDynPropValue.c['FK_MonitoredSiteDynProp']
        )

        joinTableIndividual1 = join(
            joinTable, 
            TIndividualDynProp, 
            (TIndividualDynProp.c['Name'] + '_FK_Individual_1') == (TMonitoredSiteDynProp.c['Name']), 
            isouter=True
        ) 

        joinTableIndividual1 = join(
            joinTableIndividual1, 
            TIndividualDynPropValue, 
            (TIndividualDynProp.c['ID'] == TIndividualDynPropValue.c['FK_IndividualDynProp']) & (TIndividualDynPropValue.c['FK_Individual'] == MonitoredSite.FK_Individual_1), 
            isouter=True
        )

        joinTableIndividual2 = join(
            joinTable, 
            TIndividualDynProp, 
            (TIndividualDynProp.c['Name'] + '_FK_Individual_2') == (TMonitoredSiteDynProp.c['Name']), 
            isouter=True
        ) 

        joinTableIndividual2 = join(
            joinTableIndividual2, 
            TIndividualDynPropValue, 
            (TIndividualDynProp.c['ID'] == TIndividualDynPropValue.c['FK_IndividualDynProp']) & (TIndividualDynPropValue.c['FK_Individual'] == MonitoredSite.FK_Individual_2), 
            isouter=True
        )

        # Dynamic properties of identified individuals
        query_identified_individual_1 = select([
            TMonitoredSiteDynProp.c.Name,
            TIndividualDynPropValue.c.StartDate,
            TIndividualDynPropValue.c.ValueInt,
            TIndividualDynPropValue.c.ValueString,
            TIndividualDynPropValue.c.ValueDate,
            TIndividualDynPropValue.c.ValueFloat
        ]).select_from(joinTableIndividual1
        ).where(
            and_(
                MonitoredSite.ID == self.objectDB.ID,
                TMonitoredSiteDynProp.c['Name'].like('%FK_Individual_1'),
                MonitoredSite.FK_Individual_1 != None
            ) 
        )

        query_identified_individual_2 = select([
            TMonitoredSiteDynProp.c.Name,
            TIndividualDynPropValue.c.StartDate,
            TIndividualDynPropValue.c.ValueInt,
            TIndividualDynPropValue.c.ValueString,
            TIndividualDynPropValue.c.ValueDate,
            TIndividualDynPropValue.c.ValueFloat
        ]).select_from(joinTableIndividual2
        ).where(
            and_(
                MonitoredSite.ID == self.objectDB.ID,
                TMonitoredSiteDynProp.c['Name'].like('%FK_Individual_2'),
                MonitoredSite.FK_Individual_2 != None
            ) 
        )
        
        # Dynamic properties of the non identified individuals
        query_unidentified_individual_1 = select([
            TMonitoredSiteDynProp.c.Name,
            TMonitoredSiteDynPropValue.c.StartDate,
            TMonitoredSiteDynPropValue.c.ValueInt,
            TMonitoredSiteDynPropValue.c.ValueString,
            TMonitoredSiteDynPropValue.c.ValueDate,
            TMonitoredSiteDynPropValue.c.ValueFloat
        ]).select_from(joinTable
        ).where(
            and_(
                MonitoredSite.ID == self.objectDB.ID,
                MonitoredSite.FK_Individual_1 == None,
                TMonitoredSiteDynProp.c.Name.like('%FK_Individual_1')
        ))
        
        query_unidentified_individual_2 = select([
            TMonitoredSiteDynProp.c.Name,
            TMonitoredSiteDynPropValue.c.StartDate,
            TMonitoredSiteDynPropValue.c.ValueInt,
            TMonitoredSiteDynPropValue.c.ValueString,
            TMonitoredSiteDynPropValue.c.ValueDate,
            TMonitoredSiteDynPropValue.c.ValueFloat
        ]).select_from(joinTable
        ).where(
            and_(
                MonitoredSite.ID == self.objectDB.ID,
                MonitoredSite.FK_Individual_2 == None,
                TMonitoredSiteDynProp.c.Name.like('%FK_Individual_2')
        ))

        # Dynamic properties of the Monitored Sites
        query_monitored_sites_properties =  select([
            TMonitoredSiteDynProp.c.Name,
            TMonitoredSiteDynPropValue.c.StartDate,
            TMonitoredSiteDynPropValue.c.ValueInt,
            TMonitoredSiteDynPropValue.c.ValueString,
            TMonitoredSiteDynPropValue.c.ValueDate,
            TMonitoredSiteDynPropValue.c.ValueFloat
        ]).select_from(joinTable
        ).where(
            and_(
                MonitoredSite.ID == self.objectDB.ID,
                TMonitoredSiteDynProp.c.Name.in_(['Taxon', 'Experimentation']))
        )

        # query
        tmp = [query_identified_individual_1, query_identified_individual_2, query_monitored_sites_properties, query_unidentified_individual_1, query_unidentified_individual_2]
        query = union(*tmp)

        result = self.session.execute(query).fetchall()

        response = []
        for row in result:
            curRow = OrderedDict(row)
            tmp = curRow['Name'].split('_FK_Individual_')
            curRow['Name'] = tmp[0]
            curRow['Parent'] = ''
            if len(tmp) > 1:
                curRow['Parent'] = 'Parent ' + tmp[1]
            if(curRow['StartDate'] is not None):
                curRow['StartDate'] = curRow['StartDate'].strftime('%Y-%m-%d %H:%M:%S')
            curRow['Value'] = None
            if curRow['ValueInt'] is not None:
                curRow['Value'] = curRow['ValueInt']
            if curRow['ValueString'] is not None:
                curRow['Value'] = curRow['ValueString']
            if curRow['ValueDate'] is not None:
                curRow['Value'] = curRow['ValueDate']
            if curRow['ValueFloat'] is not None:
                curRow['Value'] = curRow['ValueFloat']
            curRow.pop('ValueInt')
            curRow.pop('ValueString')
            curRow.pop('ValueDate')
            curRow.pop('ValueFloat')
            response.append(curRow)
        return response

    def getStations(self):
        id_site = self.objectDB.ID
        joinTable = join(Station, fieldActivity,
                         Station.fieldActivityId == fieldActivity.ID)
        query = select([Station.StationDate,
                        Station.LAT,
                        Station.LON,
                        Station.ID,
                        Station.Name,
                        fieldActivity.Name.label('fieldActivity_Name')]
                       ).select_from(joinTable
                                     ).where(Station.FK_MonitoredSite == id_site)

        result = self.session.execute(query).fetchall()
        response = []
        for row in result:
            row = dict(row)
            row['StationDate'] = row['StationDate'].strftime('%Y-%m-%d %H:%M:%S')
            response.append(row)
        return response

    def getEquipment(self):
        id_site = self.objectDB.ID
        table = Main_Db_Base.metadata.tables['MonitoredSiteEquipment']

        joinTable = join(table, Sensor, table.c['FK_Sensor'] == Sensor.ID)
        joinTable = join(joinTable, SensorType,
                         Sensor._type_id == SensorType.ID)
        query = select([table.c['StartDate'],
                        table.c['EndDate'],
                        Sensor.UnicIdentifier,
                        Sensor.ID.label('SensorID'),
                        table.c['FK_MonitoredSite'],
                        SensorType.Name.label('Type')]
                       ).select_from(joinTable
                                     ).where(table.c['FK_MonitoredSite'] == id_site
                                             ).order_by(desc(table.c['StartDate']))

        result = self.session.execute(query).fetchall()
        response = []
        for row in result:
            curRow = OrderedDict(row)
            curRow['StartDate'] = curRow['StartDate'].strftime('%Y-%m-%d %H:%M:%S')
            if curRow['EndDate'] is not None:
                curRow['EndDate'] = curRow['EndDate'].strftime('%Y-%m-%d %H:%M:%S')
            else:
                curRow['EndDate'] = ''
            response.append(curRow)

        return response


class MonitoredSitesResource(DynamicObjectCollectionResource):

    Collection = MonitoredSiteCollection
    model = MonitoredSite
    moduleFormName = 'MonitoredSiteForm'
    moduleGridName = 'MonitoredSiteGrid'

    children = [('{int}', MonitoredSiteResource)]

    __acl__ = context_permissions['monitoredSites']

    def __init__(self, ref, parent):
        DynamicObjectCollectionResource.__init__(self, ref, parent)
        # self.__acl__ = context_permissions[ref]

    def insert(self):
        try:
            response = DynamicObjectCollectionResource.insert(self)
        except IntegrityError as e:
            self.session.rollback()
            self.request.response.status_code = 409
            self.request.response.text = "This name is already used for another Monitored site. Please change it."
            return self.request.response
        return response


RootCore.children.append(('monitoredSites', MonitoredSitesResource))
