from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Sequence,
    Boolean,
    func,
    event,
    insert,
    update,
    delete,
    text,
    bindparam)
from datetime import timedelta
from sqlalchemy.orm import (
    relationship
)
from sqlalchemy import inspect

from ecoreleve_server.core import Main_Db_Base
from .observation_model import Observation


class Equipment(Main_Db_Base):
    __tablename__ = 'Equipment'

    ID = Column(Integer, Sequence('Equipment__id_seq'), primary_key=True)
    FK_Sensor = Column(Integer, ForeignKey('Sensor.ID'))
    FK_Individual = Column(Integer, ForeignKey('Individual.ID'))
    FK_MonitoredSite = Column(Integer, ForeignKey('MonitoredSite.ID'))
    FK_Observation = Column(Integer, ForeignKey('Observation.ID'))
    StartDate = Column(DateTime, default=func.now())
    Deploy = Column(Boolean)

    Individuals = relationship('Individual')
    MonitoredSites = relationship('MonitoredSite')

    __mapper_args__ = {
        'version_id_col': ID,
        'version_id_generator': False,
    }

def checkEquip(fk_sensor, equipDate, session):
    query = text('''DECLARE @result int;
                 EXEC dbo.[pr_checkSensorAvailability] :date, :sensorID, @result OUTPUT;
                 SELECT @result
                 ''').bindparams(bindparam('sensorID', fk_sensor),
                                 bindparam('date', equipDate))
    Nb = session.get_bind(Equipment).execute(query).scalar()
    if Nb > 0:
        return True
    else:
        return {'equipment_error': True}


def check_availability(
    FK_Sensor=None,
    FK_Individual=None,
    FK_MonitoredSite=None,
    FK_Observation=None,
    Deploy=None,
    StartDate=None,
    connection=None
):
    query = None
    if Deploy is True:
        query = text(
            '''DECLARE @result int;
            EXEC dbo.[pr_checkSensorAvailability] :date, :sensorID, @result OUTPUT;
            SELECT @result
            ''')
        query = query.bindparams(
            bindparam('sensorID', FK_Sensor),
            bindparam('date', StartDate)
        )
    if Deploy is False:
        query = text(
            '''DECLARE @result int;
            EXEC dbo.[pr_CheckSensorUnequipment] :date, :sensorID, :siteID, :indID, @result OUTPUT;
            SELECT @result
            ''')
        query = query.bindparams(
            bindparam('sensorID', FK_Sensor),
            bindparam('siteID', FK_MonitoredSite),
            bindparam('indID', FK_Individual),
            bindparam('date', StartDate)
        )
    if query is None:
        raise(Exception('system error query could not be None'))
    # result = connection.get_bind(Equipment).execute(query).scalar()
    result = connection.execute(query).scalar()
    if not(result > 0):
        if Deploy is True:
            raise ErrorAvailable({'equipment_error': True})
        if Deploy is False:
            if FK_Individual is not None:
                raise ErrorAvailable({'unequipment_error': 'Individual'})
            if FK_MonitoredSite is not None:
                raise ErrorAvailable({'unequipment_error': 'Monitored Site'})

    return True


@event.listens_for(Observation, 'after_update')
@event.listens_for(Observation, 'after_insert')
def set_equipment(mapper, connection, target):
    '''
    sry for "this" dunno a better way with the legacy code
    sqlalchemy have orm event like a trigger in db

    we want to trigger an action when we create
    or edit an observation for some "type" of observation

    if the "type" match  with the list
    we have to handle an Equipment object
    insert or update
    '''
    __OBSERVATIONDYNPROP_ID__ = 2938
    protocols_equipment = {
        'Individual_Equipment': {
            'deploy': True,
            'properties': [
                'FK_Sensor',
                'FK_Individual'
            ]
        },
        'Site_Equipment': {
            'deploy': True,
            'properties': [
                'FK_Sensor',
                'FK_MonitoredSite'
            ]
        },
        'Individual_Unequipment': {
            'deploy': False,
            'properties': [
                'FK_Sensor',
                'FK_Individual'
            ]
        },
        'Site_Unequipment': {
            'deploy': False,
            'properties': [
                'FK_Sensor',
                'FK_MonitoredSite'
            ]
        }
    }
    current_protocol = getattr(target, 'type', None)

    if current_protocol is None:
        raise(Exception('Something goes wrong no protocolType found'))

    need_to_handle_equipment = protocols_equipment.get(current_protocol, None)

    if need_to_handle_equipment is None:
        print(f'No equipment to handle for protocol {current_protocol}')
        return

    list_of_properties = need_to_handle_equipment.get('properties', None)
    if list_of_properties is None:
        raise(
            Exception(
                f'No properties defined for protocol {current_protocol}'
            )
        )

    datas_to_store = dict(
        FK_Sensor=None,
        FK_Observation=None,
        FK_Individual=None,
        FK_MonitoredSite=None,
        Deploy=None,
        StartDate=None
    )
    deploy = need_to_handle_equipment.get('deploy', None)
    if deploy is None:
        raise(
            Exception(
                f'No deploy value defined for protocol {current_protocol}'
            )
        )
    datas_to_store['Deploy'] = deploy

    station_target = getattr(target, 'Station', None)
    if station_target is None:
        raise Exception('Something goes wrong no station for this observation')

    equipDate = getattr(station_target, 'StationDate', None)
    if equipDate is None:
        raise Exception('Something goes wrong station have no StationDate')
    if deploy is False:
        equipDate = equipDate - timedelta(seconds=1)

    datas_to_store['StartDate'] = equipDate
    datas_to_store['FK_Observation'] = target.ID

    for key in list_of_properties:
        if key == 'FK_Sensor':
            datas_to_store[key] = target.values.get('FK_Sensor', None)
        if key == 'FK_Individual':
            datas_to_store[key] = target.values.get('FK_Individual', None)
        if key == 'FK_MonitoredSite':
            datas_to_store[key] = getattr(target.Station, 'FK_MonitoredSite', None)
            if datas_to_store[key] is None:
                raise ErrorAvailable({'errorSite': True})

    old_equipment = getattr(target, 'Equipment', None)
    # If equipment dont exist we create it with data send
    # no matter we are in create or update obs
    if old_equipment is None:
        availability = check_availability(
            FK_Sensor=datas_to_store['FK_Sensor'],
            FK_Individual=datas_to_store['FK_Individual'],
            FK_MonitoredSite=datas_to_store['FK_MonitoredSite'],
            FK_Observation=datas_to_store['FK_Observation'],
            Deploy=datas_to_store['Deploy'],
            StartDate=datas_to_store['StartDate'],
            connection=connection
        )
        stmt = insert(Equipment.__table__)
        stmt = stmt.values(**datas_to_store)
        connection.execute(stmt)
        if availability is not True:
            raise(ErrorAvailable(availability))
        return

    sensor_or_individual_or_monitoredsite_edited = False
    if old_equipment is not None:
        state_observation = inspect(target)
        changes = {}

        for attr in state_observation.attrs:
            hist = attr.load_history()

            if not hist.has_changes():
                continue

            # hist.deleted holds old value
            # hist.added holds new value
            changes[attr.key] = hist.added

        # now changes map keys to new values

        list_dynamic_values = changes.get('_dynamicValues', None)
        list_fk_individual = changes.get('FK_Individual', None)
        fk_sensor_for_update = None
        if list_dynamic_values is not None:
            flag_dynprop_find = False
            for row in list_dynamic_values:
                fk_property = getattr(row, 'fk_property', None)
                if fk_property == __OBSERVATIONDYNPROP_ID__:
                    if flag_dynprop_find is True:
                        raise Exception(
                            'Something goes wrong more than one FK_Sensor value'
                        )
                    flag_dynprop_find = True
                    fk_sensor_for_update = getattr(row, 'ValueInt', None)
                    if fk_sensor_for_update is None:
                        raise Exception(
                            'Something goes wrong more than one FK_Sensor value'
                        )

        if fk_sensor_for_update is not None:
            datas_to_store['FK_Sensor'] = fk_sensor_for_update
        if list_fk_individual is not None:
            datas_to_store['FK_Individual'] = list_fk_individual[0]
        # two kind of equipment protocol
        # first is individual_equipment/unequipment
        # and we have an FK_Sensor AND FK_Individual
        # second is site_equipment/unequipment
        # and we have an FK_Sensor AND FK_MonitoredSite
        sensor_or_individual_or_monitoredsite_edited = (
            (
                int(old_equipment.FK_Sensor)
                !=
                int(datas_to_store['FK_Sensor'])
            )
            or
            (
                old_equipment.FK_Individual is not None
                and
                (
                    int(old_equipment.FK_Individual)
                    !=
                    int(datas_to_store['FK_Individual'])
                )
            )
            or
            (
                # FK_MonitoredSite property is not in the observation
                # it's from the station where the observation is attached
                # but we need to handle the same way or refact all the feature
                # the business ruler doesn't allow to modify a monitored site
                # with the stored procedure [check_SiteEquipmentOnStation]
                old_equipment.FK_MonitoredSite is not None
                and
                (
                    int(old_equipment.FK_MonitoredSite)
                    !=
                    int(datas_to_store['FK_MonitoredSite'])
                )
            )
        )
        # handle equipement:
        # when no equipment exist in db (old_equipment is None)
        # we create it

        # creation case


        # edition case
        if sensor_or_individual_or_monitoredsite_edited is True:

            # When equipement exist
            # we need to check if we can delete it first
            if old_equipment is not None:
                stmt = 'SELECT COUNT(*) FROM [SensorEquipment] '
                stmt += 'WHERE '
                if old_equipment.FK_Sensor is None:
                    stmt += '[FK_Sensor] IS NULL '
                if old_equipment.FK_Sensor is not None:
                    stmt += f'[FK_Sensor] = {old_equipment.FK_Sensor} '
                stmt += 'AND '
                if old_equipment.FK_Individual is None:
                    stmt += '[FK_Individual] IS NULL '
                if old_equipment.FK_Individual is not None:
                    stmt += f'[FK_Individual] = {old_equipment.FK_Individual} '
                stmt += 'AND '
                if old_equipment.FK_MonitoredSite is None:
                    stmt += '[FK_MonitoredSite] IS NULL '
                if old_equipment.FK_MonitoredSite is not None:
                    stmt += f'[FK_MonitoredSite] = {old_equipment.FK_MonitoredSite} '
                stmt += 'AND '
                if old_equipment.Deploy is True:
                    # equipment
                    # we cannot delete a deploy session if an undeploy is linked
                    stmt += f'[StartDate] = \'{old_equipment.StartDate:%Y-%m-%dT%H:%M:%S}\' '
                    stmt += 'AND '
                    stmt += '[EndDate] IS NOT NULL '
                if old_equipment.Deploy is False:
                    # unequipement
                    # we cannot delete an undeploy if another session exist after
                    stmt += f'[StartDate] >= \'{old_equipment.StartDate:%Y-%m-%dT%H:%M:%S}\' '

                try:
                    can_we_delete = connection.execute(stmt).scalar()
                    if can_we_delete >= 1:
                        if datas_to_store['Deploy'] is True:
                            raise(
                                ErrorAvailable(
                                    "You cannot edit this observation because "
                                    "an unequipment is linked "
                                    "with this current equipment in db"
                                )
                            )
                        if datas_to_store['Deploy'] is False:
                            raise(
                                ErrorAvailable(
                                    "You cannot edit this observation because "
                                    "an another equipment exist after "
                                    "this current undeploy in db"
                                )
                            )
                except ErrorAvailable as e:
                    raise e
                except Exception:
                    # if this happend it's a bug
                    # meaning something goes wrong with the view SensorEquipment]
                    # or with the statement build
                    raise(
                        ErrorAvailable(
                            "Something goes wrong. Contact an admin"
                        )
                    )


                stmt = delete(Equipment.__table__)
                stmt = stmt.where(
                        Equipment.__table__.c.ID == old_equipment.ID
                    )

                connection.execute(stmt)

                if sensor_or_individual_or_monitoredsite_edited:
                    availability = check_availability(
                        FK_Sensor=datas_to_store['FK_Sensor'],
                        FK_Individual=datas_to_store['FK_Individual'],
                        FK_MonitoredSite=datas_to_store['FK_MonitoredSite'],
                        FK_Observation=datas_to_store['FK_Observation'],
                        Deploy=datas_to_store['Deploy'],
                        StartDate=datas_to_store['StartDate'],
                        connection=connection
                    )

                    if availability is True:
                        stmt = insert(Equipment.__table__)
                        stmt = stmt.values(**datas_to_store)
                        connection.execute(stmt)

                    if availability is not True:
                        # if not available error raise
                        # transaction is rollback
                        # deleted equipment is restored
                        raise(ErrorAvailable(availability))


class ErrorAvailable(Exception):

    def __init__(self, value):
        self.value = value
        print("PTT Not available")

    def __str__(self):
        return repr(self.value)