import os
from pathlib import Path
from traceback import print_exc
from pyramid.httpexceptions import HTTPConflict
from pyramid.request import Request
from pyramid.view import view_config
from sqlalchemy import select, and_, join, func, case, union, text
from traceback import print_exc
from sqlalchemy.orm import (
    Query
)
from sqlalchemy.orm.exc import NoResultFound
from ecoreleve_server.core import (
    RootCore,
    DynamicObjectResource,
    DynamicObjectCollectionResource,
    dbConfig,
)
from ecoreleve_server.core.log_view import sendLog
from ecoreleve_server.modules.stations import Station
from ecoreleve_server.modules.field_activities import ProtocoleType
from .observation_model import Observation
from .observation_collection import ObservationCollection
from ..field_activities import FieldActivity_ProtocoleType
from ..media_files import MediasFiles
from ..permissions import context_permissions
from .equipment_model import ErrorAvailable
import collections


class MetaUtils:
    def getGUID(self, mediaFilePath):
        guid = None

        pathSegments = mediaFilePath.split("/")
        fullFilename = pathSegments[-1]
        filenameSegments = fullFilename.split(".")

        if len(filenameSegments) == 1:
            guid = filenameSegments[0]
        if len(filenameSegments) == 2:
            guid = ".".join(filenameSegments[0:-1])

        return guid

    def get_dyn_prop_id_name_for_mediaFile(self, observation):
        dictIdName = {}
        if getattr(observation, 'request', None) is None:
            observation.request = self.request
        form = observation.getForm()
        schema = form.get("schema")
        if schema:
            for dynPropName, dynPropSchema in schema.items():
                inputType = dynPropSchema.get("type")
                if inputType and inputType == "MediaFile":
                    observationDynProp = observation.properties_by_name.get(
                        dynPropName
                    )
                    id = observationDynProp.get("ID")
                    name = observationDynProp.get("Name")
                    if dictIdName.get(id) is None:
                        dictIdName = {**dictIdName, **{id: name}}

        return dictIdName

    def addFkToNewMediaFileInserted(
        self, newMediaFilePath, observationDynPropValueID
    ):
        guid = self.getGUID(newMediaFilePath)

        if guid:
            query = self.session.query(MediasFiles)
            query = query.filter(MediasFiles.GUID == guid)
            try:
                newMediafile_in_db = query.one()
                newMediafile_in_db.FK_DynPropValue = observationDynPropValueID
                self.session.add(newMediafile_in_db)
                self.session.flush()
            except Exception as e:
                raise e


class utilsForObservationsCollection(MetaUtils):
    def handle_new_media_file_in_obs(self, observation, newObsValues):
        dictIdName = self.get_dyn_prop_id_name_for_mediaFile(
            observation=observation
        )
        observationDynPropValues = observation._dynamicValues

        for dynpropId, dynPropName in dictIdName.items():
            newMediaFilePath = newObsValues.get(dynPropName)
            if newMediaFilePath:
                for observationDynPropValue in observationDynPropValues:
                    fk_dynPropId = observationDynPropValue.fk_property
                    mediaFilePath = observationDynPropValue.ValueString
                    matchingRowInHistory = (
                        fk_dynPropId == dynpropId
                        and mediaFilePath == newMediaFilePath
                    )
                    if matchingRowInHistory:
                        observationDynPropValueID = observationDynPropValue.ID
                        mediaFilePath = observationDynPropValue.ValueString
                        self.addFkToNewMediaFileInserted(
                            newMediaFilePath=mediaFilePath,
                            observationDynPropValueID=observationDynPropValueID,
                        )

    def applyRulesWhenAddMediaFile(self, observation, newObsValues):
        self.handle_new_media_file_in_obs(
            observation=observation, newObsValues=newObsValues
        )


class utilsForObservationsItem(utilsForObservationsCollection):
    def removeFile(self, fk_station, mediafile):
        path = f"{fk_station}\\\{mediafile.GUID}"
        if mediafile.Extension:
            path = f"{path}.{mediafile.Extension}"
        try:
            fullPath = os.path.join(
                dbConfig["mediasFiles"]["path"],
                fk_station,
                mediafile.GUID,
            )
            if mediafile.Extension:
                fullPath = f"{fullPath}.{mediafile.Extension}"
            fileObj = Path(fullPath)
            if fileObj.is_file():
                os.remove(fullPath)
        except OSError as e:
            raise e

    def deleteMediaFile(self, mediaFilePath, fk_station):
        guid = self.getGUID(mediaFilePath)

        if guid:
            query = self.session.query(MediasFiles)
            query = query.filter(MediasFiles.GUID == guid)
            try:
                mediafile_in_db = query.one()
                self.removeFile(
                    fk_station=fk_station, mediafile=mediafile_in_db
                )
                self.session.delete(mediafile_in_db)
                self.session.flush()
            except NoResultFound:
                raise HTTPConflict(body=f'The media file with guid "{guid}" does not exist in database. Please contact an administrator.')
            except Exception as e:
                raise e

    def getListIdObservationDynPropValues(self, observation):
        listId = []
        observationDynPropValues = observation._dynamicValues
        for observationDynPropValue in observationDynPropValues:
            observationDynPropValueId = observationDynPropValue.ID
            listId.append(observationDynPropValueId)

        return listId

    def updateNewMediaFile(
        self, mediaFilePath, observation, observationDynPropInDb
    ):

        guid = self.getGUID(mediaFilePath)
        if guid:
            query = self.session.query(MediasFiles)
            query = query.filter(MediasFiles.GUID == guid)
            try:
                newMediafile_in_db = query.one()
                newMediafile_in_db.FK_Observation = observation.ID
                newMediafile_in_db.FK_DynProp = observationDynPropInDb.get("ID")
                self.session.add(newMediafile_in_db)
                self.session.flush()
            except Exception as e:
                raise e

    def applyRulesBeforeEditMediaFile(self, observation, newObsValues):
        dictIdName = self.get_dyn_prop_id_name_for_mediaFile(observation)
        curObsValues = getattr(observation, "values")
        if curObsValues:
            for dynpropId, dynPropName in dictIdName.items():
                oldMediaFilePath = curObsValues.get(dynPropName)
                if oldMediaFilePath is not None:
                    newMediaFilePath = newObsValues.get(dynPropName)
                    needToRemoveOldFile = oldMediaFilePath != newMediaFilePath
                    if needToRemoveOldFile:
                        fk_station = f"{observation.FK_Station}"
                        self.deleteMediaFile(
                            mediaFilePath=oldMediaFilePath, fk_station=fk_station
                        )

    def applyRulesAfterEditMediaFile(self, observation, newObsValues):
        self.applyRulesWhenAddMediaFile(
            observation=observation, newObsValues=newObsValues
        )

    def applyRulesWhenDeleteObservation(self, observation):
        listId = self.getListIdObservationDynPropValues(observation=observation)
        fk_station = f"{observation.FK_Station}"
        query = self.session.query(MediasFiles)
        query = query.filter(MediasFiles.FK_DynPropValue.in_(listId))

        for mediaFile in query:
            self.removeFile(fk_station=fk_station, mediafile=mediaFile)


class ObservationResource(DynamicObjectResource, utilsForObservationsItem):

    model = Observation
    __acl__ = context_permissions["observations"]

    def update(self, json_body=None):

        if not json_body:
            data = self.request.json_body
        else:
            data = json_body

        curObs = self.objectDB

        listOfSubProtocols = []
        responseBody = {"id": curObs.ID}

        for items, value in data.items():
            if isinstance(value, list) and items != "children":
                listOfSubProtocols = value

        data["Observation_childrens"] = listOfSubProtocols

        self.applyRulesBeforeEditMediaFile(
            observation=curObs, newObsValues=data
        )
        curObs.values = data
        try:
            self.session.add(curObs)
            self.session.flush()
            self.session.refresh(curObs)
        except ErrorAvailable as e:
            self.session.rollback()
            self.request.response.status_code = 409
            responseBody["response"] = e.value

        self.applyRulesAfterEditMediaFile(
            observation=curObs,
            newObsValues=data
        )
        return responseBody

    def delete(self):
        if self.objectDB:
            id_ = self.objectDB.ID
            DynamicObjectResource.delete(self)

            self.applyRulesWhenDeleteObservation(observation=self.objectDB)

        else:
            id_ = None
        response = {"id": id_}
        return response


class ObservationsResource(
    DynamicObjectCollectionResource, utilsForObservationsCollection
):

    Collection = ObservationCollection
    model = Observation
    moduleFormName = "ObservationForm"
    moduleGridName = "ObservationFilter"
    children = [("{int}", ObservationResource)]
    item = ObservationResource

    __acl__ = context_permissions["observations"]

    def __init__(self, ref, parent):
        DynamicObjectCollectionResource.__init__(self, ref, parent)
        # self.POSTactions = {'batch': self.batch}
        self.parent = parent
        if "objectType" in self.request.params:
            self.typeObj = int(self.request.params["objectType"])

    def retrieve(self):
        if self.parent.__class__.__name__ == "StationResource":
            if self.typeObj:
                return self.getObservationsWithType()
            else:
                return self.getProtocolsofStation()
        else:
            self.request.response.status_code = 520
            response = self.request.response
            response.text = "A station ID is needed, try this url */stations/{id}/observations"
            return response

    def insert(self, json_body=None):
        if not json_body:
            json_body = self.request.json_body

        data = {}
        for items, value in json_body.items():
            data[items] = value

        sta = self.parent.objectDB
        curObs = self.model(
            type_id=data["FK_ProtocoleType"],
            FK_Station=sta.ID,
            session=self.session,
            request=self.request
        )
        listOfSubProtocols = []

        for items, value in data.items():
            if isinstance(value, list) and items != "children":
                listOfSubProtocols = value

        data["Observation_childrens"] = listOfSubProtocols

        responseBody = {}

        try:
            curObs.values = data
            curObs.Station = sta
            self.session.add(curObs)
            self.session.flush()
            self.session.refresh(curObs)
            self.applyRulesWhenAddMediaFile(
                observation=curObs, newObsValues=data
            )
            responseBody["id"] = curObs.ID
        except Exception as e:
            # print(e)
            self.session.rollback()
            self.request.response.status_code = 409
            responseBody["response"] = e.value
            sendLog(
                logLevel=1,
                domaine=3,
                request=self.request,
                msg_number=self.request.response.status_code,
            )
        return responseBody

    def batch(self):
        rowData = self.request.json_body["rowData"]
        responseBody = {"updatedObservations": [], "createdObservations": []}

        for row in rowData:
            if "delete" in self.request.json_body:
                item = self.item(row["ID"], self)
                responseBody["updatedObservations"].append(item.delete())
                continue

            if "ID" in row:
                item = self.item(row["ID"], self)
                responseBody["updatedObservations"].append(item.update(row))
            else:
                row["FK_ProtocoleType"] = self.request.json_body[
                    "FK_ProtocoleType"
                ]
                responseBody["createdObservations"].append(self.insert(row))

        return responseBody

    def count_(self):
        query = select([func.count(Observation.ID)])
        if "protocolTypeId" in self.request.params:
            typeId = self.request.params["protocolTypeId"]
            query = query.where(Observation.FK_ProtocoleType == int(typeId))

        result = self.session.execute(query).scalar()
        return result

    def getObservationsWithType(self):
        print("in get observation grid")
        sta_id = self.parent.objectDB.ID
        listObs = list(
            self.session.query(Observation)
            .filter(Observation.type_id == self.typeObj)
            .filter(Observation.FK_Station == sta_id)
        )
        values = []
        for i in range(len(listObs)):
            curObs = listObs[i]
            if getattr(curObs, 'request', None) is None:
                curObs.request = self.request
            if getattr(curObs, 'session', None) is None:
                curObs.session = self.session
            values.append(curObs.getDataWithSchema()["data"])
        return values

    def getProtocolsofStation(self):
        dbsession = self.session
        curSta = self.parent.objectDB
        response = []

        query = Query(
            entities=[
                ProtocoleType.ID.label('ID'),
                ProtocoleType.Name.label('Name'),
                FieldActivity_ProtocoleType.Order.label('Order'),
                Observation.ID.label('Obs_ID'),
                ProtocoleType.Status.label('Status')
            ],
            session=dbsession
        )
        query = query.select_from(Station)
        query = query.join(
            FieldActivity_ProtocoleType,
            Station.fieldActivityId == FieldActivity_ProtocoleType.FK_fieldActivity
        )
        query = query.join(
            ProtocoleType,
            (
                (ProtocoleType.ID == FieldActivity_ProtocoleType.FK_ProtocoleType)
                &
                (ProtocoleType.Status.in_([1, 2]))
            ),
            isouter=True
        )
        query = query.join(
            Observation,
            (
                (Observation.FK_Station == Station.ID)
                &
                (Observation._type_id == ProtocoleType.ID)
            ),
            isouter=True
        )
        query = query.filter(
            Station.ID == curSta.ID
        )

        query2 = Query(
            entities=[
                ProtocoleType.ID.label('ID'),
                ProtocoleType.Name.label('Name'),
                case(
                    [
                        (FieldActivity_ProtocoleType.Order.__eq__(None), 9999),
                    ],
                    else_=FieldActivity_ProtocoleType.Order

                ).label('Order'),
                Observation.ID.label('Obs_ID'),
                ProtocoleType.Status.label('Status')
            ],
            session=dbsession
        )
        query2 = query2.select_from(Station)
        query2 = query2.join(
            Observation,
            Observation.FK_Station == Station.ID
        )
        query2 = query2.join(
            ProtocoleType,
            (
                (ProtocoleType.ID == Observation._type_id)
                &
                (ProtocoleType.Status.in_([1, 2]))
            )
        )
        query2 = query2.join(
            FieldActivity_ProtocoleType,
            (
                (Station.fieldActivityId == FieldActivity_ProtocoleType.FK_fieldActivity)
                &
                (FieldActivity_ProtocoleType.FK_ProtocoleType == ProtocoleType.ID)
            ),
            isouter=True
        )
        query2 = query2.filter(
            Station.ID == curSta.ID
        )

        union_query = union(query, query2).alias('T')

        final_query = Query(
            entities=[
                union_query.c.ID,
                union_query.c.Name,
                union_query.c.Order,
                union_query.c.Obs_ID,
                union_query.c.Status
            ],
            session=dbsession
        )

        final_query = final_query.order_by(
            text('[Order]'),
            text('[Name]'),
            text('[Obs_ID]')
        )

        response = []
        proto = {}
        for row in final_query:
            proto_id = row.ID
            obs_id = row.Obs_ID
            cur_proto = proto.get(proto_id, None)
            cur_status = row.Status
            cur_name = row.Name.replace("_", " ")

            cur_obs = Observation(type_id=proto_id, session=self.session, request=self.request)
            cur_form = cur_obs.getForm(displayMode="edit")
            if cur_proto is None:
                proto[row.ID] = {
                    'ID': row.ID,
                    'Name': cur_name,
                    'schema': cur_form.get("schema", None),
                    'fieldsets': cur_form.get("fieldsets", None),
                    'grid': False,
                    'obs': []
                }
                response.append(proto[row.ID])
                if cur_status == 1:
                    proto[row.ID]['grid'] = False
                if cur_status == 2:
                    proto[row.ID]['grid'] = True
                if obs_id is not None:
                    proto[row.ID]['obs'].append(obs_id)
            if cur_proto is not None:
                cur_proto['obs'].append(obs_id)

        return response


    def getType(self):
        ProtocoleType = Observation.TypeClass
        if "FieldActivityID" in self.request.params:
            fieldActivityID = self.request.params["FieldActivityID"]
            join_table = join(
                ProtocoleType,
                FieldActivity_ProtocoleType,
                ProtocoleType.ID
                == FieldActivity_ProtocoleType.FK_ProtocoleType,
            )
            query = (
                select([ProtocoleType.ID, ProtocoleType.Name])
                .where(
                    and_(
                        ProtocoleType.Status.in_([1, 2]),
                        FieldActivity_ProtocoleType.FK_fieldActivity
                        == fieldActivityID,
                    )
                )
                .select_from(join_table)
            )
        else:
            query = select([ProtocoleType.ID, ProtocoleType.Name]).where(
                ProtocoleType.Status.in_([1, 2])
            )
        query = query.where(ProtocoleType.obsolete == False)
        result = self.session.execute(query).fetchall()
        res = []
        for row in result:
            elem = {}
            elem["ID"] = row["ID"]
            elem["Name"] = row["Name"].replace("_", " ")
            res.append(elem)
        res = sorted(res, key=lambda k: k["Name"])
        return res


RootCore.children.append(("protocols", ObservationsResource))
RootCore.children.append(("observations", ObservationsResource))
