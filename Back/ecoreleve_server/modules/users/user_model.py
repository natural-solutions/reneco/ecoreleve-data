from sqlalchemy import (
    Column,
    DateTime,
    Index,
    Integer,
    Sequence,
    String,
    func,
    select
)
from sqlalchemy.ext.hybrid import hybrid_property
from ecoreleve_server.core import Main_Db_Base


class User(Main_Db_Base):
    __tablename__ = 'User'
    id = Column('ID', Integer, Sequence('seq_user_pk_id'), primary_key=True)
    Lastname = Column(String(50), nullable=False)
    Firstname = Column(String(50), nullable=False)
    CreationDate = Column(DateTime, nullable=False, server_default=func.now())
    Login = Column(String(250), nullable=False)
    Password = Column(String, nullable=False)
    Language = Column(String(2))
    ModificationDate = Column(
        DateTime,
        nullable=False,
        server_default=func.now()
    )

    __mapper_args__ = {
        'version_id_col': id,
        'version_id_generator': False,
    }

    __table_args__ = (
        Index(
            'idx_Tuser_lastname_firstname',
            Lastname,
            Firstname,
            mssql_include=[id]
        ),
        {'implicit_returning': False}
    )


    @hybrid_property
    def fullname(self):
        """ Return the fullname of a user.
        """
        return self.Lastname + ' ' + self.Firstname
