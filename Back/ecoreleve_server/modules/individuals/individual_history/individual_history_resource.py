from sqlalchemy import select, join, not_, desc

from ecoreleve_server.core.base_resource import (
    DynamicValueResource,
    DynamicValuesResource
)
from ecoreleve_server.core import Main_Db_Base
from ..individual_model import Individual
from ecoreleve_server.modules.permissions import context_permissions
from collections import OrderedDict
IndividualDynPropValue = Individual.DynamicValuesClass


class IndividualValueResource(DynamicValueResource):
    model = IndividualDynPropValue

    __acl__ = context_permissions['individuals_history']

    def retrieve(self):
        pass


class IndividualValuesResource(DynamicValuesResource):
    model = IndividualDynPropValue
    children = [('{int}', IndividualValueResource)]

    def retrieve(self):
        from ecoreleve_server.utils.parseValue import formatThesaurus

        propertiesTable = Main_Db_Base.metadata.tables[self.__parent__.objectDB.TypeClass.PropertiesClass.__tablename__]
        dynamicValuesTable = Main_Db_Base.metadata.tables[self.__parent__.objectDB.DynamicValuesClass.__tablename__]
        labelTable = Main_Db_Base.metadata.tables["ModuleForms"]
        frontModulesTable = Main_Db_Base.metadata.tables["FrontModules"]

        FK_name = 'FK_' + self.__parent__.objectDB.__tablename__
        FK_property_name = self.__parent__.objectDB.fk_table_DynProp_name

        moduleIdQuery = select(
            [frontModulesTable.c.ID]
        ).where(
            frontModulesTable.c.Name == "IndivForm"
        )
        
        moduleId = self.session.execute(moduleIdQuery).fetchone().ID

        tableJoin = join(
            dynamicValuesTable, propertiesTable, 
            dynamicValuesTable.c[FK_property_name] == propertiesTable.c['ID']
        ).join(
            labelTable,
            propertiesTable.c['Name'] == labelTable.c['Name']
        )

        query = select([
            dynamicValuesTable, 
            propertiesTable.c['Name'].label('CodeName'), 
            labelTable.c['Label'].label('Name')
        ]).select_from(tableJoin).where(
            dynamicValuesTable.c[FK_name] == self.__parent__.objectDB.ID
        ).where(
            labelTable.c['Module_ID'] == moduleId
        )

        query = query.where(
            not_(
                propertiesTable.c['Name'].in_([
                    'Release_Comments',
                    'Breeding ring kept after release',
                    'Box_ID',
                    'Date_Sortie',
                    'Poids'
            ]))
        ).order_by(desc(dynamicValuesTable.c['StartDate'])).distinct()

        result = self.session.execute(query).fetchall()
        response = []

        for row in result:
            curRow = OrderedDict(row)
            dictRow = {}
            for key in curRow:
                if curRow[key] is not None:
                    if key == 'ValueString' in key and curRow[key] is not None:
                        try:
                            thesauralValueObj = formatThesaurus(
                                data=curRow[key],
                                request=self.request
                            )
                            dictRow['value'] = thesauralValueObj['displayValue']
                        except:
                            dictRow['value'] = curRow[key]
                    elif 'FK' not in key:
                        dictRow[key] = curRow[key]
                if key == 'Name' and (curRow[key] is None or curRow[key] == ''):
                    dictRow['Name'] = curRow['CodeName']
            dictRow['StartDate'] = curRow[
                'StartDate'].strftime('%Y-%m-%d %H:%M:%S')
            response.append(dictRow)

        return response
