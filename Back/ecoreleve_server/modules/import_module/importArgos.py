import pandas as pd
import numpy as np
import chardet
import os
import re
import shutil
from datetime import datetime
import itertools
from traceback import print_exc
from sqlalchemy import select, and_

import ecoreleve_server
from ecoreleve_server.core import dbConfig
from ..sensors.sensor_data import ArgosGps, ArgosEngineering
from .import_model import Import

import uuid
import pathlib
import subprocess


def find_working_folder():
    file_path = os.path.abspath(ecoreleve_server.__file__)
    directory_name = os.path.dirname(file_path)
    return directory_name


def uploadFileArgos(request):

    def create_temporary_folder(working_folder):
        prefix = 'third_party\\ecoReleve_import\\argos'
        temporary_folder = str(uuid.uuid4())

        absolute_path_import = os.path.join(
            working_folder,
            prefix,
            temporary_folder
        )
        try:
            pathlib.Path(absolute_path_import).mkdir(
                parents=True,
                exist_ok=True
            )
        except Exception as error:
            raise error

        return absolute_path_import

    def copy_file_send(absolute_path_import, file_to_save):
        absolute_path_file_saved = os.path.join(
            absolute_path_import,
            file_to_save.filename
        )
        try:
            with open(absolute_path_file_saved, "wb") as output_file:
                shutil.copyfileobj(file_to_save.file, output_file)
        except Exception as error:
            raise error

        return absolute_path_file_saved

    session = request.dbsession
    user = request.authenticated_userid['sub']

    working_folder = find_working_folder()
    absolute_path_import = create_temporary_folder(
        working_folder=working_folder
    )

    file_to_save = request.POST['file']
    absolute_path_file_saved = copy_file_send(
        absolute_path_import=absolute_path_import,
        file_to_save=file_to_save
    )

    import_obj_in_db = Import(
        ImportFileName=file_to_save.filename,
        FK_User=user,
        ImportType='Argos'
    )
    session.add(import_obj_in_db)
    session.flush()

    message = {}
    if 'DIAG' in file_to_save.filename.upper():
        message, nbRows, nbInserted, maxDate, minDate = parseDIAGFileAndInsert(
            absolute_path_file_saved,
            session,
            import_obj_in_db.ID
        )
    elif 'DS' in file_to_save.filename.upper():
        message, nbRows, nbInserted, maxDate, minDate = parseDSFileAndInsert(
            storing_folder=absolute_path_import,
            file_send=absolute_path_file_saved,
            session=session,
            import_id=import_obj_in_db.ID
        )

    if message:
        import_obj_in_db.nbRows = nbRows
        import_obj_in_db.nbInserted = nbInserted
        import_obj_in_db.maxDate = maxDate
        import_obj_in_db.minDate = minDate
        session.flush()
    else:
        message = 'Argos File type name did not recognized'
    return message


def call_MTI_parser(file_send, storing_folder, sub_folder='output_files'):
    '''
        "mtiwingps.exe [-option1] [-option2] inputfile1 inputfile2 ..."

        -nographics             Run without graphical mode
        -ptts ##,##,...         selects Argos IDs to process
        -out "DIRNAME"          selects the Output Directory
        -neglat                 Reverse sign of latitude
        -neglong                Reverse sign of longitude
        -noclean                Show attempted (failed) GPS fixes
        -splitdate              Split Date and Time into two columns
        -title                  Column titles in output files
        -eng                    Output Engineering data [*e.txt]
        -argos                  Output Argos locations [*a.txt]
        -dir                    Output files in separate directories
        -rawdata                Output Raw (uncalculated) Values
        -kml                    Output Google maps [*.kml]
    '''
    working_folder = find_working_folder()
    path_MTI_parser = f'{working_folder}\\third_party\\MTIwinGPS.exe'
    execution_path = os.getcwd()
    destination_files_parsed = f'{storing_folder}\\{sub_folder}'
    args_cmd = [
        f'{path_MTI_parser}',
        '-nographics',
        '-eng',
        '-title',
        '-out',
        f'"{destination_files_parsed}"',
        f'"{file_send}"'
    ]

    process = None
    try:
        if (os.path.exists(f'{execution_path}\\init.txt')):
            os.remove(f'{execution_path}\\init.txt')
        process = subprocess.Popen(args_cmd)
        process.wait()
    except Exception as error:
        raise error
    finally:
        if (os.path.exists(f'{execution_path}\\init.txt')):
            os.remove(f'{execution_path}\\init.txt')
        if process is not None:
            process.terminate()


def parseDSFileAndInsert(
    storing_folder,
    file_send,
    session,
    import_id
):
    def get_files_generated_by_MTI(storing_folder, sub_folder):
        files_name = []
        target_folder = os.path.join(
            storing_folder,
            sub_folder
        )
        if (os.path.exists(target_folder)):
            for file_name in os.listdir(target_folder):
                files_name.append(
                    os.path.join(
                        target_folder,
                        file_name
                    )
                )
        return files_name

    files_name = []
    sub_folder = 'output_files'

    try:
        # will call mti parser in cmd
        # mti parser will read a DS file and generate files
        # were name start by the ID of the sensor
        # and will end by:
        # "g" for GPS datas
        # "e" or "d" for Engineering datas
        call_MTI_parser(
            file_send=file_send,
            storing_folder=storing_folder,
            sub_folder=sub_folder
        )
        files_name = get_files_generated_by_MTI(
            storing_folder=storing_folder,
            sub_folder=sub_folder
        )

        EngData = None
        GPSData = None
        EngDataBis = None
        nb_gps_data = None
        nb_existingGPS = None
        nb_eng = 0
        nb_existingEng = 0
        maxDateGPS = None
        minDateGPS = None

        for file_name in files_name:
            absolute_path, file_name_with_extension = os.path.split(file_name)
            name_file, extension_file = os.path.splitext(
                file_name_with_extension
            )

            fk_ptt = name_file[0:-1]
            encoding = None

            with open(file_name, 'rb') as f:
                read_file = f.read()
                result = chardet.detect(read_file)
                encoding = result.get('encoding', None)

            if name_file.endswith("g"):
                data_frame = pd.read_csv(
                    file_name,
                    encoding=encoding,
                    sep='\t',
                    parse_dates=['Date/Time'],
                    infer_datetime_format=True  # increase parsing perf
                )
                data_frame['Date/Time'] = pd.to_datetime(
                    data_frame['Date/Time'],
                    errors='coerce'
                )
                data_frame.dropna(subset=['Date/Time'], inplace=True)
                data_frame['ptt'] = fk_ptt
                if GPSData is None:
                    GPSData = data_frame
                else:
                    GPSData = GPSData.append(data_frame)
            elif name_file.endswith("e"):
                data_frame = pd.read_csv(
                    file_name,
                    encoding=encoding,
                    sep='\t',
                    parse_dates=['Tx Date/Time'],
                    infer_datetime_format=True  # increase parsing perf
                )
                # Normalize columns name
                # if columns exists will be renamed
                data_frame.rename(
                    columns={
                        'Tx Date/Time': 'txDate',
                        'PTT Date/Time': 'pttDate',
                        'Int Date/Time': 'pttDate',
                        'Satellite ID': 'satId',
                        'Activity': 'activity',
                        'Tx Count': 'txCount',
                        'Temperature (°C)': 'temp',
                        'Battery Voltage (V)': 'batt',
                        'GPS fix time': 'fixTime',
                        'Satellite Count': 'satCount',
                        'Hours Since Reset': 'resetHours',
                        'Hour': 'resetHours',
                        'Days since GPS fix': 'fixDays',
                        'Season': 'season',
                        'Shunt': 'shunt',
                        'Mortality GT': 'mortalityGT',
                        'Seasonal GT': 'seasonalGT',
                        'Season GT': 'seasonalGT',
                        'Latest Latitude(N)': 'latestLat',
                        'Latest Longitude(E)': 'latestLon'
                    },
                    inplace=True
                )

                data_frame['txDate'] = pd.to_datetime(
                    data_frame['txDate'],
                    errors='coerce'
                )
                data_frame.dropna(subset=['txDate'], inplace=True)

                if 'pttDate' not in data_frame.columns:
                    data_frame['pttDate'] = data_frame['txDate']

                data_frame['pttDate'] = pd.to_datetime(
                    data_frame['pttDate'],
                    errors='coerce'
                )
                data_frame.dropna(subset=['pttDate'], inplace=True)

                data_frame['ptt'] = fk_ptt
                if EngData is None:
                    EngData = data_frame
                else:
                    EngData = EngData.append(data_frame)

            elif name_file.endswith("d"):
                data_frame = pd.read_csv(
                    file_name,
                    encoding=encoding,
                    sep='\t',
                    parse_dates=['Tx Date/Time'],
                    infer_datetime_format=True  # increase parsing perf
                )
                data_frame['Tx Date/Time'] = pd.to_datetime(
                    data_frame['Tx Date/Time'],
                    errors='coerce'
                )
                data_frame.dropna(subset=['Tx Date/Time'], inplace=True)
                data_frame.rename(
                    columns={
                        'Tx Date/Time': 'txDate',
                        'Temperature': 'temp',
                        'Battery Voltage': 'batt',
                        'Transmission Count': 'txCount',
                        'Activity Count': 'activity'
                    },
                    inplace=True
                )
                data_frame['ptt'] = fk_ptt
                data_frame['pttDate'] = data_frame['txDate']
                if EngDataBis is None:
                    EngDataBis = data_frame
                else:
                    EngDataBis = EngDataBis.append(data_frame)

        if EngData is not None:
            EngToInsert = checkExistingEng(EngData, session)
            nb_existingEng += EngData.shape[0]
            if EngToInsert.shape[0] != 0:
                # Insert non existing data into DB
                EngToInsert.loc[:, ('FK_Import')] = list(
                    itertools.repeat(import_id, len(EngToInsert.index))
                )
                nb_eng += EngToInsert.shape[0]
                # filter data frame columns for matching table columns
                matchingEngColumns = EngToInsert.filter([
                    'FK_ptt',
                    'pttDate',
                    'txDate',
                    'satId',
                    'txCount',
                    'temp',
                    'batt',
                    'fixTime',
                    'satCount',
                    'resetHours',
                    'fixDays',
                    'season',
                    'shunt',
                    'mortalityGT',
                    'seasonalGT',
                    'latestLat',
                    'latestLon'
                ])
                matchingEngColumns.to_sql(
                    ArgosEngineering.__table__.name,
                    session.get_bind(ArgosEngineering),
                    if_exists='append',
                    schema=dbConfig['sensor_schema'],
                    index=False
                )

        if EngDataBis is not None:
            EngBisToInsert = checkExistingEng(EngDataBis, session)
            nb_existingEng += EngDataBis.shape[0]
            if EngBisToInsert.shape[0] != 0:
                EngToInsert.loc[:, ('FK_Import')] = list(
                    itertools.repeat(import_id, len(EngToInsert.index))
                )
                nb_eng += EngBisToInsert.shape[0]
                # Insert non existing data into DB
                EngBisToInsert.to_sql(
                    ArgosEngineering.__table__.name,
                    session.get_bind(ArgosEngineering),
                    if_exists='append',
                    schema=dbConfig['sensor_schema'],
                    index=False
                )

        if GPSData is not None:
            if 'datetime' in GPSData:
                maxDateGPS = GPSData['datetime'].max()
                minDateGPS = GPSData['datetime'].min()
            GPSData = GPSData.replace(["neg alt"], [-999])
            DFToInsert = checkExistingGPS(GPSData, session)
            DFToInsert.loc[:, ('FK_Import')] = list(
                itertools.repeat(import_id, len(DFToInsert.index))
            )
            nb_gps_data = DFToInsert.shape[0]
            nb_existingGPS = GPSData.shape[0] - DFToInsert.shape[0]
            if DFToInsert.shape[0] != 0:
                # Insert non existing data into DB
                DFToInsert.to_sql(
                    ArgosGps.__table__.name,
                    session.get_bind(ArgosGps),
                    if_exists='append',
                    schema=dbConfig['sensor_schema'],
                    index=False
                )

        # os.remove(full_filename)
        # shutil.rmtree(out_path)
        message = {
            'inserted gps': nb_gps_data,
            'existing gps': nb_existingGPS,
            'inserted Engineering': nb_eng,
            'existing Engineering': nb_existingEng - nb_eng,
            'inserted argos': 0,
            'existing argos': 0
        }

        nbRows = (
            (nb_existingEng or 0)
            +
            (nb_existingGPS or 0)
            +
            (nb_gps_data or 0)
        )
        nbInserted = (nb_gps_data or 0) + (nb_eng or 0)
        return message, nbRows, nbInserted, maxDateGPS, minDateGPS

    except Exception as error:
        raise error


def checkExistingEng(EngData, session):
    EngData['id'] = range(EngData.shape[0])
    EngData.dropna(how='all', inplace=True)
    try:
        if 'pttDate' in EngData.columns:
            EngData['pttDate'] = pd.to_datetime(EngData['pttDate'])

        EngData['txDate'] = EngData.apply(
            lambda row: np.datetime64(row['txDate']).astype(datetime), axis=1)
        maxDate = EngData['txDate'].max()
        minDate = EngData['txDate'].min()

        # Retrieve data from Database for test existing
        queryEng = select([ArgosEngineering.fk_ptt, ArgosEngineering.txDate])
        queryEng = queryEng.where(
            and_(
                ArgosEngineering.txDate >= minDate,
                ArgosEngineering.txDate <= maxDate
            )
        )
        data = session.execute(queryEng).fetchall()
        session.commit()

        # Load DB data into a pandas DataFrame
        EngRecords = pd.DataFrame.from_records(
            data,
            columns=[
                ArgosEngineering.fk_ptt.name,
                ArgosEngineering.txDate.name
            ]
        )

        # apply a merge/join beetween dataframes with data from Files and data
        # from DB
        EngData = EngData.astype({'ptt': 'int64'})
        merge = pd.merge(
            EngData,
            EngRecords,
            left_on=['txDate', 'ptt'],
            right_on=['txDate', 'FK_ptt']
        )

        # Extract non existing data
        DFToInsert = EngData[~EngData['id'].isin(merge['id'])]

        # rename column
        DFToInsert.loc[:, ('creationDate')] = list(
            itertools.repeat(datetime.now(), len(DFToInsert.index)))
        DFToInsert['FK_ptt'] = DFToInsert['ptt']
        DFToInsert = DFToInsert.drop(['id', 'ptt'], 1)
    except Exception:
        print_exc()
        DFToInsert = pd.DataFrame()
    return DFToInsert


def checkExistingGPS(GPSData, session):
    GPSData['datetime'] = GPSData.apply(lambda row: np.datetime64(
        row['Date/Time']).astype(datetime), axis=1)
    GPSData['id'] = range(GPSData.shape[0])
    maxDateGPS = GPSData['datetime'].max()
    minDateGPS = GPSData['datetime'].min()

    # round lat/lon decimal 3 for data from Files
    GPSData['lat'] = GPSData['Latitude(N)'].round(3)
    GPSData['lon'] = GPSData['Longitude(E)'].round(3)

    # Retrieve exisintg data from DB
    queryGPS = select([
        ArgosGps.pk_id,
        ArgosGps.date,
        ArgosGps.lat,
        ArgosGps.lon,
        ArgosGps.ptt
    ]).where(ArgosGps.type_ == 'GPS')
    queryGPS = queryGPS.where(
        and_(
            ArgosGps.date >= minDateGPS,
            ArgosGps.date <= maxDateGPS
        )
    )
    data = session.execute(queryGPS).fetchall()
    session.commit()

    # Load data from DB into dataframe
    GPSrecords = pd.DataFrame.from_records(
        data,
        columns=[
            ArgosGps.pk_id.name,
            ArgosGps.date.name,
            ArgosGps.lat.name,
            ArgosGps.lon.name,
            ArgosGps.ptt.name
        ],
        coerce_float=True)

    # round_ lat/lon decimal 3 for data from DB
    GPSrecords['lat'] = GPSrecords['lat'].round(3)
    GPSrecords['lon'] = GPSrecords['lon'].round(3)

    # apply a merge/join beetween dataframes with data from Files and data
    # from DB
    GPSData = GPSData.astype({'ptt': 'int64'})
    merge = pd.merge(
        GPSData,
        GPSrecords,
        left_on=['datetime', 'lat', 'lon', 'ptt'],
        right_on=['date', 'lat', 'lon', 'FK_ptt']
    )
    DFToInsert = GPSData[~GPSData['id'].isin(merge['id'])]

    DFToInsert = DFToInsert.drop(['id', 'datetime', 'lat', 'lon'], 1)
    DFToInsert.columns = ['date', 'lat', 'lon',
                          'speed', 'course', 'ele', 'FK_ptt']

    DFToInsert = DFToInsert.replace('2D fix', np.nan)
    DFToInsert = DFToInsert.replace('low alt', np.nan)
    DFToInsert.loc[:, ('type')] = list(
        itertools.repeat('GPS', len(DFToInsert.index)))
    DFToInsert.loc[:, ('checked')] = list(
        itertools.repeat(0, len(DFToInsert.index)))
    DFToInsert.loc[:, ('imported')] = list(
        itertools.repeat(0, len(DFToInsert.index)))
    DFToInsert.loc[:, ('creationDate')] = list(
        itertools.repeat(datetime.now(), len(DFToInsert.index)))

    return DFToInsert


def parseDIAGFileAndInsert(full_filename, session, importID):

    # PArse DIAG File with Regex
    with open(full_filename, 'r') as f:
        content = f.read()
        content = re.sub('\\s+Prog+\\s\\d{5}', "", content)
        content2 = re.sub('[\n\r]\\s{10,14}[0-9A-F\\s]+[\n\r]', "\n", content)
        content2 = re.sub('[\n\r]\\s{10,14}[0-9A-F\\s]+$', "\n", content2)
        content2 = re.sub('^[\n\r\\s]+', "", content2)
        content2 = re.sub('[\n\r\\s]+$', "", content2)
        splitBlock = 'm[\n\r]'
        blockPosition = re.split(splitBlock, content2)

    colsInBlock = ['FK_ptt', 'date', 'lc', 'iq', 'lat1',
                   'lon1', 'lat2', 'lon2', 'nbMsg',
                   'nbMsg120', 'bestLevel', 'passDuration',
                   'nopc', 'freq', 'ele']
    ListOfdictParams = []

    for block in blockPosition:
        block = re.sub('[\n\r]+', "", block)
        # block = re.sub('[a-zA-VX-Z]\s+Lat'," Lat",block)
        # block = re.sub('[a-zA-DF-Z]\s+Lon'," Lon",block)
        block = re.sub('[a-zA-Z]\\s+Nb', " Nb", block)
        block = re.sub('[a-zA-Z]\\s+NOPC', " NOPC", block)
        block = re.sub('IQ', "#IQ", block)
        # block = re.sub('[a-zA-Z]\s[a-zA-Z]',"O",block)
        # print(block)
        split = '\\#?[a-zA-Z0-9\\-\\>]+\\s:\\s'
        splitParameters = re.split(split, block)
        curDict = {}

        for i in range(len(splitParameters)):
            if re.search('[?]+([a-zA-Z]+)?', splitParameters[i]):
                splitParameters[i] = re.sub(
                    '[?]+([a-zA-Z]{1,2})?', "NaN", splitParameters[i])
            if re.search('[0-9]', splitParameters[i]):
                splitParameters[i] = re.sub(
                    '[a-zA-DF-MO-RT-VX-Z]', " ", splitParameters[i])
            if colsInBlock[i] == 'date':
                curDict[colsInBlock[i]] = datetime.strptime(
                    splitParameters[i], '%d.%m.%y %H:%M:%S ')
            else:
                try:
                    splitParameters[i] = re.sub(
                        '[\\s]', " ", splitParameters[i])
                    a = 1
                    if colsInBlock[i] in ['lon1', 'lon2', 'lat1', 'lat2']:
                        if (
                            'W' in splitParameters[i]
                            or
                            'S' in splitParameters[i]
                        ):
                            a = -1
                        splitParameters[i] = re.sub(
                            '[a-zA-Z]', " ", splitParameters[i])
                    curDict[colsInBlock[i]] = a * float(splitParameters[i])
                except Exception:
                    try:
                        splitParameters[i] = re.sub(
                            '[a-zA-Z]', " ", splitParameters[i])
                        curDict[colsInBlock[i]] = int(splitParameters[i])
                    except Exception:
                        if re.search('\\s{1,10}', splitParameters[i]):
                            splitParameters[i] = None
                        curDict[colsInBlock[i]] = splitParameters[i]
        ListOfdictParams.append(curDict)

    # Load parsed value into DataFrame
    df = pd.DataFrame.from_dict(ListOfdictParams)
    df = df.dropna(subset=['date'])
    DFToInsert = checkExistingArgos(df, session)
    DFToInsert.loc[:, ('type')] = list(
        itertools.repeat('Argos', len(DFToInsert.index)))
    DFToInsert.loc[:, ('checked')] = list(
        itertools.repeat(0, len(DFToInsert.index)))
    DFToInsert.loc[:, ('imported')] = list(
        itertools.repeat(0, len(DFToInsert.index)))
    DFToInsert.loc[:, ('creationDate')] = list(
        itertools.repeat(datetime.now(), len(DFToInsert.index)))
    DFToInsert = DFToInsert.drop(['id', 'lat1', 'lat2', 'lon1', 'lon2'], 1)
    DFToInsert.loc[:, ('FK_Import')] = list(
        itertools.repeat(importID, len(DFToInsert.index))
    )

    if DFToInsert.shape[0] != 0:
        DFToInsert.to_sql(
            ArgosGps.__table__.name,
            session.get_bind(ArgosGps),
            if_exists='append',
            schema=dbConfig['sensor_schema'],
            index=False
        )
    os.remove(full_filename)

    message = {
        'inserted gps': 0,
        'existing gps': 0,
        'inserted Engineering': 0,
        'existing Engineering': 0,
        'inserted argos': DFToInsert.shape[0],
        'existing argos': df.shape[0] - DFToInsert.shape[0]
    }

    nbRows = df.shape[0]
    nbInserted = DFToInsert.shape[0]
    maxDate = df['date'].max()
    minDate = df['date'].min()
    return message, nbRows, nbInserted, maxDate, minDate


def checkExistingArgos(dfToCheck, session):
    dfToCheck['id'] = range(dfToCheck.shape[0])
    dfToCheck.loc[:, ('lat')] = dfToCheck['lat1'].astype(float)
    dfToCheck.loc[:, ('lon')] = dfToCheck['lon1'].astype(float)
    maxDate = dfToCheck['date'].max()
    minDate = dfToCheck['date'].min()

    dfToCheck['lat2'] = dfToCheck['lat'].round(3)
    dfToCheck['lon2'] = dfToCheck['lon'].round(3)
    # Retrieve data from DB
    queryArgos = select([ArgosGps.pk_id,
                         ArgosGps.date,
                         ArgosGps.lat,
                         ArgosGps.lon,
                         ArgosGps.ptt]
                        ).where(ArgosGps.type_ == 'Argos')
    queryArgos = queryArgos.where(
        and_(ArgosGps.date >= minDate, ArgosGps.date <= maxDate))
    data = session.execute(queryArgos).fetchall()
    session.commit()

    # load data from Db into DF
    ArgosRecords = pd.DataFrame.from_records(
        data,
        columns=[ArgosGps.pk_id.name,
                 ArgosGps.date.name,
                 ArgosGps.lat.name,
                 ArgosGps.lon.name,
                 ArgosGps.ptt.name],
        coerce_float=True)

    ArgosRecords.loc[:, ('lat')] = ArgosRecords['lat'].round(3)
    ArgosRecords.loc[:, ('lon')] = ArgosRecords['lon'].round(3)

    merge = pd.merge(dfToCheck,
                     ArgosRecords,
                     left_on=['date', 'lat2', 'lon2', 'FK_ptt'],
                     right_on=['date', 'lat', 'lon', 'FK_ptt'])
    DFToInsert = dfToCheck[~dfToCheck['id'].isin(merge['id'])]

    return DFToInsert
