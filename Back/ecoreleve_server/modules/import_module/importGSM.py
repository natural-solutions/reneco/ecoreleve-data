from sqlalchemy import select
from traceback import print_exc
import pandas as pd
import re
import datetime
import itertools
import uuid

from ecoreleve_server.core import dbConfig
from ..sensors.sensor_data import Gsm, GsmEngineering
from .import_model import Import
from pyramid.httpexceptions import (
    HTTPConflict
)


def uploadFilesGSM(request):
    # Import unchecked GSM data.
    session = request.dbsession
    response = {
        'valid gps submitted': 0,
        'inserted gps': 0,
        'existing gps': 0,
        'valid engineering submitted': 0,
        'inserted engineering': 0,
        'existing engineering': 0
    }
    user = request.authenticated_userid['sub']

    # detect if is a row file retrieve directly from mail
    ptt_pattern = re.compile('[0]*(?P<platform>[0-9]+)g')
    eng_pattern = re.compile('[0]*(?P<platform>[0-9]+)e')

    # detect if is concatenated file retrieve from exctract GSM python software
    ALL_ptt_pattern = re.compile('GPS')
    ALL_eng_pattern = re.compile('Engineering')

    tran = None
    with session.get_bind(Gsm).connect() as connection:
        try:
            # The idea of the import
            # create Import object
            # create empty dataframe with good schema
            # create dataframe from csv
            # alter the csv_data for matching 'empty' dataframe
            # insert all rows for csv to 'empty'
            # insert all rows from 'empty' to temp table
            # insert all rows not existing in prod table
            tran = connection.begin()
            file_obj = request.POST['file']
            filename = request.POST['file'].filename
            temporary_table_name = f'#gms_import_{uuid.uuid4()}'

            stmt = f"""
                INSERT INTO [Import]
                ([ImportFileName],[ImportType],[FK_User])
                OUTPUT INSERTED.ID
                VALUES
                ('{filename}','GSM',{user})
            """
            result = connection.execute(stmt)
            imported_id = result.fetchone()[0]


            # Create empty dataframe with the 'good' schema
            # the order of columns must match the order of sql table columns
            # because we gonna create a temporary table with raw data
            # then insert all non existing row in the matching table
            # with a "insert into [table] select * from #temp"
            # order MUST match
            tgsm_dataframe = pd.DataFrame(
                {
                    'platform_': pd.Series(dtype='int'),
                    'DateTime': pd.to_datetime(
                        pd.Series(dtype='str'),
                        format='%Y-%m-%d %H:%M:%S'
                    ),
                    'Latitude_N': pd.Series(dtype='float64'),
                    'Longitude_E': pd.Series(dtype='float64'),
                    'Speed': pd.Series(dtype='int'),
                    'Course': pd.Series(dtype='int'),
                    'Altitude_m': pd.Series(dtype='int'),
                    'HDOP': pd.Series(dtype='float64'),
                    'VDOP': pd.Series(dtype='float64'),
                    'SatelliteCount': pd.Series(dtype='int'),
                    'file_date': pd.to_datetime(
                        pd.Series(dtype='str'),
                        format='%Y-%m-%d'
                    ),
                    'checked': pd.Series(dtype='bool'),
                    'imported': pd.Series(dtype='bool'),
                    'validated': pd.Series(dtype='bool'),
                    'FK_Import': pd.Series(dtype='int')
                }
            )
            tgsm_dataframe.set_index(['platform_', 'DateTime'])

            tengineering_gsm_dataframe = pd.DataFrame(
                {
                    'DateTime': pd.to_datetime(
                        pd.Series(dtype='str'),
                        format='%Y-%m-%d %H:%M:%S'
                    ),
                    'Temperature_C': pd.Series(dtype='float64'),
                    'BatteryVoltage_V': pd.Series(dtype='float64'),
                    'ActivityCount': pd.Series(dtype='int'),
                    'platform_': pd.Series(dtype='int'),
                    'file_date': pd.to_datetime(
                        pd.Series(dtype='str'),
                        format='%Y-%m-%d'
                    ),
                    'FK_Import': pd.Series(dtype='int')
                }
            )
            tengineering_gsm_dataframe.set_index(['platform_', 'DateTime'])

            if ALL_ptt_pattern.search(filename) or ptt_pattern.search(filename):
                if ALL_ptt_pattern.search(filename):
                    tgsm_dataframe = get_ALL_gps_toInsert(
                        file_obj=file_obj,
                        tgsm_dataframe=tgsm_dataframe,
                        FK_import=imported_id
                    )
                if ptt_pattern.search(filename):
                    tgsm_dataframe = get_gps_toInsert(
                        file_obj=file_obj,
                        tgsm_dataframe=tgsm_dataframe,
                        FK_import=imported_id
                    )
                tgsm_dataframe.to_sql(
                    name=temporary_table_name,
                    con=connection,
                    index=False
                )

                nb_rows = connection.execute(f'SELECT count(*) FROM [{temporary_table_name}];').scalar()
                stmt = f'''
                    INSERT INTO [{Gsm.__tablename__}] (
                    [platform_],
                    [DateTime],
                    [Latitude_N],
                    [Longitude_E],
                    [Speed],
                    [Course],
                    Altitude_m],
                    [HDOP],
                    [VDOP],
                    [SatelliteCount],
                    [file_date],
                    [checked],
                    [imported],
                    [validated],
                    [FK_Import]
                    )
                    SELECT
                    [platform_],
                    [DateTime],
                    [Latitude_N],
                    [Longitude_E],
                    [Speed],
                    [Course],
                    [Altitude_m],
                    [HDOP],
                    [VDOP],
                    [SatelliteCount],
                    [file_date],
                    [checked],
                    [imported],
                    [validated],
                    [FK_Import]
                    FROM [{temporary_table_name}] AS T1
                    WHERE NOT EXISTS(
                        SELECT 1
                        FROM [{Gsm.__tablename__}] AS T2
                        WHERE
                        T2.[platform_] = T1.[platform_]
                        AND
                        T2.[DateTime] = T1.[DateTime]
                    )
                '''
                result = connection.execute(stmt)
                nb_inserted = result.rowcount

                max_date = tgsm_dataframe['DateTime'].max()
                min_date = tgsm_dataframe['DateTime'].min()

                update_stmt = f"""
                    UPDATE [Import]
                    SET [minDate] = '{min_date.isoformat()}',
                    [maxDate] = '{max_date.isoformat()}',
                    [nbInserted] = {nb_inserted},
                    [nbRows] = {nb_rows}
                    WHERE ID = {imported_id}
                """
                connection.execute(update_stmt)
                response['valid gps submitted'] = nb_rows
                response['inserted gps'] = nb_inserted
                response['existing gps'] = (nb_rows - nb_inserted)

            if ALL_eng_pattern.search(filename) or eng_pattern.search(filename):
                if ALL_eng_pattern.search(filename):
                    tengineering_gsm_dataframe = get_ALL_eng_toInsert(
                        file_obj=file_obj,
                        tengineering_gsm_dataframe=tengineering_gsm_dataframe,
                        FK_import=imported_id
                    )
                if eng_pattern.search(filename):
                    tengineering_gsm_dataframe = get_eng_toInsert(
                        file_obj=file_obj,
                        tengineering_gsm_dataframe=tengineering_gsm_dataframe,
                        FK_import=imported_id
                    )
                tengineering_gsm_dataframe.to_sql(
                    name=temporary_table_name,
                    con=connection,
                    index=False
                )

                nb_rows = connection.execute(f'SELECT count(*) FROM [{temporary_table_name}];').scalar()
                stmt = f'''
                    INSERT INTO [{GsmEngineering.__tablename__}] (
                    [DateTime],
                    [Temperature_C],
                    [BatteryVoltage_V],
                    [ActivityCount],
                    [platform_],
                    [file_date],
                    [FK_Import]
                    )
                    SELECT
                    [DateTime],
                    [Temperature_C],
                    [BatteryVoltage_V],
                    [ActivityCount],
                    [platform_],
                    [file_date],
                    [FK_Import]
                    FROM [{temporary_table_name}] AS T1
                    WHERE NOT EXISTS(
                        SELECT 1
                        FROM [{GsmEngineering.__tablename__}] AS T2
                        WHERE
                        T2.[platform_] = T1.[platform_]
                        AND
                        T2.[DateTime] = T1.[DateTime]
                    )
                '''
                result = connection.execute(stmt)
                nb_inserted = result.rowcount

                max_date = tengineering_gsm_dataframe['DateTime'].max()
                min_date = tengineering_gsm_dataframe['DateTime'].min()

                update_stmt = f"""
                    UPDATE [Import]
                    SET [minDate] = '{min_date.isoformat()}',
                    [maxDate] = '{max_date.isoformat()}',
                    [nbInserted] = {nb_inserted},
                    [nbRows] = {nb_rows}
                    WHERE ID = {imported_id}
                """
                connection.execute(update_stmt)
                response['valid engineering submitted'] = nb_rows
                response['inserted engineering'] = nb_inserted
                response['existing engineering'] = (nb_rows - nb_inserted)

        except Exception as e:
            tran.rollback()
            raise HTTPConflict(
                headers={
                    "content_type": 'application/json',
                    "charset": 'utf-8',
                },
                body=f'{e}'
            )

    return response


def get_ALL_gps_toInsert(file_obj, tgsm_dataframe, FK_import):
    # Load raw csv data
    csv_data = pd.read_csv(
        file_obj.file,
        sep='\t',
        index_col=['DateTime'],
        parse_dates=['DateTime', 'file_date'],
        # date_parser=column_datetime_parser,
        na_values=['No Fix', 'Batt Drain', 'Low Voltage']
    )

    # Remove the lines containing NaN
    csv_data.dropna(inplace=True)
    # set nan for value with 'NegAlt', '2D Fix'
    csv_data['Altitude_m'] = pd.to_numeric(csv_data['Altitude_m'], errors='coerce')
    # add columns
    csv_data.rename(columns={'GSM_ID': 'platform_'}, inplace=True)
    csv_data['FK_Import'] = FK_import
    csv_data['checked'] = 0
    csv_data['imported'] = 0
    csv_data['validated'] = 0
    # reset index
    csv_data.reset_index(inplace=True)
    csv_data.set_index(['platform_', 'DateTime'])
    columns = tgsm_dataframe.columns
    tgsm_dataframe = tgsm_dataframe.append(csv_data, ignore_index=True, sort=False)
    tgsm_dataframe = tgsm_dataframe.reindex(columns=columns)

    return tgsm_dataframe


def get_gps_toInsert(file_obj, tgsm_dataframe, FK_import):
    platform_ = None
    ptt_pattern = re.compile('[0]*(?P<platform>[0-9]+)g')
    platform_ = int(ptt_pattern.search(file_obj.filename).group('platform'))

    csv_data = pd.read_csv(
        file_obj.file,
        sep='\t',
        index_col=['DateTime'],
        parse_dates=['DateTime'],
        na_values=['No Fix', 'Batt Drain', 'Low Voltage']
    )
    # Remove the lines containing NaN
    csv_data.dropna(inplace=True)
    # set nan for value with 'NegAlt', '2D Fix'
    csv_data['Altitude_m'] = pd.to_numeric(csv_data['Altitude_m'], errors='coerce')
    # add columns
    csv_data['platform_'] = platform_
    csv_data['FK_Import'] = FK_import
    csv_data['checked'] = 0
    csv_data['imported'] = 0
    csv_data['validated'] = 0
    # reset index
    csv_data.reset_index(inplace=True)
    csv_data.set_index(['platform_', 'DateTime'])
    columns = tgsm_dataframe.columns
    tgsm_dataframe = tgsm_dataframe.append(csv_data, ignore_index=True, sort=False)
    tgsm_dataframe = tgsm_dataframe.reindex(columns=columns)

    return tgsm_dataframe


def get_eng_toInsert(file_obj, tengineering_gsm_dataframe, FK_import):
    filename = file_obj.filename
    eng_pattern = re.compile('[0]*(?P<platform>[0-9]+)e')
    platform_ = int(eng_pattern.search(filename).group('platform'))
    # Load raw csv data
    csv_data = pd.read_csv(
        file_obj.file,
        sep='\t',
        index_col=['DateTime'],
        parse_dates=['DateTime']
    )
    # Remove the lines containing NaN
    csv_data.dropna(inplace=True)
    # add columns
    csv_data['platform_'] = platform_
    csv_data['FK_Import'] = FK_import
    # reset index
    csv_data.reset_index(inplace=True)
    csv_data.set_index(['platform_', 'DateTime'])
    columns = tengineering_gsm_dataframe.columns
    tengineering_gsm_dataframe = tengineering_gsm_dataframe.append(
        csv_data,
        ignore_index=True,
        sort=False
    )
    tengineering_gsm_dataframe = tengineering_gsm_dataframe.reindex(
        columns=columns
    )

    return tengineering_gsm_dataframe


def get_ALL_eng_toInsert(file_obj, tengineering_gsm_dataframe, FK_import):
    # Load raw csv data
    csv_data = pd.read_csv(
        file_obj.file,
        sep='\t',
        index_col=['DateTime'],
        parse_dates=['DateTime', 'file_date']
    )
    # Remove the lines containing NaN
    csv_data.dropna(inplace=True)
    # add columns
    csv_data.rename(columns={'GSM_ID': 'platform_'}, inplace=True)
    csv_data['FK_Import'] = FK_import
    # reset index
    csv_data.reset_index(inplace=True)
    csv_data.set_index(['platform_', 'DateTime'])
    columns = tengineering_gsm_dataframe.columns
    tengineering_gsm_dataframe = tengineering_gsm_dataframe.append(
        csv_data,
        ignore_index=True,
        sort=False
    )
    tengineering_gsm_dataframe = tengineering_gsm_dataframe.reindex(columns=columns)

    return tengineering_gsm_dataframe
