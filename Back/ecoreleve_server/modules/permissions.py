from pyramid.security import (
    Allow,
    ALL_PERMISSIONS,
    Deny
)
###
# RTFM!!!!!!!
# https://docs.pylonsproject.org/projects/pyramid/en/latest/narr/security.html#elements-of-an-acl
#
# SO and acl = (A,B,C)
# A could be Allow or Deny
# B is a principal
# C is the persmission or sequence of permissions
# example:
# (Allow, 'group:admin' , ('create','update','read') )
# means IF your principal is 'group:admin'
# you are ALLOWED to (create, update, read)
###


# NEED A TRUE REFACT (
# that's not just autorization access for ressources we need a global vision
# for now we gonna create a "special" action 'fixForOld'
# )

context_permissions = {
    'dashboard': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', 'read'),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'export': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', 'read'),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'fieldActivities': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', 'read'),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'importHistory': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', 'read'),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'individuals': [
        (Allow, 'group:admin', ('create', 'read', 'update')),
        (Allow, 'group:superUser', ('update', 'read')),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'individuals_history': [
        (Allow, 'group:admin', ('read', 'update', 'delete')),
        (Allow, 'group:superUser', ('read', 'update', 'delete')),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'individual_locations': [
        (Allow, 'group:admin', ('read', 'update', 'delete')),
        (Deny, 'group:superUser', ('update', 'delete')),
        (Allow, 'group:superUser', ('read')),
        (Allow, 'group:user', ('read')),
        (Allow, 'group:guest', 'read')
    ],
    'monitoredSites': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ('create', 'read', 'update')),
        (Allow, 'group:user', ('create', 'read', 'update')),
        (Allow, 'group:guest', 'read')
    ],
    'sensors': [
        (Allow, 'group:admin', ('create', 'read', 'update')),
        (Allow, 'group:superUser', ('create', 'read', 'update')),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'sensors_history': [
        (Allow, 'group:admin', ('read', 'update', 'delete')),
        (Allow, 'group:superUser', ('read', 'update', 'delete')),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
    'SensorDatasByType': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ALL_PERMISSIONS),
        (Allow, 'group:user', ('read', 'create', 'update')),
        (Allow, 'group:guest', 'read')
    ],
    'release': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ALL_PERMISSIONS),
        (Deny, 'group:user', ALL_PERMISSIONS),
        (Allow, 'group:guest', 'read')
    ],
    'regions': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ('create', 'read', 'update')),
        (Allow, 'group:user', ('read')),
        (Allow, 'group:guest', 'read')
    ],
    'import': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ('create', 'read', 'update')),
        (Allow, 'group:user', ('create', 'read', 'update')),
        (Allow, 'group:guest', 'read')
    ],
    'stations': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (
            Allow,
            'group:superUser',
            (
                'create',
                'read',
                'update',
                'delete_station_cam'
            )
        ),
        (
            Allow,
            'group:user',
            (
                'create',
                'read',
                'update',
                'delete_station_cam'
            )
        ),
        (Allow, 'group:guest', 'read')
    ],
    'observations': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ALL_PERMISSIONS),
        (Allow, 'group:user', ALL_PERMISSIONS),
        (Allow, 'group:guest', 'read')
    ],
    'mediasfiles': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ALL_PERMISSIONS),
        (Allow, 'group:user', ALL_PERMISSIONS),
        (Allow, 'group:guest', 'read')
    ],
    'formbuilder': [
        (Allow, 'group:admin', ALL_PERMISSIONS),
        (Allow, 'group:superUser', ('create', 'update', 'read')),
        (Allow, 'group:user', 'read'),
        (Allow, 'group:guest', 'read')
    ],
}


class Default_Context_Factory:

    def __init__(self, request):
        self.request = request

    @property
    def __acl__(self):
        return []


class Autocomplete_Resource(Default_Context_Factory):
    def __acl__(self):
        return [
            (Allow, 'group:admin', 'read'),
            (Allow, 'group:superUser', 'read'),
            (Allow, 'group:user', 'read'),
            (Allow, 'group:guest', 'read')
        ]


class Statistics_Resource(Default_Context_Factory):
    def __acl__(self):
        return [
            (Allow, 'group:admin', 'read'),
            (Allow, 'group:superUser', 'read'),
            (Allow, 'group:user', 'read'),
            (Allow, 'group:guest', 'read')
        ]


class Users_Resource(Default_Context_Factory):
    def __acl__(self):
        return [
            (Allow, 'group:admin', 'read'),
            (Allow, 'group:superUser', 'read'),
            (Allow, 'group:user', 'read'),
            (Allow, 'group:guest', 'read')
        ]


class Sensors_Resource(Default_Context_Factory):

    def __acl__(self):
        toRet = []
        try:
            sensor_type = self.request.matchdict.get('type', None)
            if sensor_type == 'rfid':
                toRet = [
                    (Allow, 'group:admin', 'create'),
                    (Allow, 'group:superUser', 'create'),
                    (Allow, 'group:user', 'create'),
                    (Deny, 'group:guest', 'create')
                ]
            if sensor_type == 'gsm':
                toRet = [
                    (Allow, 'group:admin', 'create'),
                    (Allow, 'group:superUser', 'create'),
                    (Allow, 'group:user', 'create'),
                    (Deny, 'group:guest', 'create')
                ]
            if sensor_type == 'argos':
                toRet = [
                    (Allow, 'group:admin', 'create'),
                    (Allow, 'group:superUser', 'create'),
                    (Deny, 'group:user', 'create'),
                    (Deny, 'group:guest', 'create')
                ]
            if sensor_type == 'gpx':
                toRet = [
                    (Allow, 'group:admin', 'create'),
                    (Allow, 'group:superUser', 'create'),
                    (Allow, 'group:user', 'create'),
                    (Deny, 'group:guest', 'create')
                ]
        except AttributeError:
            pass
        finally:
            return toRet


class CameraTrap_Resource(Default_Context_Factory):
    def __acl__(self):
        return [
            (Allow, 'group:admin', 'read'),
            (Allow, 'group:superUser', 'read'),
            (Allow, 'group:user', 'read'),
            (Allow, 'group:guest', 'read')
        ]
