from ecoreleve_server.core.init_db import dbConfig
from ecoreleve_server.modules.permissions import (
    Autocomplete_Resource,
    Users_Resource,
    Statistics_Resource,
    Sensors_Resource,
    CameraTrap_Resource
)


def add_routes(config):

    with config.route_prefix_context(dbConfig.get('prefixapi')):

        config.add_route(
            'weekData',
            '/weekData',
            factory=Statistics_Resource
        )
        config.add_route(
            'location_graph',
            '/individuals/location/graph',
            factory=Statistics_Resource
        )
        config.add_route(
            'station_graph',
            '/stations/graph',
            factory=Statistics_Resource
        )
        config.add_route(
            'individual_graph',
            '/stats/individuals/graph',
            factory=Statistics_Resource
        )
        config.add_route(
            'individual_monitored',
            '/stats/individuals/monitored/graph',
            factory=Statistics_Resource
        )
        config.add_route(
            'uncheckedDatas_graph',
            '/sensor/uncheckedDatas/graph',
            factory=Statistics_Resource
        )

        # Security routes
        config.add_route(
            'security/logout',
            '/security/logout'
        )
        config.add_route(
            'security/has_access',
            '/security/has_access'
        )

        # User
        config.add_route(
            'users/id',
            '/users/{id}',
            factory=Users_Resource
        )
        config.add_route(
            'core/user',
            '/user',
            factory=Users_Resource
        )
        config.add_route(
            'core/currentUser',
            '/currentUser',
            factory=Users_Resource
        )

        config.add_route(
            'autocomplete',
            '/autocomplete/{obj}/{prop}',
            factory=Autocomplete_Resource
        )
        config.add_route(
            'autocomplete/ID',
            '/autocomplete/{obj}/{prop}/{valReturn}',
            factory=Autocomplete_Resource
        )

        # Sensors datas (Argos + GSM + RFID)
        config.add_route(
            'sensors/datas',
            '/sensors/{type}/datas',
            factory=Sensors_Resource
        )

        config.add_route(
            'sensors/statut',
            '/sensors/{type}/statut'
        )

        config.add_route(
            'cameratrap',
            '/photos/',
            factory=CameraTrap_Resource
        )
        config.add_route(
            'getSessionZip',
            '/photos/export/',
            factory=CameraTrap_Resource
        )
