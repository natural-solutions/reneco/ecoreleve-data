from sqlalchemy import Column, Integer, Sequence, String, Numeric, DateTime
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import func
from shapely.wkt import loads
from geojson import Feature

from ecoreleve_server.core import Main_Db_Base
from ecoreleve_server.core.base_types import Geometry


class AdministrativeArea(Main_Db_Base):
    __tablename__ = 'AdministrativeArea'
    ID = Column(Integer, Sequence('PK_AdministrativeArea'), primary_key=True)
    Country = Column(String(255))
    Department = Column(String(255))
    Municipality = Column(String(255))
    Name = Column(String(255))
    fullpath = Column(String(255))
    type_ = Column(String(50))
    max_lat = Column(Numeric(9, 5))
    min_lat = Column(Numeric(9, 5))
    max_lon = Column(Numeric(9, 5))
    min_lon = Column(Numeric(9, 5))
    SHAPE_Leng = Column(Numeric(19, 6))
    SHAPE_Area = Column(Numeric(19, 6))
    valid_geom = Column(Geometry)
    geom = Column(Geometry)
    date_modif = Column(DateTime)
    Centroid_Latitude = Column(Numeric(9, 5))
    Centroid_Longitude = Column(Numeric(9, 5))

    __mapper_args__ = {
        'version_id_col': ID,
        'version_id_generator': False,
    }
class GeomaticLayer(Main_Db_Base):

    __tablename__ = 'GeomaticLayer'
    ID = Column(Integer, Sequence('GeomaticLayer__id_seq'), primary_key=True)
    Name = Column(String(255))
    geom = Column(Geometry)
    type_ = Column(String(25))

    __mapper_args__ = {
        'version_id_col': ID,
        'version_id_generator': False,
    }

    @hybrid_property
    def geom_WKT(self):
        return func.geo.wkt(self.geom)

    @geom_WKT.expression
    def geom_WKT(cls):
        return func.geo.wkt(cls.geom)

    @geom_WKT.setter
    def geom_WKT(cls):
        return func.geo.wkt(cls.geom)

    @hybrid_property
    def geom_json(self):
        return Feature(
            id=self.ID,
            geometry=loads(self.geom),
            properties={"name": self.Name}
        )


class Region(Main_Db_Base):

    __tablename__ = 'Region'
    ID = Column(Integer, Sequence(
        'Region__id_seq'), primary_key=True)
    fullpath = Column(String(255))
    Country = Column(String(255))
    Area = Column(String(255))
    Region = Column(String(255))
    Subregion = Column(String(255))
    geom = Column(Geometry)
    valid_geom = Column(Geometry)

    __mapper_args__ = {
        'version_id_col': ID,
        'version_id_generator': False,
    }

    @hybrid_property
    def geom_WKT(self):
        return func.geo.wkt(self.valid_geom)

    @geom_WKT.expression
    def geom_WKT(cls):
        return func.geo.wkt(cls.valid_geom)

    @geom_WKT.setter
    def geom_WKT(cls):
        return func.geo.wkt(cls.valid_geom)

    @hybrid_property
    def geom_json(self):
        return Feature(
            id=self.ID,
            geometry=loads(self.valid_geom),
            properties={'fullpath': self.fullpath,
                        'Country': self.Country,
                        'W_Area': self.Area,
                        'W_Region': self.Region,
                        'Mgmt_Unit': self.Subregion
                        }
        )


class FieldworkArea(Main_Db_Base):

    __tablename__ = 'FieldworkArea'
    ID = Column(Integer, Sequence(
        'FieldworkArea__id_seq'), primary_key=True)
    Working_Area = Column(String(255))
    Working_Region = Column(String(255))
    Management_Unit = Column(String(255))
    Name = Column(String(255))
    type_ = Column(String(50))
    fullpath = Column(String(255))
    valid_geom = Column(Geometry)
    geom = Column(Geometry)

    __mapper_args__ = {
        'version_id_col': ID,
        'version_id_generator': False,
    }