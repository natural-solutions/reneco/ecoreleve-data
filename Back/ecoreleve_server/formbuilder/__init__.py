from ecoreleve_server.formbuilder.formbuilder_ressource import (
    FormBuilderRessource
)


def root_factory_formbuilder(request):

    return FormBuilderRessource('', None, request)
