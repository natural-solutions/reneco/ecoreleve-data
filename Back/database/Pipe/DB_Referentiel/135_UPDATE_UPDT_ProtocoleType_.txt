ALTER PROCEDURE [dbo].[UPDT_ProtocoleType]
	@IDFormFormBuilder INT,
	@Debug INT = 0
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN

		DECLARE @formbuilderDBNAME VARCHAR(64)
		SET @formbuilderDBNAME = 'FormBuilder_DEV_Form_InitialID'

		IF @Debug = 1
			BEGIN
				PRINT(CONCAT('UPDT_ProtocoleType Start for @IDFormFormBuilder : ',@IDFormFormBuilder))
			END

		IF OBJECT_ID('tempdb..#ProtocoleTypeTMP') IS NOT NULL
			BEGIN
				DROP TABLE #ProtocoleTypeTMP
			END

		CREATE TABLE #ProtocoleTypeTMP (
			[ID] [int] NULL,
			[Name] [nvarchar](250) NULL,
			[Status] [int] NULL,
			[OriginalId] [nvarchar](250) NULL,
			[obsolete] [bit] NULL
			)

		INSERT INTO #ProtocoleTypeTMP([Name],[Status],[OriginalId],[obsolete])
		SELECT
		PIVOTTABLE.[FormName] AS [Name],
		CASE
			WHEN ( PIVOTTABLE.[issubprotocol] = '0' AND PIVOTTABLE.[isgrid] = '0' ) THEN 1
			WHEN ( PIVOTTABLE.[issubprotocol] = '0' AND PIVOTTABLE.[isgrid] = '1' ) THEN 2
			WHEN ( PIVOTTABLE.[issubprotocol] = '1' AND PIVOTTABLE.[isgrid] = '0' ) THEN 3
			WHEN ( PIVOTTABLE.[issubprotocol] = '1' AND PIVOTTABLE.[isgrid] = '1' ) THEN 4
		END AS [Status],
		CONCAT(@formbuilderDBNAME, '_',PIVOTTABLE.[initialID]) AS [OriginalID],
		PIVOTTABLE.[obsolete] AS [obsolete]
		FROM
		(
		SELECT
		F.[pk_Form],
		F.[name] AS FormName,
		F.[creationDate],
		F.[modificationDate],
		F.[curStatus],
		F.[obsolete],
		F.[context],
		F.[originalID],
		F.[propagate],
		F.[state],
		F.[initialID],
		CONVERT(VARCHAR(max) , FP.[name])  AS FormPropertyName,
		CONVERT (VARCHAR(max), CASE WHEN FP.[value] = '' THEN NULL ELSE FP.[value] END) AS FormPropertyValue
		FROM [syn_Formbuilder.Form] AS F
		JOIN [syn_Formbuilder.FormProperty] AS FP ON F.[pk_Form] = FP.[fk_Form]
		WHERE
		F.[pk_Form] = @IDFormFormBuilder
		) AS SOURCETABLE
		PIVOT
		(
		MAX([FormPropertyValue])
		FOR [FormPropertyName] IN ([actif],[author],[defaultforfieldactivity],[hideprotocolname],[isgrid],[isGridRanged],[issubprotocol],[nbFixedCol]) --list of rows name you want in columns
		) AS PIVOTTABLE


		IF @Debug = 1
			BEGIN
				SELECT 'DATA TO MERGE'

				SELECT * FROM #ProtocoleTypeTMP

				SELECT 'WITH'
				--request from with
				SELECT
				P.*
				FROM [protocoleType] AS P
				JOIN [syn_Formbuilder.Form] AS F ON P.[OriginalId] = CONCAT(@formbuilderDBNAME,'_', F.[initialID])
				WHERE F.[pk_Form] = @IDFormFormBuilder

			END

		---- MERGE TABLE ---
		--1) CREATE TABLE FROM MERGING
		IF OBJECT_ID('tempdb..#tempMergeForm') IS NOT NULL
			BEGIN
				DROP TABLE #tempMergeForm
			END

		CREATE TABLE #tempMergeForm(
		[action] VARCHAR(MAX),
		[SourceId] INT,
		[sourceName] VARCHAR(MAX),
		[sourceStatus] VARCHAR(MAX),
		[sourceOriginalId] VARCHAR(MAX),
		[sourceObsolete] BIT,
		[TargetId] INT,
		[TargetName] VARCHAR(MAX),
		[TargetStatus] VARCHAR(MAX),
		[TargetOriginalId] VARCHAR(MAX),
		[TargetObsolete] BIT,
		);


		-- FILTER YOUR TARGET RESOURCE
		WITH ProtocoleTypeFiltered AS ( --filtered proto if new proto from fb request return 0 row ===> insert from SOURCE
			SELECT
			P.*
			FROM [protocoleType] AS P
			JOIN [syn_Formbuilder.Form] AS F ON P.[OriginalId] = CONCAT(@formbuilderDBNAME, '_', F.[initialID])
			WHERE F.[pk_Form] = @IDFormFormBuilder
		)


		MERGE  ProtocoleTypeFiltered AS [TARGET]
		USING #ProtocoleTypeTMP AS [SOURCE]
		ON ([TARGET].[OriginalId] = [SOURCE].[originalID] ) -- name proto is unique

		WHEN MATCHED -- if matched present in db we update originalid then output the id for source
			THEN
				UPDATE SET
				-- [TARGET].[Name] = [SOURCE].[Name], we can't update name
				[TARGET].[Status] = [SOURCE].[Status],
				[TARGET].[obsolete] = [SOURCE].[obsolete]

		WHEN NOT MATCHED BY TARGET THEN --case when new proto from fb
		INSERT ([Name],[Status],[OriginalId],[obsolete])
		VALUES ([SOURCE].[Name],[SOURCE].[Status],[SOURCE].[OriginalId],[SOURCE].[obsolete])

		WHEN NOT MATCHED BY SOURCE THEN -- case when proto deleted in fb
		DELETE

		OUTPUT $action AS [action],
		INSERTED.[ID] AS [SOURCEId],
		INSERTED.[name] AS [sourceName],
		INSERTED.[Status]  AS [sourceStatus],
		INSERTED.[OriginalId] AS [sourceOriginalId],
		INSERTED.[obsolete] AS [sourceObsolete],
		DELETED.[ID] AS [TargetId],
		DELETED.[name] AS [TargetName],
		DELETED.[Status]  AS [TargetStatus],
		DELETED.[OriginalId] AS [TargetOriginalId],
		DELETED.[obsolete] AS [TargetObsolete]
		INTO #tempMergeForm([action],[SourceId],[sourceName],[sourceStatus],[sourceOriginalId],[sourceObsolete],[TargetId],[TargetName],[TargetStatus],[TargetOriginalId],[TargetObsolete]);


		IF @Debug = 1
			BEGIN
				SELECT 'RESULT MERGE'

				SELECT * FROM #tempMergeForm
			END
		-- on edite le formulaire du formbuilder pour lui donner l'id du protocole dans ecoreleve
		-- sera reprit dans la suite du processus pour récuperer cette fameuse ID d'ecoreleve
		UPDATE FormFB
		SET [originalID] = tmf.[SOURCEId]
		FROM [syn_Formbuilder.Form] AS FormFB
		JOIN #tempMergeForm AS tmf ON tmf.[sourceOriginalId] = CONCAT(@formbuilderDBNAME, '_',  FormFB.[initialID])

				IF @@TRANCOUNT > 0
					IF @Debug = 1
						BEGIN
							ROLLBACK TRAN
						END
					ELSE
						BEGIN
							COMMIT TRAN
						END
	END TRY
    BEGIN CATCH
		PRINT('ERROR CATCHED')

        IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRAN
			END


        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT

        SELECT
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE()

        RAISERROR (
			@ErrorMessage, -- Message text.
            @ErrorSeverity, -- Severity.
            @ErrorState -- State.
        )
    END CATCH
END
GO


INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('135_UPDATE_UPDT_ProtocoleType_',GETDATE(),(SELECT db_name()))


GO
