ALTER PROCEDURE [dbo].[EXEC_Send_Data_To_Referential]
	@IDFormFormBuilder INT,
	@Debug INT = 0
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN

		EXEC [dbo].[UPDT_ProtocoleType] @IDFormFormBuilder, @Debug = 0

		EXEC [dbo].[UPDT_ObservationDynProp] @IDFormFormBuilder, @Debug = 0

		EXEC [dbo].[UPDT_ModuleFormsInternationalization] @IDFormFormBuilder, @Debug = 0

		EXEC [dbo].[UPDT_ProtocoleType_ObservationDynProp] @IDFormFormBuilder, @Debug = 0

		EXEC [dbo].[UPDT_ModuleForms] @IDFormFormBuilder, @Debug = 0


		IF @@TRANCOUNT > 0
			IF @Debug = 1
				BEGIN
					ROLLBACK TRAN
				END
			ELSE
				BEGIN
					COMMIT TRAN
				END
	END TRY
    BEGIN CATCH
		PRINT('ERROR CATCHED')

        IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRAN
			END

        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT

        SELECT
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE()

        RAISERROR (
			@ErrorMessage, -- Message text.
            @ErrorSeverity, -- Severity.
            @ErrorState -- State.
        )
    END CATCH
END
GO


INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('124_UPDATE_EXEC_Send_Data_To_Referential',GETDATE(),(SELECT db_name()))


GO
