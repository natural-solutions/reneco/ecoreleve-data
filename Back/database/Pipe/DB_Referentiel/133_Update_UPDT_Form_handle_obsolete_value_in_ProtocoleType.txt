ALTER PROCEDURE [dbo].[UPDT_Form]
	@Debug INT = 0,
	@ForceRollback INT = 0,
	@context VARCHAR(MAX),
	@uniqCurrentDate DATETIME,
	@PrefixOriginalId VARCHAR(MAX)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN

			IF OBJECT_ID('tempdb..#ProtocoleType_Form') IS NOT NULL
				BEGIN
					DROP TABLE #ProtocoleType_Form
				END

			CREATE TABLE #ProtocoleType_Form(
				[pk_Form] INT NOT NULL,
				[pk_protocoleType] INT NOT NULL
			)

			INSERT INTO [syn_Formbuilder.Form]([name],[tag],[creationDate],[modificationDate],[curStatus],[obsolete],[isTemplate],[context],[originalID],[propagate],[state],[initialID])
			OUTPUT INSERTED.[pk_Form], INSERTED.[originalID] AS [pk_protocoleType] INTO #ProtocoleType_Form(pk_form, pk_protocoleType)
			SELECT
			PT.[Name] AS [name],
			NULL AS [tag],
			@uniqCurrentDate AS [creationDate],
			NULL AS [modificationDate],
			1 AS [curStatus],
			PT.[obsolete] AS [obsolete],
			0 AS [isTemplate],
			@context AS [context],
			PT.[ID] AS [originalID],
			0 AS [propagate],
			1 AS [state],
			-1 AS [initialID]
			FROM [ProtocoleType] AS PT


			--update initialID with his own pk for history of forms schema in formbuilder
			UPDATE [syn_Formbuilder.Form]
				SET [initialID] = [pk_Form]
				WHERE [pk_Form] in ( SELECT [pk_Form] FROM #ProtocoleType_Form);
			--update Original id for protocoletype with ID of Form as Formbuilder create it
			UPDATE PT
				SET PT.[OriginalId] = CONCAT(@PrefixOriginalId,PT_F.[pk_Form])
				FROM [ProtocoleType] AS PT
				JOIN [#ProtocoleType_Form] AS PT_F ON PT_F.[pk_protocoleType] = PT.[ID];

			IF @@TRANCOUNT > 0
				IF @ForceRollback = 1
					BEGIN
						PRINT('FORCED ROLLBACK UPDT_FORM')
						ROLLBACK TRAN
					END
				ELSE
					BEGIN
						PRINT('COMMITTED UPDT_FORM')
						COMMIT TRAN
					END
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			BEGIN
				ROLLBACK TRAN
			END


        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT

        SELECT
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE()

        RAISERROR (
			@ErrorMessage, -- Message text.
            @ErrorSeverity, -- Severity.
            @ErrorState -- State.
        )
	END CATCH
END
GO


INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('133_Update_UPDT_Form_handle_obsolete_value_in_ProtocoleType',GETDATE(),(SELECT db_name()))


GO
