EXEC sp_rename 'Sensor.Compagny', 'Company', 'COLUMN';

UPDATE ModuleGrids
SET Name = 'Company', Label = 'Company'
WHERE Name = 'Compagny'

UPDATE ModuleGrids
SET Name = 'Transmitter_Company', Label = 'Transmitter Company'
WHERE Name = 'Transmitter_Compagny'

UPDATE ModuleForms
SET Name = 'Company', Label = 'Company'
WHERE Name = 'Compagny'

UPDATE ModuleForms
SET Name = 'Transmitter_Company', Label = 'Transmitter Company'
WHERE Name = 'Transmitter_Compagny'

UPDATE IndividualDynProp
SET Name = 'Transmitter_Company'
WHERE Name = 'Transmitter_Compagny'

INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('261_UPDATE_Company_Field_In_EcoR',GETDATE(),(SELECT db_name()))


GO
