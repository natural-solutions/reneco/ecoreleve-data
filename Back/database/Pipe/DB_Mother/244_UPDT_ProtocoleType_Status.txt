/********************************************
 Reminder for status values
+--------+------------------------------+
| Status |  Description                 |
+--------+------------------------------+
|   1    | Form display Normal          |
+--------+------------------------------+
|   2    | Form display Grid            |
+--------+------------------------------+
|   3    | SubForm display Normal       |
+--------+------------------------------+
|   4    | SubForm display Grid         |
+--------+------------------------------+

***********************************************/



--All "Normal" Forms to Normal Form
UPDATE [ProtocoleType]
SET [Status] = 1
WHERE [Status] = 4

--All "Grid" Forms to Grid Form
UPDATE [ProtocoleType]
SET [Status] = 2
WHERE [Status] = 10

--All old SubForms to Grid Sub Form
UPDATE [ProtocoleType]
SET [Status] = 4
WHERE [Status] = 6

--"Normal" Sub Forms to Normal Sub Form
UPDATE [ProtocoleType]
SET [Status] = 3
WHERE [Name] in ('Capture_Individual','Release_Individual')
GO

INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('244_UPDT_ProtocoleType_Status',GETDATE(),(SELECT db_name()))


GO
