UPDATE [ModuleForms]
SET [Options] = '{"usedLabel":"Name","operator":"startswith","triggerAt":3}'
WHERE
[InputType] = 'ObjectPicker'
AND
[Name] = 'FK_MonitoredSite'

UPDATE [ModuleForms]
SET [Options] = '{"usedLabel":"UnicIdentifier","operator":"startswith","triggerAt":3}'
WHERE
[InputType] = 'ObjectPicker'
AND
[Name] = 'FK_Sensor'


UPDATE [ModuleForms]
SET [Options] = '{"usedLabel":"ID","operator":"startswith","triggerAt":3}'
WHERE
[InputType] = 'ObjectPicker'
AND
[Name] ='FK_Individual'




INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('260_UPDATE_Conf_For_ObjectPicker_With_New_Properties',GETDATE(),(SELECT db_name()))


GO
