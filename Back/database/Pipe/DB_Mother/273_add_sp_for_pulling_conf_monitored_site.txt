-- UPGRADE

CREATE PROCEDURE [dbo].[MERGE_MonitoredSiteDynProp]
    @Debug INT=1,
    @ForceRollback INT=1
AS
BEGIN
    BEGIN TRY
    BEGIN TRAN

        IF @Debug = 1
        BEGIN
            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
        END

        IF OBJECT_ID('tempdb..#MonitoredSiteDynPropTMP') IS NOT NULL
        BEGIN
            DROP TABLE #MonitoredSiteDynPropTMP
        END

        CREATE TABLE #MonitoredSiteDynPropTMP (
            [ID] INT NULL,
            [Name] NVARCHAR(250) NULL,
            [TypeProp] NVARCHAR(250) NULL
        )

        INSERT INTO #MonitoredSiteDynPropTMP([ID],[Name],[TypeProp])
        SELECT
        [ID],
        [Name],
        [TypeProp]
        FROM [syn_Referentiel.MonitoredSiteDynProp]



        ---- MERGE TABLE ---
        IF OBJECT_ID('tempdb..#tempMergeMonitoredSiteDynProp') IS NOT NULL
        BEGIN
            DROP TABLE #tempMergeMonitoredSiteDynProp
        END

        CREATE TABLE #tempMergeMonitoredSiteDynProp(
        [action] VARCHAR(MAX),
        [SourceId] INT,
        [sourceName] VARCHAR(MAX),
        [sourceTypeProp] VARCHAR(MAX),
        [TargetId] INT,
        [TargetName] VARCHAR(MAX),
        [TargetTypeProp] VARCHAR(MAX)
        );

        IF @debug = 1
        BEGIN
            SELECT 'DATA TO MERGE'
            SELECT
            *
            FROM #MonitoredSiteDynPropTMP

            SELECT 'WILL BE MERGED WITH TARGET'
            SELECT
            [ID],
            [Name],
            [TypeProp]
            FROM [MonitoredSiteDynProp]
        END


        SET IDENTITY_INSERT [MonitoredSiteDynProp] ON;
        PRINT('SET IDENTITY_INSERT ON FOR [MonitoredSiteDynProp]');

        WITH MonitoredSiteDynPropFiltered AS (
            SELECT
            [ID],
            [Name],
            [TypeProp]
            FROM [MonitoredSiteDynProp]
        )
        MERGE MonitoredSiteDynPropFiltered AS [TARGET]
        USING #MonitoredSiteDynPropTMP AS [SOURCE]
        ON ( [TARGET].[ID] = [SOURCE].[ID])

        WHEN MATCHED THEN
            UPDATE SET
            TARGET.[Name] = SOURCE.[Name],
            TARGET.[TypeProp] = SOURCE.[TypeProp]

        WHEN NOT MATCHED BY TARGET THEN
            INSERT ([ID],[Name],[TypeProp])
            VALUES ([ID],[Name],[TypeProp])

        WHEN NOT MATCHED BY SOURCE THEN
            DELETE

        OUTPUT $action AS [action],
        INSERTED.[ID] AS [SourceId],
        INSERTED.[name] AS [sourceName],
        INSERTED.[TypeProp] AS [sourceTypeProp],
        DELETED.[ID] AS [TargetId],
        DELETED.[name] AS [TargetName],
        DELETED.[TypeProp] AS [TargetTypeProp]
        INTO #tempMergeMonitoredSiteDynProp([action],[SourceId],[sourceName],[sourceTypeProp],[TargetId],[TargetName],[TargetTypeProp]);

        SET IDENTITY_INSERT [MonitoredSiteDynProp] OFF;
        PRINT('SET IDENTITY_INSERT OFF FOR [MonitoredSiteDynProp]');

        IF @Debug = 1
        BEGIN
            SELECT 'RESULT MERGE'

            SELECT * FROM #tempMergeMonitoredSiteDynProp
        END

        IF @@TRANCOUNT > 0
        BEGIN
            IF @ForceRollback = 1
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                ;THROW 50001, N'FORCED ROLLBACK', 1;
                ROLLBACK TRAN
            END
            ELSE
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                COMMIT TRAN
            END
        END
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRAN
            END
            ;THROW
    END CATCH
END
GO

CREATE PROCEDURE [dbo].[MERGE_MonitoredSiteType]
    @Debug INT=1,
    @ForceRollback INT=1
AS
BEGIN
    BEGIN TRY
    BEGIN TRAN

        IF @Debug = 1
        BEGIN
            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
        END

        IF OBJECT_ID('tempdb..#MonitoredSiteTypeTMP') IS NOT NULL
        BEGIN
            DROP TABLE #MonitoredSiteTypeTMP
        END

        CREATE TABLE #MonitoredSiteTypeTMP(
            [ID] [int] NOT NULL,
            [Name] [nvarchar](250) NULL,
            [Status] [int] NULL
        )

        INSERT INTO #MonitoredSiteTypeTMP([ID],[Name],[Status])
        SELECT
        [ID],
        [Name],
        [Status]
        FROM [syn_Referentiel.MonitoredSiteType]

        ---- MERGE TABLE ---
        IF OBJECT_ID('tempdb..#tempMergeMonitoredSiteType') IS NOT NULL
        BEGIN
            DROP TABLE #tempMergeMonitoredSiteType
        END

        CREATE TABLE #tempMergeMonitoredSiteType(
            [action] VARCHAR(MAX),
            [SourceID] [int],
            [SourceName] [nvarchar](250),
            [SourceStatus] [int],
            [TargetID] [int],
            [TargetName] [nvarchar](250),
            [TargetStatus] [int]
        )


        IF @debug = 1
        BEGIN
            SELECT 'DATA TO MERGE'
            SELECT
            *
            FROM #MonitoredSiteTypeTMP

            SELECT 'WILL BE MERGED WITH TARGET'
            SELECT
            [ID],
            [Name],
            [Status]
            FROM [MonitoredSiteType]
        END

        SET IDENTITY_INSERT [MonitoredSiteType] ON;
        PRINT('SET IDENTITY_INSERT ON FOR [MonitoredSiteType]');

        PRINT('MERGE TABLE');
        WITH MonitoredSiteTypeFiltered AS
        (
            SELECT
            [ID],
            [Name],
            [Status]
            FROM [MonitoredSiteType]
        )
        MERGE MonitoredSiteTypeFiltered AS [TARGET]
        USING #MonitoredSiteTypeTMP AS [SOURCE]
        ON ( TARGET.[ID] = SOURCE.[ID] )
        WHEN MATCHED THEN
            UPDATE SET
            TARGET.[Name] = SOURCE.[Name],
            TARGET.[Status] = SOURCE.[Status]

        WHEN NOT MATCHED BY TARGET THEN
            INSERT ([ID], [Name], [Status])
            VALUES ([ID], [Name], [Status])
        WHEN NOT MATCHED BY SOURCE THEN
            DELETE

        OUTPUT $action AS [action],
        INSERTED.[ID] AS [SourceID],
        INSERTED.[Name] AS [SourceName],
        INSERTED.[Status]  AS [SourceStatus],
        DELETED.[ID] AS [TargetID],
        DELETED.[Name]  AS [TargetName],
        DELETED.[Status]  AS [TargetStatus]
        INTO #tempMergeMonitoredSiteType([action],[SourceID],[SourceName],[SourceStatus],[TargetID],[TargetName],[TargetStatus]);

        SET IDENTITY_INSERT [MonitoredSiteType] OFF;
        PRINT('SET IDENTITY_INSERT OFF FOR [MonitoredSiteType]');
        IF @Debug = 1
        BEGIN
            SELECT 'RESULT MERGE'

            SELECT * FROM #tempMergeMonitoredSiteType
        END


        IF @@TRANCOUNT > 0
        BEGIN
            IF @ForceRollback = 1
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                ;THROW 50001, N'FORCED ROLLBACK', 1;
                ROLLBACK TRAN
            END
            ELSE
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                COMMIT TRAN
            END
        END
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRAN
            END
            ;THROW
    END CATCH
END
GO
CREATE PROCEDURE [dbo].[MERGE_MonitoredSiteType_MonitoredSiteDynProp]
    @Debug INT=1,
    @ForceRollback INT=1
AS
BEGIN
    BEGIN TRY
    BEGIN TRAN

        IF @Debug = 1
        BEGIN
            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
        END

        IF OBJECT_ID('tempdb..#MonitoredSiteType_MonitoredSiteDynPropTMP') IS NOT NULL
        BEGIN
            DROP TABLE #MonitoredSiteType_MonitoredSiteDynPropTMP
        END

        CREATE TABLE #MonitoredSiteType_MonitoredSiteDynPropTMP(
            [ID] [int] NOT NULL,
            [Required] [int] NOT NULL,
            [FK_MonitoredSiteType] [int] NULL,
            [FK_MonitoredSiteDynProp] [int] NULL
        )

        INSERT INTO #MonitoredSiteType_MonitoredSiteDynPropTMP([ID],[Required],[FK_MonitoredSiteType],[FK_MonitoredSiteDynProp])
        SELECT
        [ID],
        [Required],
        [FK_MonitoredSiteType],
        [FK_MonitoredSiteDynProp]
        FROM [syn_Referentiel.MonitoredSiteType_MonitoredSiteDynProp]

        ---- MERGE TABLE ---
        IF OBJECT_ID('tempdb..#tempMergeMonitoredSiteType_MonitoredSiteDynProp') IS NOT NULL
        BEGIN
            DROP TABLE #tempMergeMonitoredSiteType_MonitoredSiteDynProp
        END

        CREATE TABLE #tempMergeMonitoredSiteType_MonitoredSiteDynProp(
            [action] VARCHAR(MAX),
            [SourceID] [int],
            [SourceRequired] [int],
            [SourceFK_MonitoredSiteType] [int],
            [SourceFK_MonitoredSiteDynProp] [int],
            [TargetID] [int],
            [TargetRequired] [int],
            [TargetFK_MonitoredSiteType] [int],
            [TargetFK_MonitoredSiteDynProp] [int]
        )

        IF @debug = 1
        BEGIN
            SELECT 'DATA TO MERGE'
            SELECT
            *
            FROM #tempMergeMonitoredSiteType_MonitoredSiteDynProp

            SELECT 'WILL BE MERGED WITH TARGET'
            SELECT
            [ID],
            [Required],
            [FK_MonitoredSiteType],
            [FK_MonitoredSiteDynProp]
            FROM [MonitoredSiteType_MonitoredSiteDynProp]
        END

        SET IDENTITY_INSERT [MonitoredSiteType_MonitoredSiteDynProp] ON;
        PRINT('SET IDENTITY_INSERT ON FOR [MonitoredSiteType_MonitoredSiteDynProp]');

        WITH MonitoredSiteType_MonitoredSiteDynPropFiltered AS
        (
            SELECT
            [ID],
            [Required],
            [FK_MonitoredSiteType],
            [FK_MonitoredSiteDynProp]
            FROM [MonitoredSiteType_MonitoredSiteDynProp]
        )
        MERGE MonitoredSiteType_MonitoredSiteDynPropFiltered AS [TARGET]
        USING #MonitoredSiteType_MonitoredSiteDynPropTMP AS [SOURCE]
        ON ( TARGET.[ID] = SOURCE.[ID] )
        WHEN MATCHED THEN
            UPDATE SET
            TARGET.[Required] = SOURCE.[Required],
            TARGET.[FK_MonitoredSiteType] = SOURCE.[FK_MonitoredSiteType],
            TARGET.[FK_MonitoredSiteDynProp] = SOURCE.[FK_MonitoredSiteDynProp]

        WHEN NOT MATCHED BY TARGET THEN
            INSERT ([ID], [Required], [FK_MonitoredSiteType], [FK_MonitoredSiteDynProp])
            VALUES ([ID], [Required], [FK_MonitoredSiteType], [FK_MonitoredSiteDynProp])
        WHEN NOT MATCHED BY SOURCE THEN
            DELETE

        OUTPUT $action AS [action],
        INSERTED.[ID] AS [SourceID],
        INSERTED.[Required] AS [SourceRequired],
        INSERTED.[FK_MonitoredSiteType]  AS [SourceFK_MonitoredSiteType],
        INSERTED.[FK_MonitoredSiteDynProp] AS [SourceFK_MonitoredSiteDynProp],
        DELETED.[ID] AS [TargetID],
        DELETED.[Required]  AS [TargetRequired],
        DELETED.[FK_MonitoredSiteType]  AS [TargetFK_MonitoredSiteType],
        DELETED.[FK_MonitoredSiteDynProp]  AS [TargetFK_MonitoredSiteDynProp]
        INTO #tempMergeMonitoredSiteType_MonitoredSiteDynProp([action],[SourceID],[SourceRequired],[SourceFK_MonitoredSiteType],[SourceFK_MonitoredSiteDynProp],[TargetID],[TargetRequired],[TargetFK_MonitoredSiteType],[TargetFK_MonitoredSiteDynProp]);

        SET IDENTITY_INSERT [MonitoredSiteType_MonitoredSiteDynProp] OFF;
        PRINT('SET IDENTITY_INSERT OFF FOR [MonitoredSiteType_MonitoredSiteDynProp]');

        IF @Debug = 1
        BEGIN
            SELECT 'RESULT MERGE'

            SELECT * FROM #tempMergeMonitoredSiteType_MonitoredSiteDynProp
        END


        IF @@TRANCOUNT > 0
        BEGIN
            IF @ForceRollback = 1
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                ;THROW 50001, N'FORCED ROLLBACK', 1;
                ROLLBACK TRAN
            END
            ELSE
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                COMMIT TRAN
            END
        END
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRAN
            END
            ;THROW
    END CATCH
END
GO
ALTER PROCEDURE [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables]
    @Debug INT=1,
    @ForceRollback INT=1
AS
BEGIN
    BEGIN TRY
    BEGIN TRAN
        IF @Debug = 1
        BEGIN
            PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
        END

        --- Disable all constraints for database
        EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

        --- ORDER IS  NOT IMPORTANT  BECAUSE WE DISABLE ALL CONSTRAINTS ---
        EXEC [MERGE_fieldActivity_ProtocoleType]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_ProtocoleType]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_fieldActivity]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_FrontModules]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_ModuleForms]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_ModuleGrids]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_ProtocoleType_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
        EXEC [MERGE_ModuleFormsInternationalization]  @Debug = 0, @ForceRollback = 0
        EXEC MERGE_MonitoredSiteDynProp  @Debug = 0, @ForceRollback = 0
        EXEC MERGE_MonitoredSiteType  @Debug = 0, @ForceRollback = 0
        EXEC MERGE_MonitoredSiteType_MonitoredSiteDynProp  @Debug = 0, @ForceRollback = 0

        --- Enable all constraints for database and launch check  ---
        EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

        IF @@TRANCOUNT > 0
        BEGIN
            IF @ForceRollback = 1
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                ;THROW 50001, N'FORCED ROLLBACK', 1;
                ROLLBACK TRAN
            END
            ELSE
            BEGIN
                IF @Debug = 1
                BEGIN
                    PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
                END
                COMMIT TRAN
            END
        END
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRAN
            END
            ;THROW
    END CATCH
END
GO

---- DOWNGRADE

-- IF OBJECT_ID('[MERGE_MonitoredSiteDynProp]', 'P') IS NOT NULL
-- DROP PROCEDURE [MERGE_MonitoredSiteDynProp]

-- IF OBJECT_ID('[MERGE_MonitoredSiteType]', 'P') IS NOT NULL
-- DROP PROCEDURE [MERGE_MonitoredSiteType]

-- IF OBJECT_ID('[MERGE_MonitoredSiteType_MonitoredSiteDynProp]', 'P') IS NOT NULL
-- DROP PROCEDURE [MERGE_MonitoredSiteType_MonitoredSiteDynProp]

-- ALTER PROCEDURE [dbo].[EXEC_MERGE_All_Referential_Configurations_Tables]
--     @Debug INT=1,
--     @ForceRollback INT=1
-- AS
-- BEGIN
--     BEGIN TRY
--     BEGIN TRAN
--         IF @Debug = 1
--         BEGIN
--             PRINT(CONCAT(OBJECT_SCHEMA_NAME(@@PROCID), '.', OBJECT_NAME(@@PROCID),' ', 'Start'))
--         END

--         --- Disable all constraints for database
--         EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

--         --- ORDER IS  NOT IMPORTANT  BECAUSE WE DISABLE ALL CONSTRAINTS ---
--         EXEC [MERGE_fieldActivity_ProtocoleType]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_ProtocoleType]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_fieldActivity]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_FrontModules]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_ModuleForms]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_ModuleGrids]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_ProtocoleType_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_ObservationDynProp]  @Debug = 0, @ForceRollback = 0
--         EXEC [MERGE_ModuleFormsInternationalization]  @Debug = 0, @ForceRollback = 0

--         --- Enable all constraints for database and launch check  ---
--         EXEC sp_msforeachtable "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"

--         IF @@TRANCOUNT > 0
--         BEGIN
--             IF @ForceRollback = 1
--             BEGIN
--                 IF @Debug = 1
--                 BEGIN
--                     PRINT(CONCAT('FORCED ROLLBACK', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
--                 END
--                 ;THROW 50001, N'FORCED ROLLBACK', 1;
--                 ROLLBACK TRAN
--             END
--             ELSE
--             BEGIN
--                 IF @Debug = 1
--                 BEGIN
--                     PRINT(CONCAT('COMMITTED', ' ', OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)))
--                 END
--                 COMMIT TRAN
--             END
--         END
--     END TRY
--     BEGIN CATCH
--         IF @@TRANCOUNT > 0
--             BEGIN
--                 ROLLBACK TRAN
--             END
--             ;THROW
--     END CATCH
-- END



INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('273_add_sp_for_pulling_conf_monitored_site',GETDATE(),(SELECT db_name()))


GO
