CREATE PROCEDURE [dbo].[sp_PullModuleFormsInternationalization]
	@IdProtocoleType INT,
	@Debug INT = 0,
	@RollbackForced INT = 0
AS
BEGIN
	BEGIN TRY
	    BEGIN TRAN

            IF @Debug = 1
                BEGIN
                    PRINT('sp_PullModuleFormsInternationalization Start')
                END

            IF OBJECT_ID('tempdb..#ModuleFormsInternationalizationTMP') IS NOT NULL
                BEGIN
                    DROP TABLE #ModuleFormsInternationalizationTMP
                END

            CREATE TABLE #ModuleFormsInternationalizationTMP (
                [Fk_ProtocoleType] [INT] NOT NULL,
                [Fk_Language] [VARCHAR] (2) NOT NULL,
                [Property_Name] [VARCHAR] (255) NULL,
                [Description] [VARCHAR] (300) NULL,
                [Label] [VARCHAR] (255) NULL,
                [Help] [VARCHAR] (255) NULL,
                [Keywords] [VARCHAR] (255) NULL
                )

            INSERT INTO #ModuleFormsInternationalizationTMP([Fk_ProtocoleType],[Fk_Language],[Property_Name],[Description],[Label],[Help],[Keywords])
            SELECT
            [Fk_ProtocoleType],
            [Fk_Language],
            [Property_Name],
            [Description],
            [Label],
            [Help],
            [Keywords]
            FROM [syn_Referentiel.ModuleFormsInternationalization]
            WHERE
            [Fk_ProtocoleType] = @IdProtocoleType


            ---- MERGE TABLE ---
            IF OBJECT_ID('tempdb..#tempMergeModuleFormsInternationalization') IS NOT NULL
                BEGIN
                    DROP TABLE #tempMergeModuleFormsInternationalization
                END

            CREATE TABLE #tempMergeModuleFormsInternationalization(
                [action] VARCHAR(MAX),
                [SourceFk_ProtocoleType] INT,
                [SourceFk_Language] VARCHAR(2),
                [SourceProperty_Name] VARCHAR(255),
                [SourceDescription] VARCHAR(300),
                [SourceLabel] VARCHAR(255),
                [SourceHelp] VARCHAR(255),
                [SourceKeywords] VARCHAR(255),
                [TargetFk_ProtocoleType] INT,
                [TargetFk_Language] VARCHAR(2),
                [TargetProperty_Name] VARCHAR(255),
                [TargetDescription] VARCHAR(300),
                [TargetLabel] VARCHAR(255),
                [TargetHelp] VARCHAR(255),
                [TargetKeywords] VARCHAR(255)
            );

            IF @debug = 1
                BEGIN
                    SELECT 'DATA TO MERGE'
                    SELECT
                    *
                    FROM #ModuleFormsInternationalizationTMP

                    SELECT 'WILL BE MERGED WITH TARGET'
                    SELECT
                    [Fk_ProtocoleType],
                    [Fk_Language],
                    [Property_Name],
                    [Description],
                    [Label],
                    [Help],
                    [Keywords]
                    FROM [ModuleFormsInternationalization]
                    WHERE
                    [Fk_ProtocoleType] = @IdProtocoleType
                END

            PRINT('MERGE TABLE');
            WITH ModuleFormsInternationalizationFiltered AS
            (
                SELECT
                [Fk_ProtocoleType],
                [Fk_Language],
                [Property_Name],
                [Description],
                [Label],
                [Help],
                [Keywords]
                FROM [ModuleFormsInternationalization]
                WHERE
                [Fk_ProtocoleType] = @IdProtocoleType
            )
            MERGE ModuleFormsInternationalizationFiltered AS [TARGET]
            USING #ModuleFormsInternationalizationTMP AS [SOURCE]
            ON (
                (
                [TARGET].[Fk_ProtocoleType] = [SOURCE].[Fk_ProtocoleType]
                AND
                [TARGET].[Fk_Language] = [SOURCE].[Fk_Language]
                AND
                [TARGET].[Property_Name] = [SOURCE].[Property_Name]
                )
                OR
                (
                    (
                    [TARGET].[Property_Name] IS NULL
                    AND
                    [SOURCE].[Property_Name] IS NULL
                    )
                AND
                [TARGET].[Fk_ProtocoleType] = [SOURCE].[Fk_ProtocoleType]
                AND
                [TARGET].[Fk_Language] = [SOURCE].[Fk_Language]
                )
            )
            WHEN MATCHED THEN
                UPDATE SET
                [TARGET].[Description] = [SOURCE].[Description],
                [TARGET].[Label] = [SOURCE].[Label],
                [TARGET].[Help] = [SOURCE].[Help],
                [TARGET].[Keywords] = [SOURCE].[Keywords]
            WHEN NOT MATCHED BY TARGET THEN
                INSERT ([Fk_ProtocoleType],[Fk_Language],[Property_Name],[Description],[Label],[Help],[Keywords])
                VALUES ([SOURCE].[Fk_ProtocoleType],[SOURCE].[Fk_Language],[SOURCE].[Property_Name],[SOURCE].[Description],[SOURCE].[Label],[SOURCE].[Help],[SOURCE].[Keywords])
            WHEN NOT MATCHED BY SOURCE THEN
            DELETE

            OUTPUT $action AS [action],
            INSERTED.[Fk_ProtocoleType] AS [SourceFk_ProtocoleType],
            INSERTED.[Fk_Language] AS [SourceFk_Language],
            INSERTED.[Property_Name] AS [SourceProperty_Name],
            INSERTED.[Description] AS [SourceDescription],
            INSERTED.[Label] AS [SourceLabel],
            INSERTED.[Help] AS [SourceHelp],
            INSERTED.[Keywords] AS [SourceKeywords],
            DELETED.[Fk_ProtocoleType] AS [TargetFk_ProtocoleType],
            DELETED.[Fk_Language] AS [TargetFk_Language],
            DELETED.[Property_Name] AS [TargetProperty_Name],
            DELETED.[Description] AS [TargetDescription],
            DELETED.[Label] AS [TargetLabel],
            DELETED.[Help] AS [TargetHelp],
            DELETED.[Keywords] AS [TargetKeywords]
            INTO #tempMergeModuleFormsInternationalization([action],[SourceFk_ProtocoleType],[SourceFk_Language],[SourceProperty_Name],[SourceDescription],[SourceLabel],[SourceHelp],[SourceKeywords],[TargetFk_ProtocoleType],[TargetFk_Language],[TargetProperty_Name],[TargetDescription],[TargetLabel],[TargetHelp],[TargetKeywords]);

            IF @Debug = 1
                BEGIN
                    SELECT 'RESULT MERGE'

                    SELECT * FROM #tempMergeModuleFormsInternationalization
                END
            IF @@TRANCOUNT > 0
                BEGIN
                    IF @RollbackForced = 1
                        BEGIN
                            PRINT('ROLLBACK TRAN')
                            ROLLBACK TRAN
                        END
                    ELSE
                        BEGIN
                            COMMIT TRAN
                        END
                END
	END TRY
    BEGIN CATCH
		PRINT('ERROR CATCHED')

        IF @@TRANCOUNT > 0
			ROLLBACK TRAN;

        DECLARE @ErrorMessage NVARCHAR(4000);
        DECLARE @ErrorSeverity INT;
        DECLARE @ErrorState INT;

        SELECT
                @ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE();

        RAISERROR (@ErrorMessage, -- Message text.
                            @ErrorSeverity, -- Severity.
                            @ErrorState -- State.
                            );
    END CATCH
END
GO



INSERT INTO [dbo].[TVersion] (TVer_FileName,TVer_Date,TVer_DbName) VALUES ('233_CREATE_sp_PullModuleFormsInternationalization',GETDATE(),(SELECT db_name()))


GO
