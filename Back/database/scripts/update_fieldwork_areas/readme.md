# Automated update of the fieldwork areas

This file describes the automated process of updating the fieldwork areas in the EcoReleve project. The update of the fieldwork areas can be launched from the console in admin mode by the command:

```cmd
cd c:\inetpub\wwwroot\ecoreleve\dev\Back\database\scripts\update_fieldwork_areas
fieldwork_area_update.bat --sql [instance] --db [database] --dev [boolean] --shp [shapefile]
```

Thress arguments are needed:
- `--sql` : name of the SQL instance
- `--db` : name of the EcoRelevé database
- `--dev` : boolean indicating wether it is a dev or a prod instance
- `--shp` : path to access to the fieldwork areas shapefile

> **Important note** <br>
> Even if a automatic backup is done during the update, don't forget to manually back up the database before proceeding the update in order to avoid any inconvenience.