::
::
::
::
::
:: use:
::      fieldwork_area_update.bat --sql [SQL_INSTANCE] --db [DATABASE_NAME] --dev [TRUE or FALSE] --shp [SHAPEFILE_PATH] --all
::


@echo off
set error=0

:: check arguments
echo --- check arguments
call src/check_arguments.bat %*
if errorlevel 1 goto usage

:: installation of GDAL if it doesn't exist
echo --- installation of GDAL if needed
call src/install_gdal.bat
if errorlevel 1 goto error

:: stop production EcoReleve service
echo --- Stop service
call src/stop_service.bat
if errorlevel 1 goto error

:: back-up of the database
echo --- back-up of the database
call src/backup_database.bat
if errorlevel 1 goto error

:: import of the shapefile in the database
echo --- import of the shapefile in the database
call src/import_shp.bat
if errorlevel 1 goto error

:: execute the stored procedure for updating fieldwork areas
echo --- execute the stored procedure for updating fieldwork areas
call src/execute_FA_sp.bat
if errorlevel 1 goto error

:: end of the process
:end
echo --- end of the process
exit /b %error%

:: error in script usage 
:usage
echo --- error in script usage: invalid arguments
echo usage: fieldwork_area_update.bat --sql [SQL_INSTANCE] --db [DATABASE_NAME] --dev [TRUE or FALSE] --shp [SHAPEFILE_PATH] --all
goto error

:: manage of the errors
:error
echo Error during the process. The update have been stopped.
set error=1
goto end