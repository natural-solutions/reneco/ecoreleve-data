
set shp_error=0

:: setting environment for GDAL utilities
call C:\"Program Files"\GDAL\GDALShell.bat
if errorlevel 1 (
    echo --- error during data import when setting of GDAL environment
    set shp_error=1
    goto end
)

:: import shapefile into SQL Server
ogr2ogr -overwrite -progress -nln "reneco_fieldworkarea" -f "MSSQLSpatial" -lco "GEOM_TYPE=geometry" -a_srs "EPSG:4326" "MSSQL:server=%sql%; database=%db%; trusted_connection=yes" %shp%
if errorlevel 1 (
    echo --- error during data import when converting shapefile into MSSQLSpatial format
    set shp_error=1
    goto end
)

:end
exit /b %shp_error%