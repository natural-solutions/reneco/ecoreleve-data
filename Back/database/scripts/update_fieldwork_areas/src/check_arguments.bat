@echo off

set arg_count=0
set arg_flag=0

for %%x in (%*) do set /A arg_count+=1

if %arg_count% neq 8 if %arg_count% neq 9 goto usage

if %1 neq --sql goto usage
if %3 neq --db goto usage
if %5 neq --dev goto usage
if %6 neq TRUE if %6 neq FALSE goto usage
if %7 neq --shp goto usage
set sql=%2
set db=%4
set dev=%6
set shp=%8
set all=0

if %arg_count% equ 9 goto setall
goto end

:usage
set arg_flag=1

:end
exit /b %arg_flag%

:setall
if %9 neq --all goto usage
set all=1
goto end