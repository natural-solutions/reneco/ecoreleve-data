:: Install GDAL in the Program Files directory
:: if not already installed
:: No intervention from the user is required
:: 
:: use :
::      install_gdal.bat

@echo off

set gdal_error=0

if exist "C:\Program Files\GDAL" ( 
    echo.
    echo GDAL already installed in "C:\Program Files"
) else (    
    :: download installer file
    curl --remote-name http://download.gisinternals.com/sdk/downloads/release-1900-x64-gdal-3-0-4-mapserver-7-4-3/gdal-300-1900-x64-core.msi

    :: typical GDAL install
    msiexec /i gdal-300-1900-x64-core.msi /passive
    if errorlevel 1 (
        echo --- error during GDAL install
        set gdal_error=1
    )

    :: delete installer file
    del /f gdal-300-1900-x64-core.msi
)

exit /b %gdal_error%