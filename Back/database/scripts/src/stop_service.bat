
set service_error=0

if %dev%==TRUE (
    nsappsm stop nsecorelevedevservice
    if errorlevel 1 goto error
)
if %dev%==FALSE (
    nsappsm stop nsecoreleveservice
    if errorlevel 1 goto error
)
goto :end

:error
set service_error=1

:end
exit /b %service_error%
