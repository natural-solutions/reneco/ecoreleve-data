
set sp_error=0

sqlcmd -E -S %sql% -d %db% -Q "EXEC sp_FA_Update @Debug=0, @ForceRollback=0"
if errorlevel 1 (
    echo --- error during execution of the sp_FA_Update stored procedure
    set sp_error=1
)

exit /b %sp_error%