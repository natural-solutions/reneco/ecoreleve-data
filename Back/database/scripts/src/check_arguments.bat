@echo off

set arg_count=0
set arg_flag=0

for %%x in (%*) do set /A arg_count+=1

if %arg_count% neq 10 goto usage
if %1 neq --sql goto usage
set sql=%2

if %3 neq --db goto usage
set db=%4

if %5 neq --dev goto usage
if %6 neq TRUE if %6 neq FALSE goto usage
set dev=%6

if %7 neq --shp goto usage
set shp=%8

if %9 neq --area goto usage
shift
if %9 neq fieldwork if %9 neq administrative goto usage
set area=%9

goto end

:usage
set arg_flag=1

:end
exit /b %arg_flag%