
set backup_error=0

:: set date variable as yearmonthday (used for backup)
set date_now=%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%
set time_now=%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%

:: back up the production EcoReleve database
sqlcmd -S %sql% -U NsApp -b -Q "BACKUP DATABASE [%db%] TO DISK = N'D:\BACKUPS\MANUAL-BACKUP\%db%_NS_FA_%date_now%_%time_now%.bak' WITH NOFORMAT, NOINIT, NAME = N'%db%-Complete Database Base Backup on %date_now%', SKIP, NOREWIND, NOUNLOAD, STATS = 10"
if errorlevel 1 (
    echo --- error during back-up
    set backup_error=1
)

exit /b %backup_error%