
set sp_error=0

sqlcmd -E -S %sql% -d %db% -Q "EXEC sp_AA_Update @Debug=0, @ForceRollback=0"
if errorlevel 1 (
    echo --- error during execution of the sp_AA_Update stored procedure
    set sp_error=1
)

exit /b %sp_error%