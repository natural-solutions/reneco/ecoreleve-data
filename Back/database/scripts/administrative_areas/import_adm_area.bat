::
:: use:
::      .\import_adm_area.bat [SQL-INSTANCE] [DATABASE_NAME] [SHAPEFILE_PATH]
::-----------------------

:: setting environment for using GDAL utilities
call C:\"Program Files"\GDAL\GDALShell.bat

:: convert shapefile into MSSQLSpatial format
ogr2ogr -overwrite -progress -nln "reneco_administrativearea" -f "MSSQLSpatial" -lco "GEOM_TYPE=geometry" -a_srs "EPSG:4326" "MSSQL:server=%1; database=%2; trusted_connection=yes" %3