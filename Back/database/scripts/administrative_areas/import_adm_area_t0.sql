BEGIN TRY

	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	DECLARE @COUNT INT;
	DECLARE @ROWS INT;

	-- Set to null all FK_AdministrativeArea
	BEGIN TRY
		PRINT('Set to null all FK_AdministrativeArea')
		SET @COUNT = 0
		SET @ROWS = 0
		IF OBJECT_ID('reneco_administrativearea') IS NOT NULL
		BEGIN
			WHILE 1 = 1
			BEGIN
				BEGIN TRAN
				SELECT @count = COUNT(*) FROM Individual_Location WITH (NOLOCK) WHERE [FK_AdministrativeArea] IS NOT NULL
				PRINT(CONCAT('Individual location  FK_AdministrativeArea :',@COUNT,' NOT NULL'))
				UPDATE TOP (50000) Individual_Location
				SET [FK_AdministrativeArea] = NULL
				WHERE [FK_AdministrativeArea] IS NOT NULL

				SET @ROWS = @@ROWCOUNT
				COMMIT
				PRINT('COMMIT')

				IF @ROWS < 50000 BREAK;
			END

			WHILE 1 = 1
			BEGIN
				BEGIN TRAN
				SELECT @count = COUNT(*) FROM Station WITH (NOLOCK) WHERE [FK_AdministrativeArea] IS NOT NULL
				PRINT(CONCAT('Station FK_AdministrativeArea:',@COUNT,' NOT NULL'))
				UPDATE TOP (50000) Station
				SET [FK_AdministrativeArea] = NULL
				WHERE [FK_AdministrativeArea] IS NOT NULL

				SET @ROWS = @@ROWCOUNT
				COMMIT
				PRINT('COMMIT')

				IF @ROWS < 50000 BREAK;
			END

			WHILE 1 = 1
			BEGIN
				BEGIN TRAN
				SELECT @count = COUNT(*) FROM MonitoredSitePosition WITH (NOLOCK) WHERE [FK_AdministrativeArea] IS NOT NULL
				PRINT(CONCAT('MonitoredSitePosition FK_AdministrativeArea:',@COUNT,' NOT NULL'))
				UPDATE TOP (50000) MonitoredSitePosition
				SET [FK_AdministrativeArea] = NULL
				WHERE [FK_AdministrativeArea] IS NOT NULL

				SET @ROWS = @@ROWCOUNT
				COMMIT
				PRINT('COMMIT')

				IF @ROWS < 50000 BREAK;
			END
		END
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK; -- annule la transaction en cas d'erreur
			PRINT('ROLLBACK')
		END

		-- Renvoyer une erreur avec RAISERROR
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @ErrorSeverity = ERROR_SEVERITY();
		SET @ErrorState = ERROR_STATE();

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH;

	-- Empty AdminArea Table
	BEGIN TRY
		PRINT('Empty AdminArea Table')
		BEGIN TRAN

		-- Remove all lines
		DELETE FROM AdministrativeArea;

		COMMIT;
		PRINT('COMMIT')
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK;
			PRINT('ROLLBACK')
		END

		-- Renvoyer une erreur avec RAISERROR
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @ErrorSeverity = ERROR_SEVERITY();
		SET @ErrorState = ERROR_STATE();

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH;

	-- Insert data in AdministrativeArea Table
	BEGIN TRY
		PRINT('Insert data in AdministrativeArea Table')
		BEGIN TRAN

		DBCC CHECKIDENT ('AdministrativeArea', RESEED, 0);

		IF OBJECT_ID('reneco_administrativearea') IS NOT NULL
		BEGIN
			INSERT INTO AdministrativeArea (
				[Country],
				[Department], 
				[Municipality], 
				[Name], 
				[fullpath], 
				[type_], 
				[max_lat], 
				[min_lat], 
				[max_lon],
				[min_lon],
				[SHAPE_Leng],
				[SHAPE_Area],
				[valid_geom],
				[geom],
				[Centroid_Latitude],
				[Centroid_Longitude], 
				[Center], 
				[Data_Owner], 
				[activation_date],
				[inactivation_date],
				[last_change_attribut_date],
				[last_change_geometry_date]
			)
			SELECT
				[ctry_name],
				[depa_name],
				[muni_name],
				FirstPart.[adm_code],
				[fullpath],
				[adm_type],
				[max_lat],
				[min_lat],
				[max_lon],
				[min_lon],
				[shape_leng],
				[shape_area],
				[valid_geom],
				[geom],
				[centro_lat],
				[centro_lon],
				[center],
				[data_owner],
				[acti_d],
				[inacti_d],
				[attr_d],
				[geom_d]
			FROM (
				SELECT
					[ctry_name],
					[depa_name],
					[muni_name],
					[adm_code],
					CASE 
						WHEN [adm_type] = 'municipality' AND [ctry_name] IS NOT NULL AND [depa_name] IS NOT NULL AND [muni_name] IS NOT NULL
							THEN CONCAT([ctry_name],'>',[depa_name],'>',[muni_name])
						WHEN [adm_type] = 'department' AND [ctry_name] IS NOT NULL AND [depa_name] IS NOT NULL
							THEN CONCAT([ctry_name],'>',[depa_name])
						WHEN [adm_type] = 'country' AND [ctry_name] IS NOT NULL
							THEN [ctry_name]
						ELSE NULL
					END AS [fullpath],
					[adm_type],
					CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STY)) AS [min_lat],
					CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STY)) AS [max_lat],
					CONVERT(DECIMAL(9,5), MIN(([ogr_geometry]).MakeValid().STEnvelope().STPointN(1).STX)) AS [min_lon],
					CONVERT(DECIMAL(9,5), MAX(([ogr_geometry]).MakeValid().STEnvelope().STPointN(3).STX)) AS [max_lon],
					[shape_leng],
					[shape_area],
					[centro_lat],
					[centro_lon],
					[center],
					[data_owner],
					[acti_d],
					[inacti_d],
					[attr_d],
					[geom_d]
				FROM reneco_administrativearea
				GROUP BY [ogr_fid], [ctry_name], [depa_name], [muni_name], [adm_code], [adm_type], [shape_leng], [shape_area], [centro_lat], [centro_lon], [acti_d], [inacti_d], [attr_d], [geom_d], [center], [data_owner]
			) AS FirstPart
			JOIN (
				SELECT 
					[adm_code],
					[ogr_geometry] AS [geom],
					[ogr_geometry].MakeValid() AS [valid_geom]
				FROM reneco_administrativearea
			) AS SecondPart
			ON FirstPart.[adm_code] = SecondPart.[adm_code]
			ORDER BY [ctry_name] ASC, [depa_name] ASC, [muni_name] ASC, FirstPart.[adm_code] ASC
		END
		COMMIT TRAN
		PRINT('COMMIT')
	END TRY
	BEGIN CATCH
		PRINT('Error during administrative areas process')
		PRINT(ERROR_MESSAGE())
		ROLLBACK TRAN
		;THROW
	END CATCH

	-- Recompute all FK_AdministrativeAreas
	BEGIN TRY
		PRINT('Compute all FK_AdministrativeAreas')
		SET @COUNT = 0
		SET @ROWS = 0

		PRINT('Individual locations')
		WHILE 1 = 1
		BEGIN
			BEGIN TRAN

			IF OBJECT_ID('tempdb..#temptableindivlocationAA') IS NOT NULL
			BEGIN
				DROP TABLE #temptableindivlocationAA
			END

			SELECT @count = COUNT(*) 
			FROM Individual_Location WITH (NOLOCK) 
			WHERE [FK_AdministrativeArea] IS NOT NULL
			
			PRINT(CONCAT('Individual location FK_AdministrativeArea:',@COUNT,' Calulated'))

			SELECT TOP(50000) 
				IL.[ID] AS [ID],
				ISNULL(
					(SELECT TOP 1 A.[ID]
					FROM AdministrativeArea AS A
					WITH(INDEX(IX_AdministrativeArea_valid_geom))
					WHERE  
						[min_lat] <= IL.[LAT] 
						AND [max_lat] >= IL.[LAT] 
						AND [min_lon] <= IL.[LON] 
						AND [max_lon] >= IL.[LON]  
						AND A.[valid_geom].STIntersects(geometry::Point(IL.[LON], IL.[LAT], 4326)) = 1
					ORDER BY A.[fullpath] DESC), 
					0
				) AS [FK_AdministrativeArea]
			INTO #temptableindivlocationAA
			FROM [Individual_Location] AS IL
			WHERE
				IL.[FK_AdministrativeArea] IS NULL
				AND IL.[LAT] IS NOT NULL
				AND IL.[LON] IS NOT NULL

			UPDATE IL
			SET IL.[FK_AdministrativeArea] = TT.[FK_AdministrativeArea]
			FROM [Individual_Location] AS IL
			JOIN #temptableindivlocationAA AS TT ON IL.[ID] = TT.[ID]

			SET @ROWS = @@ROWCOUNT
			COMMIT
			PRINT('COMMIT')

			IF @ROWS < 50000 BREAK;
		END
		IF OBJECT_ID('tempdb..#temptableindivlocationAA') IS NOT NULL
		BEGIN
			DROP TABLE #temptableindivlocationAA
		END

		PRINT('Stations')
		WHILE 1 = 1
		BEGIN
			BEGIN TRAN
			IF OBJECT_ID('tempdb..#temptablestationAA') IS NOT NULL
			BEGIN
				DROP TABLE #temptablestationAA
			END

			SELECT @count = COUNT(*) FROM Station WITH (NOLOCK) WHERE [FK_AdministrativeArea] IS NOT NULL
			PRINT(CONCAT('Station location FK_AdministrativeArea:',@COUNT,' Calulated'))

			SELECT TOP(50000) 
				S.[ID] AS [ID],
				ISNULL(
					(SELECT TOP 1 
						A.[ID]
					FROM AdministrativeArea AS A
					WITH(INDEX(IX_AdministrativeArea_valid_geom))
					WHERE  [min_lat] <= S.[LAT] 
						AND [max_lat] >= S.[LAT] 
						AND [min_lon] <= S.[LON] 
						AND [max_lon] >= S.[LON]  
						AND A.[valid_geom].STIntersects(geometry::Point(S.[LON], S.[LAT], 4326)) = 1
					ORDER BY A.[fullpath] DESC), 
					0
				) AS [FK_AdministrativeArea]
			INTO #temptablestationAA
			FROM [Station] AS S
			WHERE   
				S.[FK_AdministrativeArea] IS NULL
				AND S.LAT IS NOT NULL
				AND S.LON IS NOT NULL

			UPDATE S
			SET S.[FK_AdministrativeArea] = TT.[FK_AdministrativeArea]
			FROM [Station] AS S
			JOIN #temptablestationAA AS TT ON S.[ID] = TT.[ID]

			SET @ROWS = @@ROWCOUNT
			COMMIT
			PRINT('COMMIT')

			IF @ROWS < 50000 BREAK;
		END

		IF OBJECT_ID('tempdb..#temptablestationAA') IS NOT NULL
		BEGIN
			DROP TABLE #temptablestationAA
		END


		PRINT('Monitored Site Positions')
		WHILE 1 = 1
		BEGIN
			BEGIN TRAN
			IF OBJECT_ID('tempdb..#temptablemonitoredsitepositionAA') IS NOT NULL
			BEGIN
				DROP TABLE #temptablemonitoredsitepositionAA
			END

			SELECT @count = COUNT(*) FROM MonitoredSitePosition WITH (NOLOCK) WHERE [FK_AdministrativeArea] IS NOT NULL
			PRINT(CONCAT('MonitoredSitePosition FK_AdministrativeArea:',@COUNT,' Calulated'))

			SELECT TOP(50000) 
				MSP.[ID] AS [ID],
				ISNULL(
					(SELECT TOP 1 
						A.[ID]
					FROM AdministrativeArea AS A
					WITH(INDEX(IX_AdministrativeArea_valid_geom))
					WHERE  [min_lat] <= MSP.[LAT] 
						AND [max_lat] >= MSP.[LAT] 
						AND [min_lon] <= MSP.[LON] 
						AND [max_lon] >= MSP.[LON]  
						AND A.[valid_geom].STIntersects(geometry::Point(MSP.[LON], MSP.[LAT], 4326)) = 1
					ORDER BY A.[fullpath] DESC), 
					0
				) AS [FK_AdministrativeArea]
			INTO #temptablemonitoredsitepositionAA
			FROM [MonitoredSitePosition] AS MSP
			WHERE   
				MSP.[FK_AdministrativeArea] IS NULL
				AND MSP.LAT IS NOT NULL
				AND MSP.LON IS NOT NULL

			UPDATE MSP
			SET MSP.[FK_AdministrativeArea] = TT.[FK_AdministrativeArea]
			FROM [MonitoredSitePosition] AS MSP
			JOIN #temptablemonitoredsitepositionAA AS TT ON MSP.[ID] = TT.[ID]

			SET @ROWS = @@ROWCOUNT
			COMMIT
			PRINT('COMMIT')

			IF @ROWS < 50000 BREAK;
		END

		IF OBJECT_ID('tempdb..#temptablemonitoredsitepositionAA') IS NOT NULL
		BEGIN
			DROP TABLE #temptablemonitoredsitepositionAA
		END
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK; -- annule la transaction en cas d'erreur
			PRINT('ROLLBACK')
		END

		-- Renvoyer une erreur avec RAISERROR
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @ErrorSeverity = ERROR_SEVERITY();
		SET @ErrorState = ERROR_STATE();

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH;

	-- Clean
	BEGIN TRY
		BEGIN TRAN
		PRINT('Clean')

		IF OBJECT_ID('reneco_administrativearea') IS NOT NULL
		BEGIN
			DROP TABLE reneco_administrativearea
		END

		IF OBJECT_ID('spatial_ref_sys') IS NOT NULL
		BEGIN
			DROP TABLE spatial_ref_sys
		END

		IF OBJECT_ID('geometry_columns') IS NOT NULL
		BEGIN
			DROP TABLE geometry_columns
		END
		COMMIT
		PRINT('COMMIT')
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK; -- annule la transaction en cas d'erreur
			PRINT('ROLLBACK')
		END

		-- Renvoyer une erreur avec RAISERROR
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @ErrorSeverity = ERROR_SEVERITY();
		SET @ErrorState = ERROR_STATE();

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH;

END TRY
BEGIN CATCH
    ;THROW
END CATCH;